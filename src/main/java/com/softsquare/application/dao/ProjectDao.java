package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.ProjectMapping;
import com.softsquare.application.entity.Project;

public interface ProjectDao {

	public void saveProject(Project po) throws Exception;

	public List<Map<String, Object>> POSelect();

	public void saveGridProject(Project po);

	public void updateGridProject(Project po);

	public void remove(Project po);

	public ArrayList<Project> getProject();

	public List<Project> removeProject(Integer deletes);

	public List<Project> editProject(final int id);

	void updateProject(Project po) throws Exception;

	public List<Map<String, Object>> Search(String SearchProject, String SearchProjectType, String SearchBranch);

	public List<Map<String, Object>> HomeSelect();

	public List<Map<String, Object>> HomeSearch(String searchProject, String searchProjectType, String searchBranch);

	public ArrayList<Project> ProjectSelect(ProjectMapping projectMapping);
}
