package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.ProjectType;

@Component
public class ProjectTypeDaoImp extends AbstractDao<Integer, ProjectType> implements ProjectTypeDao {

	@Override
	public void saveProjectType(ProjectType pt) {
		getSession().save(pt);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> PTSelect() {
		Criteria criteria = getSession().createCriteria(ProjectType.class, "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pt.projectTypeID"), "projectTypeID")
				.add(Projections.property("pt.projectTypeName"), "projectTypeName")
				.add(Projections.property("pt.projectTypeDescription"), "projectTypeDescription");
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void saveGridProjectType(ProjectType pt) {
		getSession().save(pt);
	}

	@Override
	public void updateGridProjectType(ProjectType pt) {
		getSession().update(pt);
	}

	@Override
	public void remove(ProjectType pt) {
		getSession().delete(pt);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ProjectType> getProjectType() {
		Criteria criteria = getSession().createCriteria(ProjectType.class, "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pt.projectTypeID").as("projectTypeID"))
				.add(Projections.property("pt.projectTypeName").as("projectTypeName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<ProjectType> projectTypeList = (ArrayList<ProjectType>) criteria.list();
		return projectTypeList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchProjectType) {

		Criteria criteria = getSession().createCriteria(ProjectType.class, "pt");

		if (SearchProjectType.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("pt.projectTypeID"), "projectTypeID")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("pt.projectTypeDescription"), "projectTypeDescription");
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchProjectType.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("pt.projectTypeID"), "projectTypeID")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("pt.projectTypeDescription"), "projectTypeDescription");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("pt.projectTypeName", "%" + SearchProjectType + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}
}
