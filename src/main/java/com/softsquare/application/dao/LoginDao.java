package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.entity.Login;

public interface LoginDao {

	public List<Map<String, Object>> findByUsername(String userName);
	public void saveUser(Login user) throws Exception;
	public void updateUser(Login user) throws Exception;
	public void deleteUser(Login user);
	public List<Map<String, Object>> LGSelect();
	public void remove(Login lg);
	public List<Map<String, Object>> Search(String SearchRegister, String SearchPositionCombobox);
	public List<Login> editRegister(final int id) ;
	public ArrayList<Login> LoginSelect(LoginMapping loginMapping);
	public List<Login> LoginSelect1(LoginMapping loginMapping);
	public List<Login> LoginSelect1(String loginUser);
	public ArrayList<Login> RegisterSelect(LoginMapping login);
	public List<Login> LoginSelectPro(LoginMapping loginMapping);
	public List<Login> LoginSelectPass(LoginMapping loginmp);
	public List<Map<String, Object>> passSearch(LoginMapping login);

}
