package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.PositionType;

public interface PositionTypeDao {

	public void savePositionType(PositionType pt) ;

	public List<Map<String, Object>> PTSelect();

	public void saveGridPositionType(PositionType pt);

	public void updateGridPositionType(PositionType pt);

	public void remove(PositionType pt);
	
	public  ArrayList<PositionType> getPositionType();
	
	public List<Map<String, Object>> Search(String SearchPositionType);
}
