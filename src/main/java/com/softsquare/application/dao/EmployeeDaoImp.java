package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.entity.Employee;

@Component
public class EmployeeDaoImp extends AbstractDao<Integer, Employee> implements EmployeeDao {

	@Override
	public void saveEmployee(Employee it) throws Exception {
		save(it);
	}

	@Override
	public void updateEmployee(Employee em) throws Exception {
		merge(em);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> EMSelect() {
		Criteria criteria = getSession().createCriteria(Employee.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.employeeID"), "employeeID")
				.add(Projections.property("em.employeeName"), "employeeName")
				.add(Projections.property("em.employeeAddress"), "employeeAddress")
				.add(Projections.property("em.employeePhone"), "employeePhone")
				.add(Projections.property("em.employeeCitizenID"), "employeeCitizenID");
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Employee> EmployeeSelect(EmployeeMapping employeeMapping) {
		Criteria criteria = getSession().createCriteria(Employee.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.employeeID"), "employeeID")
				.add(Projections.property("em.employeeName"), "employeeName")
				.add(Projections.property("em.employeeAddress"), "employeeAddress")
				.add(Projections.property("em.employeePhone"), "employeePhone")
				.add(Projections.property("em.employeeCitizenID"), "employeeCitizenID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("em.employeeID", employeeMapping.getEmployeeID()));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Employee> employeeList = (ArrayList<Employee>) criteria.list();
		return employeeList;
	}

	@Override
	public void remove(Employee em) {
		getSession().delete(em);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Employee> getEmployee() {
		Criteria criteria = getSession().createCriteria(Employee.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.employeeID").as("employeeID"))
				.add(Projections.property("em.employeeName").as("employeeName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Employee> employeeList = (ArrayList<Employee>) criteria.list();
		return employeeList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> removeEmployee(Integer deletes) {
		Criteria criteria = getSession().createCriteria(Employee.class, "od");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("od.employeeID"), "employeeID")
				.add(Projections.property("od.employeeName"), "employeeName")
				.add(Projections.property("od.employeeAddress"), "employeeAddress")
				.add(Projections.property("od.employeePhone"), "employeePhone")
				.add(Projections.property("od.employeeCitizenID"), "employeeCitizenID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("od.employeeID", deletes));
		criteria.setResultTransformer(Transformers.aliasToBean(Employee.class));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getEmployeeName() {
		Criteria criteria = getSession().createCriteria(Employee.class, "od");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("od.employeeID"), "employeeID")
				.add(Projections.property("od.employeeName"), "employeeName")
				.add(Projections.property("od.employeeAddress"), "employeeAddress")
				.add(Projections.property("od.employeePhone"), "employeePhone")
				.add(Projections.property("od.employeeCitizenID"), "employeeCitizenID");
		criteria.setProjection(projections);
		criteria.setResultTransformer(Transformers.aliasToBean(Employee.class));
		return criteria.list();
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Employee> editEmployee(final int id) {
		Criteria criteria = getSession().createCriteria(Employee.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.employeeID"), "employeeID")
				.add(Projections.property("em.employeeName"), "employeeName")
				.add(Projections.property("em.employeeAddress"), "employeeAddress")
				.add(Projections.property("em.employeePhone"), "employeePhone")
				.add(Projections.property("em.employeeCitizenID"), "employeeCitizenID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("em.employeeID", id));
		criteria.setResultTransformer(Transformers.aliasToBean(Employee.class));
		List<Employee> editList = criteria.list();
		return editList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchEmployee) {

		Criteria criteria = getSession().createCriteria(Employee.class, "em");

		if (SearchEmployee.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("em.employeeID"), "employeeID")
					.add(Projections.property("em.employeeName"), "employeeName")
					.add(Projections.property("em.employeeAddress"), "employeeAddress")
					.add(Projections.property("em.employeePhone"), "employeePhone")
					.add(Projections.property("em.employeeCitizenID"), "employeeCitizenID");
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchEmployee.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("em.employeeID"), "employeeID")
					.add(Projections.property("em.employeeName"), "employeeName")
					.add(Projections.property("em.employeeAddress"), "employeeAddress")
					.add(Projections.property("em.employeePhone"), "employeePhone")
					.add(Projections.property("em.employeeCitizenID"), "employeeCitizenID");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("em.employeeName", "%" + SearchEmployee + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}
}
