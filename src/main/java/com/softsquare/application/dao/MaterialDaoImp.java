package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.Material;

@Component
public class MaterialDaoImp extends AbstractDao<Integer, Material> implements MaterialDao {

	@Override
	public void saveMaterial(Material ma) {
		getSession().save(ma);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> MASelect() {
		Criteria criteria = getSession().createCriteria(Material.class, "ma");
		criteria.createAlias("ma.materialType", "mt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ma.materialID"), "materialID")
				.add(Projections.property("ma.materialName"), "materialName")
				.add(Projections.property("ma.materialTypeID"), "materialTypeID")
				.add(Projections.property("ma.materialUnit"), "materialUnit")
				.add(Projections.property("mt.materialTypeName").as("materialTypeName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void saveGridMaterial(Material ma) {
		getSession().save(ma);
	}

	@Override
	public void updateGridMaterial(Material ma) {
		getSession().update(ma);
	}

	@Override
	public void remove(Material ma) {
		getSession().delete(ma);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Material> getMaterial() {
		Criteria criteria = getSession().createCriteria(Material.class, "ma");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ma.materialID").as("materialID"))
				.add(Projections.property("ma.materialName").as("materialName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Material> materialList = (ArrayList<Material>) criteria.list();
		return materialList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchMaterial, String SearchMaterialType) {

		Criteria criteria = getSession().createCriteria(Material.class, "ma");

		if (SearchMaterial.isEmpty() && SearchMaterialType.isEmpty()) {
			criteria.createAlias("ma.materialType", "mt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ma.materialID"), "materialID")
					.add(Projections.property("ma.materialName"), "materialName")
					.add(Projections.property("ma.materialTypeID"), "materialTypeID")
					.add(Projections.property("ma.materialUnit"), "materialUnit")
					.add(Projections.property("mt.materialTypeName").as("materialTypeName"));
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchMaterial.isEmpty() && SearchMaterialType.isEmpty()) {
			criteria.createAlias("ma.materialType", "mt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ma.materialID"), "materialID")
					.add(Projections.property("ma.materialName"), "materialName")
					.add(Projections.property("ma.materialTypeID"), "materialTypeID")
					.add(Projections.property("ma.materialUnit"), "materialUnit")
					.add(Projections.property("mt.materialTypeName").as("materialTypeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("ma.materialName", "%" + SearchMaterial + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchMaterial.isEmpty() && !SearchMaterialType.isEmpty()) {
			int MTT = Integer.parseInt(SearchMaterialType);
			criteria.createAlias("ma.materialType", "mt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ma.materialID"), "materialID")
					.add(Projections.property("ma.materialName"), "materialName")
					.add(Projections.property("ma.materialTypeID"), "materialTypeID")
					.add(Projections.property("ma.materialUnit"), "materialUnit")
					.add(Projections.property("mt.materialTypeName").as("materialTypeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("ma.materialTypeID", MTT));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchMaterial.isEmpty() && !SearchMaterialType.isEmpty()) {
			int MTT = Integer.parseInt(SearchMaterialType);
			criteria.createAlias("ma.materialType", "mt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ma.materialID"), "materialID")
					.add(Projections.property("ma.materialName"), "materialName")
					.add(Projections.property("ma.materialTypeID"), "materialTypeID")
					.add(Projections.property("ma.materialUnit"), "materialUnit")
					.add(Projections.property("mt.materialTypeName").as("materialTypeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("ma.materialName", "%" + SearchMaterial + "%"));
			criteria.add(Restrictions.eq("ma.materialTypeID", MTT));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}
}
