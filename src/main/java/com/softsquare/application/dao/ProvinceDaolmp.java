package com.softsquare.application.dao;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.Province;

@Component
public class ProvinceDaolmp extends AbstractDao<Integer, Province> implements ProvinceDao{
	
	@Override
	public ArrayList<Province> getProvince() {
		 Criteria criteria = getSession().createCriteria(Province.class, "pr");
		 ProjectionList projections = Projections.projectionList()
		            .add(Projections.property("pr.PROVINCE_CODE").as("PROVINCE_CODE"))
		            .add(Projections.property("pr.PROVINCE_NAME_THA").as("PROVINCE_NAME_THA"));
		 criteria.setProjection(projections);
		 criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		 ArrayList<Province> provinceList = (ArrayList<Province>) criteria.list();
		return provinceList;
	}

}
