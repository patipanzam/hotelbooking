package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.entity.Building;
import com.softsquare.application.entity.PositionType;

public interface BuildingDao {

	public void saveBuilding(Building em) throws Exception;

	public List<Map<String, Object>> EMSelect();

	public ArrayList<Building> getBuilding();

	public List<Building> removeBuilding(Integer deletes);

	public List<Building> editBuilding(final int id);

	public void updateBuilding(Building em) throws Exception;

	public List<Map<String, Object>> Search(String SearchBuilding);

	public List<Building> getBuildingName();

	public ArrayList<Building> BuildingSelect(BuildingMapping BuildingMapping);

	public List<Map<String, Object>> PTSelect();

	
public void remove(Building pt);

public void updateGridBuilding(Building pt);

public void saveGridBuilding(Building pt);



	
}
