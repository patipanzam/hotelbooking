package com.softsquare.application.dao;

import org.springframework.stereotype.Component;

import com.softsquare.application.entity.Payment;

@Component
public class PaymentDaoImp extends AbstractDao<Integer, Payment> implements PaymentDao{

	@Override
	public void savePayment(Payment pmt) throws Exception {
		save(pmt);
		
	}

	@Override
	public void updatePayment(Payment pmt)  throws Exception{
		merge(pmt);
		
	}

}
