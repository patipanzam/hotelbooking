package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.entity.Login;
import com.softsquare.application.entity.Promotion;
import com.softsquare.application.entity.Role;

@Repository()
@Component
public class LoginDaoImp extends AbstractDao<Integer, Login> implements LoginDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> findByUsername(String userName) {
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		criteria.createAlias("login.role", "role");
		
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.username").as("username"))
				.add(Projections.property("login.password").as("password"))
				.add(Projections.property("role.roleName").as("roleCode"));
				
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("login.username", userName));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> loginList = criteria.list();
		return loginList;
	}

	@Override
	public void saveUser(Login user) throws Exception {
		save(user);
	}

	@Override
	public void updateUser(Login user) throws Exception {
		merge(user);
	}

	@Override
	public void deleteUser(Login user) {
		delete(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> LGSelect() {
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		criteria.createAlias("login.role", "role");
		ProjectionList projections = Projections.projectionList().add(Projections.property("login.id").as("id"))
				.add(Projections.property("login.username").as("LGUSERNAME"))
				.add(Projections.property("login.password").as("LGPASSWORD"))
				.add(Projections.property("role.roleName").as("ROLENAME"))
				.add(Projections.property("login.firstName").as("USERFNAME"))
				.add(Projections.property("login.lastName").as("USERLNAME"))
				.add(Projections.property("login.email").as("EMAIL"))
				.add(Projections.property("login.phone").as("PHONE"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void remove(Login lg) {
		getSession().delete(lg);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchRegister, String SearchPositionCombobox) {

		Criteria criteria = getSession().createCriteria(Login.class, "login");

		if (SearchRegister.isEmpty() && SearchPositionCombobox.isEmpty()) {
			criteria.createAlias("login.role", "role");
			ProjectionList projections = Projections.projectionList().add(Projections.property("login.id").as("id"))
					.add(Projections.property("login.username").as("LGUSERNAME"))
					.add(Projections.property("login.password").as("LGPASSWORD"))
					.add(Projections.property("role.roleName").as("ROLENAME"))
					.add(Projections.property("login.firstName").as("USERFNAME"))
					.add(Projections.property("login.lastName").as("USERLNAME"))
					.add(Projections.property("login.email").as("EMAIL"))
					.add(Projections.property("login.phone").as("PHONE"));
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchRegister.isEmpty() && SearchPositionCombobox.isEmpty()) {
			criteria.createAlias("login.role", "role");
			criteria.createAlias("login.employee", "em");
			ProjectionList projections = Projections.projectionList().add(Projections.property("login.id").as("id"))
					.add(Projections.property("login.username").as("LGUSERNAME"))
					.add(Projections.property("login.password").as("LGPASSWORD"))
					.add(Projections.property("role.roleName").as("ROLENAME"))
					.add(Projections.property("em.employeeName").as("employeeName"));
			criteria.add(Restrictions.like("em.employeeName", "%" + SearchRegister + "%"));
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchRegister.isEmpty() && !SearchPositionCombobox.isEmpty()) {
			int roleID = Integer.parseInt(SearchPositionCombobox);
			criteria.createAlias("login.role", "role");
			ProjectionList projections = Projections.projectionList().add(Projections.property("login.id").as("id"))
					.add(Projections.property("login.username").as("LGUSERNAME"))
					.add(Projections.property("login.password").as("LGPASSWORD"))
					.add(Projections.property("role.roleName").as("ROLENAME"))
					.add(Projections.property("role.firstName").as("USERFNAME"))
					.add(Projections.property("role.lastName").as("USERLNAME"))
					.add(Projections.property("role.email").as("EMAIL"))
					.add(Projections.property("role.phone").as("PHONE"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("role.id", roleID));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchRegister.isEmpty() && !SearchPositionCombobox.isEmpty()) {
			int roleID = Integer.parseInt(SearchPositionCombobox);
			criteria.createAlias("login.role", "role");
			criteria.createAlias("login.employee", "em");
			ProjectionList projections = Projections.projectionList().add(Projections.property("login.id").as("id"))
					.add(Projections.property("login.username").as("LGUSERNAME"))
					.add(Projections.property("login.password").as("LGPASSWORD"))
					.add(Projections.property("role.roleName").as("ROLENAME"))
					.add(Projections.property("em.employeeName").as("employeeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("role.id", roleID));
			criteria.add(Restrictions.like("em.employeeName", "%" + SearchRegister + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Login> editRegister(final int id) {
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.id"), "id")
				.add(Projections.property("login.username"), "username")
				.add(Projections.property("login.password"), "password")
				.add(Projections.property("login.roleId"), "roleId")
				.add(Projections.property("login.firstName").as("firstName"))
				.add(Projections.property("login.lastName").as("lastName"))
				.add(Projections.property("login.email").as("email"))
				.add(Projections.property("login.phone").as("phone"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("login.id", id));
		criteria.setResultTransformer(Transformers.aliasToBean(Login.class));
		List<Login> editList = criteria.list();
		return editList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Login> LoginSelect(LoginMapping loginMapping) {
		System.out.println(LoginUtils.getUsername()+"555555555555555555555555555555555"); 
		String loginUser = LoginUtils.getUsername();
		List<Login> login = LoginSelect1(loginUser);
		System.out.println(login.get(0).getId()+"----8888888888888888888888888888888");
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.id"), "id")
				.add(Projections.property("login.username"), "username")
				.add(Projections.property("login.password"), "password")
				.add(Projections.property("login.roleId"), "roleId")
				.add(Projections.property("login.firstName"),"firstName")
				.add(Projections.property("login.lastName"),("lastName"))
				.add(Projections.property("login.email"),("email"))
				.add(Projections.property("login.phone"),("phone"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("login.id", login.get(0).getId()));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Login> loginList = (ArrayList<Login>) criteria.list();
		return loginList;
	}

	@Override
	public List<Login> LoginSelect1(LoginMapping loginMapping) {
		String loginUser = LoginUtils.getUsername();
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.id"), "id")
				.add(Projections.property("login.username"), "username")
				.add(Projections.property("login.password"), "password")
				.add(Projections.property("login.roleId"), "roleId")
				.add(Projections.property("login.firstName"),"firstName")
				.add(Projections.property("login.lastName"),("lastName"))
				.add(Projections.property("login.email"),("email"))
				.add(Projections.property("login.phone"),("phone"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("login.username", loginUser));
		criteria.setResultTransformer(Transformers.aliasToBean(Login.class));
		return criteria.list();
	}

	@Override
	public List<Login> LoginSelect1(String loginUser) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Login> RegisterSelect(LoginMapping login) {
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		criteria.createAlias("login.role", "role");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.id"), "id")
				.add(Projections.property("login.username"), "username")
				.add(Projections.property("login.password"), "password")
				.add(Projections.property("login.roleId"), "roleId")
				.add(Projections.property("login.firstName"),"firstName")
				.add(Projections.property("login.lastName"),("lastName"))
				.add(Projections.property("login.email"),("email"))
				.add(Projections.property("login.phone"),("phone"));
//		        .add(Projections.property("login.roleName"),("roleName"));
		
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("login.id", login.getId()));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Login> registerList = (ArrayList<Login>) criteria.list();
		return registerList;
	}

	@Override
	public List<Login> LoginSelectPro(LoginMapping loginMapping) {
		String loginUser = LoginUtils.getUsername();
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.id"), "id")
				.add(Projections.property("login.username"), "username")
				.add(Projections.property("login.password"), "password")
				.add(Projections.property("login.roleId"), "roleId")
				.add(Projections.property("login.firstName"),"firstName")
				.add(Projections.property("login.lastName"),("lastName"))
				.add(Projections.property("login.email"),("email"))
				.add(Projections.property("login.phone"),("phone"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("login.username", loginUser));
		criteria.setResultTransformer(Transformers.aliasToBean(Login.class));
		return criteria.list();
	}

	@Override
	public List<Login> LoginSelectPass(LoginMapping loginmp) {
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.id"), "id")
				.add(Projections.property("login.username"), "username")
				.add(Projections.property("login.password"), "password")
				.add(Projections.property("login.email"),("email"));
			
	    criteria.add(Restrictions.like("login.email","%"+ loginmp.getEmail()+"%")); 
		criteria.setProjection(projections);
		criteria.setResultTransformer(Transformers.aliasToBean(Login.class));
		return criteria.list();
	}

	@Override
	public List<Map<String, Object>> passSearch(LoginMapping login) {
		System.out.println(login.getEmail()+"555555555555555555");
		Criteria criteria = getSession().createCriteria(Login.class, "login");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("login.id"), "id")
				.add(Projections.property("login.username"), "username")
				.add(Projections.property("login.password"), "password")
				.add(Projections.property("login.email"),("email"));
			
	    criteria.add(Restrictions.like("login.email","%"+ login.getEmail()+"%")); 
		criteria.setProjection(projections);
		criteria.setResultTransformer(Transformers.aliasToBean(Login.class));
		return criteria.list();
	}
}
