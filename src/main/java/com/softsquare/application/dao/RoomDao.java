package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Room;

public interface RoomDao {

	public	void saveRoom(Room rooms) throws Exception;

	public void updateRoom(Room rooms) throws Exception;

	public void deleteRoom(Room rm);

	public List<Map<String, Object>> LGSelect();

	public	List<Room> editRoom(int roomID);

	public ArrayList<Room> RoomSelect(RoomMapping roomMapping);
	
	public void remove(Room rm);

	public RoomMapping editRoom1(RoomMapping roomMapping);

	public List<Map<String, Object>> roomSearch(RoomMapping room);

	public List<Room> RoomSelect1(RoomMapping room);

	public List<Room> RoomSelectBk(RoomMapping room);

	public List<Map<String, Object>> LGSelectUs();

}
