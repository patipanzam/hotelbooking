package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.MaterialType;

@Component
public class MaterialTypeDaoImp extends AbstractDao<Integer, MaterialType> implements MaterialTypeDao {

	@Override
	public void saveMaterialType(MaterialType mat) {
		getSession().save(mat);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> MATSelect() {
		Criteria criteria = getSession().createCriteria(MaterialType.class, "mat");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("mat.materialTypeID"), "materialTypeID")
				.add(Projections.property("mat.materialTypeName"), "materialTypeName");
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void saveGridMaterialType(MaterialType mat) {
		getSession().save(mat);
	}

	@Override
	public void updateGridMaterialType(MaterialType mat) {
		getSession().update(mat);
	}

	@Override
	public void remove(MaterialType mat) {
		getSession().delete(mat);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<MaterialType> getMaterialType() {
		Criteria criteria = getSession().createCriteria(MaterialType.class, "mat");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("mat.materialTypeID").as("materialTypeID"))
				.add(Projections.property("mat.materialTypeName").as("materialTypeName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<MaterialType> materialTypeList = (ArrayList<MaterialType>) criteria.list();
		return materialTypeList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchMaterialType) {

		Criteria criteria = getSession().createCriteria(MaterialType.class, "mat");

		if (SearchMaterialType.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("mat.materialTypeID"), "materialTypeID")
					.add(Projections.property("mat.materialTypeName"), "materialTypeName");
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchMaterialType.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("mat.materialTypeID"), "materialTypeID")
					.add(Projections.property("mat.materialTypeName"), "materialTypeName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("mat.materialTypeName", "%" + SearchMaterialType + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}
}
