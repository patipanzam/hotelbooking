package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.entity.Building;
import com.softsquare.application.entity.Building;

@Component
public class BuildingDaoImp extends AbstractDao<Integer, Building> implements BuildingDao {

	@Override
	public void saveBuilding(Building it) throws Exception {
		save(it);
	}

	@Override
	public void updateBuilding(Building em) throws Exception {
		merge(em);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> EMSelect() {
		Criteria criteria = getSession().createCriteria(Building.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.BuildingID"), "BuildingID")
				.add(Projections.property("em.BuildingName"), "BuildingName")
				.add(Projections.property("em.BuildingCode"), "BuildingCode");
				
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Building> BuildingSelect(BuildingMapping buildingMapping) {
		Criteria criteria = getSession().createCriteria(Building.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.BuildingID"), "BuildingID")
				.add(Projections.property("em.BuildingName"), "BuildingName")
				.add(Projections.property("em.BuildingCode"), "BuildingCode");
		criteria.setProjection(projections);
//		criteria.add(Restrictions.eq("em.BuildingID", BuildingMapping.getBuildingID()));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Building> BuildingList = (ArrayList<Building>) criteria.list();
		return BuildingList;
	}

	

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Building> getBuilding() {
		Criteria criteria = getSession().createCriteria(Building.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.buildingID").as("buildingID"))
				.add(Projections.property("em.buildingName").as("buildingName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Building> BuildingList = (ArrayList<Building>) criteria.list();
		return BuildingList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Building> removeBuilding(Integer deletes) {
		Criteria criteria = getSession().createCriteria(Building.class, "od");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.BuildingID"), "BuildingID")
				.add(Projections.property("em.BuildingName"), "BuildingName")
				.add(Projections.property("em.BuildingCode"), "BuildingCode");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("od.BuildingID", deletes));
		criteria.setResultTransformer(Transformers.aliasToBean(Building.class));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Building> getBuildingName() {
		Criteria criteria = getSession().createCriteria(Building.class, "od");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("od.BuildingID"), "BuildingID")
				.add(Projections.property("od.BuildingName"), "BuildingName");
		criteria.setProjection(projections);
		criteria.setResultTransformer(Transformers.aliasToBean(Building.class));
		return criteria.list();
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Building> editBuilding(final int id) {
		Criteria criteria = getSession().createCriteria(Building.class, "em");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("em.BuildingID"), "BuildingID")
				.add(Projections.property("em.BuildingName"), "BuildingName")
				.add(Projections.property("em.BuildingCode"), "BuildingCode");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("em.BuildingID", id));
		criteria.setResultTransformer(Transformers.aliasToBean(Building.class));
		List<Building> editList = criteria.list();
		return editList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchBuilding) {

		Criteria criteria = getSession().createCriteria(Building.class, "em");

		if (SearchBuilding.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("em.BuildingID"), "BuildingID")
					.add(Projections.property("em.BuildingName"), "BuildingName")
					.add(Projections.property("em.BuildingCode"), "BuildingCode");
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchBuilding.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("em.BuildingID"), "BuildingID")
					.add(Projections.property("em.BuildingName"), "BuildingName")
					.add(Projections.property("em.BuildingName"), "BuildingCode");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("em.BuildingName", "%" + SearchBuilding + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> PTSelect() {

		Criteria criteria = getSession().createCriteria(Building.class, "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pt.buildingID"), "buildingID")
				.add(Projections.property("pt.buildingName"), "buildingName");
		criteria.setProjection(projections);
//		criteria.add(Restrictions.ne("pt.buildingID", 1));
//		criteria.add(Restrictions.ne("pt.buildingID", 2));
//		criteria.add(Restrictions.ne("pt.buildingID", 3));
//		criteria.add(Restrictions.ne("pt.buildingID", 4));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void remove(Building pt) {
		getSession().delete(pt);
		
	}

	@Override
	public void updateGridBuilding(Building pt) {
		getSession().update(pt);
		
	}

	@Override
	public void saveGridBuilding(Building pt) {
		getSession().save(pt);
		
	}
}
