package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.common.util.NumberUtils;
import com.softsquare.application.entity.Position;

@Component
public class PositionDaoImp extends AbstractDao<Integer, Position> implements PositionDao {

	@Override
	public void savePosition(Position po) {
		getSession().save(po);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> POSelect() {

		Criteria criteria = getSession().createCriteria(Position.class, "po");
		criteria.createAlias("po.positionType", "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("po.positionID"), "positionID")
				.add(Projections.property("po.positionName"), "positionName")
				.add(Projections.property("pt.positionTypeID"), "positionTypeID")
				.add(Projections.property("pt.positionTypeName").as("positionTypeName"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.ne("po.positionID", 1));
		criteria.add(Restrictions.ne("po.positionID", 2));
		criteria.add(Restrictions.ne("po.positionID", 3));
		criteria.add(Restrictions.ne("po.positionID", 4));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void saveGridPosition(Position po) {
		getSession().save(po);
	}

	@Override
	public void updateGridPosition(Position po) {
		getSession().update(po);
	}

	@Override
	public void remove(Position po) {
		getSession().delete(po);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Position> getPosition(String positionTypeID) {

		Criteria criteria = getSession().createCriteria(Position.class, "po");
		criteria.createAlias("po.positionType", "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("po.positionID").as("positionID"))
				.add(Projections.property("po.positionName").as("positionName"));
		criteria.setProjection(projections);

		if (BeanUtils.isNotEmpty(positionTypeID)) {
			criteria.add(Restrictions.eq("pt.positionTypeID", NumberUtils.toInteger(positionTypeID)));
		}

		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Position> positionList = (ArrayList<Position>) criteria.list();
		return positionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchPosition, String SearchPositionType) {

		Criteria criteria = getSession().createCriteria(Position.class, "po");

		if (SearchPosition.isEmpty() && SearchPositionType.isEmpty()) {
			criteria.createAlias("po.positionType", "pt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.positionID"), "positionID")
					.add(Projections.property("po.positionName"), "positionName")
					.add(Projections.property("po.positionTypeID"), "positionTypeID")
					.add(Projections.property("pt.positionTypeName").as("positionTypeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.ne("po.positionID", 1));
			criteria.add(Restrictions.ne("po.positionID", 2));
			criteria.add(Restrictions.ne("po.positionID", 3));
			criteria.add(Restrictions.ne("po.positionID", 4));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchPosition.isEmpty() && SearchPositionType.isEmpty()) {
			criteria.createAlias("po.positionType", "pt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.positionID"), "positionID")
					.add(Projections.property("po.positionName"), "positionName")
					.add(Projections.property("po.positionTypeID"), "positionTypeID")
					.add(Projections.property("pt.positionTypeName").as("positionTypeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.ne("po.positionID", 1));
			criteria.add(Restrictions.ne("po.positionID", 2));
			criteria.add(Restrictions.ne("po.positionID", 3));
			criteria.add(Restrictions.ne("po.positionID", 4));
			criteria.add(Restrictions.like("po.positionName", "%" + SearchPosition + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchPosition.isEmpty() && !SearchPositionType.isEmpty()) {
			int SPT = Integer.parseInt(SearchPositionType);
			criteria.createAlias("po.positionType", "pt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.positionID"), "positionID")
					.add(Projections.property("po.positionName"), "positionName")
					.add(Projections.property("po.positionTypeID"), "positionTypeID")
					.add(Projections.property("pt.positionTypeName").as("positionTypeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.ne("po.positionID", 1));
			criteria.add(Restrictions.ne("po.positionID", 2));
			criteria.add(Restrictions.ne("po.positionID", 3));
			criteria.add(Restrictions.ne("po.positionID", 4));
			criteria.add(Restrictions.eq("pt.positionTypeID", SPT));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchPosition.isEmpty() && !SearchPositionType.isEmpty()) {
			int SPT = Integer.parseInt(SearchPositionType);
			criteria.createAlias("po.positionType", "pt");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.positionID"), "positionID")
					.add(Projections.property("po.positionName"), "positionName")
					.add(Projections.property("po.positionTypeID"), "positionTypeID")
					.add(Projections.property("pt.positionTypeName").as("positionTypeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.ne("po.positionID", 1));
			criteria.add(Restrictions.ne("po.positionID", 2));
			criteria.add(Restrictions.ne("po.positionID", 3));
			criteria.add(Restrictions.ne("po.positionID", 4));
			criteria.add(Restrictions.like("po.positionName", "%" + SearchPosition + "%"));
			criteria.add(Restrictions.eq("pt.positionTypeID", SPT));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}
}
