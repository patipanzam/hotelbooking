package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.entity.Skill;

@Component
public class SkillDaoImp extends AbstractDao<Integer, Skill> implements SkillDao {

	@Override
	public void saveGridSkill(Skill sk) throws Exception {
		save(sk);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> SKSelect(int id) {
		Criteria criteria = getSession().createCriteria(Skill.class, "sk");
		criteria.createAlias("sk.position", "po");
		criteria.createAlias("po.positionType", "pt");
		ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
				.add(Projections.property("sk.salary"), "salary")
				.add(Projections.property("sk.employeeID"), "employeeID")
				.add(Projections.property("sk.positionID"), "positionID")
				.add(Projections.property("po.positionName").as("positionName"))
				.add(Projections.property("pt.positionTypeName").as("positionTypeName"))
				.add(Projections.property("pt.positionTypeID").as("positionTypeID"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("sk.employeeID", id));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void updateGridSkill(Skill sk) throws Exception {
		merge(sk);
	}

	@Override
	public void remove(Skill sk) {
		delete(sk);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Skill> getSkill(String positionID, String EmployeeName) {
		Criteria criteria = getSession().createCriteria(Skill.class, "sk");
		criteria.createAlias("sk.employee", "em");
		criteria.createAlias("sk.position", "po");
		ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID").as("skillID"))
				.add(Projections.property("sk.employeeID").as("employeeID"))
				.add(Projections.property("sk.positionID"), "positionID")
				.add(Projections.property("em.employeeName").as("employeeName"))
				.add(Projections.property("po.positionName").as("positionName"));
		criteria.setProjection(projections);

		if (BeanUtils.isNotEmpty(positionID)) {
			int PositionID = Integer.parseInt(positionID);
			criteria.add(Restrictions.eq("po.positionID", PositionID));
		}

		if (BeanUtils.isNotEmpty(positionID)) {
			criteria.add(Restrictions.like("em.employeeName", "%" + EmployeeName + "%"));
		}

		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Skill> skillList = (ArrayList<Skill>) criteria.list();
		return skillList;
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Skill> editSkill(final int id) {
		Criteria criteria = getSession().createCriteria(Skill.class, "sk");
		ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
				.add(Projections.property("sk.salary"), "salary")
				.add(Projections.property("sk.employeeID"), "employeeID")
				.add(Projections.property("sk.positionID"), "positionID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("sk.employeeID", id));
		criteria.setResultTransformer(Transformers.aliasToBean(Skill.class));
		List<Skill> editList = criteria.list();
		return editList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Skill> removeSkill(int employeeID) {
		Criteria criteria = getSession().createCriteria(Skill.class, "sk");
		ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
				.add(Projections.property("sk.salary"), "salary")
				.add(Projections.property("sk.employeeID"), "employeeID")
				.add(Projections.property("sk.positionID"), "positionID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("sk.employeeID", employeeID));
		criteria.setResultTransformer(Transformers.aliasToBean(Skill.class));
		List<Skill> editList = criteria.list();
		return editList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> RegisterSelect() {
		Criteria criteria = getSession().createCriteria(Skill.class, "sk");
		criteria.createAlias("sk.position", "po");
		criteria.createAlias("sk.employee", "em");
		ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
				.add(Projections.property("sk.employeeID"), "employeeID")
				.add(Projections.property("sk.positionID"), "positionID")
				.add(Projections.property("po.positionName").as("positionName"))
				.add(Projections.property("em.employeeName").as("employeeName"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.or(Restrictions.eq("sk.positionID", 1), Restrictions.eq("sk.positionID", 2),
				Restrictions.eq("sk.positionID", 3), Restrictions.eq("sk.positionID", 4)));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> SearchRegister(String SearchRegister, String SearchPositionCombobox) {

		Criteria criteria = getSession().createCriteria(Skill.class, "sk");

		if (SearchRegister.isEmpty() && SearchPositionCombobox.isEmpty()) {
			criteria.createAlias("sk.position", "po");
			criteria.createAlias("sk.employee", "em");
			ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
					.add(Projections.property("sk.employeeID"), "employeeID")
					.add(Projections.property("sk.positionID"), "positionID")
					.add(Projections.property("po.positionName").as("positionName"))
					.add(Projections.property("em.employeeName").as("employeeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.or(Restrictions.eq("sk.positionID", 1), Restrictions.eq("sk.positionID", 2),
					Restrictions.eq("sk.positionID", 3), Restrictions.eq("sk.positionID", 4)));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchRegister.isEmpty() && SearchPositionCombobox.isEmpty()) {
			criteria.createAlias("sk.position", "po");
			criteria.createAlias("sk.employee", "em");
			ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
					.add(Projections.property("sk.employeeID"), "employeeID")
					.add(Projections.property("sk.positionID"), "positionID")
					.add(Projections.property("po.positionName").as("positionName"))
					.add(Projections.property("em.employeeName").as("employeeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.or(Restrictions.eq("sk.positionID", 1), Restrictions.eq("sk.positionID", 2),
					Restrictions.eq("sk.positionID", 3), Restrictions.eq("sk.positionID", 4)));
			criteria.add(Restrictions.like("em.employeeName", "%" + SearchRegister + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchRegister.isEmpty() && !SearchPositionCombobox.isEmpty()) {
			int roleID = Integer.parseInt(SearchPositionCombobox);
			criteria.createAlias("sk.position", "po");
			criteria.createAlias("sk.employee", "em");
			ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
					.add(Projections.property("sk.employeeID"), "employeeID")
					.add(Projections.property("sk.positionID"), "positionID")
					.add(Projections.property("po.positionName").as("positionName"))
					.add(Projections.property("em.employeeName").as("employeeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.or(Restrictions.eq("sk.positionID", 1), Restrictions.eq("sk.positionID", 2),
					Restrictions.eq("sk.positionID", 3), Restrictions.eq("sk.positionID", 4)));
			criteria.add(Restrictions.eq("sk.positionID", roleID));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchRegister.isEmpty() && !SearchPositionCombobox.isEmpty()) {
			int roleID = Integer.parseInt(SearchPositionCombobox);
			criteria.createAlias("sk.position", "po");
			criteria.createAlias("sk.employee", "em");
			ProjectionList projections = Projections.projectionList().add(Projections.property("sk.skillID"), "skillID")
					.add(Projections.property("sk.employeeID"), "employeeID")
					.add(Projections.property("sk.positionID"), "positionID")
					.add(Projections.property("po.positionName").as("positionName"))
					.add(Projections.property("em.employeeName").as("employeeName"));
			criteria.setProjection(projections);
			criteria.add(Restrictions.or(Restrictions.eq("sk.positionID", 1), Restrictions.eq("sk.positionID", 2),
					Restrictions.eq("sk.positionID", 3), Restrictions.eq("sk.positionID", 4)));
			criteria.add(Restrictions.eq("sk.positionID", roleID));
			criteria.add(Restrictions.like("em.employeeName", "%" + SearchRegister + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}
}
