package com.softsquare.application.dao;

import java.util.ArrayList;

import com.softsquare.application.entity.Province;

public interface ProvinceDao {
	public  ArrayList<Province> getProvince();
}
