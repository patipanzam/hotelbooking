package com.softsquare.application.dao;

import com.softsquare.application.entity.Payment;

public interface PaymentDao {

	void savePayment(Payment pmt) throws Exception;

	void updatePayment(Payment pmt) throws Exception;

}
