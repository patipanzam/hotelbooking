package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.Position;

public interface PositionDao {

	public void savePosition(Position po);

	public List<Map<String, Object>> POSelect();

	public void saveGridPosition(Position po);

	public void updateGridPosition(Position po);

	public void remove(Position po);

	public ArrayList<Position> getPosition(String positionTypeID);

	public List<Map<String, Object>> Search(String SearchPosition, String SearchPositionType);
}
