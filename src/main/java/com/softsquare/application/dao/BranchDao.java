package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.Branch;

public interface BranchDao {

	public void saveBranch(Branch ba);

	public List<Map<String, Object>> BASelect();

	public void saveGridBranch(Branch ba);

	public void updateGridBranch(Branch ba);

	public void remove(Branch ba);

	public ArrayList<Branch> getBranch();

	public List<Map<String, Object>> Search(String SearchBranch, String SearchProvince);
}
