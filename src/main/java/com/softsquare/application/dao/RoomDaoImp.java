package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.softsquare.application.common.util.DateUtils;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Booking;
import com.softsquare.application.entity.Room;


@Repository()
@Component
public class RoomDaoImp extends AbstractDao<Integer, Room>implements RoomDao {

	@Override
	public void saveRoom(Room rooms) throws Exception {
		save(rooms);

	}

	@Override
	public void updateRoom(Room rooms) throws Exception {
		merge(rooms);

	}

	@Override
	public void deleteRoom(Room rm) {
		delete(rm);

	}

	@Override
	public List<Map<String, Object>> LGSelect() {
		Criteria criteria = getSession().createCriteria(Room.class, "room");
		criteria.createAlias("room.building", "building");
		criteria.createAlias("room.roomType", "roomType");
		ProjectionList projections = Projections.projectionList().add(Projections.property("room.roomID").as("roomID"))
				.add(Projections.property("room.roomBeds").as("ROOMBEDS"))
				.add(Projections.property("building.buildingID").as("BUILDINGID"))
				.add(Projections.property("roomType.roomTypeName").as("ROOMTYPENAME"))
				.add(Projections.property("room.roomDate").as("ROOMDATE"))
				.add(Projections.property("room.roomPrice").as("ROOMPRICE"))
				.add(Projections.property("room.roomNo").as("ROOMNO"))
				.add(Projections.property("room.typeDetail").as("TYPEDETAIL"))
		        .add(Projections.property("room.freewifi").as("FREEWIFI"))
		        .add(Projections.property("room.smocking").as("SMOKING"))
		        .add(Projections.property("room.service").as("SERVICE"))
		        .add(Projections.property("room.image").as("IMAGE"))
		        .add(Projections.property("room.breaks").as("BREAK"))
		        .add(Projections.property("room.Capacity").as("CAPACITYS"))
		        .add(Projections.property("room.udloginname").as("UDLOGINNAME"));
		
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public List<Room> editRoom(int roomID) {
		Criteria criteria = getSession().createCriteria(Room.class, "room");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("room.roomID"), "roomID")
				.add(Projections.property("room.roomBeds").as("roomBeds"))
				.add(Projections.property("room.buildingID").as("buildingID"))
				.add(Projections.property("room.roomTypeName").as("roomTypeName"))
				.add(Projections.property("room.roomDate").as("roomDate"))
				.add(Projections.property("room.roomPrice").as("roomPrice"))
				.add(Projections.property("room.roomNo").as("roomNo"))
				.add(Projections.property("room.typeDetail").as("typeDetail"))
		        .add(Projections.property("room.freewifi").as("freewifi"))
		        .add(Projections.property("room.smocking").as("smocking"))
		        .add(Projections.property("room.service").as("service"))
		        .add(Projections.property("room.image").as("image"))
		        .add(Projections.property("room.Capacity").as("Capacity"))
		        .add(Projections.property("room.breaks").as("breaks"));
		
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("room.roomID", roomID));
		criteria.setResultTransformer(Transformers.aliasToBean(Room.class));
		List<Room> editList = criteria.list();
		return editList;
	}
	
	@Override
	public RoomMapping editRoom1(RoomMapping roomMapping) {
		Criteria criteria = getSession().createCriteria(Room.class, "room");
		criteria.createAlias("room.roomType", "roomType");
		criteria.createAlias("roomType.promotion", "promotion");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("room.roomID"), "roomID")
				.add(Projections.property("room.roomBeds").as("roomBeds"))
				.add(Projections.property("room.buildingID").as("buildingID"))
				.add(Projections.property("room.roomTypeName").as("roomTypeName"))
				.add(Projections.property("room.roomDate").as("roomDate"))
				.add(Projections.property("room.roomPrice").as("price"))
				.add(Projections.property("room.roomNo").as("roomNo"))
				.add(Projections.property("room.typeDetail").as("typeDetail"))
		        .add(Projections.property("room.freewifi").as("freewifi"))
		        .add(Projections.property("room.smocking").as("smocking"))
		        .add(Projections.property("room.service").as("service"))
		        .add(Projections.property("room.image").as("imageByte"))
		        .add(Projections.property("room.Capacity").as("Capacity"))
		        .add(Projections.property("room.breaks").as("breaks"))
		        .add(Projections.property("promotion.rate").as("rate"))
		        ;
		
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("room.roomID", roomMapping.getRoomID()));
		criteria.setResultTransformer(Transformers.aliasToBean(RoomMapping.class));
		RoomMapping editList = (RoomMapping) criteria.uniqueResult();
		return editList;
	}

	@Override
	public ArrayList<Room> RoomSelect(RoomMapping roomMapping) {
		Criteria criteria = getSession().createCriteria(Room.class, "room");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("room.roomID"), "roomID")
				.add(Projections.property("room.roomBeds").as("roomBeds"))
				.add(Projections.property("room.buildingID").as("buildingID"))
				.add(Projections.property("room.roomTypeName").as("roomTypeName"))
				.add(Projections.property("room.roomDate").as("roomDate"))
				.add(Projections.property("room.roomPrice").as("roomPrice"))
				.add(Projections.property("room.roomNo").as("roomNo"))
				.add(Projections.property("room.typeDetail").as("typeDetail"))
		        .add(Projections.property("room.freewifi").as("freewifi"))
		        .add(Projections.property("room.smocking").as("smocking"))
		        .add(Projections.property("room.service").as("service"))
		        .add(Projections.property("room.image").as("image"))
		        .add(Projections.property("room.Capacity").as("Capacity"))
		        .add(Projections.property("room.breaks").as("breaks"));
		
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("room.roomID", roomMapping.getRoomID()));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Room> roomList = (ArrayList<Room>) criteria.list();
		return roomList;
	}

	@Override
	public void remove(Room rm) {
		getSession().delete(rm);
		
	}

	@Override
	public List<Map<String, Object>> roomSearch(RoomMapping room) {
		System.out.println(room.getRoomTypeName()+"8888888888888888888888888888888");
		Criteria criteria = getSession().createCriteria(Room.class, "room");
		criteria.createAlias("room.roomType", "roomType");
		criteria.createAlias("roomType.promotion", "promotion");
		ProjectionList projections = Projections.projectionList()
					 .add(Projections.property("roomType.roomID"), "roomID")
						.add(Projections.property("room.roomBeds").as("roomBeds"))
						.add(Projections.property("room.buildingID").as("buildingID"))
						.add(Projections.property("room.roomTypeName").as("roomTypeName"))
						.add(Projections.property("room.roomDate").as("roomDate"))
						.add(Projections.property("room.roomPrice").as("roomPrice"))
						.add(Projections.property("room.roomNo").as("roomNo"))
						.add(Projections.property("room.typeDetail").as("typeDetail"))
				        .add(Projections.property("room.freewifi").as("freewifi"))
				        .add(Projections.property("room.smocking").as("smocking"))
				        .add(Projections.property("room.service").as("service"))
				        .add(Projections.property("room.image").as("image"))
				        .add(Projections.property("room.breaks").as("breaks"))
				        .add(Projections.property("room.Capacity").as("Capacity"))
				        .add(Projections.property("promotion.rate").as("rate"))
				        .add(Projections.property("roomType.max").as("max"))
				        
		                .add(Projections.property("room.udloginname").as("udloginname"));
		
			 criteria.add(Restrictions.like("room.roomTypeName","%"+ room.getRoomTypeName()+"%")); 
			 
			 DetachedCriteria userSubquery = DetachedCriteria.forClass(Booking.class, "booking");
			  ProjectionList subProjections = Projections.projectionList()
			 .add(Projections.property("booking.roomID").as("bookingid"));
			  userSubquery.setProjection(subProjections);
			  userSubquery.add(Restrictions.between("booking.checkIN", DateUtils.parseShortDate(room.getFromDate()), DateUtils.parseShortDate(room.getToDate())));
			  userSubquery.add(Restrictions.between("booking.checkOUT", DateUtils.parseShortDate(room.getFromDate()), DateUtils.parseShortDate(room.getToDate())));

			  criteria.add(Subqueries.propertyNotIn("room.roomID", userSubquery));
			 criteria.setProjection(projections);
			 criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			 return criteria.list();
		}

	@Override
	public List<Room> RoomSelect1(RoomMapping room) {
		Criteria criteria = getSession().createCriteria(Room.class, "room");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("room.roomID"), "roomID")
				.add(Projections.property("room.roomBeds").as("roomBeds"))
				.add(Projections.property("room.buildingID").as("buildingID"))
				.add(Projections.property("room.roomTypeName").as("roomTypeName"))
				.add(Projections.property("room.roomDate").as("roomDate"))
				.add(Projections.property("room.roomPrice").as("roomPrice"))
				.add(Projections.property("room.roomNo").as("roomNo"))
				.add(Projections.property("room.typeDetail").as("typeDetail"))
		        .add(Projections.property("room.freewifi").as("freewifi"))
		        .add(Projections.property("room.smocking").as("smocking"))
		        .add(Projections.property("room.service").as("service"))
		        .add(Projections.property("room.image").as("image"))
		        .add(Projections.property("room.breaks").as("breaks"));
		
		criteria.setProjection(projections);
//		criteria.add(Restrictions.eq("room.roomID", room.getRoomID()));
		criteria.add(Restrictions.like("room.roomTypeName","%"+ room.getRoomTypeName()+"%"));
		
		criteria.setResultTransformer(Transformers.aliasToBean(Room.class));
		List<Room> editList = criteria.list();
		return editList;
	}

	@Override
	public List<Room> RoomSelectBk(RoomMapping room) {
		Criteria criteria = getSession().createCriteria(Room.class, "booking");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("booking.roomID"), "roomID")
				.add(Projections.property("booking.roomBeds").as("roomBeds"))
				.add(Projections.property("booking.buildingID").as("buildingID"))
				.add(Projections.property("booking.roomTypeName").as("roomTypeName"))
				.add(Projections.property("booking.roomDate").as("roomDate"))
				.add(Projections.property("booking.roomPrice").as("roomPrice"))
				.add(Projections.property("booking.roomNo").as("roomNo"))
				.add(Projections.property("booking.typeDetail").as("typeDetail"))
		        .add(Projections.property("booking.freewifi").as("freewifi"))
		        .add(Projections.property("booking.smocking").as("smocking"))
		        .add(Projections.property("booking.service").as("service"))
		        .add(Projections.property("booking.image").as("image"))
		        .add(Projections.property("booking.Capacity").as("Capacity"))
		        .add(Projections.property("booking.breaks").as("breaks"));
		
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("booking.roomID", room.getRoomID()));
		criteria.setResultTransformer(Transformers.aliasToBean(Room.class));
		List<Room> editList = criteria.list();
		return editList;
	}

	@Override
	public List<Map<String, Object>> LGSelectUs() {
		Criteria criteria = getSession().createCriteria(Room.class, "room");
		criteria.createAlias("room.building", "building");
		criteria.createAlias("room.roomType", "roomType");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("roomType.roomID").as("roomID"))
				.add(Projections.property("building.buildingID").as("BUILDINGID"))
				.add(Projections.property("roomType.roomTypeName").as("ROOMTYPENAME"))
				.add(Projections.property("room.roomPrice").as("ROOMPRICE"))
				.add(Projections.property("room.roomNo").as("ROOMNO"))
				.add(Projections.property("room.typeDetail").as("TYPEDETAIL"))
		        .add(Projections.property("room.image").as("IMAGE"))
		        .add(Projections.property("room.udloginname").as("UDLOGINNAME"));
		
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();

	}
	
}
