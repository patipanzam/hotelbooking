package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.Option;

@Component
public class OptionDaoImp extends AbstractDao<Integer, Option> implements OptionDao {

	@Override
	public ArrayList<Option> getOption() {
		Criteria criteria = getSession().createCriteria(Option.class, "op");
		 ProjectionList projections = Projections.projectionList()
		            .add(Projections.property("op.optionID").as("optionID"))
		            .add(Projections.property("op.optionName").as("optionName"))
		            .add(Projections.property("op.optionPrice").as("optionPrice"))
		            .add(Projections.property("op.udloginname").as("udloginname"));
		 criteria.setProjection(projections);
		 criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		 ArrayList<Option> optionList = (ArrayList<Option>) criteria.list();
		return optionList;
	}

	@Override
	public List<Map<String, Object>> PTSelect() {
		Criteria criteria = getSession().createCriteria(Option.class, "op");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("op.optionID").as("optionID"))
	            .add(Projections.property("op.optionName").as("optionName"))
	            .add(Projections.property("op.optionPrice").as("optionPrice"))
	            .add(Projections.property("op.udloginname").as("udloginname"));
		       criteria.setProjection(projections);
		       criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void remove(Option pt) {
		getSession().delete(pt);
		
	}

	@Override
	public void updateGridOption(Option pt)throws Exception {
		merge(pt);
		
	}

	@Override
	public void saveGridOption(Option pt) throws Exception{
		    save(pt);
		
	}

}
