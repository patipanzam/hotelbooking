package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.Option;

public interface OptionDao {
	
	ArrayList<Option> getOption();

	List<Map<String, Object>> PTSelect();

	void remove(Option pt);

	void updateGridOption(Option pt) throws Exception;

	void saveGridOption(Option pt) throws Exception;


}
