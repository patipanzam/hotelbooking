package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.entity.Employee;

public interface EmployeeDao {

	public void saveEmployee(Employee em) throws Exception;

	public List<Map<String, Object>> EMSelect();

	public void remove(Employee em);

	public ArrayList<Employee> getEmployee();

	public List<Employee> removeEmployee(Integer deletes);

	public List<Employee> editEmployee(final int id);

	public void updateEmployee(Employee em) throws Exception;

	public List<Map<String, Object>> Search(String SearchEmployee);

	public List<Employee> getEmployeeName();

	public ArrayList<Employee> EmployeeSelect(EmployeeMapping employeeMapping);
}
