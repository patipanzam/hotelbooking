package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.entity.Promotion;

public interface PromotionDao {
	
	public	void savePromotion(Promotion pro) throws Exception;

	public void updatePromotion(Promotion pro) throws Exception;

	public void deletePromotion(Promotion pro);

	public List<Map<String, Object>> LGSelect();

	public	List<Promotion> editPromotion(int promotionID);

	public ArrayList<Promotion> PromotionSelect(PromotionMapping pro);
	
	public void remove(Promotion pro);

	public ArrayList<Promotion> getRoomType();

}
