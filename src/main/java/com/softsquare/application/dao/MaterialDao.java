package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.Material;

public interface MaterialDao {

	public void saveMaterial(Material ma) ;

	public List<Map<String, Object>> MASelect();

	public void saveGridMaterial(Material ma);

	public void updateGridMaterial(Material ma);

	public void remove(Material ma);
	
	public  ArrayList<Material> getMaterial();
	
	public List<Map<String, Object>> Search(String SearchMaterial, String SearchMaterialType);
}
