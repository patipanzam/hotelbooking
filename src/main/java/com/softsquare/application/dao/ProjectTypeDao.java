package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.ProjectType;

public interface ProjectTypeDao {

	public void saveProjectType(ProjectType pt);

	public List<Map<String, Object>> PTSelect();

	public void saveGridProjectType(ProjectType pt);

	public void updateGridProjectType(ProjectType pt);

	public void remove(ProjectType pt);

	public ArrayList<ProjectType> getProjectType();

	public List<Map<String, Object>> Search(String SearchProjectType);
}
