package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.entity.Promotion;
import com.softsquare.application.entity.Room;
import com.softsquare.application.entity.RoomType;

@Repository()
@Component
public class PromotionDaoImp extends  AbstractDao<Integer, Promotion>implements PromotionDao{

	@Override
	public void savePromotion(Promotion pro) throws Exception {
		save(pro);
		
	}

	@Override
	public void updatePromotion(Promotion pro) throws Exception {
		merge(pro);
		
	}

	@Override
	public void deletePromotion(Promotion pro) {
		delete(pro);
		
	}

	@Override
	public List<Map<String, Object>> LGSelect() {
		Criteria criteria = getSession().createCriteria(Promotion.class, "pro");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pro.promotionID").as("promotionID"))
				.add(Projections.property("pro.name").as("NAME"))
				.add(Projections.property("pro.description").as("DESCRIPTION"))
				.add(Projections.property("pro.fromDate").as("FROMDATE"))
				.add(Projections.property("pro.toDate").as("TODATE"))
				.add(Projections.property("pro.date").as("SUMDATE"))
		        .add(Projections.property("pro.room").as("ROOM"))
		        .add(Projections.property("pro.rate").as("RATE"))
		        .add(Projections.property("pro.status").as("STATUS"));
		       
		       
		
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	
	}

	@Override
	public List<Promotion> editPromotion(int promotionID) {
		Criteria criteria = getSession().createCriteria(Promotion.class, "pro");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pro.promotionID"),("promotionID"))
				.add(Projections.property("pro.name").as("name"))
				.add(Projections.property("pro.description").as("description"))
				.add(Projections.property("pro.fromDate").as("fromDate"))
				.add(Projections.property("pro.toDate").as("toDate"))
				.add(Projections.property("pro.date").as("date"))
		        .add(Projections.property("pro.room").as("room"))
		        .add(Projections.property("pro.rate").as("rate"))
		        .add(Projections.property("pro.status").as("status"));
		        
		
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("pro.promotionID", promotionID));
		criteria.setResultTransformer(Transformers.aliasToBean(Promotion.class));
		List<Promotion> editList = criteria.list();
		return editList;
	}

	@Override
	public ArrayList<Promotion> PromotionSelect(PromotionMapping pro) {
		Criteria criteria = getSession().createCriteria(Promotion.class, "pro");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pro.promotionID"),("promotionID"))
				.add(Projections.property("pro.name").as("name"))
				.add(Projections.property("pro.description").as("description"))
				.add(Projections.property("pro.fromDate").as("fromDate"))
				.add(Projections.property("pro.toDate").as("toDate"))
				.add(Projections.property("pro.date").as("date"))
		        .add(Projections.property("pro.room").as("room"))
		        .add(Projections.property("pro.rate").as("rate"))
		        .add(Projections.property("pro.status").as("status"));
		       
		
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("pro.promotionID", pro.getPromotionID()));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Promotion> promotionList = (ArrayList<Promotion>) criteria.list();
		return promotionList;
	}

	@Override
	public void remove(Promotion pro) {
		getSession().delete(pro);
		
	}

	@Override
	public ArrayList<Promotion> getRoomType() {
		Criteria criteria = getSession().createCriteria(Promotion.class, "pro");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pro.promotionID"),("promotionID"))
				.add(Projections.property("pro.name").as("name"))
				.add(Projections.property("pro.description").as("description"))
				.add(Projections.property("pro.fromDate").as("fromDate"))
				.add(Projections.property("pro.toDate").as("toDate"))
				.add(Projections.property("pro.date").as("date"))
		        .add(Projections.property("pro.room").as("room"))
		        .add(Projections.property("pro.rate").as("rate"))
		        .add(Projections.property("pro.status").as("status"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Promotion> promotionList = (ArrayList<Promotion>) criteria.list();
		return promotionList;
	}

}
