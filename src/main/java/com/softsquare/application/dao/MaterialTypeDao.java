package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.MaterialType;

public interface MaterialTypeDao {

	public void saveMaterialType(MaterialType mat);

	public List<Map<String, Object>> MATSelect();

	public void saveGridMaterialType(MaterialType mat);

	public void updateGridMaterialType(MaterialType mat);

	public void remove(MaterialType mat);

	public ArrayList<MaterialType> getMaterialType();

	public List<Map<String, Object>> Search(String SearchMaterialType);
}
