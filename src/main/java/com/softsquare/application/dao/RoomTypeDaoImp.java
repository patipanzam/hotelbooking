package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.Building;
import com.softsquare.application.entity.Role;
import com.softsquare.application.entity.RoomType;

@Component
public class RoomTypeDaoImp extends AbstractDao<Integer, RoomType>implements RoomTypeDao {

	@Override
	public ArrayList<RoomType> getRoomType() {
		Criteria criteria = getSession().createCriteria(RoomType.class, "rt");
		ProjectionList projections = Projections.projectionList().add(Projections.property("rt.roomID").as("roomID"))
				  .add(Projections.property("rt.roomTypeName").as("roomTypeName"))
		          .add(Projections.property("rt.udloginname").as("udloginname"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<RoomType> roomtypeList = (ArrayList<RoomType>) criteria.list();
		return roomtypeList;
	}

	@Override
	public List<Map<String, Object>> PTSelect() {
		Criteria criteria = getSession().createCriteria(RoomType.class, "rt");
		criteria.createAlias("rt.promotion", "promotion");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("rt.roomID"), "roomID")
				.add(Projections.property("rt.roomTypeName"), "roomTypeName")
				.add(Projections.property("rt.promotionID"), "promotionID")
				.add(Projections.property("rt.max"), "max")
				.add(Projections.property("promotion.rate"), "rate")
	         	.add(Projections.property("rt.udloginname").as("udloginname"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void remove(RoomType pt)throws Exception {
		delete(pt);

	}

	@Override
	public void updateGridRoomType(RoomType pt) throws Exception {
		merge(pt);
	}

	@Override
	public void saveGridRoomType(RoomType pt) throws Exception {
		    save(pt);

	}

}
