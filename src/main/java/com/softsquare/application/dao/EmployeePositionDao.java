package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.EmployeePosition;

public interface EmployeePositionDao {
	
	public void saveGridEmployeePosition(EmployeePosition ep) throws Exception;

	public List<Map<String, Object>> EPSelect(int id);

	public void updateGridEmployeePosition(EmployeePosition ep) throws Exception;

	public void remove(EmployeePosition ep);
	
	public  ArrayList<EmployeePosition> getEmployeePosition();
	
	public List<EmployeePosition> removeEmployeePosition(int projectID);

}
