package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.entity.Booking;
import com.softsquare.application.entity.Promotion;

public interface BookingDao {

	public void saveBooking(Booking book) throws Exception;

    public	void updateBooking(Booking book)throws Exception;
    
    public void deleteBooking(Booking book);

	public List<Map<String, Object>> PTSelect();

	public void bookingMapping(Booking pt) throws Exception;

	public void updateGridRoomType(Booking pt)throws Exception;

	public void remove(Booking pt)throws Exception;

	public void bookremove(Booking rmd);

	public List<Map<String, Object>> LGSelect();

	
////////////////////////////////Check cancel booking////////////////////////////////////////////	
	public List<Map<String, Object>> BookingSearch(BookingMapping bookingMap);

	public void updategrid(Booking book);

	////////////////////////////////Check cancel booking////////////////////////////////////////////
	List<Map<String, Object>> CheckCancelSelect();
	
	ArrayList<Booking> getBooking();
	
	void removeGridCheckCancel(Booking bk);

	void saveGridCheckCancel(Booking bk) throws Exception;

	void updateGridCheckCancel(Booking bk) throws Exception;

}
