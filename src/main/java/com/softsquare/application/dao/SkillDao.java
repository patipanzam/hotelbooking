package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.Skill;

public interface SkillDao {

	public List<Map<String, Object>> SKSelect(int id);

	public void saveGridSkill(Skill sk) throws Exception;

	public void updateGridSkill(Skill sk) throws Exception;

	public void remove(Skill sk);
	
	public  ArrayList<Skill> getSkill(String positionID, String EmployeeName);
	
	public List<Skill> removeSkill(int employeeID);
	
	public List<Skill> editSkill(final int id);

	public List<Map<String, Object>> RegisterSelect();
	
	public List<Map<String, Object>> SearchRegister(String SearchRegister, String SearchPositionCombobox);
}
