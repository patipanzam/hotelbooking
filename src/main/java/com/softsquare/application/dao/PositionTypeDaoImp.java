package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.PositionType;

@Component
public class PositionTypeDaoImp extends AbstractDao<Integer, PositionType> implements PositionTypeDao {

	@Override
	public void savePositionType(PositionType pt) {
		getSession().save(pt);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> PTSelect() {

		Criteria criteria = getSession().createCriteria(PositionType.class, "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pt.positionTypeID"), "positionTypeID")
				.add(Projections.property("pt.positionTypeName"), "positionTypeName");
		criteria.setProjection(projections);
		criteria.add(Restrictions.ne("pt.positionTypeID", 1));
		criteria.add(Restrictions.ne("pt.positionTypeID", 2));
		criteria.add(Restrictions.ne("pt.positionTypeID", 3));
		criteria.add(Restrictions.ne("pt.positionTypeID", 4));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void saveGridPositionType(PositionType pt) {
		getSession().save(pt);
	}

	@Override
	public void updateGridPositionType(PositionType pt) {
		getSession().update(pt);
	}

	@Override
	public void remove(PositionType pt) {
		getSession().delete(pt);

	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<PositionType> getPositionType() {
		Criteria criteria = getSession().createCriteria(PositionType.class, "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("pt.positionTypeID").as("positionTypeID"))
				.add(Projections.property("pt.positionTypeName").as("positionTypeName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<PositionType> positionTypeList = (ArrayList<PositionType>) criteria.list();
		return positionTypeList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchPositionType) {

		Criteria criteria = getSession().createCriteria(PositionType.class, "pt");

		if (SearchPositionType.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("pt.positionTypeID"), "positionTypeID")
					.add(Projections.property("pt.positionTypeName"), "positionTypeName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.ne("pt.positionTypeID", 1));
			criteria.add(Restrictions.ne("pt.positionTypeID", 2));
			criteria.add(Restrictions.ne("pt.positionTypeID", 3));
			criteria.add(Restrictions.ne("pt.positionTypeID", 4));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchPositionType.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("pt.positionTypeID"), "positionTypeID")
					.add(Projections.property("pt.positionTypeName"), "positionTypeName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.ne("pt.positionTypeID", 1));
			criteria.add(Restrictions.ne("pt.positionTypeID", 2));
			criteria.add(Restrictions.ne("pt.positionTypeID", 3));
			criteria.add(Restrictions.ne("pt.positionTypeID", 4));
			criteria.add(Restrictions.like("pt.positionTypeName", "%" + SearchPositionType + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}
}
