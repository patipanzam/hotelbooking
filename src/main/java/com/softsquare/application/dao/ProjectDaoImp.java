package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.domain.ProjectMapping;
import com.softsquare.application.entity.Project;

@Component
public class ProjectDaoImp extends AbstractDao<Integer, Project> implements ProjectDao {

	@Override
	public void saveProject(Project it) throws Exception {
		save(it);
	}

	@Override
	public void updateProject(Project po) throws Exception {
		merge(po);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> POSelect() {
		Criteria criteria = getSession().createCriteria(Project.class, "po");
		criteria.createAlias("po.projectType", "pt");
		criteria.createAlias("po.branch", "b");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("po.projectID"), "projectID")
				.add(Projections.property("po.projectName"), "projectName")
				.add(Projections.property("po.projectLocation"), "projectLocation")
				.add(Projections.property("po.projectDateStart"), "projectDateStart")
				.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
				.add(Projections.property("po.projectBudget"), "projectBudget")
				.add(Projections.property("po.projectDescription"), "projectDescription")
				.add(Projections.property("pt.projectTypeName"), "projectTypeName")
				.add(Projections.property("b.branchName"), "branchName");
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Project> ProjectSelect(ProjectMapping projectMapping) {
		Criteria criteria = getSession().createCriteria(Project.class, "po");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("po.projectID"), "projectID")
				.add(Projections.property("po.projectName"), "projectName")
				.add(Projections.property("po.projectLocation"), "projectLocation")
				.add(Projections.property("po.projectDateStart"), "projectDateStart")
				.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
				.add(Projections.property("po.projectBudget"), "projectBudget")
				.add(Projections.property("po.projectDescription"), "projectDescription")
				.add(Projections.property("po.projectTypeID"), "projectTypeID")
				.add(Projections.property("po.branchID"), "branchID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("po.projectID", projectMapping.getProjectID()));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Project> projectList = (ArrayList<Project>) criteria.list();
		return projectList;
	}

	@Override
	public void saveGridProject(Project po) {
		getSession().save(po);
	}

	@Override
	public void updateGridProject(Project po) {
		getSession().update(po);
	}

	@Override
	public void remove(Project po) {
		getSession().delete(po);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Project> getProject() {
		Criteria criteria = getSession().createCriteria(Project.class, "po");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("po.projectID").as("projectID"))
				.add(Projections.property("po.projectName").as("projectName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Project> projectList = (ArrayList<Project>) criteria.list();
		return projectList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> removeProject(Integer deletes) {
		Criteria criteria = getSession().createCriteria(Project.class, "po");
		ProjectionList projections = Projections.projectionList().add(Projections.property("po.projectID"), "projectID")
				.add(Projections.property("po.projectName"), "projectName")
				.add(Projections.property("po.projectLocation"), "projectLocation")
				.add(Projections.property("po.projectDateStart"), "projectDateStart")
				.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
				.add(Projections.property("po.projectBudget"), "projectBudget")
				.add(Projections.property("po.projectDescription"), "projectDescription")
				.add(Projections.property("po.projectTypeID"), "projectTypeID")
				.add(Projections.property("po.branchID"), "branchID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("po.projectID", deletes));
		criteria.setResultTransformer(Transformers.aliasToBean(Project.class));
		return criteria.list();

	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Project> editProject(final int id) {
		Criteria criteria = getSession().createCriteria(Project.class, "po");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("po.projectID"), "projectID")
				.add(Projections.property("po.projectName"), "projectName")
				.add(Projections.property("po.projectLocation"), "projectLocation")
				.add(Projections.property("po.projectDateStart"), "projectDateStart")
				.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
				.add(Projections.property("po.projectBudget"), "projectBudget")
				.add(Projections.property("po.projectDescription"), "projectDescription")
				.add(Projections.property("po.projectTypeID"), "projectTypeID")
				.add(Projections.property("po.branchID"), "branchID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("po.projectID", id));
		criteria.setResultTransformer(Transformers.aliasToBean(Project.class));
		List<Project> editList = criteria.list();
		return editList;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> Search(String SearchProject, String SearchProjectType, String SearchBranch) {

		Criteria criteria = getSession().createCriteria(Project.class, "po");

		if (SearchProject.isEmpty() && SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchProject.isEmpty() && SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchProject.isEmpty() && !SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
			int PT = Integer.parseInt(SearchProjectType);
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("po.projectTypeID", PT));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchProject.isEmpty() && SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
			int B = Integer.parseInt(SearchBranch);
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("po.branchID", B));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchProject.isEmpty() && !SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
			int PT = Integer.parseInt(SearchProjectType);
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
			criteria.add(Restrictions.eq("po.projectTypeID", PT));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchProject.isEmpty() && !SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
			int PT = Integer.parseInt(SearchProjectType);
			int B = Integer.parseInt(SearchBranch);
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("po.projectTypeID", PT));
			criteria.add(Restrictions.eq("po.branchID", B));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchProject.isEmpty() && SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
			int B = Integer.parseInt(SearchBranch);
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
			criteria.add(Restrictions.eq("po.branchID", B));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchProject.isEmpty() && !SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
			int PT = Integer.parseInt(SearchProjectType);
			int B = Integer.parseInt(SearchBranch);
			criteria.createAlias("po.projectType", "pt");
			criteria.createAlias("po.branch", "b");
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("po.projectID"), "projectID")
					.add(Projections.property("po.projectName"), "projectName")
					.add(Projections.property("po.projectLocation"), "projectLocation")
					.add(Projections.property("po.projectDateStart"), "projectDateStart")
					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
					.add(Projections.property("po.projectBudget"), "projectBudget")
					.add(Projections.property("po.projectDescription"), "projectDescription")
					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
					.add(Projections.property("b.branchName"), "branchName");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
			criteria.add(Restrictions.eq("po.projectTypeID", PT));
			criteria.add(Restrictions.eq("po.branchID", B));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> HomeSelect() {

//		int empID = Integer.parseInt(LoginUtils.getEmployeeID());
		Criteria criteria = getSession().createCriteria(Project.class, "po");
		criteria.createAlias("po.projectType", "pt");
		criteria.createAlias("po.branch", "b");
		criteria.createAlias("po.employeePosition", "ep");
		criteria.createAlias("ep.skill", "sk");
		ProjectionList projections = Projections.projectionList().add(Projections.property("po.projectID"), "projectID")
				.add(Projections.property("po.projectName"), "projectName")
				.add(Projections.property("po.projectLocation"), "projectLocation")
				.add(Projections.property("po.projectDateStart"), "projectDateStart")
				.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
				.add(Projections.property("po.projectBudget"), "projectBudget")
				.add(Projections.property("po.projectDescription"), "projectDescription")
				.add(Projections.property("pt.projectTypeName"), "projectTypeName")
				.add(Projections.property("b.branchName"), "branchName");
		criteria.setProjection(projections);
//		criteria.add(Restrictions.eq("sk.employeeID", empID));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> HomeSearch(String SearchProject, String SearchProjectType, String SearchBranch) {
		
//		int empID = Integer.parseInt(LoginUtils.getEmployeeID());
//		Criteria criteria = getSession().createCriteria(Project.class, "po");
//
//		if (SearchProject.isEmpty() && SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
////			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//
//		if (!SearchProject.isEmpty() && SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
//			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//
//		if (SearchProject.isEmpty() && !SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
//			int PT = Integer.parseInt(SearchProjectType);
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
//			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.add(Restrictions.eq("po.projectTypeID", PT));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//
//		if (SearchProject.isEmpty() && SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
//			int B = Integer.parseInt(SearchBranch);
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
//			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.add(Restrictions.eq("po.branchID", B));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//
//		if (!SearchProject.isEmpty() && !SearchProjectType.isEmpty() && SearchBranch.isEmpty()) {
//			int PT = Integer.parseInt(SearchProjectType);
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
//			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
//			criteria.add(Restrictions.eq("po.projectTypeID", PT));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//
//		if (SearchProject.isEmpty() && !SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
//			int PT = Integer.parseInt(SearchProjectType);
//			int B = Integer.parseInt(SearchBranch);
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
//			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.add(Restrictions.eq("po.projectTypeID", PT));
//			criteria.add(Restrictions.eq("po.branchID", B));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//
//		if (!SearchProject.isEmpty() && SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
//			int B = Integer.parseInt(SearchBranch);
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
//			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
//			criteria.add(Restrictions.eq("po.branchID", B));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//
//		if (!SearchProject.isEmpty() && !SearchProjectType.isEmpty() && !SearchBranch.isEmpty()) {
//			int PT = Integer.parseInt(SearchProjectType);
//			int B = Integer.parseInt(SearchBranch);
//			criteria.createAlias("po.projectType", "pt");
//			criteria.createAlias("po.branch", "b");
//			criteria.createAlias("po.employeePosition", "ep");
//			criteria.createAlias("ep.skill", "sk");
//			ProjectionList projections = Projections.projectionList()
//					.add(Projections.property("po.projectID"), "projectID")
//					.add(Projections.property("po.projectName"), "projectName")
//					.add(Projections.property("po.projectLocation"), "projectLocation")
//					.add(Projections.property("po.projectDateStart"), "projectDateStart")
//					.add(Projections.property("po.projectDateEnd"), "projectDateEnd")
//					.add(Projections.property("po.projectBudget"), "projectBudget")
//					.add(Projections.property("po.projectDescription"), "projectDescription")
//					.add(Projections.property("pt.projectTypeName"), "projectTypeName")
//					.add(Projections.property("b.branchName"), "branchName");
//			criteria.setProjection(projections);
//			criteria.add(Restrictions.eq("sk.employeeID", empID));
//			criteria.add(Restrictions.like("po.projectName", "%" + SearchProject + "%"));
//			criteria.add(Restrictions.eq("po.projectTypeID", PT));
//			criteria.add(Restrictions.eq("po.branchID", B));
//			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
//		}
//		return criteria.list();
		return null;
	}
}
