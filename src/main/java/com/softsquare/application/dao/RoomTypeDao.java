package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.entity.RoomType;

public interface RoomTypeDao {

	ArrayList<RoomType> getRoomType();

	List<Map<String, Object>> PTSelect();

	void remove(RoomType pt) throws Exception;

	void updateGridRoomType(RoomType pt)throws Exception;

	void saveGridRoomType(RoomType pt) throws Exception;



}
