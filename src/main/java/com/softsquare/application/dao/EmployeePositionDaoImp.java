package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.EmployeePosition;

@Component
public class EmployeePositionDaoImp extends AbstractDao<Integer, EmployeePosition> implements EmployeePositionDao {

	@Override
	public void saveGridEmployeePosition(EmployeePosition ep) throws Exception  {
		save(ep);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> EPSelect(int id) {
		Criteria criteria = getSession().createCriteria(EmployeePosition.class, "ep");
		criteria.createAlias("ep.skill", "sk");
		criteria.createAlias("sk.employee", "em");
		criteria.createAlias("sk.position", "po");
		criteria.createAlias("po.positionType", "pt");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ep.employeePositionID"), "employeePositionID")
				.add(Projections.property("ep.skillID"), "skillID")
				.add(Projections.property("ep.projectID"), "projectID")
				.add(Projections.property("em.employeeName"), "employeeName")
				.add(Projections.property("em.employeeID"), "employeeID")
				.add(Projections.property("po.positionName"), "positionName")
				.add(Projections.property("pt.positionTypeName"), "positionTypeName")
				.add(Projections.property("po.positionID"), "positionID")
				.add(Projections.property("pt.positionTypeID"), "positionTypeID");
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("ep.projectID", id));
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void updateGridEmployeePosition(EmployeePosition ep) throws Exception {
		merge(ep);
	}

	@Override
	public void remove(EmployeePosition ep) {
		getSession().delete(ep);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<EmployeePosition> getEmployeePosition() {
		Criteria criteria = getSession().createCriteria(EmployeePosition.class, "ep");
		criteria.createAlias("ep.employee", "em");
		criteria.createAlias("ep.position", "po");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ep.employeePositionID").as("employeePositionID"))
				.add(Projections.property("ep.employeeID").as("employeeID"))
				.add(Projections.property("em.employeeName").as("employeeName"))
				.add(Projections.property("em.employeeID").as("employeeID"))
				.add(Projections.property("po.positionName").as("positionName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<EmployeePosition> employeePositionList = (ArrayList<EmployeePosition>) criteria.list();
		return employeePositionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeePosition> removeEmployeePosition(int projectID) {
		Criteria criteria = getSession().createCriteria(EmployeePosition.class, "ep");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ep.employeePositionID").as("employeePositionID"))
				.add(Projections.property("ep.projectID").as("projectID"));
		criteria.setProjection(projections);
		criteria.add(Restrictions.eq("ep.projectID", projectID));
		criteria.setResultTransformer(Transformers.aliasToBean(EmployeePosition.class));
		List<EmployeePosition> editList = criteria.list();
		return editList;
	}
}
