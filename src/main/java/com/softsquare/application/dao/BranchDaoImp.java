package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softsquare.application.entity.Branch;
import com.softsquare.application.entity.Branch;

@Component
public class BranchDaoImp extends AbstractDao<Integer, Branch> implements BranchDao {

	@Override
	public void saveBranch(Branch ba) {
		getSession().save(ba);

	}

	@Override
	public List<Map<String, Object>> BASelect() {
		Criteria criteria = getSession().createCriteria(Branch.class, "ba");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ba.branchID"), "branchID")
				.add(Projections.property("ba.branchName"), "branchName")
				.add(Projections.property("ba.province"), "province")
				.add(Projections.property("ba.branchLocation"), "branchLocation")
				.add(Projections.property("ba.branchTel"), "branchTel");
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void saveGridBranch(Branch ba) {
		getSession().save(ba);

	}

	@Override
	public void updateGridBranch(Branch ba) {
		getSession().update(ba);

	}

	@Override
	public void remove(Branch ba) {
		getSession().delete(ba);

	}

	@Override
	public ArrayList<Branch> getBranch() {
		Criteria criteria = getSession().createCriteria(Branch.class, "ba");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ba.branchID").as("branchID"))
				.add(Projections.property("ba.branchName").as("branchName"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		ArrayList<Branch> branchList = (ArrayList<Branch>) criteria.list();
		return branchList;
	}

	@Override
	public List<Map<String, Object>> Search(String SearchBranch, String SearchProvince) {

		Criteria criteria = getSession().createCriteria(Branch.class, "ba");

		if (SearchBranch.isEmpty() && SearchProvince.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ba.branchID"), "branchID")
					.add(Projections.property("ba.branchName"), "branchName")
					.add(Projections.property("ba.province"), "province")
					.add(Projections.property("ba.branchLocation"), "branchLocation")
					.add(Projections.property("ba.branchTel"), "branchTel");
			criteria.setProjection(projections);
			// criteria.add(Restrictions.eq("ca.id", 3));
			// whare เงื่อนไขในการค้นหา
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchBranch.isEmpty() && SearchProvince.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ba.branchID"), "branchID")
					.add(Projections.property("ba.branchName"), "branchName")
					.add(Projections.property("ba.province"), "province")
					.add(Projections.property("ba.branchLocation"), "branchLocation")
					.add(Projections.property("ba.branchTel"), "branchTel");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("ba.branchName", "%" + SearchBranch + "%"));
			// whare เงื่อนไขในการค้นหา
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (SearchBranch.isEmpty() && !SearchProvince.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ba.branchID"), "branchID")
					.add(Projections.property("ba.branchName"), "branchName")
					.add(Projections.property("ba.province"), "province")
					.add(Projections.property("ba.branchLocation"), "branchLocation")
					.add(Projections.property("ba.branchTel"), "branchTel");
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("ba.province", SearchProvince));
			// whare เงื่อนไขในการค้นหา
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		if (!SearchBranch.isEmpty() && !SearchProvince.isEmpty()) {
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("ba.branchID"), "branchID")
					.add(Projections.property("ba.branchName"), "branchName")
					.add(Projections.property("ba.province"), "province")
					.add(Projections.property("ba.branchLocation"), "branchLocation")
					.add(Projections.property("ba.branchTel"), "branchTel");
			criteria.setProjection(projections);
			criteria.add(Restrictions.like("ba.branchName", "%" + SearchBranch + "%"));
			criteria.add(Restrictions.eq("ba.province", SearchProvince));
			// whare เงื่อนไขในการค้นหา
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		}

		return criteria.list();
	}

}
