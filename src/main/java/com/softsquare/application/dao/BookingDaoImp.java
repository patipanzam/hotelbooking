package com.softsquare.application.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.entity.Booking;

@Component
public class BookingDaoImp extends AbstractDao<Integer, Booking> implements BookingDao{

	@Override
	public void saveBooking(Booking book) throws Exception {
		save(book);
		
	}

	@Override
	public void updateBooking(Booking book) throws Exception {
		merge(book);
		
	}

	@Override
	public void deleteBooking(Booking book) {
		delete(book);
		
	}

	@Override
	public List<Map<String, Object>> PTSelect() {
		Criteria criteria = getSession().createCriteria(Booking.class, "bk");
		criteria.createAlias("bk.room", "room");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("bk.bookingID"), "bookingID")
				.add(Projections.property("room.roomTypeName"), "roomTypeName")
				.add(Projections.property("bk.price"), "price")
				.add(Projections.property("bk.udloginname").as("udloginname"));
		criteria.setProjection(projections);
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		return criteria.list();
	}

	@Override
	public void bookingMapping(Booking pt) throws Exception {
		       save(pt);
		
	}

	@Override
	public void updateGridRoomType( Booking pt)throws Exception {
		      merge(pt);
		
	}

	@Override
	public void remove(Booking pt) throws Exception {
		      save(pt);
		
	}

	@Override
	public void bookremove(Booking rmd) {
		getSession().delete(rmd);
		
	}
////////////////////////////////////////+++++++++++++++++++Booking list++++++++++++++++++++////////////////////////////////////////////////////////////
	@Override
	public List<Map<String, Object>> LGSelect() {
		Criteria criteria = getSession().createCriteria(Booking.class, "bk");
		criteria.createAlias("bk.room", "roomTypeName");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("bk.bookingID"), "bookingID")
				.add(Projections.property("room.roomTypeName"), "roomTypeName")
				.add(Projections.property("bk.udloginname").as("udloginname"))
				.add(Projections.property("bk.night"), "night")
				.add(Projections.property("bk.fname"), "fname").add(Projections.property("bk.lname"), "lname")
				.add(Projections.property("bk.phone"), "phone").add(Projections.property("bk.email"), "email")
				.add(Projections.property("bk.country"), "country")
				.add(Projections.property("bk.roomID"), "roomID")
				.add(Projections.property("bk.sumPrice"), "sumPrice")
				.add(Projections.property("bk.checkIN"), "checkIN")
				.add(Projections.property("bk.checkOUT"), "checkOUT")
				.add(Projections.property("bk.createUser"), "createUser")
				.add(Projections.property("bk.status"), "status")
				.add(Projections.property("bk.CancelStatus"),"CancelStatus");
			criteria.setProjection(projections);
			criteria.add(Restrictions.ne("ca.CancelStatus", "Approve"));
			criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			return criteria.list();
	}
////////////////////////////////////////+++++++Cancel Booking++++++++////////////////////////////////////////////////////////////
	@Override
	public List<Map<String, Object>> BookingSearch(BookingMapping booking) {
		Criteria criteria = getSession().createCriteria(Booking.class, "ca");
		ProjectionList projections = Projections.projectionList()
				.add(Projections.property("ca.bookingID"), "bookingID")
				.add(Projections.property("ca.night"), "night")
				.add(Projections.property("ca.fname"), "fname")
				.add(Projections.property("ca.lname"), "lname")
				.add(Projections.property("ca.phone"), "phone")
				.add(Projections.property("ca.email"), "email")
				.add(Projections.property("ca.country"), "country")
				.add(Projections.property("ca.roomID"), "roomID")
				.add(Projections.property("ca.sumPrice"), "sumPrice")
				.add(Projections.property("ca.checkIN"), "checkIN")
				.add(Projections.property("ca.checkOUT"), "checkOUT")
				.add(Projections.property("ca.createUser"), "createUser")
				.add(Projections.property("ca.CancelStatus"),"CancelStatus")
				.add(Projections.property("ca.rooms"),"rooms")
				.add(Projections.property("ca.status"), "status");
		criteria.setProjection(projections);
		if (!LoginUtils.getRole().equals("administrator")) {
			criteria.add(Restrictions.eq("ca.createUser", LoginUtils.getUsername()));
			criteria.add(Restrictions.ne("ca.CancelStatus", "Approve"));
		}
		criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> loadgrid = criteria.list();
		return loadgrid;
	}

	@Override
	public void updategrid(Booking book) {
		getSession().merge(book);

	}
////////////////////////////////////////+++++++Check Cancel Booking++++++++/////////////////////////////////////////


	@Override
	public void removeGridCheckCancel(Booking bk) {
		getSession().delete(bk);
		
	}

	@Override
	public void updateGridCheckCancel(Booking bk)throws Exception {
		merge(bk);
		
	}

	@Override
	public void saveGridCheckCancel(Booking bk) throws Exception{
		    save(bk);
		
	}	
	
@Override
public ArrayList<Booking> getBooking() {
Criteria criteria = getSession().createCriteria(Booking.class, "bk");
ProjectionList projections = Projections.projectionList()
.add(Projections.property("bk.night"), "night")
.add(Projections.property("bk.fname"), "fname").add(Projections.property("bk.lname"), "lname")
.add(Projections.property("bk.phone"), "phone").add(Projections.property("bk.email"), "email")
.add(Projections.property("bk.country"), "country")
.add(Projections.property("bk.roomID"), "roomID")
.add(Projections.property("bk.sumPrice"), "sumPrice")
.add(Projections.property("bk.checkIN"), "checkIN")
.add(Projections.property("bk.checkOUT"), "checkOUT")
.add(Projections.property("bk.createUser"), "createUser")
.add(Projections.property("bk.status"), "status")
.add(Projections.property("bk.CancelStatus"),"CancelStatus");
criteria.setProjection(projections);
criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
ArrayList<Booking> bookingList = (ArrayList<Booking>) criteria.list();
return bookingList;
}

@Override
public List<Map<String, Object>> CheckCancelSelect() {
Criteria criteria = getSession().createCriteria(Booking.class, "bk");
ProjectionList projections = Projections.projectionList()
.add(Projections.property("bk.bookingID"), "bookingID")
.add(Projections.property("bk.night"), "night")
.add(Projections.property("bk.fname"), "fname")
.add(Projections.property("bk.lname"), "lname")
.add(Projections.property("bk.phone"), "phone")
.add(Projections.property("bk.email"), "email")
.add(Projections.property("bk.country"), "country")
.add(Projections.property("bk.roomID"), "roomID")
.add(Projections.property("bk.sumPrice"), "sumPrice")
.add(Projections.property("bk.checkIN"), "checkIN")
.add(Projections.property("bk.checkOUT"), "checkOUT")
.add(Projections.property("bk.createUser"), "createUser")
.add(Projections.property("bk.CancelStatus"),"CancelStatus")
.add(Projections.property("bk.rooms"),"rooms")
.add(Projections.property("bk.status"), "status");
criteria.setProjection(projections);		       
criteria.add(Restrictions.between("bk.CancelStatus", "Cancel","Pending"));
//criteria.add(Restrictions.eq("bk.CancelStatus", "Approve"));
criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
return criteria.list();
}










}
