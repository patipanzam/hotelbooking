package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.PositionTypeDao;
import com.softsquare.application.domain.PositionTypeMapping;
import com.softsquare.application.entity.PositionType;

@Service
public class PositionTypeServiceImp implements PositionTypeService {

	
	@Autowired
	private PositionTypeDao positionTypeDao;
	
	@Override
	public void savePositionType(PositionTypeMapping positionType)throws Exception {
		PositionType pt = new PositionType();
		pt.setPositionTypeName(positionType.getPositionTypeName());
		positionTypeDao.savePositionType(pt);	

	}
	
	@Override
	public List<Map<String,Object>> PTSelect() {
		List<Map<String,Object>> select = positionTypeDao.PTSelect();
		return select;
	}

	@Override
	public void saveGridPositionType(PositionTypeMapping stPositionTypeMapping) {
		
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stPositionTypeMapping.getGridStore_jsonCreateRecords())){
			PositionType[] arrayRs = gson.fromJson(stPositionTypeMapping.getGridStore_jsonCreateRecords(), PositionType[].class);
			for (PositionType pt : arrayRs) {
				positionTypeDao.saveGridPositionType(pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(stPositionTypeMapping.getGridStore_jsonUpdateRecords())){
			PositionType[] arrayRs = gson.fromJson(stPositionTypeMapping.getGridStore_jsonUpdateRecords(), PositionType[].class);
			for (PositionType pt : arrayRs) {
				positionTypeDao.updateGridPositionType(pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(stPositionTypeMapping.getGridStore_jsonDestroyRecords())){
			PositionType[] arrayRs = gson.fromJson(stPositionTypeMapping.getGridStore_jsonDestroyRecords(), PositionType[].class);
			for (PositionType pt : arrayRs) {
				positionTypeDao.remove(pt);
			}
		}
	}

	@Override
	public ArrayList<PositionType> getPositionType() {
		return positionTypeDao.getPositionType();
	}
	
	@Override
	public List<Map<String, Object>> Search(String SearchPositionType) {
		List<Map<String, Object>> select = positionTypeDao.Search(SearchPositionType);
		return select;
	}

}
