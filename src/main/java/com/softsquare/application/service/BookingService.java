package com.softsquare.application.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonElement;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.entity.Booking;

public interface BookingService {

public Booking saveBooking(BookingMapping bk) throws Exception;

 List<Map<String, Object>> PTSelect();

public void saveGridBooking(BookingMapping bookingMapping)    throws Exception;

public void remove(Integer deletes);

public List<Map<String, Object>> Search(String searchRegister, String searchPositionCombobox);

public List<Map<String,Object>> LGSelect();

public void saveGridBooking1(BookingMapping bookingMapping);

public List<Map<String, Object>> BookingSearch(BookingMapping booking) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;

List<Map<String, Object>> CheckCancelSelect();

ArrayList<Booking> getBooking();

void saveGridCheckCancel(BookingMapping bookingMapping) throws Exception;

void updategrid(BookingMapping[] bookinggrid);



}
