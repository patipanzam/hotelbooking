package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.EmployeePositionMapping;
import com.softsquare.application.entity.EmployeePosition;
import com.softsquare.application.entity.Project;

public interface EmployeePositionService {
	
	public List<Map<String, Object>> EPSelect(int id);

	public void saveGridEmployeePosition(EmployeePositionMapping stEmployeePositionMapping, Project project) throws Exception;
	
	public ArrayList<EmployeePosition> getEmployeePosition();

}
