package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.ProjectTypeMapping;
import com.softsquare.application.entity.ProjectType;

public interface ProjectTypeService {

	public List<Map<String, Object>> PTSelect();

	public void saveGridProjectType(ProjectTypeMapping stProjectTypeMapping);

	public ArrayList<ProjectType> getProjectType();

	public List<Map<String, Object>> Search(String SearchProjectType);

}
