package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softsquare.application.dao.EmployeePositionDao;
import com.softsquare.application.dao.ProjectDao;
import com.softsquare.application.domain.ProjectMapping;
import com.softsquare.application.entity.EmployeePosition;
import com.softsquare.application.entity.Project;

@Service
@Transactional
public class ProjectServiceImp implements ProjectService {

	@Autowired
	private ProjectDao projectDao;

	@Autowired
	private EmployeePositionDao employeePositionDao;

	@Override
	public Project saveProject(ProjectMapping project) throws Exception {
		Project po = new Project();
		po.setProjectName(project.getProjectName());
		po.setProjectLocation(project.getProjectLocation());
		po.setProjectDateStart(project.getProjectDateStart());
		po.setProjectDateEnd(project.getProjectDateEnd());
		po.setProjectBudget(project.getProjectBudget());
		po.setProjectDescription(project.getProjectDescription());
		po.setProjectTypeID(project.getProjectTypeID());
		po.setBranchID(project.getBranchID());
		
		if (project.getProjectID() == null) {
			projectDao.saveProject(po);
		} else {
			po.setProjectID(project.getProjectID());
			projectDao.updateProject(po);
		}
		return po;
	}

	@Override
	public List<Map<String, Object>> POSelect() {
		List<Map<String, Object>> select = projectDao.POSelect();
		return select;
	}
	
	
	@Override
	public ArrayList<Project> ProjectSelect(ProjectMapping projectMapping) {
		
		return projectDao.ProjectSelect(projectMapping);
	}

	@Override
	public void remove(Integer deletes) {
		List<Project> employeePosition = projectDao.removeProject(deletes);
		Project po = new Project();
		po.setProjectID(employeePosition.get(0).getProjectID());

		List<EmployeePosition> removeEmployeePosition = employeePositionDao.removeEmployeePosition(po.getProjectID());

		int a = removeEmployeePosition.size();
		for (int i = 0; i < a; i++) {
			EmployeePosition ep = new EmployeePosition();
			ep.setEmployeePositionID(removeEmployeePosition.get(i).getEmployeePositionID());
			employeePositionDao.remove(ep);
		}
		Project od = new Project();
		od.setProjectID(deletes);
		projectDao.remove(od);
	}

	@Override
	public ArrayList<Project> getProject() {
		return projectDao.getProject();
	}

	@Override
	public ProjectMapping editProject(int id) {

		ProjectMapping projectMapping = new ProjectMapping();

		List<Project> emp = projectDao.editProject(id);

		// Date Start Convert
		String dateStringStart = emp.get(0).getProjectDateStart();
		String yearStart = dateStringStart.substring(0, 4);
		int yearIntStart = Integer.parseInt(yearStart);
		int yearPStart = yearIntStart;
		String dateStart = dateStringStart.substring(8, 10) + "/" + dateStringStart.substring(5, 7) + "/" + yearPStart;

		// Date End Convert
		String dateStringEnd = emp.get(0).getProjectDateEnd();
		String yearEnd = dateStringEnd.substring(0, 4);
		int yearIntEnd = Integer.parseInt(yearEnd);
		int yearPEnd = yearIntEnd;
		String dateEnd = dateStringEnd.substring(8, 10) + "/" + dateStringEnd.substring(5, 7) + "/" + yearPEnd;

		projectMapping.setProjectID(emp.get(0).getProjectID());
		projectMapping.setProjectName(emp.get(0).getProjectName());
		projectMapping.setProjectLocation(emp.get(0).getProjectLocation());
		projectMapping.setProjectDateStart(dateStart);
		projectMapping.setProjectDateEnd(dateEnd);
		projectMapping.setProjectBudget(emp.get(0).getProjectBudget());
		projectMapping.setProjectDescription(emp.get(0).getProjectDescription());
		projectMapping.setProjectTypeID(emp.get(0).getProjectTypeID());
		projectMapping.setBranchID(emp.get(0).getBranchID());

		return projectMapping;
	}

	@Override
	public List<Map<String, Object>> saveGridEmployeePosition(ProjectMapping stProjectMapping,
			ProjectMapping projectDetail) throws Exception {

		EmployeePosition ep = new EmployeePosition();
		ep.setEmployeePositionID(stProjectMapping.getEmployeePositionID());
		ep.setSkillID(stProjectMapping.getSkillID());
		ep.setProjectID(stProjectMapping.getProjectID());

		Project po = new Project();
		po.setProjectID(projectDetail.getProjectID());
		po.setProjectName(projectDetail.getProjectName());
		po.setProjectLocation(projectDetail.getProjectLocation());
		po.setProjectDateStart(projectDetail.getProjectDateStart());
		po.setProjectDateEnd(projectDetail.getProjectDateEnd());
		po.setProjectBudget(projectDetail.getProjectBudget());
		po.setProjectDescription(projectDetail.getProjectDescription());
		po.setProjectTypeID(projectDetail.getProjectTypeID());
		po.setBranchID(projectDetail.getBranchID());

		if (projectDetail.getProjectID() == null) {
			projectDao.saveProject(po);
		} else {
			po.setProjectID(projectDetail.getProjectID());
			projectDao.updateProject(po);
		}

		if (stProjectMapping.getEmployeePositionID() == null) {
			employeePositionDao.saveGridEmployeePosition(ep);
		} else {
			ep.setEmployeePositionID(stProjectMapping.getEmployeePositionID());
			employeePositionDao.updateGridEmployeePosition(ep);
		}
		return null;
	}

	@Override
	public List<Map<String, Object>> Search(String SearchProject, String SearchProjectType, String SearchBranch) {
		List<Map<String, Object>> select = projectDao.Search(SearchProject, SearchProjectType, SearchBranch);
		return select;
	}

	@Override
	public List<Map<String, Object>> HomeSelect() {
		List<Map<String, Object>> select = projectDao.HomeSelect();
		return select;
	}

	@Override
	public List<Map<String, Object>> HomeSearch(String SearchProject, String SearchProjectType, String SearchBranch) {
		List<Map<String, Object>> select = projectDao.HomeSearch(SearchProject, SearchProjectType, SearchBranch);
		return select;
	}
}
