package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.MaterialTypeMapping;
import com.softsquare.application.entity.MaterialType;

public interface MaterialTypeService {

	public void saveMaterialType(MaterialTypeMapping materialType) throws Exception;

	public List<Map<String, Object>> MATSelect();

	public void saveGridMaterialType(MaterialTypeMapping stMaterialTypeMapping);

	public ArrayList<MaterialType> getMaterialType();

	public List<Map<String, Object>> Search(String SearchMaterialType);

}
