package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.EmployeePositionDao;
import com.softsquare.application.domain.EmployeePositionMapping;
import com.softsquare.application.entity.EmployeePosition;
import com.softsquare.application.entity.Project;

@Service
public class EmployeePositionServiceImp implements EmployeePositionService {

	@Autowired
	private EmployeePositionDao employeePositionDao;

	@Override
	public List<Map<String, Object>> EPSelect(int id) {
		List<Map<String, Object>> select = employeePositionDao.EPSelect(id);
		return select;
	}

	@Override
	public void saveGridEmployeePosition(EmployeePositionMapping stEmployeePositionMapping, Project project) throws Exception {
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stEmployeePositionMapping.getGridStore_jsonCreateRecords()) && BeanUtils.isNotNull(project.getProjectID())){
			EmployeePosition[] arrayRs = gson.fromJson(stEmployeePositionMapping.getGridStore_jsonCreateRecords(), EmployeePosition[].class);
			for (EmployeePosition employeePosition : arrayRs) {
				employeePosition.setProjectID(project.getProjectID());
				employeePositionDao.saveGridEmployeePosition(employeePosition);
			}
		}
		
		if(BeanUtils.isNotEmpty(stEmployeePositionMapping.getGridStore_jsonUpdateRecords()) && BeanUtils.isNotNull(project.getProjectID())){
			EmployeePosition[] arrayRs = gson.fromJson(stEmployeePositionMapping.getGridStore_jsonUpdateRecords(), EmployeePosition[].class);
			for (EmployeePosition employeePosition : arrayRs) {
				employeePosition.setProjectID(project.getProjectID());
				employeePositionDao.updateGridEmployeePosition(employeePosition);
			}
		}	
		
		if(BeanUtils.isNotEmpty(stEmployeePositionMapping.getGridStore_jsonDestroyRecords())){
			EmployeePosition[] arrayRs = gson.fromJson(stEmployeePositionMapping.getGridStore_jsonDestroyRecords(), EmployeePosition[].class);
			for (EmployeePosition employeePosition : arrayRs) {
				employeePositionDao.remove(employeePosition);
			}
		}
	}

	@Override
	public ArrayList<EmployeePosition> getEmployeePosition() {
		return employeePositionDao.getEmployeePosition();
	}
}
