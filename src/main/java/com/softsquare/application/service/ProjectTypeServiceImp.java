package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.ProjectTypeDao;
import com.softsquare.application.domain.ProjectTypeMapping;
import com.softsquare.application.entity.ProjectType;

@Service
public class ProjectTypeServiceImp implements ProjectTypeService {

	@Autowired
	private ProjectTypeDao projectTypeDao;

	@Override
	public List<Map<String, Object>> PTSelect() {
		List<Map<String, Object>> select = projectTypeDao.PTSelect();
		return select;
	}

	@Override
	public void saveGridProjectType(ProjectTypeMapping stProjectTypeMapping) {
		Gson gson = new Gson();
		if (BeanUtils.isNotEmpty(stProjectTypeMapping.getGridStore_jsonCreateRecords())) {
			ProjectType[] arrayRs = gson.fromJson(stProjectTypeMapping.getGridStore_jsonCreateRecords(),
					ProjectType[].class);
			for (ProjectType pt : arrayRs) {
				projectTypeDao.saveGridProjectType(pt);
			}
		}

		if (BeanUtils.isNotEmpty(stProjectTypeMapping.getGridStore_jsonUpdateRecords())) {
			ProjectType[] arrayRs = gson.fromJson(stProjectTypeMapping.getGridStore_jsonUpdateRecords(),
					ProjectType[].class);
			for (ProjectType pt : arrayRs) {
				projectTypeDao.updateGridProjectType(pt);
			}
		}

		if (BeanUtils.isNotEmpty(stProjectTypeMapping.getGridStore_jsonDestroyRecords())) {
			ProjectType[] arrayRs = gson.fromJson(stProjectTypeMapping.getGridStore_jsonDestroyRecords(),
					ProjectType[].class);
			for (ProjectType pt : arrayRs) {
				projectTypeDao.remove(pt);
			}
		}
	}

	@Override
	public ArrayList<ProjectType> getProjectType() {
		return projectTypeDao.getProjectType();
	}

	@Override
	public List<Map<String, Object>> Search(String SearchProjectType) {
		List<Map<String, Object>> select = projectTypeDao.Search(SearchProjectType);
		return select;
	}
}
