package com.softsquare.application.service;

import com.softsquare.application.domain.PaymentMapping;
import com.softsquare.application.entity.Payment;

public interface PaymentService {

	public Payment savePayment(PaymentMapping pm)throws Exception;

}
