package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.BranchMapping;
import com.softsquare.application.entity.Branch;

public interface BranchService {

	public void saveBranch(BranchMapping branchMappping) throws Exception;

	public List<Map<String, Object>> BASelect();

	public void saveGridBranch(BranchMapping stBranchMapping);


	public ArrayList<Branch> getBranch();

	public List<Map<String, Object>> Search(String SearchBranch, String SearchProvince);
}
