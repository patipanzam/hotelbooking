package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.dao.PromotionDao;
import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Promotion;
import com.softsquare.application.entity.Room;

@Service
public class PromotionServiceImp implements PromotionService {
	
	@Autowired
	private PromotionDao promotionDao;

	@Override
	public Promotion savePromotion(PromotionMapping pro) throws Exception {
		Promotion pm = new Promotion();
		pm.setName(pro.getName());
		pm.setDescription(pro.getDescription());
		pm.setFromDate(pro.getFromDate());
		pm.setToDate(pro.getToDate());
		pm.setDate(pro.getDate());
		pm.setRate(pro.getRate());
		pm.setRoom(pro.getRoom());
	    pm.setStatus(pro.getStatus());
		pm.setUdloginname(LoginUtils.getUsername());
		
		
		if (pro.getPromotionID() == null) {
			promotionDao.savePromotion(pm);
		} else {
			pm.setPromotionID(pro.getPromotionID());
			promotionDao.updatePromotion(pm);
		}
		return pm;
	}

	@Override
	public List<Map<String, Object>> LGSelect() {
		List<Map<String, Object>> select = promotionDao.LGSelect();
		return select;
	}

	@Override
	public void remove(Integer deletes) {
		Promotion pro = new Promotion();
		pro.setPromotionID(deletes);
		promotionDao.remove(pro);
		
	}

	@Override
	public List<Map<String, Object>> Search(String SearchRegister, String SearchPositionCombobox) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PromotionMapping editPromotion(int promotionID) {
		
		PromotionMapping promotionMapping = new PromotionMapping();
		List<Promotion> pro = promotionDao.editPromotion(promotionID);
		promotionMapping.setPromotionID(pro.get(0).getPromotionID());
		promotionMapping.setName(pro.get(0).getName());
		promotionMapping.setDescription(pro.get(0).getDescription());
		promotionMapping.setFromDate(pro.get(0).getFromDate());
		promotionMapping.setToDate(pro.get(0).getToDate());
		promotionMapping.setDate(pro.get(0).getDate());
		promotionMapping.setRoom(pro.get(0).getRoom());
		promotionMapping.setRate(pro.get(0).getRate());
		promotionMapping.setStatus(pro.get(0).getStatus());
		promotionMapping.setUdloginname(pro.get(0).getUdloginname());
		
		return promotionMapping;
	}

	@Override
	public ArrayList<Promotion> PromotionSelect(PromotionMapping pro) {
		return promotionDao.PromotionSelect(pro);
	
	}

	@Override
	public ArrayList<Promotion> getPromotion() {
		return promotionDao.getRoomType();
	}

}
