package com.softsquare.application.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softsquare.application.dao.ProvinceDao;
import com.softsquare.application.entity.Province;


@Service
public class ProvinceServiceImp implements ProvinceService{
	
	@Autowired
	private ProvinceDao provinceDao;

	@Override
	public ArrayList<Province> getProvince() {
		return provinceDao.getProvince();
	}
	
}