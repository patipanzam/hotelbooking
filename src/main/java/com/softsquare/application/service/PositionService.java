package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.PositionMapping;
import com.softsquare.application.entity.Position;

public interface PositionService {

	void savePosition(PositionMapping position) throws Exception;

	List<Map<String, Object>> POSelect();

	public void saveGridPosition(PositionMapping stPositionMapping);

	public ArrayList<Position> getPosition(String positionTypeID);

	public List<Map<String, Object>> Search(String SearchPosition, String SearchPositionType);


}
