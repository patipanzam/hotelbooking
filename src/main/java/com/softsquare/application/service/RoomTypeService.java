package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonElement;
import com.softsquare.application.domain.RoomTypeMapping;
import com.softsquare.application.entity.RoomType;

public interface RoomTypeService {

	ArrayList<RoomType> getRoomType();
  
	List<Map<String, Object>> PTSelect();
	
 public  void saveGridRoomType(RoomTypeMapping roomtypeMapping) throws Exception;

	

}
