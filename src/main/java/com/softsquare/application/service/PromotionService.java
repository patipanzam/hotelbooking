package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.entity.Promotion;
import com.softsquare.application.entity.Room;

public interface PromotionService {
	
	public Promotion savePromotion(PromotionMapping pro) throws Exception;

	public List<Map<String,Object>> LGSelect();
	public void remove(Integer deletes);
	public List<Map<String, Object>> Search( String SearchRegister,String SearchPositionCombobox);
	public PromotionMapping editPromotion(int promotionID);
	public ArrayList<Promotion> PromotionSelect(PromotionMapping pro);

	public ArrayList<Promotion> getPromotion();

}
