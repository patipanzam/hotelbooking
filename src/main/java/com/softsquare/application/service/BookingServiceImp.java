package com.softsquare.application.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.common.util.DateUtils;
import com.softsquare.application.dao.BookingDao;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.domain.RoomTypeMapping;
import com.softsquare.application.entity.Booking;
import com.softsquare.application.entity.Room;
import com.softsquare.application.entity.RoomType;

@Service
public class BookingServiceImp implements BookingService{
	
	@Autowired
	private BookingDao bookingDao;

	@Override
public Booking saveBooking(BookingMapping bk) throws Exception {
	      
		Booking book = new Booking();
		book.setBookingID(bk.getBookingID());
		book.setRoomID(bk.getRoomID());
		book.setCheckIN(bk.getCheckIN());
		book.setCheckOUT(bk.getCheckOUT());
		book.setNight(bk.getNight());
		book.setSumPrice(bk.getSumPrice());
		book.setFname(bk.getFname());
		book.setLname(bk.getLname());
		book.setEmail(bk.getEmail());
		book.setPhone(bk.getPhone());
		book.setCountry(bk.getCountry());
//		book.setLarge_bed(bk.getLarge_bed());
//		book.setEarly_check(bk.getEarly_check());
//		book.setHigh_floor(bk.getHigh_floor());
//		book.setLate_ckeckin(bk.getLate_ckeckin());
//		book.setTransfer(bk.getTransfer());
//		book.setTwin_bed(bk.getTwin_bed());
//		book.setComment(bk.getComment());
		
		if (bk.getBookingID() == null) {
			bookingDao.saveBooking(book);
		} else {
			book.setBookingID(bk.getBookingID());
			bookingDao.updateBooking(book);
		}
		return book;
	}

	@Override
	public List<Map<String, Object>> PTSelect() {
		
		List<Map<String,Object>> select = bookingDao.PTSelect();
		return select;
	}

	@Override
	public void saveGridBooking(BookingMapping bookingMapping) throws Exception {
		       
		         Gson gson = new Gson();
			if(BeanUtils.isNotEmpty(bookingMapping.getGridStore_jsonCreateRecords())){
				Booking[] arrayRs = gson.fromJson(bookingMapping.getGridStore_jsonCreateRecords(), Booking[].class);
				for (Booking pt : arrayRs) {
					pt.setBookingID(bookingMapping.getBookingID());
					pt.setCheckIN(bookingMapping.getCheckIN());
					pt.setCheckOUT(bookingMapping.getCheckOUT());
					pt.setLname(bookingMapping.getLname());
					pt.setFname(bookingMapping.getFname());
					pt.setEmail(bookingMapping.getEmail());
					pt.setPhone(bookingMapping.getPhone());
					pt.setNight(bookingMapping.getNight());
//					pt.setCreateUser(bookingMapping.getCreateUser());
					pt.setStatus("haven't paid");
					pt.setCancelStatus("Booking");
					
				bookingDao.bookingMapping( pt);
				}
			}
			
			if(BeanUtils.isNotEmpty(bookingMapping.getGridStore_jsonUpdateRecords())){
				Booking[] arrayRs = gson.fromJson(bookingMapping.getGridStore_jsonUpdateRecords(), Booking[].class);
				for (Booking pt : arrayRs) {
					pt.setCreateUser("james");
					pt.setBookingID(bookingMapping.getBookingID());
					pt.setCheckIN(bookingMapping.getCheckIN());
					pt.setCheckOUT(bookingMapping.getCheckOUT());
					pt.setLname(bookingMapping.getLname());
					pt.setFname(bookingMapping.getFname());
					pt.setEmail(bookingMapping.getEmail());
					pt.setPhone(bookingMapping.getPhone());
					pt.setNight(bookingMapping.getNight());
//					pt.setCreateUser(bookingMapping.getCreateUser());
					pt.setStatus("haven't paid");
					pt.setCancelStatus("Booking");
				bookingDao.updateGridRoomType(pt);
				}
			}
			
			if(BeanUtils.isNotEmpty(bookingMapping.getGridStore_jsonDestroyRecords())){
				Booking[] arrayRs = gson.fromJson(bookingMapping.getGridStore_jsonDestroyRecords(), Booking[].class);
				for (Booking pt : arrayRs) {
					bookingDao.remove(pt);
				}
			}
	
		}

	@Override
	public void remove(Integer deletes) {
		Booking rmd = new Booking();
		rmd.setBookingID(deletes);
		bookingDao.bookremove(rmd);
		
	}

	@Override
	public List<Map<String, Object>> Search(String searchRegister, String searchPositionCombobox) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> LGSelect() {
		List<Map<String, Object>> select = bookingDao.LGSelect();
		return select;
	}

	@Override
	public void saveGridBooking1(BookingMapping bookingMapping) {
		// TODO Auto-generated method stub
		
	}


//////////////////////////////////////+++++++++++++++++++Cancel Booking+++++++++++++++++++/////////////////////////////////////////	
	@Override
	public void updategrid(BookingMapping[] bookinggrid) {
		for (BookingMapping bookingMapping : bookinggrid) {
			Booking book = new Booking();
			book.setBookingID(bookingMapping.getBookingID());
			book.setRoomID(bookingMapping.getRoomID());
			book.setCheckIN(bookingMapping.getCheckIN());
			book.setCheckOUT(bookingMapping.getCheckOUT());
			book.setNight(bookingMapping.getNight());
			book.setSumPrice(bookingMapping.getSumPrice());
			book.setFname(bookingMapping.getFname());
			book.setLname(bookingMapping.getLname());
			book.setEmail(bookingMapping.getEmail());
			book.setPhone(bookingMapping.getPhone());
			book.setCountry(bookingMapping.getCountry());
			book.setCancelStatus(bookingMapping.getCancelStatus());
			book.setStatus(bookingMapping.getStatus());
			book.setRooms(bookingMapping.getRooms());
		book.setUpdateUser("System");
		bookingDao.updategrid(book);
		}	
	}	
	
	@Override
	public List<Map<String, Object>> BookingSearch(BookingMapping bookingMap) throws NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return bookingDao.BookingSearch(bookingMap);
	}
////////////////////////////////////////+++++++Check Cancel Booking++++++++/////////////////////////////////////////

@Override
public void saveGridCheckCancel(BookingMapping bookingMapping) throws Exception {
Gson gson = new Gson();
if(BeanUtils.isNotEmpty(bookingMapping.getGridStore_jsonCreateRecords())){
Booking[] array1 = gson.fromJson(bookingMapping.getGridStore_jsonCreateRecords(), Booking[].class);
for (Booking bk : array1) {
bookingDao.bookingMapping(bk);
}
}

if(BeanUtils.isNotEmpty(bookingMapping.getGridStore_jsonUpdateRecords())){
Booking[] array1 = gson.fromJson(bookingMapping.getGridStore_jsonUpdateRecords(), Booking[].class);
for (Booking bk : array1) {
bookingDao.updateGridCheckCancel(bk);
}
}

if(BeanUtils.isNotEmpty(bookingMapping.getGridStore_jsonDestroyRecords())){
Booking[] array1 = gson.fromJson(bookingMapping.getGridStore_jsonDestroyRecords(), Booking[].class);
for (Booking bk : array1) {
bookingDao.remove(bk);
}
}
}

@Override
public ArrayList<Booking> getBooking() {

return  bookingDao.getBooking();
}	

@Override
public List<Map<String, Object>> CheckCancelSelect() {
List<Map<String,Object>> select = bookingDao.CheckCancelSelect();
return select;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

