package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.BuildingDao;
import com.softsquare.application.dao.EmployeeDao;
import com.softsquare.application.dao.SkillDao;
import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.entity.Building;
import com.softsquare.application.entity.Employee;
import com.softsquare.application.entity.PositionType;
import com.softsquare.application.entity.Skill;

@Service
public class BuildingServiceImp implements BuildingService {

	@Autowired
	private BuildingDao buildingDao;

	@Override
	public Building saveBuilding(BuildingMapping Building) throws Exception {
		Building em = new Building();
		em.setBuildingID(Building.getBuildingID());
		em.setBuildingName(Building.getBuildingName());
//		em.setBuildingCode(Building.getBuildingCode());
	

		if (Building.getBuildingID() == null) {
			buildingDao.saveBuilding(em);
		} else {
			em.setBuildingID(Building.getBuildingID());
			buildingDao.updateBuilding(em);
		}
	return em;
	}

	@Override
	public List<Map<String, Object>> EMSelect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(Integer deletes) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Building> getBuilding() {
		return buildingDao.getBuilding();
	}

	@Override
	public BuildingMapping editBuilding(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> Search(String SearchBuilding) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Building> BuildingSelect(BuildingMapping BuildingMapping) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> PTSelect() {
		List<Map<String,Object>> select = buildingDao.PTSelect();
		return select;
	}

	

	@Override
	public void saveGridBuilding(BuildingMapping stPositionTypeMapping) {
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stPositionTypeMapping.getGridStore_jsonCreateRecords())){
			Building[] arrayRs = gson.fromJson(stPositionTypeMapping.getGridStore_jsonCreateRecords(), Building[].class);
			for (Building pt : arrayRs) {
				buildingDao.saveGridBuilding( pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(stPositionTypeMapping.getGridStore_jsonUpdateRecords())){
			Building[] arrayRs = gson.fromJson(stPositionTypeMapping.getGridStore_jsonUpdateRecords(), Building[].class);
			for (Building pt : arrayRs) {
				buildingDao.updateGridBuilding(pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(stPositionTypeMapping.getGridStore_jsonDestroyRecords())){
			Building[] arrayRs = gson.fromJson(stPositionTypeMapping.getGridStore_jsonDestroyRecords(), Building[].class);
			for (Building pt : arrayRs) {
				buildingDao.remove(pt);
			}
		}
	}
		
	}

	


