package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.MaterialTypeDao;
import com.softsquare.application.domain.MaterialTypeMapping;
import com.softsquare.application.entity.MaterialType;

@Service
public class MaterialTypeServiceImp implements MaterialTypeService {

	@Autowired
	private MaterialTypeDao materialTypeDao;

	@Override
	public void saveMaterialType(MaterialTypeMapping materialType) throws Exception {
		MaterialType mat = new MaterialType();
		mat.setMaterialTypeName(materialType.getMaterialTypeName());
		materialTypeDao.saveMaterialType(mat);

	}

	@Override
	public List<Map<String, Object>> MATSelect() {
		List<Map<String, Object>> select = materialTypeDao.MATSelect();
		return select;
	}

	@Override
	public void saveGridMaterialType(MaterialTypeMapping stMaterialTypeMapping) {

		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stMaterialTypeMapping.getGridStore_jsonCreateRecords())){
			MaterialType[] arrayRs = gson.fromJson(stMaterialTypeMapping.getGridStore_jsonCreateRecords(), MaterialType[].class);
			for (MaterialType mt : arrayRs) {
				materialTypeDao.saveGridMaterialType(mt);
			}
		}
		
		if(BeanUtils.isNotEmpty(stMaterialTypeMapping.getGridStore_jsonUpdateRecords())){
			MaterialType[] arrayRs = gson.fromJson(stMaterialTypeMapping.getGridStore_jsonUpdateRecords(), MaterialType[].class);
			for (MaterialType mt : arrayRs) {
				materialTypeDao.updateGridMaterialType(mt);
			}
		}
		
		if(BeanUtils.isNotEmpty(stMaterialTypeMapping.getGridStore_jsonDestroyRecords())){
			MaterialType[] arrayRs = gson.fromJson(stMaterialTypeMapping.getGridStore_jsonDestroyRecords(), MaterialType[].class);
			for (MaterialType mt : arrayRs) {
				materialTypeDao.remove(mt);
			}
		}
	}

	@Override
	public ArrayList<MaterialType> getMaterialType() {
		return materialTypeDao.getMaterialType();
	}

	@Override
	public List<Map<String, Object>> Search(String SearchMaterialType) {
		List<Map<String, Object>> select = materialTypeDao.Search(SearchMaterialType);
		return select;
	}
}
