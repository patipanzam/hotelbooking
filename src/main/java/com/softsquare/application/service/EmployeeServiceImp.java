package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softsquare.application.dao.EmployeeDao;
import com.softsquare.application.dao.SkillDao;
import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.entity.Employee;
import com.softsquare.application.entity.Skill;

@Service
public class EmployeeServiceImp implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private SkillDao skillDao;

	@Override
	public Employee saveEmployee(EmployeeMapping employee) throws Exception {
		Employee em = new Employee();
			em.setEmployeeName(employee.getEmployeeName());
			em.setEmployeeAddress(employee.getEmployeeAddress());
			em.setEmployeePhone(employee.getEmployeePhone());
			em.setEmployeeCitizenID(employee.getEmployeeCitizenID());

			if (employee.getEmployeeID() == null) {
				employeeDao.saveEmployee(em);
			} else {
				em.setEmployeeID(employee.getEmployeeID());
				employeeDao.updateEmployee(em);
			}
		return em;
	}

	@Override
	public List<Map<String, Object>> EMSelect() {

		List<Map<String, Object>> select = employeeDao.EMSelect();
		return select;
	}
	
	@Override
	public ArrayList<Employee> EmployeeSelect(EmployeeMapping employeeMapping) {
		return employeeDao.EmployeeSelect(employeeMapping);
	}

	@Override
	public void remove(Integer deletes) {
		List<Employee> skill = employeeDao.removeEmployee(deletes);
		Employee emp = new Employee();
		emp.setEmployeeID(skill.get(0).getEmployeeID());

		List<Skill> removeskill = skillDao.removeSkill(emp.getEmployeeID());

		int a = removeskill.size();
		for (int i = 0; i < a; i++) {
			Skill ski = new Skill();
			ski.setSkillID(removeskill.get(i).getSkillID());
			skillDao.remove(ski);

		}

		Employee od = new Employee();
		od.setEmployeeID(deletes);
		employeeDao.remove(od);

	}

	@Override
	public ArrayList<Employee> getEmployee() {
		return employeeDao.getEmployee();
	}

	@Override
	public EmployeeMapping editEmployee(int id) {

		EmployeeMapping employeeMapping = new EmployeeMapping();

		List<Employee> emp = employeeDao.editEmployee(id);

		employeeMapping.setEmployeeID(emp.get(0).getEmployeeID());
		employeeMapping.setEmployeeName(emp.get(0).getEmployeeName());
		employeeMapping.setEmployeeAddress(emp.get(0).getEmployeeAddress());
		employeeMapping.setEmployeePhone(emp.get(0).getEmployeePhone());
		employeeMapping.setEmployeeCitizenID(emp.get(0).getEmployeeCitizenID());

		return employeeMapping;
	}

	@Override
	public List<Map<String, Object>> Search(String SearchEmployee) {
		List<Map<String, Object>> select = employeeDao.Search(SearchEmployee);
		return select;
	}

}
