package com.softsquare.application.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.dao.RoomDao;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Room;
import com.sun.org.apache.xml.internal.security.utils.Base64;

@SuppressWarnings("restriction")
@Service
public class RoomServiceImp implements RoomService{
	
	@Autowired
	private RoomDao roomDao;

	@Override
	public Room saveRoom(RoomMapping room) throws Exception {
		
		Room rooms = new Room();
		rooms.setRoomTypeName(room.getRoomTypeName());
		rooms.setBuildingID(room.getBuildingID());
		rooms.setRoomBeds(room.getBeds());
		rooms.setRoomDate(room.getRoomDate());
		rooms.setRoomPrice(room.getRoomPrice().toString());
		rooms.setFreewifi(room.getFreewifi());
		rooms.setSmocking(room.getSmocking());
		rooms.setService(room.getService());
		rooms.setBreaks(room.getBreaks());
		rooms.setRoomPrice(room.getRoomPrice().toString());
		rooms.setRoomNo(room.getRoomNo());
		rooms.setTypeDetail(room.getTypeDetail());
		rooms.setUdloginname(LoginUtils.getUsername());
		rooms.setCapacity(room.getCapacity());
		byte[] imageDataBytes;
		imageDataBytes = Base64.decode(room.getImage());
		rooms.setImage(imageDataBytes);
		
		if (room.getRoomID() == null) {
			roomDao.saveRoom(rooms);
		} else {
			rooms.setRoomID(room.getRoomID());
			roomDao.updateRoom(rooms);
		}
		return rooms;
	}

	@Override
	public List<Map<String, Object>> LGSelect() {
		List<Map<String, Object>> select = roomDao.LGSelect();
		return select;
	}

	@Override
	public void remove(Integer deletes) {
		Room rm = new Room();
		rm.setRoomID(deletes);
		roomDao.remove(rm);
	}

	@Override
	public List<Map<String, Object>> Search(String SearchRegister, String SearchPositionCombobox) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RoomMapping editRoom(int roomID) {
		RoomMapping roomMapping = new RoomMapping();

		List<Room> rm = roomDao.editRoom(roomID);
		roomMapping.setRoomID(rm.get(0).getRoomID());
		roomMapping.setBuildingID(rm.get(0).getBuildingID());
		roomMapping.setRoomTypeName(rm.get(0).getRoomTypeName());
		roomMapping.setBeds(rm.get(0).getRoomBeds());
		roomMapping.setFreewifi(rm.get(0).getFreewifi());
		roomMapping.setSmocking(rm.get(0).getSmocking());
		roomMapping.setBreaks(rm.get(0).getBreaks());
//		String dateString = rm.get(0).getRoomDate();
//		String year = dateString.substring(0, 4);
//		int yearInt = Integer.parseInt(year);
//		int yearP = yearInt + 543;
//		String newDate = dateString.substring(8, 10) + "/"
//				+ dateString.substring(5, 7) + "/" + yearP;
//		roomMapping.setRoomDate(newDate);		
		roomMapping.setRoomDate(rm.get(0).getRoomDate());
		roomMapping.setRoomPrice(rm.get(0).getRoomPrice());
		roomMapping.setRoomNo(rm.get(0).getRoomNo());
		roomMapping.setTypeDetail(rm.get(0).getTypeDetail());
		System.out.println(rm.get(0).getImage()+"zzzzzzzzzzzzzzzzzzzzzz");
		roomMapping.setImage(DatatypeConverter.printBase64Binary(rm.get(0).getImage()));
		System.out.println(DatatypeConverter.printBase64Binary(rm.get(0).getImage())+"999999999999999");
		
		roomMapping.setUdloginname(rm.get(0).getUdloginname());
		
		return roomMapping;
	}

	@Override
	public RoomMapping RoomSelect(RoomMapping roomMapping) {
		RoomMapping rooms = roomDao.editRoom1(roomMapping);
		BigDecimal rateBigDe = new BigDecimal(rooms.getRate());
		BigDecimal percentFull = new BigDecimal(100);
		BigDecimal priceBigDe = new BigDecimal(rooms.getPrice());
		BigDecimal percentCal =  priceBigDe.multiply(rateBigDe.divide(percentFull, 2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal finalPrice = priceBigDe.subtract(percentCal);
		rooms.setRoomPrice(priceBigDe.toString());
		rooms.setSumPrice(finalPrice.toString());
		rooms.setRate(rateBigDe.toString());
		rooms.setImage(DatatypeConverter.printBase64Binary(rooms.getImageByte()));
		return rooms;
	}

	@Override
	public List<Map<String, Object>> room(RoomMapping room) {
		// TODO Auto-generated method stub
		return roomDao.roomSearch(room);
	}

	@Override
	public RoomMapping RoomSelect1(RoomMapping room) {
		
		RoomMapping rm = new RoomMapping();
		
		List<Room> rooms = roomDao.RoomSelect1(room);
		rm.setRoomID(rooms.get(0).getRoomID());
		rm.setBuildingID(rooms.get(0).getBuildingID());
		rm.setRoomTypeName(rooms.get(0).getRoomTypeName());
		rm.setBeds(rooms.get(0).getRoomBeds());
		rm.setFreewifi(rooms.get(0).getFreewifi());
		rm.setSmocking(rooms.get(0).getSmocking());
		rm.setBreaks(rooms.get(0).getBreaks());
//		String dateString = rm.get(0).getRoomDate();
//		String year = dateString.substring(0, 4);
//		int yearInt = Integer.parseInt(year);
//		int yearP = yearInt + 543;
//		String newDate = dateString.substring(8, 10) + "/"
//				+ dateString.substring(5, 7) + "/" + yearP;
//		roomMapping.setRoomDate(newDate);		
		rm.setRoomDate(rooms.get(0).getRoomDate());
		rm.setRoomPrice(rooms.get(0).getRoomPrice());
		rm.setRoomNo(rooms.get(0).getRoomNo());
		rm.setCapacity(rooms.get(0).getCapacity());
		rm.setTypeDetail(rooms.get(0).getTypeDetail());
		rm.setImage(DatatypeConverter.printBase64Binary(rooms.get(0).getImage()));
		
		rm.setUdloginname(rooms.get(0).getUdloginname());
		
		return rm;
	}

	@Override
	public RoomMapping RoomSelectBk(RoomMapping room) {
             
		RoomMapping rm = new RoomMapping();
		List<Room> rooms = roomDao.RoomSelectBk(room);
		rm.setRoomID(rooms.get(0).getRoomID());
		rm.setBuildingID(rooms.get(0).getBuildingID());
		rm.setRoomTypeName(rooms.get(0).getRoomTypeName());
		rm.setBeds(rooms.get(0).getRoomBeds());
		rm.setFreewifi(rooms.get(0).getFreewifi());
		rm.setSmocking(rooms.get(0).getSmocking());
		rm.setBreaks(rooms.get(0).getBreaks());
		rm.setRoomDate(rooms.get(0).getRoomDate());
		rm.setRoomPrice(rooms.get(0).getRoomPrice());
		rm.setRoomNo(rooms.get(0).getRoomNo());
		rm.setCapacity(rooms.get(0).getCapacity());
		rm.setTypeDetail(rooms.get(0).getTypeDetail());
		rm.setImage(DatatypeConverter.printBase64Binary(rooms.get(0).getImage()));
		
		rm.setUdloginname(rooms.get(0).getUdloginname());
		
		return rm;
	}

	@Override
	public List<Map<String, Object>> LGSelectUs() {
		List<Map<String, Object>> select = roomDao.LGSelectUs();
		return select;
	}
	
}
