package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.MaterialDao;
import com.softsquare.application.domain.MaterialMapping;
import com.softsquare.application.entity.Material;

@Service
public class MaterialServiceImp implements MaterialService {

	
	@Autowired
	private MaterialDao materialDao;
	
	@Override
	public void saveMaterial(MaterialMapping material)throws Exception {
		Material ma = new Material();
		ma.setMaterialName(material.getMaterialName());
		ma.setMaterialTypeID(material.getMaterialTypeID());
		ma.setMaterialUnit(material.getMaterialUnit());
		materialDao.saveMaterial(ma);	
	}
	
	@Override
	public List<Map<String,Object>> MASelect() {
		List<Map<String,Object>> select = materialDao.MASelect();
		return select;
	}

	@Override
	public void saveGridMaterial(MaterialMapping stMaterialMapping) {
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stMaterialMapping.getGridStore_jsonCreateRecords())){
			Material[] arrayRs = gson.fromJson(stMaterialMapping.getGridStore_jsonCreateRecords(), Material[].class);
			for (Material ma : arrayRs) {
				materialDao.saveGridMaterial(ma);
			}
		}
		
		if(BeanUtils.isNotEmpty(stMaterialMapping.getGridStore_jsonUpdateRecords())){
			Material[] arrayRs = gson.fromJson(stMaterialMapping.getGridStore_jsonUpdateRecords(), Material[].class);
			for (Material ma : arrayRs) {
				materialDao.updateGridMaterial(ma);
			}
		}
		
		if(BeanUtils.isNotEmpty(stMaterialMapping.getGridStore_jsonDestroyRecords())){
			Material[] arrayRs = gson.fromJson(stMaterialMapping.getGridStore_jsonDestroyRecords(), Material[].class);
			for (Material ma : arrayRs) {
				materialDao.remove(ma);
			}
		}
	}

	@Override
	public ArrayList<Material> getMaterial() {
		return materialDao.getMaterial();
	}

	@Override
	public List<Map<String, Object>> Search(String SearchMaterial, String SearchMaterialType) {
		List<Map<String, Object>> select = materialDao.Search(SearchMaterial, SearchMaterialType);
		return select;
	}
}
