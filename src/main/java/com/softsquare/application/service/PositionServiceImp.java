package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.PositionDao;
import com.softsquare.application.domain.PositionMapping;
import com.softsquare.application.entity.Position;

@Service
public class PositionServiceImp implements PositionService {

	
	@Autowired
	private PositionDao positionDao;
	
	@Override
	public void savePosition(PositionMapping position)throws Exception {
		Position po = new Position();
		po.setPositionName(position.getPositionName());
		po.setPositionTypeID(position.getPositionTypeID());
		positionDao.savePosition(po);	
	}
	
	@Override
	public List<Map<String,Object>> POSelect() {
		return positionDao.POSelect();
	}
	
	@Override
	public void saveGridPosition(PositionMapping stPositionMapping) {
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stPositionMapping.getGridStore_jsonCreateRecords())){
			Position[] arrayRs = gson.fromJson(stPositionMapping.getGridStore_jsonCreateRecords(), Position[].class);
			for (Position po : arrayRs) {
				positionDao.saveGridPosition(po);
			}
		}
		
		if(BeanUtils.isNotEmpty(stPositionMapping.getGridStore_jsonUpdateRecords())){
			Position[] arrayRs = gson.fromJson(stPositionMapping.getGridStore_jsonUpdateRecords(), Position[].class);
			for (Position po : arrayRs) {
				positionDao.updateGridPosition(po);
			}
		}
		
		if(BeanUtils.isNotEmpty(stPositionMapping.getGridStore_jsonDestroyRecords())){
			Position[] arrayRs = gson.fromJson(stPositionMapping.getGridStore_jsonDestroyRecords(), Position[].class);
			for (Position po : arrayRs) {
				positionDao.remove(po);
			}
		}
	}

	@Override
	public ArrayList<Position> getPosition(String positionTypeID) {
		return positionDao.getPosition(positionTypeID);
	}
	
	@Override
	public List<Map<String, Object>> Search(String SearchPosition, String SearchPositionType) {
		List<Map<String, Object>> select = positionDao.Search(SearchPosition, SearchPositionType);
		return select;
	}
}
