package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.RoomTypeDao;
import com.softsquare.application.domain.RoomTypeMapping;
import com.softsquare.application.entity.Building;
import com.softsquare.application.entity.RoomType;

@Service
public class RoomTypeServiceImp implements RoomTypeService {
	
	@Autowired
	private RoomTypeDao roomtypeDao;

	@Override
	public ArrayList<RoomType> getRoomType() {
		return roomtypeDao.getRoomType();
	
	}

	@Override
	public List<Map<String, Object>> PTSelect() {
		List<Map<String,Object>> select = roomtypeDao.PTSelect();
		return select;
	}

	@Override
	public void saveGridRoomType(RoomTypeMapping roomtypeMapping) throws Exception {
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(roomtypeMapping.getGridStore_jsonCreateRecords())){
			RoomType[] arrayRs = gson.fromJson(roomtypeMapping.getGridStore_jsonCreateRecords(), RoomType[].class);
			for (RoomType pt : arrayRs) {
				pt.setUdloginname(roomtypeMapping.getUdloginname());
				roomtypeDao.saveGridRoomType( pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(roomtypeMapping.getGridStore_jsonUpdateRecords())){
			RoomType[] arrayRs = gson.fromJson(roomtypeMapping.getGridStore_jsonUpdateRecords(), RoomType[].class);
			for (RoomType pt : arrayRs) {
				pt.setUdloginname(roomtypeMapping.getUdloginname());
				roomtypeDao.updateGridRoomType(pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(roomtypeMapping.getGridStore_jsonDestroyRecords())){
			RoomType[] arrayRs = gson.fromJson(roomtypeMapping.getGridStore_jsonDestroyRecords(), RoomType[].class);
			for (RoomType pt : arrayRs) {
				roomtypeDao.remove(pt);
			}
		}
	}


	

}
