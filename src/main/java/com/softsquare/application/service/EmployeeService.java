package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.entity.Employee;

public interface EmployeeService {

	public Employee saveEmployee(EmployeeMapping employee) throws Exception;

	public List<Map<String, Object>> EMSelect();

	public void remove(Integer deletes);

	public ArrayList<Employee> getEmployee();

	public EmployeeMapping editEmployee(int id);

	public List<Map<String, Object>> Search(String SearchEmployee);

	public ArrayList<Employee> EmployeeSelect(EmployeeMapping employeeMapping);

}
