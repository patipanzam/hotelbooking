package com.softsquare.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softsquare.application.dao.PaymentDao;
import com.softsquare.application.domain.PaymentMapping;
import com.softsquare.application.entity.Payment;
import com.softsquare.application.entity.Promotion;

@Service
public class PaymentServiceImp implements PaymentService{
	
	@Autowired
	PaymentDao paymentDao;

	@Override
	public Payment savePayment(PaymentMapping pm) throws Exception {
		
		Payment pmt = new Payment();
		pmt.setBank(pm.getBank());
		pmt.setBookingID(pm.getBookingID());
		pmt.setSumPrice(pm.getSumPrice());
		pmt.setTime(pm.getTime());
		
		
		
		if (pm.getPaymentID() == null) {
			paymentDao.savePayment(pmt);
		} else {
			pmt.setPaymentID(pm.getPaymentID());
			paymentDao.updatePayment(pmt);
		}
		return pmt;

		
		
	}

}
