package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.SkillDao;
import com.softsquare.application.domain.SkillMapping;
import com.softsquare.application.entity.Employee;
import com.softsquare.application.entity.Skill;

@Service
public class SkillServiceImp implements SkillService {

	@Autowired
	private SkillDao skillDao;

	@Override
	public List<Map<String, Object>> SKSelect(int id) {
		List<Map<String, Object>> select = skillDao.SKSelect(id);
		return select;
	}
	
	@Override
	public List<Map<String, Object>> RegisterSelect() {
		List<Map<String, Object>> select = skillDao.RegisterSelect();
		return select;
	}

	@Override
	public void saveGridSkill(SkillMapping stSkillMapping, Employee employee) throws Exception {
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stSkillMapping.getGridStore_jsonCreateRecords()) && BeanUtils.isNotNull(employee.getEmployeeID())){
			Skill[] arrayRs = gson.fromJson(stSkillMapping.getGridStore_jsonCreateRecords(), Skill[].class);
			for (Skill skill : arrayRs) {
				skill.setEmployeeID(employee.getEmployeeID());
				skillDao.saveGridSkill(skill);
			}
		}
		
		if(BeanUtils.isNotEmpty(stSkillMapping.getGridStore_jsonUpdateRecords()) && BeanUtils.isNotNull(employee.getEmployeeID())){
			Skill[] arrayRs = gson.fromJson(stSkillMapping.getGridStore_jsonUpdateRecords(), Skill[].class);
			for (Skill skill : arrayRs) {
				skill.setEmployeeID(employee.getEmployeeID());
				skillDao.updateGridSkill(skill);
			}
		}	
		
		if(BeanUtils.isNotEmpty(stSkillMapping.getGridStore_jsonDestroyRecords())){
			Skill[] arrayRs = gson.fromJson(stSkillMapping.getGridStore_jsonDestroyRecords(), Skill[].class);
			for (Skill skill : arrayRs) {
				skillDao.remove(skill);
			}
		}
	}

	@Override
	public ArrayList<Skill> getSkill(String positionID, String EmployeeName) {
		return skillDao.getSkill(positionID , EmployeeName);
	}

	@Override
	public List<Map<String, Object>> SearchRegister( String SearchRegister,String SearchPositionCombobox) {
		List<Map<String, Object>> select = skillDao.SearchRegister(SearchRegister , SearchPositionCombobox);
		return select;
	}
}
