package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.OptionMapping;
import com.softsquare.application.entity.Option;

public interface OptionService {
	
	ArrayList<Option> getOption();
	  
	List<Map<String, Object>> PTSelect();
	
   public void saveGridOption(OptionMapping optionMapping) throws Exception;

}
