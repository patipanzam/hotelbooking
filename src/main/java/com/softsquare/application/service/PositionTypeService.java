package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.PositionTypeMapping;
import com.softsquare.application.entity.PositionType;

public interface PositionTypeService {

	public void savePositionType(PositionTypeMapping positionType) throws Exception;

	List<Map<String, Object>> PTSelect();

	public void saveGridPositionType(PositionTypeMapping stPositionTypeMapping);

	public ArrayList<PositionType> getPositionType();

	public List<Map<String, Object>> Search(String SearchPositionType);

}
