package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonElement;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Login;
import com.softsquare.application.entity.Room;

public interface RoomService {

	public Room saveRoom(RoomMapping room) throws Exception;

	public List<Map<String,Object>> LGSelect();
	public void remove(Integer deletes);
	public List<Map<String, Object>> Search( String SearchRegister,String SearchPositionCombobox);
	public RoomMapping editRoom(int roomID);
	public RoomMapping RoomSelect(RoomMapping roomMapping);

	public List<Map<String, Object>> room(RoomMapping room);

	public RoomMapping RoomSelect1(RoomMapping room);

	public RoomMapping RoomSelectBk(RoomMapping room);

	public List<Map<String,Object>> LGSelectUs();

}
