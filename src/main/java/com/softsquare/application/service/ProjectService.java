package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.ProjectMapping;
import com.softsquare.application.entity.Project;

public interface ProjectService {

	public Project saveProject(ProjectMapping project) throws Exception;

	public List<Map<String, Object>> POSelect();

	public void remove(Integer deletes);

	public ArrayList<Project> getProject();

	public ProjectMapping editProject(int id);

	public List<Map<String, Object>> saveGridEmployeePosition(ProjectMapping stProjectMapping,ProjectMapping projectDetail) throws Exception;

	public List<Map<String, Object>> Search(String SearchProject, String SearchProjectType, String SearchBranch);
	
	public List<Map<String, Object>> HomeSelect();
	
	public List<Map<String, Object>> HomeSearch(String SearchProject, String SearchProjectType, String SearchBranch);

	public  ArrayList<Project> ProjectSelect(ProjectMapping projectMapping);

}
