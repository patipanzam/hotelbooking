package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.SkillMapping;
import com.softsquare.application.entity.Employee;
import com.softsquare.application.entity.Skill;

public interface SkillService {
	
	public List<Map<String, Object>> SKSelect(int id);

	public void saveGridSkill(SkillMapping stSkillMapping, Employee employee) throws Exception;
	
	public ArrayList<Skill> getSkill(String positionID, String EmployeeName);

	public List<Map<String, Object>> RegisterSelect();
	
	public List<Map<String, Object>> SearchRegister( String SearchRegister,String SearchPositionCombobox);

}
