package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.OptionDao;
import com.softsquare.application.domain.OptionMapping;
import com.softsquare.application.entity.Option;

@Service
public class OptionServiceImp implements OptionService {
	
	@Autowired
	private OptionDao optionDao;

	@Override
	public ArrayList<Option> getOption() {
		
		return  optionDao.getOption();
	}

	@Override
	public List<Map<String, Object>> PTSelect() {
		List<Map<String,Object>> select = optionDao.PTSelect();
		return select;
	}

	@Override
	public void saveGridOption(OptionMapping optionMapping) throws Exception {
		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(optionMapping.getGridStore_jsonCreateRecords())){
			Option[] arrayRs = gson.fromJson(optionMapping.getGridStore_jsonCreateRecords(), Option[].class);
			for (Option pt : arrayRs) {
				pt.setUdloginname(optionMapping.getUdloginname());
				optionDao.saveGridOption( pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(optionMapping.getGridStore_jsonUpdateRecords())){
			Option[] arrayRs = gson.fromJson(optionMapping.getGridStore_jsonUpdateRecords(), Option[].class);
			for (Option pt : arrayRs) {
				
				pt.setUdloginname(optionMapping.getUdloginname());
				optionDao.updateGridOption(pt);
			}
		}
		
		if(BeanUtils.isNotEmpty(optionMapping.getGridStore_jsonDestroyRecords())){
			Option[] arrayRs = gson.fromJson(optionMapping.getGridStore_jsonDestroyRecords(), Option[].class);
			for (Option pt : arrayRs) {
				optionDao.remove(pt);
			}
		}
	}
	}

	


