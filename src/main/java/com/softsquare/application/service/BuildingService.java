package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonElement;
import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.entity.Building;

public interface BuildingService {

	public Building saveBuilding(BuildingMapping buildingMapping) throws Exception;

	public List<Map<String, Object>> EMSelect();

	public void remove(Integer deletes);

	public ArrayList<Building> getBuilding();

	public BuildingMapping editBuilding(int id);

	public List<Map<String, Object>> Search(String SearchBuilding);

	public ArrayList<Building> BuildingSelect(BuildingMapping BuildingMapping);

	List<Map<String, Object>> PTSelect();

	public void saveGridBuilding(BuildingMapping stPositionTypeMapping);
	
	

	

	



}
