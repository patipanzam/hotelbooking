package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.dao.LoginDao;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.entity.Login;
import com.softsquare.application.entity.Promotion;

@Service
public class LoginServiceImp implements LoginService {

	protected final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private LoginDao loginDao;

	@Override
	public LoginMapping getUser(String userName) {
		List<Map<String, Object>> login = loginDao.findByUsername(userName);
		LoginMapping loginMapping = new LoginMapping();
		loginMapping.setUsername(login.get(0).get("username").toString());
		loginMapping.setPassword(new BCryptPasswordEncoder().encode(login.get(0).get("password").toString()));
		loginMapping.setRole(login.get(0).get("roleCode").toString());
//		loginMapping.setFirstName(login.get(0).get("firstname").toString());
//		loginMapping.setLastName(login.get(0).get("lastname").toString());
//		loginMapping.setEmail(login.get(0).get("email").toString());
//		loginMapping.setPhone(login.get(0).get("phone").toString());
		
		return loginMapping;
	}

	@Override
	public Login saveUser(LoginMapping user) throws Exception {

		Login login = new Login();
		login.setUsername(user.getUsername());
		login.setPassword(user.getPassword());
		login.setRoleId(user.getRoleId());
//		login.setEmployeeID(user.getEmployeeID());
		login.setFirstName(user.getFirstName());
		login.setLastName(user.getLastName());
		login.setEmail(user.getEmail());
		login.setPhone(user.getPhone());
		

		if (user.getId() == null) {
			loginDao.saveUser(login);
		} else {
			login.setId(user.getId());
			loginDao.updateUser(login);
		}
		return login;
	}

	@Override
	public List<Map<String, Object>> LGSelect() {
		List<Map<String, Object>> select = loginDao.LGSelect();
		return select;
	}

	@Override
	public void remove(Integer deletes) {
		Login lg = new Login();
		lg.setId(deletes);
		loginDao.remove(lg);
	}

	@Override
	public List<Map<String, Object>> Search(String SearchRegister, String SearchPositionCombobox) {
		List<Map<String, Object>> select = loginDao.Search(SearchRegister, SearchPositionCombobox);
		return select;
	}
	
	@Override
	public LoginMapping editRegister(int id) {

		LoginMapping loginMapping = new LoginMapping();

		List<Login> lg = loginDao.editRegister(id);

		loginMapping.setId(lg.get(0).getId());
		loginMapping.setUserName(lg.get(0).getUsername());
		loginMapping.setPassword(lg.get(0).getPassword());
		loginMapping.setRoleId(lg.get(0).getRoleId());
		loginMapping.setFirstName(lg.get(0).getFirstName());
		loginMapping.setLastName(lg.get(0).getLastName());
		loginMapping.setEmail(lg.get(0).getEmail());
		loginMapping.setPhone(lg.get(0).getPhone());
		
		
		return loginMapping;
	}
	
	@Override
	public LoginMapping LoginSelect(LoginMapping loginMapping) {
		System.out.println("fffffffffffffffffff");
		LoginMapping loginmp = new LoginMapping();
		
		List<Login> login = loginDao.LoginSelect1(loginMapping);
		loginmp.setId(login.get(0).getId());
		loginmp.setUserName(login.get(0).getUsername());
		loginmp.setPassword(login.get(0).getPassword());
		loginmp.setRoleId(login.get(0).getRoleId());
		loginmp.setFirstName(login.get(0).getFirstName());
		loginmp.setLastName(login.get(0).getLastName());
		loginmp.setEmail(login.get(0).getEmail());
		loginmp.setPhone(login.get(0).getPhone());
		System.out.println(login.get(0).getUsername()+"-----------------------------------333");
		System.out.println(login.get(0).getFirstName()+"-------------------------------2222");
		return loginmp;
	}

	@Override
	public ArrayList<Login> RegisterSelect(LoginMapping login) {
		return loginDao.RegisterSelect(login);
	
	}

	@Override
	public LoginMapping LoginSelectpro(LoginMapping loginMapping) {
       
		LoginMapping loginmp = new LoginMapping();
		
		List<Login> login = loginDao.LoginSelectPro(loginMapping);
		loginmp.setId(login.get(0).getId());
		loginmp.setUserName(login.get(0).getUsername());
		loginmp.setPassword(login.get(0).getPassword());
		loginmp.setRoleId(login.get(0).getRoleId());
		loginmp.setFirstName(login.get(0).getFirstName());
		loginmp.setLastName(login.get(0).getLastName());
		loginmp.setEmail(login.get(0).getEmail());
		loginmp.setPhone(login.get(0).getPhone());
		System.out.println(login.get(0).getUsername()+"-----------------------------------333");
		System.out.println(login.get(0).getFirstName()+"-------------------------------2222");
		return loginmp;
	}

	@Override
	public LoginMapping LoginSelectPass(LoginMapping login) {
		
		LoginMapping loginmp = new LoginMapping();
		List<Login> logins = loginDao.LoginSelectPass(loginmp);
		
		loginmp.setEmail(logins.get(0).getEmail());
		loginmp.setPassword(logins.get(0).getPassword());
		return loginmp;
	}

	@Override
	public List<Map<String, Object>> pass(LoginMapping login) {
		
		return loginDao.passSearch(login);
	}
	

}

