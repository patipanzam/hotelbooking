package com.softsquare.application.service;

import java.util.ArrayList;

import com.softsquare.application.entity.Province;


public interface ProvinceService {
	
	public ArrayList<Province> getProvince();
	
}
