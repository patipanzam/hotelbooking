package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.dao.BranchDao;
import com.softsquare.application.domain.BranchMapping;
import com.softsquare.application.entity.Branch;

@Service
public class BranchServiceImp implements BranchService {

	@Autowired
	private BranchDao branchDao;

	@Override
	public void saveBranch(BranchMapping branchMappping) throws Exception {
		Branch ba = new Branch();
		ba.setBranchName(branchMappping.getBranchName());
		ba.setProvince(branchMappping.getProvince());
		ba.setBranchLocation(branchMappping.getBranchLocation());
		ba.setBranchTel(branchMappping.getBranchTel());
		branchDao.saveBranch(ba);

	}

	@Override
	public List<Map<String, Object>> BASelect() {
		List<Map<String, Object>> select = branchDao.BASelect();
		return select;
	}

	@Override
	public void saveGridBranch(BranchMapping stBranchMapping) {

		Gson gson = new Gson();
		if(BeanUtils.isNotEmpty(stBranchMapping.getGridStore_jsonCreateRecords())){
			Branch[] arrayRs = gson.fromJson(stBranchMapping.getGridStore_jsonCreateRecords(), Branch[].class);
			for (Branch br : arrayRs) {
				branchDao.saveGridBranch(br);
			}
		}
		
		if(BeanUtils.isNotEmpty(stBranchMapping.getGridStore_jsonUpdateRecords())){
			Branch[] arrayRs = gson.fromJson(stBranchMapping.getGridStore_jsonUpdateRecords(), Branch[].class);
			for (Branch br : arrayRs) {
				branchDao.updateGridBranch(br);
			}
		}
		
		if(BeanUtils.isNotEmpty(stBranchMapping.getGridStore_jsonDestroyRecords())){
			Branch[] arrayRs = gson.fromJson(stBranchMapping.getGridStore_jsonDestroyRecords(), Branch[].class);
			for (Branch br : arrayRs) {
				branchDao.remove(br);
			}
		}
	}

	@Override
	public ArrayList<Branch> getBranch() {
		return branchDao.getBranch();
	}
	
	@Override
	public List<Map<String, Object>> Search(String SearchBranch, String SearchProvince) {
		List<Map<String, Object>> select = branchDao.Search(SearchBranch, SearchProvince);
		return select;
	}

}
