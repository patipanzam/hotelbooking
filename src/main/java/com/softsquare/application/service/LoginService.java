package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonElement;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Login;
import com.softsquare.application.entity.Promotion;


public interface LoginService {
	
	public LoginMapping getUser(String userName);
	public Login saveUser(LoginMapping user) throws Exception;
	public List<Map<String,Object>> LGSelect();
	public void remove(Integer deletes);
	public List<Map<String, Object>> Search( String SearchRegister,String SearchPositionCombobox);
	public LoginMapping editRegister(int id);
	public LoginMapping LoginSelect(LoginMapping loginMapping);
	public ArrayList<Login> RegisterSelect(LoginMapping login);
	public LoginMapping LoginSelectpro(LoginMapping loginMapping);
	public LoginMapping LoginSelectPass(LoginMapping login);
	public List<Map<String, Object>> pass(LoginMapping login);
	
	
}
