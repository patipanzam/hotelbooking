package com.softsquare.application.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.softsquare.application.domain.MaterialMapping;
import com.softsquare.application.entity.Material;

public interface MaterialService {

	public void saveMaterial(MaterialMapping material) throws Exception;

	public List<Map<String, Object>> MASelect();

	public void saveGridMaterial(MaterialMapping stMaterialMapping);

	public ArrayList<Material> getMaterial();

	public List<Map<String, Object>> Search(String SearchMaterial, String SearchMaterialType);

}
