package com.softsquare.application.common.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.controller.ControllerDefault;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.domain.RoomMapping;

public class HotelUtils {

	private List<String> varjs = new ArrayList<String>();
	
//	public  void formObject(String name ,HotelMapping hotel) {
//		Gson gson = new Gson();
//		String json = gson.toJson(hotel);
//		String formstrign = "var "+name+" = "+json+";";
//		varjs.add(formstrign);
//	}

//	public ModelAndView renderView(ModelAndView model) {
//		model.addObject("javascriptrender", varjs);
//		return model;
//	}
	
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
	ModelAndView mav = new ModelAndView();
	mav.setViewName("result");
	mav.addObject("javascriptrender", varjs);
	return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	public ModelAndView bk(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("complete");
		mav.addObject("javascriptrender", varjs);
		return ControllerDefault.DefaultModelAndView(mav, request);
		}
	
	
	public  void roomObject(String name ,RoomMapping room) {
		
//		@RequestParam(value = "SearchProjectTypeCombobox", required = false) String SearchProjectType,
		
		Gson gson = new Gson();
		String json = gson.toJson(room);
		String formstrign = "var "+name+" = "+json+";";
		varjs.add(formstrign);
	}

	public void formObject(String name, RoomMapping roomMap) {
		// TODO Auto-generated method stub
		
	}

	public void roomObject(String name, BuildingMapping building) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		String json = gson.toJson(building);
		String formstrign = "var "+name+" = "+json+";";
		varjs.add(formstrign);
	}

	



public void bookingObject(String name, BookingMapping booking) {
	// TODO Auto-generated method stub
	Gson gson = new Gson();
	String json = gson.toJson(booking);
	String formstrign = "var "+name+" = "+json+";";
	varjs.add(formstrign);
}
	
public ModelAndView book(HttpServletRequest request, HttpServletResponse response) {
	ModelAndView mav = new ModelAndView();
	mav.setViewName("complete");
	mav.addObject("javascriptrender", varjs);
	return ControllerDefault.DefaultModelAndView(mav, request);
	}



}