package com.softsquare.application.common.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.ProjectMapping;

public class FromUtils {

	private List<String> varjs = new ArrayList<String>();
	
	public  void formObject(String name ,EmployeeMapping employeeMapping) {
		Gson gson = new Gson();
		String json = gson.toJson(employeeMapping);
		String formstrign = "var "+name+" = "+json+";";
		varjs.add(formstrign);
	}
	
	public  void formObject2(String name ,ProjectMapping projectMapping) {
		Gson gson = new Gson();
		String json = gson.toJson(projectMapping);
		String formstrign = "var "+name+" = "+json+";";
		varjs.add(formstrign);
	}
	
	public  void formObject(String name ,LoginMapping loginMapping) {
		Gson gson = new Gson();
		String json = gson.toJson(loginMapping);
		String formstrign = "var "+name+" = "+json+";";
		varjs.add(formstrign);
	}

	public ModelAndView renderView(ModelAndView model) {
		model.addObject("javascriptrender", varjs);
		return model;
	}
}
