package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.common.util.HotelUtils;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.domain.RoomMapping;

@RestController
@RequestMapping("/complete.html")
@Configurable
public class CompleteController {
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response,@ModelAttribute RoomMapping roomMapping){
    	ModelAndView mav = new ModelAndView();
     	mav.setViewName("complete");
     	if(BeanUtils.isNotNull(roomMapping.getRoomID())){
    		mav.addObject("headerId", roomMapping.getRoomID());
    	}
     	if(BeanUtils.isNotEmpty(roomMapping.getToDate())){
     		mav.addObject("toDate", roomMapping.getToDate());
     	}
     	if(BeanUtils.isNotEmpty(roomMapping.getFromDate())){
     		mav.addObject("fromDate", roomMapping.getFromDate());
     	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }
	
	
	@RequestMapping(params = { "method=viewBook" }, method = RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute BookingMapping booking,@RequestParam(value = "id", required = false) Integer roomid,
	HttpServletRequest request, HttpServletResponse response) {
		
		HotelUtils edit = new HotelUtils();
		
        edit.book(request, response);
		edit.bookingObject("bookingvalue", booking);

		return edit.book(request, response);
	}

	

}
