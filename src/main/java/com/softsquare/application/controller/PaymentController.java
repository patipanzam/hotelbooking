package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.PaymentMapping;
import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.entity.Payment;
import com.softsquare.application.entity.Promotion;
import com.softsquare.application.service.PaymentService;

@RestController
@RequestMapping("/payment.html")
@Configurable
public class PaymentController {
	
	@Autowired
	PaymentService paymentService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("payment");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	
	 @RequestMapping(params =  "method=save" , method=RequestMethod.POST)
	    public void login(HttpServletRequest request, HttpServletResponse response, @ModelAttribute PaymentMapping pmt) throws Throwable{
	    	Gson gson = new Gson();
	    	Payment emp = paymentService.savePayment(pmt);
			String json = gson.toJson(emp);
			response.getWriter().write("{records:" + json + "}");
			
			
		}

}
