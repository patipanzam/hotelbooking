package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.domain.EmployeePositionMapping;
import com.softsquare.application.domain.ProjectMapping;
import com.softsquare.application.entity.Project;
import com.softsquare.application.service.EmployeePositionService;
import com.softsquare.application.service.ProjectService;


@RestController
@RequestMapping("/project.html")
@Configurable
public class ProjectController {
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private EmployeePositionService employeePositionService;
	
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response, @ModelAttribute ProjectMapping projectMapping){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("project");
    	if(BeanUtils.isNotNull(projectMapping.getProjectID())){
    		mav.addObject("headerId", projectMapping.getProjectID());
    	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }

    @RequestMapping(method = RequestMethod.POST, params = { "method=saveProject" })
	public void saveProject(@ModelAttribute ProjectMapping projectMapping,@ModelAttribute EmployeePositionMapping employeePositionMapping,
			Model model, HttpServletRequest request,
			HttpServletResponse response) throws Exception  {
    	Gson gson = new Gson();
    	Project pro = projectService.saveProject(projectMapping); //save emp
    	employeePositionService.saveGridEmployeePosition(employeePositionMapping, pro); //save skill
    	String json = gson.toJson(pro);
		response.getWriter().write("{records:" + json + "}");
	}
    

	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectProjectGrid(@ModelAttribute EmployeePositionMapping employeePositionMapping,
			@RequestParam(value = "id", required = false) int id, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(employeePositionService.EPSelect(id));

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
	public void selectProjectDetail(@ModelAttribute ProjectMapping projectMapping, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(projectService.ProjectSelect(projectMapping));
		response.getWriter().write("{records:" + json + "}");
	}
    
}
