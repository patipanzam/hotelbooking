package com.softsquare.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.service.LoginService;

@RestController
@RequestMapping("/profile.html")
@Configurable
public class ProfileController {
	
	@Autowired
	private LoginService loginService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("profile");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}

	@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
	public void selectEmployeeDetail(@ModelAttribute LoginMapping login, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(loginService.LoginSelect(login));
		response.getWriter().write("{records:" + json + "}");
		
		
	}
}
