package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.MaterialMapping;
import com.softsquare.application.domain.MaterialTypeMapping;
import com.softsquare.application.service.MaterialService;
import com.softsquare.application.service.MaterialTypeService;

@RestController
@RequestMapping("/material.html")
@Configurable
public class MaterialController {

	@Autowired
	private MaterialTypeService MaterialTypeService;
	@Autowired
	private MaterialService MaterialService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("material");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectMaterialTypeDetail(@ModelAttribute MaterialTypeMapping materialTypeDetail,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(MaterialTypeService.MATSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@RequestMapping(method = RequestMethod.POST, params = { "method=saveMaterialType" })
	public void saveMaterialType(@ModelAttribute MaterialTypeMapping materialTypeDetail, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		MaterialTypeService.saveGridMaterialType(materialTypeDetail);
	}
	
	@RequestMapping(params = { "method=selectGrid2", "xaction=read" }, method = RequestMethod.POST)
	public void selectMaterialDetail(@ModelAttribute MaterialMapping materialDetail,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(MaterialService.MASelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(method = RequestMethod.POST, params = { "method=saveMaterial" })
	public void saveMaterial(@ModelAttribute MaterialMapping materialDetail, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		MaterialService.saveGridMaterial(materialDetail);
	}
	
	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void Search(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute MaterialMapping materialDetail,@RequestParam(value = "SearchMaterialType", required = false) String SearchMaterialType) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(MaterialTypeService.Search(SearchMaterialType)); 
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(params = "method=search2", method = RequestMethod.POST)
	public void Search2(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute MaterialMapping materialDetail, @RequestParam(value = "SearchMaterial", required = false) String SearchMaterial,
			@RequestParam(value = "SearchMaterialTypeCombobox", required = false) String SearchMaterialType) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(MaterialService.Search(SearchMaterial, SearchMaterialType)); 
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
