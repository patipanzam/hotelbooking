package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Login;
import com.softsquare.application.entity.Room;
import com.softsquare.application.service.RoomService;

@RestController
@RequestMapping("/room.html")
@Configurable
public class RoomController {

	@Autowired
  RoomService rooms;
//	@Autowired
//	BuildingService buildingService;
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response,@ModelAttribute RoomMapping roomMapping){
    	ModelAndView mav = new ModelAndView();
     	mav.setViewName("room");
     	if(BeanUtils.isNotNull(roomMapping.getRoomID())){
    		mav.addObject("headerId", roomMapping.getRoomID());
    	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }
	
	
	 @RequestMapping(params =  "method=save" , method=RequestMethod.POST)
	    public void login(HttpServletRequest request, HttpServletResponse response, @ModelAttribute RoomMapping room) throws Throwable{
	    	Gson gson = new Gson();
	    	Room emp = rooms.saveRoom(room);
			String json = gson.toJson(emp);
			response.getWriter().write("{records:" + json + "}");
		}
	    
		@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
		public void selectEmployeeDetail(@ModelAttribute RoomMapping room, HttpServletRequest request,
				HttpServletResponse response) throws Throwable {
			Gson gson = new Gson();
			String json = gson.toJson(rooms.RoomSelect(room));
			response.getWriter().write("{records:" + json + "}");
			
			
		}
//    
//    @RequestMapping(params =  "method=save" , method=RequestMethod.POST)
//    public void saveRoom(HttpServletRequest request, HttpServletResponse response, @ModelAttribute RoomMapping room) throws Throwable{
//		System.out.println("getBeds : "+room.getRoomBeds());
////		System.out.println("getFacilities : "+room.getRoomFacilities());
//		System.out.println("getFreewiwi : "+room.getFreewifi());
//		System.out.println("getSmocking : "+room.getSmocking());
//		System.out.println("getService : "+room.getService());
//		System.out.println("getBreaks : "+room.getBreaks());
//		System.out.println("getDate : "+room.getRoomDate());
//		System.out.println("getPrice : "+room.getRoomPrice());
//		System.out.println("getPromotion : "+room.getRoomPromotion());
//		System.out.println("getNo : "+room.getRoomNo());
//		System.out.println("getService : "+room.getRoomService());
//		System.out.println("getStatus : "+room.getRoomStatus());
//		System.out.println("getName : "+room.getTypeName());
//		System.out.println("getDetail : "+room.getTypeDetail());
//		
//		
//		
//		rooms.RoomSSave(room);
//	}
//    
//    @RequestMapping(params = { "method=view" }, method = RequestMethod.POST)
//	public ModelAndView edit(@ModelAttribute RoomMapping room,
//			@RequestParam(value = "id", required = false) Integer roomid,
//			HttpServletRequest request, HttpServletResponse response) {
//		room = rooms.editRoom(roomid);
//		System.out.println(roomid+"------------------------Id");
//
//		HotelUtils edit = new HotelUtils();
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("room");
//
//		edit.roomObject("roomvalue", room);
//		System.out.println(room);
//		return edit.renderView(mav);
//	}
//    
//    @RequestMapping(params = "method=update", method = RequestMethod.POST)
//	public void updateHRoom(HttpServletRequest request,
//			HttpServletResponse response,
//			@ModelAttribute RoomMapping roomMap) throws Throwable {
//		System.out.println("-------Update---------------");
//		System.out.println(roomMap.getRoomStatus()+"----------666666666666666");
//		rooms.updateRoom(roomMap);
//	}
//    
//    @RequestMapping(params =  {"method=search"} , method=RequestMethod.POST)
//    public void bookingList(HttpServletRequest request, HttpServletResponse response ){
//    	
////    	System.out.println(room.getTypeName()+"Typename-------------");
////		System.out.println(room.getFromDate()+"Fromdate-------------");
//   	  	
//   	 	List<Map<String, Object>> roomList =rooms.roomSearch();
//   		Gson gson = new Gson();
//       	String json = gson.toJson(roomList);
//       	System.out.println("5555555");
//       	try{
//       		response.getWriter().write("{records:"+ json + "}");
//       	
//       	}catch (IOException e){
//       		e.printStackTrace();
//       		
//       	}
//   	}
    
//    @RequestMapping(params = { "method=viewlist" }, method = RequestMethod.POST)
//	public ModelAndView edit(@ModelAttribute RoomMapping room,
//			HttpServletRequest request, HttpServletResponse response) {
////		List<Map<String, Object>>roomList=rooms.room(room);
////		 Gson gson = new Gson();
////		String  json = gson.toJson(roomList);
////		try {
////			response.getWriter().write("{records:"+json+"}");
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
//	
//		HotelUtils edit = new HotelUtils();
//		ModelAndView mav = new ModelAndView();
//		mav.setViewName("listroom");
//
//		edit.roomObject("roomvalue", room);
//    	mav.addObject("userNameSystem", LoginUtils.getUsername());
//    	mav.addObject("roleSystem", LoginUtils.getRole());
//		return edit.renderView(mav);
//	}
//    
    
  }