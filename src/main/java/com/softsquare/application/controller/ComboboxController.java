package com.softsquare.application.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.softsquare.application.entity.Branch;
import com.softsquare.application.entity.Building;
import com.softsquare.application.entity.Employee;
import com.softsquare.application.entity.Material;
import com.softsquare.application.entity.MaterialType;
import com.softsquare.application.entity.Position;
import com.softsquare.application.entity.PositionType;
import com.softsquare.application.entity.ProjectType;
import com.softsquare.application.entity.Promotion;
import com.softsquare.application.entity.Province;
import com.softsquare.application.entity.Role;
import com.softsquare.application.entity.RoomType;
import com.softsquare.application.entity.Skill;
import com.softsquare.application.service.BranchService;
import com.softsquare.application.service.BuildingService;
import com.softsquare.application.service.EmployeeService;
import com.softsquare.application.service.MaterialService;
import com.softsquare.application.service.MaterialTypeService;
import com.softsquare.application.service.PositionService;
import com.softsquare.application.service.PositionTypeService;
import com.softsquare.application.service.ProjectTypeService;
import com.softsquare.application.service.PromotionService;
import com.softsquare.application.service.ProvinceService;
import com.softsquare.application.service.RoleService;
import com.softsquare.application.service.RoomTypeService;
import com.softsquare.application.service.SkillService;

@RestController
@RequestMapping("/combobox.html")
@Configurable
public class ComboboxController {

	@Autowired
	private RoleService roleSerivce;

	@Autowired
	private PositionTypeService positionTypeService;

	@Autowired
	private MaterialTypeService materialTypeService;
	
	@Autowired
	private MaterialService materialService;
	
	@Autowired
	private PositionService positionService;
	
	@Autowired
	private ProvinceService provinceService;
	
	@Autowired
	private BranchService branchService;
	
	@Autowired
	private ProjectTypeService projectTypeService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private SkillService skillService;
	
	@Autowired
	 private BuildingService buildingService;
	
	@Autowired
	private RoomTypeService roomtypeService;
	
	@Autowired
	private PromotionService promotionService;
	
	

	@RequestMapping(params = "method=role", method = RequestMethod.POST)
	public void register(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Role> roleList = roleSerivce.getRole();
		Gson gson = new Gson();
		String json = gson.toJson(roleList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(params = "method=positionType", method = RequestMethod.POST)
	public void positionType(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<PositionType> positionTypeList = positionTypeService.getPositionType();
		Gson gson = new Gson();
		String json = gson.toJson(positionTypeList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(params = "method=materialType", method = RequestMethod.POST)
	public void materialType(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<MaterialType> materialTypeList = materialTypeService.getMaterialType();
		Gson gson = new Gson();
		String json = gson.toJson(materialTypeList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@RequestMapping(params = "method=material", method = RequestMethod.POST)
	public void material(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Material> materialList = materialService.getMaterial();
		Gson gson = new Gson();
		String json = gson.toJson(materialList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@RequestMapping(params = "method=position", method = RequestMethod.POST)
	public void position(HttpServletRequest request, HttpServletResponse response
			, @RequestParam String positionTypeID) {
		ArrayList<Position> positionList = positionService.getPosition(positionTypeID);
		Gson gson = new Gson();
		String json = gson.toJson(positionList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@RequestMapping(params = "method=province", method = RequestMethod.POST)
	public void province(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Province> provinceList = provinceService.getProvince();
		Gson gson = new Gson();
		String json = gson.toJson(provinceList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@RequestMapping(params = "method=branch", method = RequestMethod.POST)
	public void branch(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Branch> branchList = branchService.getBranch();
		Gson gson = new Gson();
		String json = gson.toJson(branchList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@RequestMapping(params = "method=projectType", method = RequestMethod.POST)
	public void projectType(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<ProjectType> projectTypeList = projectTypeService.getProjectType();
		Gson gson = new Gson();
		String json = gson.toJson(projectTypeList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@RequestMapping(params = "method=employee", method = RequestMethod.POST)
	public void employee(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Employee> employeeList = employeeService.getEmployee();
		Gson gson = new Gson();
		String json = gson.toJson(employeeList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@RequestMapping(params = "method=skill", method = RequestMethod.POST)
	public void skill(HttpServletRequest request, HttpServletResponse response, @RequestParam String positionID, @RequestParam String EmployeeName) {
		ArrayList<Skill> skillList = skillService.getSkill(positionID, EmployeeName);
		Gson gson = new Gson();
		String json = gson.toJson(skillList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@RequestMapping(params = "method=building", method = RequestMethod.POST)
	public void building(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Building> buildingList = buildingService.getBuilding();
		Gson gson = new Gson();
		String json = gson.toJson(buildingList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(params = "method=roomtype", method = RequestMethod.POST)
	public void roomtype(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<RoomType> roomtypeList = roomtypeService.getRoomType();
		Gson gson = new Gson();
		String json = gson.toJson(roomtypeList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(params = "method=promotion", method = RequestMethod.POST)
	public void promotion(HttpServletRequest request, HttpServletResponse response) {
		ArrayList<Promotion> promotionList = promotionService.getPromotion();
		Gson gson = new Gson();
		String json = gson.toJson(promotionList);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
