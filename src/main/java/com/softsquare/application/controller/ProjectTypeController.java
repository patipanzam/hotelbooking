package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.ProjectTypeMapping;
import com.softsquare.application.service.ProjectTypeService;

@RestController
@RequestMapping("/projectType.html")
@Configurable
public class ProjectTypeController {

	@Autowired
	private ProjectTypeService projectTypeService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("projectType");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectProjectTypeDetail(@ModelAttribute ProjectTypeMapping projectTypeDetail,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(projectTypeService.PTSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(method = RequestMethod.POST, params = { "method=saveProjectType" })
	public void saveProjectType(@ModelAttribute ProjectTypeMapping projectTypeDetail, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		projectTypeService.saveGridProjectType(projectTypeDetail);
	}

	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void Search(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute ProjectTypeMapping projectTypeDetail,
			@RequestParam(value = "SearchProjectType", required = false) String SearchProjectType) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(projectTypeService.Search(SearchProjectType));
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
