package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.BranchMapping;
import com.softsquare.application.service.BranchService;

@RestController
@RequestMapping("/branch.html")
@Configurable
public class BranchController {

	@Autowired
	private BranchService branchService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("branch");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectBranchDetail(@ModelAttribute BranchMapping branchDetail,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(branchService.BASelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(method = RequestMethod.POST, params = { "method=saveBranch" })
	public void saveBranch(@ModelAttribute BranchMapping branchDetail, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		branchService.saveGridBranch(branchDetail);
	}

	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void Search2(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute BranchMapping branchDetail,
			@RequestParam(value = "SearchBranch", required = false) String SearchBranch,
			@RequestParam(value = "SearchProvinceCombobox", required = false) String SearchProvince) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(branchService.Search(SearchBranch, SearchProvince));
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
