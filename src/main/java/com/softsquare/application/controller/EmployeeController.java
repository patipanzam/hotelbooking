package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.domain.SkillMapping;
import com.softsquare.application.entity.Employee;
import com.softsquare.application.service.EmployeeService;
import com.softsquare.application.service.SkillService;

@RestController
@RequestMapping("/employee.html")
@Configurable
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private SkillService skillService;
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response, @ModelAttribute EmployeeMapping employeeMapping){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("employee");
    	if(BeanUtils.isNotNull(employeeMapping.getEmployeeID())){
    		mav.addObject("headerId", employeeMapping.getEmployeeID());
    	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }

	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectEmployeeGrid(@ModelAttribute SkillMapping skillMapping,
			@RequestParam(value = "id", required = false) int id, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(skillService.SKSelect(id));

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
	public void selectEmployeeDetail(@ModelAttribute EmployeeMapping employeeMapping, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(employeeService.EmployeeSelect(employeeMapping));
		response.getWriter().write("{records:" + json + "}");
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, params = { "method=saveEmployee" })
	public void saveEmployee(@ModelAttribute EmployeeMapping employeeMapping, @ModelAttribute SkillMapping skillMapping,
			Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Gson gson = new Gson();
		Employee emp = employeeService.saveEmployee(employeeMapping); // save emp
		skillService.saveGridSkill(skillMapping, emp); // save skill
		String json = gson.toJson(emp);
		response.getWriter().write("{records:" + json + "}");
	}

}
