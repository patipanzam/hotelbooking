package com.softsquare.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.BranchMapping;
import com.softsquare.application.domain.ProjectMapping;
import com.softsquare.application.service.ProjectService;

@RestController
@RequestMapping("/homeSecretary.html")
@Configurable
public class HomeSecretaryController {

	@Autowired
	private ProjectService projectService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("homeSecretary");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}

	@RequestMapping(params = "method=select", method = RequestMethod.POST)
	public void selectPositionTypeDetail(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute ProjectMapping projectMapping) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(projectService.POSelect());
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void Search2(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute BranchMapping branchDetail,
			@RequestParam(value = "SearchProject", required = false) String SearchProject,
			@RequestParam(value = "SearchProjectTypeCombobox", required = false) String SearchProjectType,
			@RequestParam(value = "SearchBranchCombobox", required = false) String SearchBranch) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(projectService.Search(SearchProject, SearchProjectType,SearchBranch));
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
