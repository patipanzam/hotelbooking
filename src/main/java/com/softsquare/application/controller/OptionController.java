package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.domain.OptionMapping;
import com.softsquare.application.service.OptionService;

@RestController
@RequestMapping("/option.html")
@Configurable
public class OptionController {
	
	@Autowired
	private OptionService optionService;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("option");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectPositionTypeDetail(@ModelAttribute OptionMapping optionMapping,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(optionService.PTSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@RequestMapping(method = RequestMethod.POST, params = { "method=saveoption" })
	public void saveBuilding(@ModelAttribute OptionMapping optionMapping, Model model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		optionService.saveGridOption(optionMapping);
		
		System.out.println(optionMapping.getUdloginname()+"999999999999999999999999999");
	}

}
