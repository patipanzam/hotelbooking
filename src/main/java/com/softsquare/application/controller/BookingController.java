package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.common.util.HotelUtils;
import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Booking;
import com.softsquare.application.service.BookingService;
import com.softsquare.application.service.LoginService;
import com.softsquare.application.service.RoomService;

@RestController
@RequestMapping("/booking.html")
@Configurable
public class BookingController {

	@Autowired
   RoomService rooms;
	@Autowired
	private LoginService loginService;
	@Autowired
	private BookingService bookingService;

	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response,@ModelAttribute RoomMapping roomMapping){
    	ModelAndView mav = new ModelAndView();
     	mav.setViewName("booking");
     	if(BeanUtils.isNotNull(roomMapping.getRoomID())){
    		mav.addObject("headerId", roomMapping.getRoomID());
    	}
     	if(BeanUtils.isNotEmpty(roomMapping.getToDate())){
     		mav.addObject("toDate", roomMapping.getToDate());
     	}
     	if(BeanUtils.isNotEmpty(roomMapping.getFromDate())){
     		mav.addObject("fromDate", roomMapping.getFromDate());
     	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }
	
	
	 @RequestMapping(params =  "method=save" , method=RequestMethod.POST)
	    public void login(HttpServletRequest request, HttpServletResponse response, @ModelAttribute BookingMapping bk) throws Throwable{
	    	Gson gson = new Gson();
	    	Booking emp = bookingService.saveBooking(bk);
			String json = gson.toJson(emp);
			response.getWriter().write("{records:" + json + "}");
		}
	    
		@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
		public void selectEmployeeDetail(@ModelAttribute RoomMapping room, HttpServletRequest request,
				HttpServletResponse response) throws Throwable {
			Gson gson = new Gson();
			String json = gson.toJson(rooms.RoomSelect(room));
			response.getWriter().write("{records:" + json + "}");
			
			
		}
		
		@RequestMapping(params = { "method=selectus", "xaction=read" }, method = RequestMethod.POST)
		public void selectEmployeeDetail(@ModelAttribute LoginMapping login, HttpServletRequest request,
				HttpServletResponse response) throws Throwable {
			Gson gson = new Gson();
			String json = gson.toJson(loginService.LoginSelect(login));
			response.getWriter().write("{records:" + json + "}");
			
			
		}
		
		 @RequestMapping(params = { "method=viewBook" }, method = RequestMethod.POST)
			public ModelAndView edit(@ModelAttribute RoomMapping room,@RequestParam(value = "id", required = false) Integer roomid,
			HttpServletRequest request, HttpServletResponse response) {
				
				HotelUtils edit = new HotelUtils();
				

				
				edit.book(request, response);

				return edit.book(request, response);
			}

    
  }