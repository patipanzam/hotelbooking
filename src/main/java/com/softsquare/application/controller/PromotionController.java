package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Promotion;
import com.softsquare.application.entity.Room;
import com.softsquare.application.service.PromotionService;

@RestController
@RequestMapping("/promotion.html")
@Configurable
public class PromotionController {
	
	@Autowired
	private PromotionService promotionService;
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response,@ModelAttribute PromotionMapping promotionMapping){
    	ModelAndView mav = new ModelAndView();
     	mav.setViewName("promotion");
     	if(BeanUtils.isNotNull(promotionMapping.getPromotionID())){
    		mav.addObject("headerId", promotionMapping.getPromotionID());
    	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }
	

	 @RequestMapping(params =  "method=save" , method=RequestMethod.POST)
	    public void login(HttpServletRequest request, HttpServletResponse response, @ModelAttribute PromotionMapping pro) throws Throwable{
	    	Gson gson = new Gson();
	    	Promotion emp = promotionService.savePromotion(pro);
			String json = gson.toJson(emp);
			response.getWriter().write("{records:" + json + "}");
		}
	    
		@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
		public void selectEmployeeDetail(@ModelAttribute PromotionMapping pro, HttpServletRequest request,
				HttpServletResponse response) throws Throwable {
			Gson gson = new Gson();
			String json = gson.toJson(promotionService.PromotionSelect(pro));
			response.getWriter().write("{records:" + json + "}");
			
			System.out.println(pro+"--------------------77777");
		}

}
