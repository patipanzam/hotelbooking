package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.HotelUtils;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.service.RoomService;

@RestController
@RequestMapping("/userHome.html")
@Configurable
public class UserHomeController {
	
	@Autowired
	private RoomService roomService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("userHome");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = "method=select", method = RequestMethod.POST)
	public void selectLoginDetail(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute RoomMapping roomMapping) throws Throwable {
		
		 Gson gson = new Gson();
		String json = gson.toJson(roomService.LGSelectUs());
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@RequestMapping(params = { "method=viewtest" }, method = RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute RoomMapping room,
			HttpServletRequest request, HttpServletResponse response) {
//		List<Map<String, Object>>roomList=rooms.room(room);
//		 Gson gson = new Gson();
//		String  json = gson.toJson(roomList);
//		try {
//			response.getWriter().write("{records:"+json+"}");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
	
		HotelUtils edit = new HotelUtils();
		edit.roomObject("roomvalue", room);
		edit.page(request, response);
		
    	
		return edit.page(request, response);
	}
	
//	@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
//	public void selectEmployeeDetail(@ModelAttribute RoomMapping room, HttpServletRequest request,
//			HttpServletResponse response) throws Throwable {
//		Gson gson = new Gson();
//		String json = gson.toJson(roomService.RoomSelect1(room));
//		response.getWriter().write("{records:" + json + "}");
//		
//		
//	}

}
