package com.softsquare.application.controller;

import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softsquare.application.common.util.LoginUtils;

@Controller
public class ApplicationController {
	protected final Logger log = LoggerFactory.getLogger(getClass());

	
    @RequestMapping(value="/*",method=RequestMethod.GET)
    public ModelAndView etc(HttpServletRequest httpServletRequest){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("login");
    	return ControllerDefault.DefaultModelAndView(mav, httpServletRequest);
    }
	
	@RequestMapping(value={"/","/login.html"},method=RequestMethod.GET)
    public ModelAndView login(HttpServletRequest httpServletRequest) throws UnknownHostException{
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("login");
    	return  ControllerDefault.DefaultModelAndView(mav, httpServletRequest);
    }
	
	@RequestMapping(value="/test",method=RequestMethod.GET)
    public ModelAndView tess(HttpServletRequest httpServletRequest){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("tess");
    	return mav;
    }
    
    @RequestMapping(value={"/loginfail.html"},method=RequestMethod.GET)
    public ModelAndView loginfail(HttpServletRequest httpServletRequest){
    	ModelAndView mav = new ModelAndView();
		mav.addObject("error", "ชื่อผู้ใช้หรือรหัสผ่านผิดพลาด");
    	mav.setViewName("login");
    	return  ControllerDefault.DefaultModelAndView(mav, httpServletRequest);
    }
    
    @RequestMapping(value={"/logoutpage.html"},method=RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest httpServletRequest){
    	ModelAndView mav = new ModelAndView();
    	mav.addObject("msg", "ออกจากระบบเสร็จสมบูรณ์");
    	mav.setViewName("login");
    	return  ControllerDefault.DefaultModelAndView(mav, httpServletRequest);
    }
    
    @RequestMapping(value="/home.html",method=RequestMethod.GET)
    public ModelAndView home(HttpServletRequest httpServletRequest){
    	ModelAndView mav = new ModelAndView();
    	
    	String rolecode = LoginUtils.getRole();
    	if("Administrator".equals(rolecode)){
    		mav.setViewName("homeAdmin");
    	}
    	
    	if("Head of the project".equals(rolecode)){
    		mav.setViewName("homeHead");
    	}
    	
    	if("Engineer".equals(rolecode)){
    		mav.setViewName("homeEngineer");
    	}
    	
    	if("Secretary".equals(rolecode)){
    		mav.setViewName("homeSecretary");
    	}
    	
    	return ControllerDefault.DefaultModelAndView(mav, httpServletRequest);
    }
    
    @RequestMapping(value="/error.html",method=RequestMethod.GET)
    public ModelAndView error(HttpServletRequest httpServletRequest){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("error");
    	return  ControllerDefault.DefaultModelAndView(mav, httpServletRequest);
    }
    
    
    
    
}
