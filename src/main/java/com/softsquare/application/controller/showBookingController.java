package com.softsquare.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.domain.RoomTypeMapping;
import com.softsquare.application.service.BookingService;

@RestController
@RequestMapping("/showBooking.html")
@Configurable
public class showBookingController {

	@Autowired
	BookingService bookingService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("showBooking");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectPositionTypeDetail(@ModelAttribute RoomTypeMapping roomtypeMapping,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(bookingService.PTSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, params = { "method=savebooking" })
	public void saveBooking(@ModelAttribute BookingMapping bookingMapping, Model model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		bookingService.saveGridBooking(bookingMapping);
	}
	
	@RequestMapping(params = { "method=remove" }, method = RequestMethod.POST)
	public void removeEmployee(@RequestParam(value = "code", required = false) List<Integer> delete,
			HttpServletRequest request, HttpServletResponse response) {
		for (Integer deletes : delete) {
			bookingService.remove(deletes);
		}

	}
	
	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void Search(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "SearchRegister", required = false) String SearchRegister,
			@RequestParam(value = "SearchPositionCombobox", required = false) String SearchPositionCombobox) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(bookingService.Search(SearchRegister , SearchPositionCombobox));
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(params = "method=select", method = RequestMethod.POST)
	public void selectLoginDetail(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute BookingMapping bookingMapping) throws Throwable {

	
		Gson gson = new Gson();
		String json = gson.toJson(bookingService.LGSelect());
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
}
}
