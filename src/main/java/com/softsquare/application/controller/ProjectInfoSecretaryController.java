package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.FromUtils;
import com.softsquare.application.domain.EmployeePositionMapping;
import com.softsquare.application.domain.ProjectMapping;
import com.softsquare.application.entity.Project;
import com.softsquare.application.service.EmployeePositionService;
import com.softsquare.application.service.ProjectService;

@RestController
@RequestMapping("/projectInfoSecretary.html")
@Configurable
public class ProjectInfoSecretaryController {

	@Autowired
	private ProjectService projectService;

	@Autowired
	private EmployeePositionService employeePositionService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("projectInfoSecretary");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}

	@RequestMapping(params = { "method=viewPo" }, method = RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute ProjectMapping projectMapping,
			@RequestParam(value = "id", required = false) int id, HttpServletRequest request,
			HttpServletResponse response) {
		projectMapping = projectService.editProject(id);

		FromUtils edit = new FromUtils();
		ModelAndView mav = new ModelAndView();
		mav.setViewName("projectInfoSecretary");

		edit.formObject2("ordersetvalue", projectMapping);
		return ControllerDefault.DefaultModelAndView(edit.renderView(mav), request);
	}

	@RequestMapping(params = "method=select", method = RequestMethod.POST)
	public void selectProjectDetail(@ModelAttribute EmployeePositionMapping employeePositionMapping,
			@RequestParam(value = "id", required = false) int id, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(employeePositionService.EPSelect(id));

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
