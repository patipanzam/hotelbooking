package com.softsquare.application.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.common.util.HotelUtils;
import com.softsquare.application.common.util.LoginUtils;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.domain.RoomTypeMapping;
import com.softsquare.application.service.BookingService;
import com.softsquare.application.service.RoomService;

@RestController
@RequestMapping("/result.html")
@Configurable
public class ResultController {
	
	@Autowired
	RoomService roomService;
	
	@Autowired
	private BookingService bookingService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("result");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
//	@RequestMapping(method = RequestMethod.GET)
//	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
//		ModelAndView mav = new ModelAndView();
//
//		System.out.println("search-------------------------room");
//		mav.setViewName("result");
//		mav.addObject("userNameSystem", LoginUtils.getUsername());
//		mav.addObject("roleSystem", LoginUtils.getRole());
//		return mav;
//	}
	
	// listdatabase
		@RequestMapping(params = { "method=search" }, method = RequestMethod.POST)
		public void Room(HttpServletRequest request, HttpServletResponse response, @ModelAttribute RoomMapping room) {
			List<Map<String, Object>> roomList = roomService.room(room);
			
         	System.out.println(room.getFromDate()+"189898989898989898");
			System.out.println(room.getToDate()+"56564565656566556");
			System.out.println(room.getRoomTypeName()+"000000000000000000000000");
			Gson gson = new Gson();
			String json = gson.toJson(roomList);
		try {
				response.getWriter().write("{records:" + json + "}");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		
//		@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
//		public void selectEmployeeDetail(@ModelAttribute RoomMapping room, HttpServletRequest request,
//				HttpServletResponse response) throws Throwable {
//			Gson gson = new Gson();
//			String json = gson.toJson(roomService.RoomSelect1(room));
//			response.getWriter().write("{records:" + json + "}");
//			
//			
//		}
		
		@RequestMapping(params = { "method=view" }, method = RequestMethod.POST)
		public ModelAndView edit(@ModelAttribute RoomMapping room,
				HttpServletRequest request, HttpServletResponse response) {
//			List<Map<String, Object>>roomList=rooms.room(room);
//			 Gson gson = new Gson();
//			String  json = gson.toJson(roomList);
//			try {
//				response.getWriter().write("{records:"+json+"}");
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
			
		
			HotelUtils edit = new HotelUtils();
			edit.roomObject("roomvalue", room);
			edit.book(request, response);
			
	    	
			return edit.book(request, response);
		}

		
	@RequestMapping(method = RequestMethod.POST, params = { "method=savebooking" })
		public void saveBooking(@ModelAttribute BookingMapping bookingMapping, Model model,
				HttpServletRequest request, HttpServletResponse response) throws Exception {
		      bookingService.saveGridBooking(bookingMapping);
		      
		      System.out.println(bookingMapping.getCheckIN()+"-------------------");
		      System.out.println(bookingMapping.getEmail()+"999999999999999999999999999");
		}
	
	@RequestMapping(params = { "method= viewbk" }, method = RequestMethod.POST)
	public ModelAndView book(@ModelAttribute RoomMapping room, HttpServletRequest request, HttpServletResponse response) {
//		List<Map<String, Object>>roomList=rooms.room(room);
//		 Gson gson = new Gson();
//		String  json = gson.toJson(roomList);
//		try {
//			response.getWriter().write("{records:"+json+"}");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
	
		HotelUtils edit = new HotelUtils();
		edit.roomObject("roomvalue", room);
		edit.bk(request, response);
		
    	
		return edit.bk(request, response);
	}
}
