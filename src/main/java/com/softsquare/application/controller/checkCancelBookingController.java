package com.softsquare.application.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.domain.OptionMapping;
import com.softsquare.application.service.BookingService;
@RestController
@RequestMapping("/checkCancelBooking.html")
@Configurable
public class checkCancelBookingController {

	@Autowired
	BookingService bookingService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("checkCancelBooking");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectPositionTypeDetail(@ModelAttribute BookingMapping bookingMapping,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(bookingService.CheckCancelSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

	
	@RequestMapping(params = "method=update", method = RequestMethod.POST)
	public void update(@ModelAttribute BookingMapping booking,
			@RequestParam(value = "savegrid1", required = false) String grid, HttpServletRequest request,
			HttpServletResponse response) {
		//System.out.println("00000000000000000000000000000000000");
		Gson gson = new Gson();
		BookingMapping[] bookinggrid = gson.fromJson(grid, BookingMapping[].class);
		bookingService.updategrid(bookinggrid);

	}	
	
//	@RequestMapping(method = RequestMethod.POST, params = { "method=savestatus" })
//	public void saveBuilding(@ModelAttribute BookingMapping bookingMapping, Model model,
//			HttpServletRequest request, HttpServletResponse response) throws Exception {
//		bookingService.saveGridCheckCancel(bookingMapping);
//	}
	
	
	
	
}
