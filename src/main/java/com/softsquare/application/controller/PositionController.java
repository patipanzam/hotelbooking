package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.PositionMapping;
import com.softsquare.application.domain.PositionTypeMapping;
import com.softsquare.application.service.PositionService;
import com.softsquare.application.service.PositionTypeService;

@RestController
@RequestMapping("/position.html")
@Configurable
public class PositionController {

	@Autowired
	private PositionTypeService PositionTypeService;
	@Autowired
	private PositionService PositionService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("position");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}
	
	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectPositionTypeDetail(@ModelAttribute PositionTypeMapping positionTypeDetail,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(PositionTypeService.PTSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(method = RequestMethod.POST, params = { "method=savePositionType" })
	public void savePositionType(@ModelAttribute PositionTypeMapping positionTypeDetail, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		PositionTypeService.saveGridPositionType(positionTypeDetail);
	}
	
	@RequestMapping(params = { "method=selectGrid2", "xaction=read" }, method = RequestMethod.POST)
	public void selectPositionDetail(@ModelAttribute PositionMapping positionDetail,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(PositionService.POSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(method = RequestMethod.POST, params = { "method=savePosition" })
	public void savePosition(@ModelAttribute PositionMapping positionDetail, Model model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PositionService.saveGridPosition(positionDetail);
	}

	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void Search(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute PositionMapping positionDetail,
			@RequestParam(value = "SearchPositionType", required = false) String SearchPositionType) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(PositionTypeService.Search(SearchPositionType));
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(params = "method=search2", method = RequestMethod.POST)
	public void Search2(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute PositionMapping positionDetail,
			@RequestParam(value = "SearchPosition", required = false) String SearchPosition,
			@RequestParam(value = "SearchPositionTypeCombobox", required = false) String SearchPositionType)
					throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(PositionService.Search(SearchPosition, SearchPositionType));
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
