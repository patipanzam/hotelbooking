package com.softsquare.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.service.EmployeeService;


@RestController
@RequestMapping("/showEmployee.html")
@Configurable
public class ShowEmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("showEmployee");
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }
    
    @RequestMapping(params = "method=select", method = RequestMethod.POST)
	public void selectPositionTypeDetail(HttpServletRequest request, HttpServletResponse response,@ModelAttribute EmployeeMapping employeeMapping) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(employeeService.EMSelect());
		System.out.println(json);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
	@RequestMapping(params = { "method=removeEmployee" }, method = RequestMethod.POST)
	public void removeEmployee(@RequestParam(value = "code", required = false) List<Integer> delete,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println(delete);

		for (Integer deletes : delete) {
			System.out.println(deletes);
			employeeService.remove(deletes);
		}

	}
	
	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void Search(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute EmployeeMapping employeeMapping, @RequestParam(value = "SearchEmployee", required = false) String SearchEmployee )throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(employeeService.Search(SearchEmployee));
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
