package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.domain.BuildingMapping;
import com.softsquare.application.domain.EmployeeMapping;
import com.softsquare.application.domain.PositionMapping;
import com.softsquare.application.domain.PositionTypeMapping;
import com.softsquare.application.domain.SkillMapping;
import com.softsquare.application.entity.Building;
import com.softsquare.application.entity.Employee;
import com.softsquare.application.service.BuildingService;
import com.softsquare.application.service.EmployeeService;
import com.softsquare.application.service.SkillService;

@RestController
@RequestMapping("/buildingpage.html")
@Configurable
public class BuildingController {

	@Autowired
	private BuildingService buildingService;

	
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response, @ModelAttribute BuildingMapping buildingMapping){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("buildingpage");
    	if(BeanUtils.isNotNull(buildingMapping.getBuildingID())){
    		mav.addObject("headerId", buildingMapping.getBuildingID());
    	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }

	

	@RequestMapping(params = { "method=selectGrid", "xaction=read" }, method = RequestMethod.POST)
	public void selectPositionTypeDetail(@ModelAttribute BuildingMapping buildingMapping,HttpServletRequest request,
			HttpServletResponse response) throws Throwable {

		Gson gson = new Gson();
		String json = gson.toJson(buildingService.PTSelect());

		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(method = RequestMethod.POST, params = { "method=savebuilding" })
	public void saveBuilding(@ModelAttribute BuildingMapping buildingMapping, Model model,
			HttpServletRequest request, HttpServletResponse response) {
		buildingService.saveGridBuilding(buildingMapping);
	}
	
//	@RequestMapping(params = "method=search", method = RequestMethod.POST)
//	public void Search(HttpServletRequest request, HttpServletResponse response,
//			@ModelAttribute BuildingMapping buildingMapping,
//			@RequestParam(value = "SearchBuilding", required = false) String SearchBuilding) throws Throwable {
//		Gson gson = new Gson();
//		String json = gson.toJson(BuildingService.Search(SearchBuilding));
//		try {
//			response.getWriter().write("{records:" + json + "}");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
