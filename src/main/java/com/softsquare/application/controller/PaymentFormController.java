package com.softsquare.application.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.BookingMapping;
import com.softsquare.application.entity.Booking;
import com.softsquare.application.service.BookingService;

@RestController
@RequestMapping("/paymentform.html")
@Configurable
public class PaymentFormController {
	@Autowired
	BookingService bookingService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("paymentform");
		return ControllerDefault.DefaultModelAndView(mav, request);
	}


	@RequestMapping(params = "method=search", method = RequestMethod.POST)
	public void searchReservation(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute BookingMapping booking) throws Throwable {
		// System.out.println(roomDetail.getName());
		List<Map<String, Object>> listbooking = bookingService.BookingSearch(booking);

		Gson gson = new Gson();
		String json = gson.toJson(listbooking);
		try {
			response.getWriter().write("{records:" + json + "}");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
//	@RequestMapping(method = RequestMethod.POST, params = { "method=savecancel" })
//	public void saveBuilding(@ModelAttribute BookingMapping bookingMapping, Model model,
//			HttpServletRequest request, HttpServletResponse response) throws Exception {
//		bookingService.saveGridCancel(bookingMapping);
//	}


}
