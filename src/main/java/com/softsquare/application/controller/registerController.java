package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.PromotionMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.entity.Login;
import com.softsquare.application.service.LoginService;

@RestController
@RequestMapping("/register.html")
@Configurable
public class registerController {
	
	@Autowired
	LoginService loginService;
	
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response, @ModelAttribute LoginMapping login){
    	ModelAndView mav = new ModelAndView();
    	mav.setViewName("register");
    	if(BeanUtils.isNotNull(login.getId())){
    		mav.addObject("headerId", login.getId());
    	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }
    
    @RequestMapping(params =  "method=save" , method=RequestMethod.POST)
    public void login(HttpServletRequest request, HttpServletResponse response, @ModelAttribute LoginMapping login) throws Throwable{
    	Gson gson = new Gson();
    	Login emp = loginService.saveUser(login);
		String json = gson.toJson(emp);
		response.getWriter().write("{records:" + json + "}");
	}
    
	@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
	public void selectEmployeeDetail(@ModelAttribute LoginMapping login, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(loginService.LoginSelect(login));
		response.getWriter().write("{records:" + json + "}");
	}
	
	@RequestMapping(params = { "method=selectrg", "xaction=read" }, method = RequestMethod.POST)
	public void selectRegister(@ModelAttribute LoginMapping login, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(loginService.RegisterSelect(login));
		response.getWriter().write("{records:" + json + "}");
		
		
	}
}
