package com.softsquare.application.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.common.util.BeanUtils;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.service.RoomService;

@RestController
@RequestMapping("/roomdetail.html")
@Configurable
public class RoomDetailController {
	
	@Autowired
	  RoomService rooms;
	
	@RequestMapping(method=RequestMethod.GET)
    public ModelAndView page(HttpServletRequest request, HttpServletResponse response,@ModelAttribute RoomMapping roomMapping){
    	ModelAndView mav = new ModelAndView();
     	mav.setViewName("roomdetail");
     	if(BeanUtils.isNotNull(roomMapping.getRoomID())){
    		mav.addObject("headerId", roomMapping.getRoomID());
    	}
    	return ControllerDefault.DefaultModelAndView(mav, request);
    }
	
	@RequestMapping(params = { "method=select", "xaction=read" }, method = RequestMethod.POST)
	public void selectEmployeeDetail(@ModelAttribute RoomMapping room, HttpServletRequest request,
			HttpServletResponse response) throws Throwable {
		Gson gson = new Gson();
		String json = gson.toJson(rooms.RoomSelect(room));
		response.getWriter().write("{records:" + json + "}");
		
		
	}

}
