package com.softsquare.application.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.softsquare.application.domain.LoginMapping;
import com.softsquare.application.domain.RoomMapping;
import com.softsquare.application.service.LoginService;
import com.softsquare.application.service.RoomService;

    @RestController
	@RequestMapping("/showRoom.html")
	@Configurable
	public class ShowRoomController {

		@Autowired
		private RoomService roomService;

		@RequestMapping(method = RequestMethod.GET)
		public ModelAndView page(HttpServletRequest request, HttpServletResponse response) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName("showRoom");
			return ControllerDefault.DefaultModelAndView(mav, request);
		}

		@RequestMapping(params = "method=select", method = RequestMethod.POST)
		public void selectLoginDetail(HttpServletRequest request, HttpServletResponse response,
				@ModelAttribute RoomMapping roomMapping) throws Throwable {
			
			 Gson gson = new Gson();
			String json = gson.toJson(roomService.LGSelect());
			try {
				response.getWriter().write("{records:" + json + "}");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@RequestMapping(params = { "method=remove" }, method = RequestMethod.POST)
		public void removeEmployee(@RequestParam(value = "code", required = false) List<Integer> delete,
				HttpServletRequest request, HttpServletResponse response) {
			for (Integer deletes : delete) {
				roomService.remove(deletes);
			}

		}

		@RequestMapping(params = "method=search", method = RequestMethod.POST)
		public void Search(HttpServletRequest request, HttpServletResponse response,
				@RequestParam(value = "SearchRegister", required = false) String SearchRegister,
				@RequestParam(value = "SearchPositionCombobox", required = false) String SearchPositionCombobox) throws Throwable {
			Gson gson = new Gson();
			String json = gson.toJson(roomService.Search(SearchRegister , SearchPositionCombobox));
			try {
				response.getWriter().write("{records:" + json + "}");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

