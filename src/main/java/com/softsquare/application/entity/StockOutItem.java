package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stockoutitem")
public class StockOutItem implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "stockOutItemID")
	private Integer stockOutItemID;

	@Column(name = "quantity", unique = true, nullable = false)
	private Integer quantity;

	@Column(name = "materialID", unique = true, nullable = false)
	private Integer materialID;

	@Column(name = "stockOutID", unique = true, nullable = false)
	private Integer stockOutID;

	public Integer getStockOutItemID() {
		return stockOutItemID;
	}

	public void setStockOutItemID(Integer stockOutItemID) {
		this.stockOutItemID = stockOutItemID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getStockOutID() {
		return stockOutID;
	}

	public void setStockOutID(Integer stockOutID) {
		this.stockOutID = stockOutID;
	}

}
