package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "requestorderitem")
public class RequestOrderItem implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "requestOrderItemID")
	private Integer requestOrderItemID;

	@Column(name = "quantity", unique = true, nullable = false)
	private Integer quantity;

	@Column(name = "materialID", unique = true, nullable = false)
	private Integer materialID;

	@Column(name = "requestOrderID", unique = true, nullable = false)
	private Integer requestOrderID;

	public Integer getRequestOrderItemID() {
		return requestOrderItemID;
	}

	public void setRequestOrderItemID(Integer requestOrderItemID) {
		this.requestOrderItemID = requestOrderItemID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getRequestOrderID() {
		return requestOrderID;
	}

	public void setRequestOrderID(Integer requestOrderID) {
		this.requestOrderID = requestOrderID;
	}

}
