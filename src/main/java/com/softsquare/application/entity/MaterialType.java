package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "materialtype")
public class MaterialType implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "materialTypeID")
	private Integer materialTypeID;

	@Column(name = "materialTypeName", unique = true, nullable = false)
	private String materialTypeName;

	public Integer getMaterialTypeID() {
		return materialTypeID;
	}

	public void setMaterialTypeID(Integer materialTypeID) {
		this.materialTypeID = materialTypeID;
	}

	public String getMaterialTypeName() {
		return materialTypeName;
	}

	public void setMaterialTypeName(String materialTypeName) {
		this.materialTypeName = materialTypeName;
	}

}
