package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "itemstock")
public class ItemStock implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "itemStockID")
	private Integer itemStockID;

	@Column(name = "quantity", unique = true, nullable = false)
	private Integer quantity;

	@Column(name = "materialID", unique = true, nullable = false)
	private Integer materialID;

	@Column(name = "projectID", unique = true, nullable = false)
	private Integer projectID;

	public Integer getItemStockID() {
		return itemStockID;
	}

	public void setItemStockID(Integer itemStockID) {
		this.itemStockID = itemStockID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
