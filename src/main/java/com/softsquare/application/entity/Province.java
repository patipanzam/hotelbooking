package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "province")
public class Province implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;
	
	@Id
	@Column(name = "PROVINCE_CODE")
	private String PROVINCE_CODE;

	@Column(name = "PROVINCE_NAME_THA", unique = true, nullable = false)
	private String PROVINCE_NAME_THA;

	@Column(name = "PROVINCE_NAME_ENG", unique = true, nullable = false)
	private String PROVINCE_NAME_ENG;

	@Column(name = "ACTIVE", unique = true, nullable = false)
	private String ACTIVE;

	@Column(name = "CR_BY", unique = true, nullable = false)
	private String CR_BY;

	@Column(name = "CR_DATE", unique = true, nullable = false)
	private String CR_DATE;

	@Column(name = "PROG_ID", unique = true, nullable = false)
	private String PROG_ID;

	@Column(name = "UPD_BY", unique = true, nullable = false)
	private String UPD_BY;

	@Column(name = "UPD_DATE", unique = true, nullable = false)
	private String UPD_DATE;

	@Column(name = "UPD_PROG_ID", unique = true, nullable = false)
	private String UPD_PROG_ID;

	public String getPROVINCE_CODE() {
		return PROVINCE_CODE;
	}

	public void setPROVINCE_CODE(String pROVINCE_CODE) {
		PROVINCE_CODE = pROVINCE_CODE;
	}

	public String getPROVINCE_NAME_THA() {
		return PROVINCE_NAME_THA;
	}

	public void setPROVINCE_NAME_THA(String pROVINCE_NAME_THA) {
		PROVINCE_NAME_THA = pROVINCE_NAME_THA;
	}

	public String getPROVINCE_NAME_ENG() {
		return PROVINCE_NAME_ENG;
	}

	public void setPROVINCE_NAME_ENG(String pROVINCE_NAME_ENG) {
		PROVINCE_NAME_ENG = pROVINCE_NAME_ENG;
	}

	public String getACTIVE() {
		return ACTIVE;
	}

	public void setACTIVE(String aCTIVE) {
		ACTIVE = aCTIVE;
	}

	public String getCR_BY() {
		return CR_BY;
	}

	public void setCR_BY(String cR_BY) {
		CR_BY = cR_BY;
	}

	public String getCR_DATE() {
		return CR_DATE;
	}

	public void setCR_DATE(String cR_DATE) {
		CR_DATE = cR_DATE;
	}

	public String getPROG_ID() {
		return PROG_ID;
	}

	public void setPROG_ID(String pROG_ID) {
		PROG_ID = pROG_ID;
	}

	public String getUPD_BY() {
		return UPD_BY;
	}

	public void setUPD_BY(String uPD_BY) {
		UPD_BY = uPD_BY;
	}

	public String getUPD_DATE() {
		return UPD_DATE;
	}

	public void setUPD_DATE(String uPD_DATE) {
		UPD_DATE = uPD_DATE;
	}

	public String getUPD_PROG_ID() {
		return UPD_PROG_ID;
	}

	public void setUPD_PROG_ID(String uPD_PROG_ID) {
		UPD_PROG_ID = uPD_PROG_ID;
	}

}
