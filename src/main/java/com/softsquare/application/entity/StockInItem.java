package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stockinitem")
public class StockInItem implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "stockInItemID")
	private Integer stockInItemID;

	@Column(name = "quantity", unique = true, nullable = false)
	private Integer quantity;

	@Column(name = "materialID", unique = true, nullable = false)
	private Integer materialID;

	@Column(name = "stockInID", unique = true, nullable = false)
	private Integer stockInID;

	public Integer getStockInItemID() {
		return stockInItemID;
	}

	public void setStockInItemID(Integer stockInItemID) {
		this.stockInItemID = stockInItemID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getStockInID() {
		return stockInID;
	}

	public void setStockInID(Integer stockInID) {
		this.stockInID = stockInID;
	}

}
