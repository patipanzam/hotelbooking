package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "workduration")
public class WorkDuration implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "workDurationID")
	private Integer workDurationID;

	@Column(name = "duration", unique = true, nullable = false)
	private Integer duration;

	@Column(name = "dateFrom", unique = true, nullable = false)
	private String dateFrom;

	@Column(name = "dateTo", unique = true, nullable = false)
	private String dateTo;

	@Column(name = "totalSarary", unique = true, nullable = false)
	private Integer totalSarary;

	@Column(name = "employeePositionID", unique = true, nullable = false)
	private Integer employeePositionID;

	@Column(name = "projectID", unique = true, nullable = false)
	private Integer projectID;

	public Integer getWorkDurationID() {
		return workDurationID;
	}

	public void setWorkDurationID(Integer workDurationID) {
		this.workDurationID = workDurationID;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public Integer getTotalSarary() {
		return totalSarary;
	}

	public void setTotalSarary(Integer totalSarary) {
		this.totalSarary = totalSarary;
	}

	public Integer getEmployeePositionID() {
		return employeePositionID;
	}

	public void setEmployeePositionID(Integer employeePositionID) {
		this.employeePositionID = employeePositionID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
