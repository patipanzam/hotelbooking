package com.softsquare.application.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "projectID")
	private Integer projectID;

	@Column(name = "projectName", unique = true, nullable = false)
	private String projectName;

	@Column(name = "projectLocation", unique = true, nullable = false)
	private String projectLocation;

	@Column(name = "projectDateStart", unique = true, nullable = false)
	private String projectDateStart;

	@Column(name = "projectDateEnd", unique = true, nullable = false)
	private String projectDateEnd;

	@Column(name = "projectBudget", unique = true, nullable = false)
	private Integer projectBudget;

	@Column(name = "projectDescription", unique = true, nullable = false)
	private String projectDescription;

	@Column(name = "projectTypeID", unique = true, nullable = false)
	private Integer projectTypeID;

	@Column(name = "branchID", unique = true, nullable = false)
	private Integer branchID;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projectTypeID", referencedColumnName = "projectTypeID", insertable = false, updatable = false)
	private ProjectType projectType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branchID", referencedColumnName = "branchID", insertable = false, updatable = false)
	private Branch branch;
	
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="projectID")
    private List<EmployeePosition> employeePosition;

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectLocation() {
		return projectLocation;
	}

	public void setProjectLocation(String projectLocation) {
		this.projectLocation = projectLocation;
	}

	public String getProjectDateStart() {
		return projectDateStart;
	}

	public void setProjectDateStart(String projectDateStart) {
		this.projectDateStart = projectDateStart;
	}

	public String getProjectDateEnd() {
		return projectDateEnd;
	}

	public void setProjectDateEnd(String projectDateEnd) {
		this.projectDateEnd = projectDateEnd;
	}

	public Integer getProjectBudget() {
		return projectBudget;
	}

	public void setProjectBudget(Integer projectBudget) {
		this.projectBudget = projectBudget;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Integer getProjectTypeID() {
		return projectTypeID;
	}

	public void setProjectTypeID(Integer projectTypeID) {
		this.projectTypeID = projectTypeID;
	}

	public Integer getBranchID() {
		return branchID;
	}

	public void setBranchID(Integer branchID) {
		this.branchID = branchID;
	}

	public ProjectType getProjectType() {
		return projectType;
	}

	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public List<EmployeePosition> getEmployeePosition() {
		return employeePosition;
	}

	public void setEmployeePosition(List<EmployeePosition> employeePosition) {
		this.employeePosition = employeePosition;
	}

}
