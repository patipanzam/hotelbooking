package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "material")
public class Material implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "materialID")
	private Integer materialID;

	@Column(name = "materialName", unique = true, nullable = false)
	private String materialName;

	@Column(name = "materialUnit", unique = true, nullable = false)
	private String materialUnit;

	@Column(name = "materialTypeID", unique = true, nullable = false)
	private Integer materialTypeID;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "materialTypeID", referencedColumnName = "materialTypeID", insertable=false, updatable=false)
    private MaterialType materialType;

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getMaterialUnit() {
		return materialUnit;
	}

	public void setMaterialUnit(String materialUnit) {
		this.materialUnit = materialUnit;
	}

	public Integer getMaterialTypeID() {
		return materialTypeID;
	}

	public void setMaterialTypeID(Integer materialTypeID) {
		this.materialTypeID = materialTypeID;
	}

	public MaterialType getMaterialType() {
		return materialType;
	}

	public void setMaterialType(MaterialType materialType) {
		this.materialType = materialType;
	}

}
