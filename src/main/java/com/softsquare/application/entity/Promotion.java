package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "PROMOTION")
public class Promotion  extends BaseEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7625340270370683284L;

	@Id
	@GeneratedValue
	@Column(name = "PROMOTIONID")
	private Integer promotionID;

	@NotEmpty
	@Column(name = "UDLOGINNAME", unique = true, nullable = false)
	private String udloginname;

	@NotEmpty
	@Column(name = "NAME")
	private String name;

	@NotEmpty
	@Column(name = "DESCRIPTION")
	private String description;

	@NotEmpty
	@Column(name = "FROMDATE")
	private String fromDate;

	@NotEmpty
	@Column(name = "TODATE")
	private String toDate;

	@NotEmpty
	@Column(name = "SUMDATE")
	private String date;

	// @NotEmpty
	@Column(name = "ROOM")
	private String room;
// @NotEmpty
	@Column(name = "RATE")
	private String rate;
	
	@NotNull
	@Column(name = "STATUS")
	private String status;
	
//	@NotNull
//	@Column(name = "ROOMID", unique = true, nullable = false)
//	private Integer roomID;
//	
//	@NotNull
//	@Column(name = "ROOMID1", unique = true, nullable = false)
//	private Integer roomID1;
//	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "ROOMID", referencedColumnName = "ROOMID", insertable = false, updatable = false)
//	private RoomType roomType;
//	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "ROOMID1", referencedColumnName = "ROOMID", insertable = false, updatable = false)
//	private RoomType roomType1;


	public Integer getPromotionID() {
		return promotionID;
	}

	public void setPromotionID(Integer promotionID) {
		this.promotionID = promotionID;
	}

	public String getUdloginname() {
		return udloginname;
	}

	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	
	
}
