package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "BUILDING")
public class Building extends BaseEntity implements Serializable{

	
    @Id
	@GeneratedValue
	@Column(name = "BUILDINGID")
    private Integer buildingID;
	
	@NotEmpty
	@Column(name = "BUILDINGNAME", unique=true, nullable = false)
    private String buildingName;
	
//	@NotEmpty
//	@Column(name = "BUILDINGCODE", unique=true, nullable = false)
//    private String buildingCode;
	
	public Integer getBuildingID() {
		return buildingID;
	}

	public void setBuildingID(Integer buildingID) {
		this.buildingID = buildingID;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}


	
	
	
	
	

	
}