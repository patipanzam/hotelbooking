package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "requestorder")
public class RequestOrder extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "requestOrderID")
	private Integer requestOrderID;

	@Column(name = "requestOrderNO", unique = true, nullable = false)
	private String requestOrderNO;

	@Column(name = "date", unique = true, nullable = false)
	private String date;

	@Column(name = "statusAprove", unique = true, nullable = false)
	private String statusAprove;

	@Column(name = "projectID", unique = true, nullable = false)
	private Integer projectID;

	public Integer getRequestOrderID() {
		return requestOrderID;
	}

	public void setRequestOrderID(Integer requestOrderID) {
		this.requestOrderID = requestOrderID;
	}

	public String getRequestOrderNO() {
		return requestOrderNO;
	}

	public void setRequestOrderNO(String requestOrderNO) {
		this.requestOrderNO = requestOrderNO;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatusAprove() {
		return statusAprove;
	}

	public void setStatusAprove(String statusAprove) {
		this.statusAprove = statusAprove;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
