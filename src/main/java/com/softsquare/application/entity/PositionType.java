package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "positiontype")
public class PositionType implements Serializable{
	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "positionTypeID")
	private Integer positionTypeID;

	@Column(name = "positionTypeName", unique = true, nullable = false)
	private String positionTypeName;

	public Integer getPositionTypeID() {
		return positionTypeID;
	}

	public void setPositionTypeID(Integer positionTypeID) {
		this.positionTypeID = positionTypeID;
	}

	public String getPositionTypeName() {
		return positionTypeName;
	}

	public void setPositionTypeName(String positionTypeName) {
		this.positionTypeName = positionTypeName;
	}

	
}
