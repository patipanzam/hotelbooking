package com.softsquare.application.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "BOOKING")
public class Booking extends BaseEntity implements Serializable{

	private static final long serialVersionUID = 1919921330545060284L;

	@Id
	@GeneratedValue
	@Column(name = "BOOKINGID")
    private Integer bookingID;
	

	@Column(name = "UDLOGINNAME") 
    private String udloginname;
	
	public String getUdloginname() {
		return udloginname;
	}

	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}
  
    @Column(name = "CHECKIN")
    private Date checkIN;
	
    @Column(name = "CHECKOUT")
    private Date checkOUT;
	
    @Column(name = "CANCELSTATUS")
    private String CancelStatus;

 
	@Column(name = "NIGHT")
    private String night;
    
//    @NotEmpty
	@Column(name = "PRICE")
    private String sumPrice;
   
//	@NotEmpty
	@Column(name = "FNAME")
    private String fname;
	
//	@NotEmpty
	@Column(name = "LNAME")
    private String lname;
	
//	@NotEmpty
	@Column(name = "EMAIL")
    private String email;
	
//	@NotEmpty
	@Column(name = "PHONE")
    private String phone;
	
//	@NotEmpty
	@Column(name = "COUNTRY")
    private String country;
	
////	@NotEmpty
//	@Column(name = "LARGEBED")
//    private String large_bed;
//	
////	@NotEmpty
//	@Column(name = "LATECHECK")
//    private String late_ckeckin;
//	
////	@NotEmpty
//	@Column(name = "HIGHFLOOR")
//    private String high_floor;
//	
////	@NotEmpty
//	@Column(name = "TRANSFER")
//    private String transfer;
//	
////	@NotEmpty
//	@Column(name = "TWINBED")
//    private String twin_bed;
//	
////	@NotEmpty
//	@Column(name = "EARLYCHECK")
//    private String early_check;
//	
////	@NotEmpty
//	@Column(name = "COMMENT")
//    private String comment;
	
//	@NotEmpty
	@Column(name = "STATUSPAYMENT")
    private String status;
	
//	@NotEmpty
	@Column(name = "ROOMS")
    private String rooms;
	
	
	
	@Column(name = "ROOMID")
    private Integer roomID;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROOMID", referencedColumnName = "ROOMID", insertable=false, updatable=false)
    private RoomType roomtype;

	public Integer getBookingID() {
		return bookingID;
	}

	public void setBookingID(Integer bookingID) {
		this.bookingID = bookingID;
	}

	public Date getCheckIN() {
		return checkIN;
	}

	public void setCheckIN(Date checkIN) {
		this.checkIN = checkIN;
	}

	public Date getCheckOUT() {
		return checkOUT;
	}

	public void setCheckOUT(Date checkOUT) {
		this.checkOUT = checkOUT;
	}

	public String getNight() {
		return night;
	}

	public void setNight(String night) {
		this.night = night;
	}

	

	public String getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRooms() {
		return rooms;
	}

	public void setRooms(String rooms) {
		this.rooms = rooms;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	  public String getCancelStatus() {
			return CancelStatus;
		}

		public void setCancelStatus(String cancelStatus) {
			CancelStatus = cancelStatus;
		}
//	public String getLarge_bed() {
//		return large_bed;
//	}
//
//	public void setLarge_bed(String large_bed) {
//		this.large_bed = large_bed;
//	}
//
//	public String getLate_ckeckin() {
//		return late_ckeckin;
//	}
//
//	public void setLate_ckeckin(String late_ckeckin) {
//		this.late_ckeckin = late_ckeckin;
//	}
//
//	public String getHigh_floor() {
//		return high_floor;
//	}
//
//	public void setHigh_floor(String high_floor) {
//		this.high_floor = high_floor;
//	}
//
//	public String getTransfer() {
//		return transfer;
//	}
//
//	public void setTransfer(String transfer) {
//		this.transfer = transfer;
//	}
//
//	public String getTwin_bed() {
//		return twin_bed;
//	}
//
//	public void setTwin_bed(String twin_bed) {
//		this.twin_bed = twin_bed;
//	}
//
//	public String getEarly_check() {
//		return early_check;
//	}
//
//	public void setEarly_check(String early_check) {
//		this.early_check = early_check;
//	}

	public Integer getRoomID() {
		return roomID;
	}

	public void setRoomID(Integer roomID) {
		this.roomID = roomID;
	}

	

	public RoomType getRoomtype() {
		return roomtype;
	}

	public void setRoomtype(RoomType roomtype) {
		this.roomtype = roomtype;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

//	public String getComment() {
//		return comment;
//	}
//
//	public void setComment(String comment) {
//		this.comment = comment;
//	}
	
	
	

}
