package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stockout")
public class StockOut extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "stockOutID")
	private Integer stockOutID;

	@Column(name = "stockOutNO", unique = true, nullable = false)
	private String stockOutNO;

	@Column(name = "date", unique = true, nullable = false)
	private String date;

	@Column(name = "projectID", unique = true, nullable = false)
	private Integer projectID;

	public Integer getStockOutID() {
		return stockOutID;
	}

	public void setStockOutID(Integer stockOutID) {
		this.stockOutID = stockOutID;
	}

	public String getStockOutNO() {
		return stockOutNO;
	}

	public void setStockOutNO(String stockOutNO) {
		this.stockOutNO = stockOutNO;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
