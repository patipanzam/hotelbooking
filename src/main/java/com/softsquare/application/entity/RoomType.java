package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table( name="ROOMTYPENAME")

public class RoomType  extends BaseEntity implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6547607805502233660L;

	@Id
	@GeneratedValue
	@Column (name="ROOMID")
	private Integer roomID;
	
	@Column (name="PROMOTIONID")
	private Integer promotionID;
	
//	@NotEmpty
	@Column (name= "UDLOGINNAME",unique = true,nullable=false)
	private String udloginname;
	
	@NotEmpty
	@Column (name="ROOMTYPENAME")
	private String roomTypeName;
	
	@NotEmpty
	@Column (name="MAXOFROOMTYPE")
	private String max;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROMOTIONID", referencedColumnName = "PROMOTIONID", insertable = false, updatable = false)
	private Promotion promotion;
//	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "ROOMID", referencedColumnName = "ROOMID", insertable = false, updatable = false)
//	private Promotion room;
//	
	

	public Integer getRoomID() {
		return roomID;
	}

	public void setRoomID(Integer roomID) {
		this.roomID = roomID;
	}

	public String getUdloginname() {
		return udloginname;
	}

	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	public Integer getPromotionID() {
		return promotionID;
	}

	public void setPromotionID(Integer promotionID) {
		this.promotionID = promotionID;
	}

	public Promotion getPromotion() {
		return promotion;
	}

	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
//
//	public Promotion getRoom() {
//		return room;
//	}
//
//	public void setRoom(Promotion room) {
//		this.room = room;
//	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	
	



}
