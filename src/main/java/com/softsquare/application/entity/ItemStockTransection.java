package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "itemstocktransection")
public class ItemStockTransection extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "itemStockTransectionID")
	private Integer itemStockTransectionID;

	@Column(name = "quantity", unique = true, nullable = false)
	private Integer quantity;

	@Column(name = "statusTransection", unique = true, nullable = false)
	private String statusTransection;

	@Column(name = "date", unique = true, nullable = false)
	private String date;

	@Column(name = "materialID", unique = true, nullable = false)
	private Integer materialID;

	@Column(name = "projectID", unique = true, nullable = false)
	private Integer projectID;

	public Integer getItemStockTransectionID() {
		return itemStockTransectionID;
	}

	public void setItemStockTransectionID(Integer itemStockTransectionID) {
		this.itemStockTransectionID = itemStockTransectionID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStatusTransection() {
		return statusTransection;
	}

	public void setStatusTransection(String statusTransection) {
		this.statusTransection = statusTransection;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
