package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stockin")
public class StockIn extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "stockInID")
	private Integer stockInID;

	@Column(name = "stockInNO", unique = true, nullable = false)
	private String stockInNO;

	@Column(name = "date", unique = true, nullable = false)
	private String date;

	@Column(name = "supplier", unique = true, nullable = false)
	private String supplier;

	@Column(name = "totalPrice", unique = true, nullable = false)
	private Integer totalPrice;

	@Column(name = "vat", unique = true, nullable = false)
	private Integer vat;

	@Column(name = "totalVat", unique = true, nullable = false)
	private Integer totalVat;

	@Column(name = "projectID", unique = true, nullable = false)
	private Integer projectID;

	public Integer getStockInID() {
		return stockInID;
	}

	public void setStockInID(Integer stockInID) {
		this.stockInID = stockInID;
	}

	public String getStockInNO() {
		return stockInNO;
	}

	public void setStockInNO(String stockInNO) {
		this.stockInNO = stockInNO;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Integer getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getVat() {
		return vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

	public Integer getTotalVat() {
		return totalVat;
	}

	public void setTotalVat(Integer totalVat) {
		this.totalVat = totalVat;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
