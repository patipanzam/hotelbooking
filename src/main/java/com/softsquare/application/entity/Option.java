package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "ROOMOPTION")
public class Option extends BaseEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6547607805502233660L;

		@Id
		@GeneratedValue
		@Column(name = "OPTIONID")
	    private Integer optionID;
	    
//	    @NotEmpty
		@Column(name = "UDLOGINNAME", unique = true, nullable = false)
		private String udloginname;
		
		@NotEmpty
		@Column(name = "OPTIONNAME", unique=true, nullable = false)
	    private String optionName;
		
		@NotEmpty
		@Column(name = "OPTIONPRICE", unique=true, nullable = false)
	    private String optionPrice;

		public Integer getOptionID() {
			return optionID;
		}

		public void setOptionID(Integer optionID) {
			this.optionID = optionID;
		}

		public String getUdloginname() {
			return udloginname;
		}

		public void setUdloginname(String udloginname) {
			this.udloginname = udloginname;
		}

		public String getOptionName() {
			return optionName;
		}

		public void setOptionName(String optionName) {
			this.optionName = optionName;
		}

		public String getOptionPrice() {
			return optionPrice;
		}

		public void setOptionPrice(String optionPrice) {
			this.optionPrice = optionPrice;
		}
		
		
		

}
