package com.softsquare.application.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
public class Employee extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -117259679410559094L;

	@Id
	@GeneratedValue
	@Column(name = "employeeID")
	private Integer employeeID;

	@Column(name = "employeeName", unique = true, nullable = false)
	private String employeeName;

	@Column(name = "employeeAddress")
	private String employeeAddress;

	@Column(name = "employeePhone")
	private String employeePhone;

	@Column(name = "employeeCitizenID", unique = true, nullable = false)
	private String employeeCitizenID;

//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "employeeID")
//	private List<Skill> skill;

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeAddress() {
		return employeeAddress;
	}

	public void setEmployeeAddress(String employeeAddress) {
		this.employeeAddress = employeeAddress;
	}

	public String getEmployeePhone() {
		return employeePhone;
	}

	public void setEmployeePhone(String employeePhone) {
		this.employeePhone = employeePhone;
	}

	public String getEmployeeCitizenID() {
		return employeeCitizenID;
	}

	public void setEmployeeCitizenID(String employeeCitizenID) {
		this.employeeCitizenID = employeeCitizenID;
	}

//	public List<Skill> getSkill() {
//		return skill;
//	}
//
//	public void setSkill(List<Skill> skill) {
//		this.skill = skill;
//	}

}
