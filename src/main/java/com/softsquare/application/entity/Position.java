package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "position")
public class Position implements Serializable{
	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "positionID")
	private Integer positionID;

	@Column(name = "positionName", unique = true, nullable = false)
	private String positionName;
	
	@Column(name = "positionTypeID", unique = true, nullable = false)
	private String positionTypeID;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "positionTypeID", referencedColumnName = "positionTypeID", insertable=false, updatable=false)
    private PositionType positionType;

	public Integer getPositionID() {
		return positionID;
	}

	public void setPositionID(Integer positionID) {
		this.positionID = positionID;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getPositionTypeID() {
		return positionTypeID;
	}

	public void setPositionTypeID(String positionTypeID) {
		this.positionTypeID = positionTypeID;
	}

	

	
}
