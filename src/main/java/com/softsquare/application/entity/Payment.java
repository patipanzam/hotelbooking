package com.softsquare.application.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PAYMENT")
public class Payment extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2935573597352150566L;
	
	@Id
	@GeneratedValue
	@Column(name = "PAYMENTID")
    private Integer paymentID;
	

	@Column(name = "UDLOGINNAME") 
    private String udloginname;
	
	@Column(name = "TIME")
    private Date time;
	
	@Column(name = "AMOUNT")
    private String sumPrice;
	
	@Column(name = "BANK")
    private String bank;
	
	@Column(name = "BOOKINGID")
    private Integer bookingID;
	
	

	public Integer getPaymentID() {
		return paymentID;
	}

	public void setPaymentID(Integer paymentID) {
		this.paymentID = paymentID;
	}

	public String getUdloginname() {
		return udloginname;
	}

	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	

	public String getSumPrice() {
		return sumPrice;
	}

	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}

	

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public Integer getBookingID() {
		return bookingID;
	}

	public void setBookingID(Integer bookingID) {
		this.bookingID = bookingID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}





}
