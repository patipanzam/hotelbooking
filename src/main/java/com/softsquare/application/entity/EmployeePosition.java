package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employeeposition")
public class EmployeePosition extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "employeePositionID")
	private Integer employeePositionID;

	@Column(name = "skillID", unique = true, nullable = false)
	private Integer skillID;

	@Column(name = "projectID", unique = true, nullable = false)
	private Integer projectID;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "skillID", referencedColumnName = "skillID", insertable=false, updatable=false)
    private Skill skill;

	public Integer getEmployeePositionID() {
		return employeePositionID;
	}

	public void setEmployeePositionID(Integer employeePositionID) {
		this.employeePositionID = employeePositionID;
	}

	public Integer getSkillID() {
		return skillID;
	}

	public void setSkillID(Integer skillID) {
		this.skillID = skillID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
