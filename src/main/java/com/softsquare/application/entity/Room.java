package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "ROOM")
public class Room extends BaseEntity implements Serializable {

	@Id
	@GeneratedValue
	@Column(name = "ROOMID")
	private Integer roomID;

	@NotEmpty
	@Column(name = "UDLOGINNAME", unique = true, nullable = false)
	private String udloginname;

	public String getUdloginname() {
		return udloginname;
	}

	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}

	@NotEmpty
	@Column(name = "ROOMBEDS")
	private String roomBeds;

	@NotEmpty
	@Column(name = "ROOMDATE")
	private String roomDate;

	@NotEmpty
	@Column(name = "ROOMPRICE")
	private String roomPrice;

	@NotEmpty
	@Column(name = "ROOMNO")
	private String roomNo;

	@NotEmpty
	@Column(name = "TYPEDETAIL")
	private String typeDetail;
	
	@NotEmpty
	@Column(name = "CAPACITY")
	private String Capacity;

	// @NotEmpty
	@Column(name = "FREEWIFI")
	private String freewifi;
	// @NotEmpty
	@Column(name = "SMOKING")
	private String smocking;
	// @NotEmpty
	@Column(name = "SERVICE")
	private String service;
	// @NotEmpty
	@Column(name = "BREAK")
	private String breaks;
	
	@Lob
	@Column(name = "IMAGE")
	private byte[] image;
	
	@NotNull
	@Column(name = "ROOMTYPENAME")
	private String roomTypeName;
	
   @NotNull
	@Column(name = "BUILDINGID", unique = true, nullable = false)
	private Integer buildingID;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BUILDINGID", referencedColumnName = "BUILDINGID", insertable = false, updatable = false)
	private Building building;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROOMTYPENAME", referencedColumnName = "ROOMTYPENAME", insertable = false, updatable = false)
	private RoomType roomType;
	
	
	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public Integer getRoomID() {
		return roomID;
	}

	public void setRoomID(Integer roomID) {
		this.roomID = roomID;
	}

	public String getRoomBeds() {
		return roomBeds;
	}

	public void setRoomBeds(String roomBeds) {
		this.roomBeds = roomBeds;
	}

	public String getRoomDate() {
		return roomDate;
	}

	public void setRoomDate(String roomDate) {
		this.roomDate = roomDate;
	}

	public String getRoomPrice() {
		return roomPrice;
	}

	public void setRoomPrice(String roomPrice) {
		this.roomPrice = roomPrice;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	

	public String getTypeDetail() {
		return typeDetail;
	}

	public void setTypeDetail(String typeDetail) {
		this.typeDetail = typeDetail;
	}

	public String getFreewifi() {
		return freewifi;
	}

	public void setFreewifi(String freewifi) {
		this.freewifi = freewifi;
	}

	public String getSmocking() {
		return smocking;
	}

	public void setSmocking(String smocking) {
		this.smocking = smocking;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getBreaks() {
		return breaks;
	}

	public void setBreaks(String breaks) {
		this.breaks = breaks;
	}

	public Integer getBuildingID() {
		return buildingID;
	}

	public void setBuildingID(Integer buildingID) {
		this.buildingID = buildingID;
	}

	
	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getCapacity() {
		return Capacity;
	}

	public void setCapacity(String capacity) {
		Capacity = capacity;
	}

	

	
	
	
	

	
}
