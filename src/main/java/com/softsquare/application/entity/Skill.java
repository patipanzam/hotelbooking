package com.softsquare.application.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "skill")
public class Skill implements Serializable {

	private static final long serialVersionUID = 6019490232774665003L;

	@Id
	@GeneratedValue
	@Column(name = "skillID")
	private Integer skillID;

	@Column(name = "salary", unique = true, nullable = false)
	private Integer salary;

	@Column(name = "employeeID", nullable = false)
	private Integer employeeID;

	@Column(name = "positionID", unique = true, nullable = false)
	private Integer positionID;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employeeID", referencedColumnName = "employeeID", insertable = false, updatable = false)
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "positionID", referencedColumnName = "positionID", insertable = false, updatable = false)
	private Position position;

	public Integer getSkillID() {
		return skillID;
	}

	public void setSkillID(Integer skillID) {
		this.skillID = skillID;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public Integer getPositionID() {
		return positionID;
	}

	public void setPositionID(Integer positionID) {
		this.positionID = positionID;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
