package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class PositionTypeMapping extends GridRecord{

	private Integer positionTypeID;
	private String positionTypeName;

	public Integer getPositionTypeID() {
		return positionTypeID;
	}

	public void setPositionTypeID(Integer positionTypeID) {
		this.positionTypeID = positionTypeID;
	}

	public String getPositionTypeName() {
		return positionTypeName;
	}

	public void setPositionTypeName(String positionTypeName) {
		this.positionTypeName = positionTypeName;
	}

}
