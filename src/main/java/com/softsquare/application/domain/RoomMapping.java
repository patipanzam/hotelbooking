package com.softsquare.application.domain;

public class RoomMapping {
	
	private Integer roomID;
	private String roomBeds;
	private String roomDate;
	private String roomPrice;
	private String roomNo;
	private String typeDetail;
    private String udloginname;
	private String updateUser;
	private String beds;
	private String freewifi;
	private String smocking;
	private String service;
	private String breaks;
	private String building;
	private Integer buildingID;
	private String roomTypeName;
	private String image;
	private byte[] imageByte;
	private String fromDate;
	private String  toDate;
	private String Capacity;
    private String rate;
    private String price;
    private String sumPrice;
    private Integer max;
	
	public Integer getRoomID() {
		return roomID;
	}
	public void setRoomID(Integer roomID) {
		this.roomID = roomID;
	}
	public String getRoomBeds() {
		return roomBeds;
	}
	public void setRoomBeds(String roomBeds) {
		this.roomBeds = roomBeds;
	}
	public String getRoomDate() {
		return roomDate;
	}
	public void setRoomDate(String roomDate) {
		this.roomDate = roomDate;
	}
	public String getRoomPrice() {
		return roomPrice;
	}
	public void setRoomPrice(String roomPrice) {
		this.roomPrice = roomPrice;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public String getTypeDetail() {
		return typeDetail;
	}
	public void setTypeDetail(String typeDetail) {
		this.typeDetail = typeDetail;
	}
	public String getUdloginname() {
		return udloginname;
	}
	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getBeds() {
		return beds;
	}
	public void setBeds(String beds) {
		this.beds = beds;
	}
	public String getFreewifi() {
		return freewifi;
	}
	public void setFreewifi(String freewifi) {
		this.freewifi = freewifi;
	}
	public String getSmocking() {
		return smocking;
	}
	public void setSmocking(String smocking) {
		this.smocking = smocking;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getBreaks() {
		return breaks;
	}
	public void setBreaks(String breaks) {
		this.breaks = breaks;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public Integer getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(Integer buildingID) {
		this.buildingID = buildingID;
	}
	public String getRoomTypeName() {
		return roomTypeName;
	}
	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}
	public byte[] getImageByte() {
		return imageByte;
	}
	public void setImageByte(byte[] imageByte) {
		this.imageByte = imageByte;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCapacity() {
		return Capacity;
	}
	public void setCapacity(String capacity) {
		Capacity = capacity;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getSumPrice() {
		return sumPrice;
	}
	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}
	public Integer getMax() {
		return max;
	}
	public void setMax(Integer max) {
		this.max = max;
	}

}
