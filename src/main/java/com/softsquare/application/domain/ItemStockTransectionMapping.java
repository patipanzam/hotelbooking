package com.softsquare.application.domain;

public class ItemStockTransectionMapping {

	private Integer itemStockTransectionID;
	private Integer quantity;
	private String statusTransection;
	private String date;
	private Integer materialID;
	private Integer projectID;

	public Integer getItemStockTransectionID() {
		return itemStockTransectionID;
	}

	public void setItemStockTransectionID(Integer itemStockTransectionID) {
		this.itemStockTransectionID = itemStockTransectionID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStatusTransection() {
		return statusTransection;
	}

	public void setStatusTransection(String statusTransection) {
		this.statusTransection = statusTransection;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
