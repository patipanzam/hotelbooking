package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class MaterialTypeMapping extends GridRecord{

	private Integer materialTypeID;
	private String materialTypeName;

	public Integer getMaterialTypeID() {
		return materialTypeID;
	}

	public void setMaterialTypeID(Integer materialTypeID) {
		this.materialTypeID = materialTypeID;
	}

	public String getMaterialTypeName() {
		return materialTypeName;
	}

	public void setMaterialTypeName(String materialTypeName) {
		this.materialTypeName = materialTypeName;
	}

}
