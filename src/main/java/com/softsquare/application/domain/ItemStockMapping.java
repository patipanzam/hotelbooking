package com.softsquare.application.domain;

public class ItemStockMapping {

	private Integer itemStockID;
	private Integer quantity;
	private Integer materialID;
	private Integer projectID;

	public Integer getItemStockID() {
		return itemStockID;
	}

	public void setItemStockID(Integer itemStockID) {
		this.itemStockID = itemStockID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
