package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class RoomTypeMapping extends GridRecord {
	
	
	private Integer roomID;
	private String roomTypeName;
	private Integer promotionID;
	private String udloginname;
	private String rate;
	private Integer max;
	
	public Integer getRoomID() {
		return roomID;
	}
	public void setRoomID(Integer roomID) {
		this.roomID = roomID;
	}
	public String getRoomTypeName() {
		return roomTypeName;
	}
	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}
	public String getUdloginname() {
		return udloginname;
	}
	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}
	public Integer getPromotionID() {
		return promotionID;
	}
	public void setPromotionID(Integer promotionID) {
		this.promotionID = promotionID;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public Integer getMax() {
		return max;
	}
	public void setMax(Integer max) {
		this.max = max;
	}
	
	
	
	

}
