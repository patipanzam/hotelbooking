package com.softsquare.application.domain;

public class RequestOrderMapping {

	private Integer requestOrderID;
	private String requestOrderNO;
	private String date;
	private String statusAprove;
	private Integer projectID;

	public Integer getRequestOrderID() {
		return requestOrderID;
	}

	public void setRequestOrderID(Integer requestOrderID) {
		this.requestOrderID = requestOrderID;
	}

	public String getRequestOrderNO() {
		return requestOrderNO;
	}

	public void setRequestOrderNO(String requestOrderNO) {
		this.requestOrderNO = requestOrderNO;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatusAprove() {
		return statusAprove;
	}

	public void setStatusAprove(String statusAprove) {
		this.statusAprove = statusAprove;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
