package com.softsquare.application.domain;

public class StockInItemMapping {

	private Integer stockInItemID;
	private String quantity;
	private String materialID;
	private String stockInID;

	public Integer getStockInItemID() {
		return stockInItemID;
	}

	public void setStockInItemID(Integer stockInItemID) {
		this.stockInItemID = stockInItemID;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getMaterialID() {
		return materialID;
	}

	public void setMaterialID(String materialID) {
		this.materialID = materialID;
	}

	public String getStockInID() {
		return stockInID;
	}

	public void setStockInID(String stockInID) {
		this.stockInID = stockInID;
	}

}
