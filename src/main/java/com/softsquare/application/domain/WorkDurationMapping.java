package com.softsquare.application.domain;

public class WorkDurationMapping {

	private Integer workDurationID;
	private Integer duration;
	private String dateFrom;
	private String dateTo;
	private Integer totalSarary;
	private Integer employeePositionID;
	private Integer projectID;

	public Integer getWorkDurationID() {
		return workDurationID;
	}

	public void setWorkDurationID(Integer workDurationID) {
		this.workDurationID = workDurationID;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public Integer getTotalSarary() {
		return totalSarary;
	}

	public void setTotalSarary(Integer totalSarary) {
		this.totalSarary = totalSarary;
	}

	public Integer getEmployeePositionID() {
		return employeePositionID;
	}

	public void setEmployeePositionID(Integer employeePositionID) {
		this.employeePositionID = employeePositionID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
