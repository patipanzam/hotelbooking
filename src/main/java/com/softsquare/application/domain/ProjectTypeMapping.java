package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class ProjectTypeMapping extends GridRecord{

	private Integer projectTypeID;
	private String projectTypeName;
	private String projectTypeDescription;

	public Integer getProjectTypeID() {
		return projectTypeID;
	}

	public void setProjectTypeID(Integer projectTypeID) {
		this.projectTypeID = projectTypeID;
	}

	public String getProjectTypeName() {
		return projectTypeName;
	}

	public void setProjectTypeName(String projectTypeName) {
		this.projectTypeName = projectTypeName;
	}

	public String getProjectTypeDescription() {
		return projectTypeDescription;
	}

	public void setProjectTypeDescription(String projectTypeDescription) {
		this.projectTypeDescription = projectTypeDescription;
	}

}
