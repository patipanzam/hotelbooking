package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class MaterialMapping extends GridRecord{

	private Integer materialID;
	private String materialName;
	private String materialUnit;
	private Integer materialTypeID;

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public String getMaterialUnit() {
		return materialUnit;
	}

	public void setMaterialUnit(String materialUnit) {
		this.materialUnit = materialUnit;
	}

	public Integer getMaterialTypeID() {
		return materialTypeID;
	}

	public void setMaterialTypeID(Integer materialTypeID) {
		this.materialTypeID = materialTypeID;
	}

}
