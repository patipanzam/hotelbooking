package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class EmployeePositionMapping extends GridRecord{

	private Integer employeePositionID;
	private Integer skillID;
	private Integer projectID;

	public Integer getEmployeePositionID() {
		return employeePositionID;
	}

	public void setEmployeePositionID(Integer employeePositionID) {
		this.employeePositionID = employeePositionID;
	}
	
	public Integer getSkillID() {
		return skillID;
	}

	public void setSkillID(Integer skillID) {
		this.skillID = skillID;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
