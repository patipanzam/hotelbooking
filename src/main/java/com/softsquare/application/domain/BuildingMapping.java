package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class BuildingMapping extends GridRecord {
	private Integer buildingID;
    private String buildingName;
	private String buildingCode;
	private String udloginname;
	private String updateUser;
	
	
	public Integer getBuildingID() {
		return buildingID;
	}
	public void setBuildingID(Integer buildingID) {
		this.buildingID = buildingID;
	}
	
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getBuildingCode() {
		return buildingCode;
	}
	public void setBuildingCode(String buildingCode) {
		this.buildingCode = buildingCode;
	}
	public String getUdloginname() {
		return udloginname;
	}
	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
}