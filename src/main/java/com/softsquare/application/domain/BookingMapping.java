package com.softsquare.application.domain;

import java.util.Date;

import com.softsquare.application.domain.grid.GridRecord;

public class BookingMapping extends GridRecord{
	
	private Integer bookingID;
	private Integer roomID;
	private Date checkIN;
	private Date checkOUT;
	private String night;
	private String guest;
	private String sumPrice;
	private String fname;
	private String lname;
	private String email;
	private String phone;
	private String country;
	private String large_bed;
	private String late_ckeckin;
	private String high_floor;
	private String transfer;
	private String twin_bed;
    private String early_check;
    private String comment;
    private String roomTypeName;
    private String status;
    private String rooms;
    private String createUser;
    private String CancelStatus;
	
    public String getCancelStatus() {
		return CancelStatus;
	}
	public void setCancelStatus(String cancelStatus) {
		CancelStatus = cancelStatus;
	}
	public Integer getBookingID() {
		return bookingID;
	}
	public void setBookingID(Integer bookingID) {
		this.bookingID = bookingID;
	}
	public Integer getRoomID() {
		return roomID;
	}
	public void setRoomID(Integer roomID) {
		this.roomID = roomID;
	}
	
	public Date getCheckIN() {
		return checkIN;
	}
	public void setCheckIN(Date checkIN) {
		this.checkIN = checkIN;
	}
	public Date getCheckOUT() {
		return checkOUT;
	}
	public void setCheckOUT(Date checkOUT) {
		this.checkOUT = checkOUT;
	}
	public String getNight() {
		return night;
	}
	public void setNight(String night) {
		this.night = night;
	}
	public String getGuest() {
		return guest;
	}
	public void setGuest(String guest) {
		this.guest = guest;
	}
	
	public String getSumPrice() {
		return sumPrice;
	}
	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLarge_bed() {
		return large_bed;
	}
	public void setLarge_bed(String large_bed) {
		this.large_bed = large_bed;
	}
	public String getLate_ckeckin() {
		return late_ckeckin;
	}
	public void setLate_ckeckin(String late_ckeckin) {
		this.late_ckeckin = late_ckeckin;
	}
	public String getHigh_floor() {
		return high_floor;
	}
	public void setHigh_floor(String high_floor) {
		this.high_floor = high_floor;
	}
	public String getTransfer() {
		return transfer;
	}
	public void setTransfer(String transfer) {
		this.transfer = transfer;
	}
	public String getTwin_bed() {
		return twin_bed;
	}
	public void setTwin_bed(String twin_bed) {
		this.twin_bed = twin_bed;
	}
	public String getEarly_check() {
		return early_check;
	}
	public void setEarly_check(String early_check) {
		this.early_check = early_check;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getRoomTypeName() {
		return roomTypeName;
	}
	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
    
    
	
	

}
