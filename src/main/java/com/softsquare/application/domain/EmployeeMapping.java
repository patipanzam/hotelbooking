package com.softsquare.application.domain;

public class EmployeeMapping {

	// Text
	private Integer employeeID;
	private String employeeName;
	private String employeeAddress;
	private String employeePhone;
	private String employeeCitizenID;

	// Grid
	private Integer skillID;
	private Integer salary;
	private Integer positionID;

	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeAddress() {
		return employeeAddress;
	}

	public void setEmployeeAddress(String employeeAddress) {
		this.employeeAddress = employeeAddress;
	}

	public String getEmployeePhone() {
		return employeePhone;
	}

	public void setEmployeePhone(String employeePhone) {
		this.employeePhone = employeePhone;
	}

	public String getEmployeeCitizenID() {
		return employeeCitizenID;
	}

	public void setEmployeeCitizenID(String employeeCitizenID) {
		this.employeeCitizenID = employeeCitizenID;
	}

	public Integer getSkillID() {
		return skillID;
	}

	public void setSkillID(Integer skillID) {
		this.skillID = skillID;
	}

	public Integer getSalary() {
		return salary;
	}

	public void setSalary(Integer salary) {
		this.salary = salary;
	}

	public Integer getPositionID() {
		return positionID;
	}

	public void setPositionID(Integer positionID) {
		this.positionID = positionID;
	}

}
