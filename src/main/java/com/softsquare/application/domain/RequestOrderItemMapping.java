package com.softsquare.application.domain;

public class RequestOrderItemMapping {

	private Integer requestOrderItemID;
	private Integer quantity;
	private Integer materialID;
	private Integer requestOrderID;

	public Integer getRequestOrderItemID() {
		return requestOrderItemID;
	}

	public void setRequestOrderItemID(Integer requestOrderItemID) {
		this.requestOrderItemID = requestOrderItemID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getRequestOrderID() {
		return requestOrderID;
	}

	public void setRequestOrderID(Integer requestOrderID) {
		this.requestOrderID = requestOrderID;
	}

}
