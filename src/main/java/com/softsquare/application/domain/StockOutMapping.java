package com.softsquare.application.domain;

public class StockOutMapping {

	private Integer stockOutID;
	private String stockOutNO;
	private String date;
	private Integer projectID;

	public Integer getStockOutID() {
		return stockOutID;
	}

	public void setStockOutID(Integer stockOutID) {
		this.stockOutID = stockOutID;
	}

	public String getStockOutNO() {
		return stockOutNO;
	}

	public void setStockOutNO(String stockOutNO) {
		this.stockOutNO = stockOutNO;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
