package com.softsquare.application.domain;

import java.util.Date;

public class PaymentMapping {
	
	private Integer paymentID;
	private String udloginname;
    private Date time;
    private String sumPrice;
    private Integer bookingID;
	private String bank;
    
    public Integer getPaymentID() {
		return paymentID;
	}
	public void setPaymentID(Integer paymentID) {
		this.paymentID = paymentID;
	}
	public String getUdloginname() {
		return udloginname;
	}
	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getSumPrice() {
		return sumPrice;
	}
	public void setSumPrice(String sumPrice) {
		this.sumPrice = sumPrice;
	}
	public Integer getBookingID() {
		return bookingID;
	}
	public void setBookingID(Integer bookingID) {
		this.bookingID = bookingID;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	
    
    

}
