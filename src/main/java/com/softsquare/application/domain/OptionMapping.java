package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class OptionMapping  extends GridRecord{
	
	
	private Integer optionID;
	private String udloginname;
	private String updateUser;
	private String optionName;
	private Integer optionPrice;
	
	
	public Integer getOptionID() {
		return optionID;
	}
	public void setOptionID(Integer optionID) {
		this.optionID = optionID;
	}
	public String getUdloginname() {
		return udloginname;
	}
	public void setUdloginname(String udloginname) {
		this.udloginname = udloginname;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public Integer getOptionPrice() {
		return optionPrice;
	}
	public void setOptionPrice(Integer optionPrice) {
		this.optionPrice = optionPrice;
	}
	
	
	
	
	
	

}
