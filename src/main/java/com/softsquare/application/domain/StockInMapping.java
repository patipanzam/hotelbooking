package com.softsquare.application.domain;

public class StockInMapping {

	private Integer stockInID;
	private String stockInNO;
	private String date;
	private String supplier;
	private Integer totalPrice;
	private Integer vat;
	private Integer totalVat;
	private Integer projectID;

	public Integer getStockInID() {
		return stockInID;
	}

	public void setStockInID(Integer stockInID) {
		this.stockInID = stockInID;
	}

	public String getStockInNO() {
		return stockInNO;
	}

	public void setStockInNO(String stockInNO) {
		this.stockInNO = stockInNO;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public Integer getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getVat() {
		return vat;
	}

	public void setVat(Integer vat) {
		this.vat = vat;
	}

	public Integer getTotalVat() {
		return totalVat;
	}

	public void setTotalVat(Integer totalVat) {
		this.totalVat = totalVat;
	}

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

}
