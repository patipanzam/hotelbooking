package com.softsquare.application.domain;

public class ProjectMapping {

	private Integer projectID;
	private String projectName;
	private String projectLocation;
	private String projectDateStart;
	private String projectDateEnd;
	private Integer projectBudget;
	private String projectDescription;
	private Integer projectTypeID;
	private Integer branchID;
	
	// Grid
	private Integer employeePositionID;
	private Integer skillID;

	public Integer getProjectID() {
		return projectID;
	}

	public void setProjectID(Integer projectID) {
		this.projectID = projectID;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectLocation() {
		return projectLocation;
	}

	public void setProjectLocation(String projectLocation) {
		this.projectLocation = projectLocation;
	}

	public String getProjectDateStart() {
		return projectDateStart;
	}

	public void setProjectDateStart(String projectDateStart) {
		this.projectDateStart = projectDateStart;
	}

	public String getProjectDateEnd() {
		return projectDateEnd;
	}

	public void setProjectDateEnd(String projectDateEnd) {
		this.projectDateEnd = projectDateEnd;
	}

	public Integer getProjectBudget() {
		return projectBudget;
	}

	public void setProjectBudget(Integer projectBudget) {
		this.projectBudget = projectBudget;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public Integer getProjectTypeID() {
		return projectTypeID;
	}

	public void setProjectTypeID(Integer projectTypeID) {
		this.projectTypeID = projectTypeID;
	}

	public Integer getBranchID() {
		return branchID;
	}

	public void setBranchID(Integer branchID) {
		this.branchID = branchID;
	}

	public Integer getEmployeePositionID() {
		return employeePositionID;
	}

	public void setEmployeePositionID(Integer employeePositionID) {
		this.employeePositionID = employeePositionID;
	}

	public Integer getSkillID() {
		return skillID;
	}

	public void setSkillID(Integer skillID) {
		this.skillID = skillID;
	}

}
