package com.softsquare.application.domain;

public class StockOutItemMapping {

	private Integer stockOutItemID;
	private Integer quantity;
	private Integer materialID;
	private Integer stockOutID;

	public Integer getStockOutItemID() {
		return stockOutItemID;
	}

	public void setStockOutItemID(Integer stockOutItemID) {
		this.stockOutItemID = stockOutItemID;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getMaterialID() {
		return materialID;
	}

	public void setMaterialID(Integer materialID) {
		this.materialID = materialID;
	}

	public Integer getStockOutID() {
		return stockOutID;
	}

	public void setStockOutID(Integer stockOutID) {
		this.stockOutID = stockOutID;
	}

}
