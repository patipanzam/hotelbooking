package com.softsquare.application.domain;

import com.softsquare.application.domain.grid.GridRecord;

public class PositionMapping extends GridRecord{

	private Integer positionID;
	private String positionName;
	private String positionTypeID;

	public Integer getPositionID() {
		return positionID;
	}

	public void setPositionID(Integer positionID) {
		this.positionID = positionID;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getPositionTypeID() {
		return positionTypeID;
	}

	public void setPositionTypeID(String positionTypeID) {
		this.positionTypeID = positionTypeID;
	}

}
