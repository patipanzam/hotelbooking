<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="webjars/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="webjars/jquery/2.1.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<body>
	<a download id="downloadFile" hidden></a>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="http://${ipDomainSystem}/hotelbooking/userHome.html"><i
					class="fa fa-home fa-fw" style="font-size: 24px"></i>HOTEL BOOKING
					SYSTEM</a>
			</div>

			<c:choose>
				<c:when test="${roleUserSystem =='user'}">

					<ul class="nav navbar-nav">
						<li><a
							href="http://${ipDomainSystem}/hotelbooking/feedback.html"><i
								class="fa fa-commenting-o" style="font-size: 18px"> Give
									system feedback</i></a></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#"><i class="fa fa-cc-visa"
								style="font-size: 20px">Payment</i> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/payment.html"><i
										class="fa fa-file" style="font-size: 18px">Payment Form</i></a></li>


							</ul></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#"><i
								class="glyphicon glyphicon-user" style="font-size: 16px">${userNameUserSystem}</i>
								<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/profile.html"><i
										class="glyphicon glyphicon-user" style="font-size: 16px">Profile</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/editprofile.html"><i
										class="glyphicon glyphicon-user" style="font-size: 16px">EditProfile</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/cancelBooking.html"><i
										class="glyphicon glyphicon-user" style="font-size: 16px">My booking</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/logoutpage.html">
										<button type="button" class="btn btn-default btn-sm">
											<span class="glyphicon glyphicon-log-out"></span> Logout
										</button>
								</a></li>
							</ul></li>
					</ul>

				</c:when>


				<c:when test="${roleUserSystem =='administrator'}">
					<ul class="nav navbar-nav">
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#"><i
								class="fa fa-folder-open-o" style="font-size: 18px">Manage
									Room</i> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/buildingpage.html"><i
										class="fa fa-plus-square" style="font-size: 18px">Add
											Building</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/roomtype.html"><i
										class="fa fa-plus-square" style="font-size: 18px">Add
											RoomType</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/room.html"><i
										class="fa fa-plus-square" style="font-size: 18px">Add Room</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/showRoom.html"><i
										class="fa fa-list" style="font-size: 18px">List Room</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/option.html"><i
										class="fa fa-cart-plus" style="font-size: 18px">Option of
											Room</i></a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#"><i class="fa fa-book"
								style="font-size: 18px">Manage Booking</i> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/branch.html"><i
										class="fa fa-list" style="font-size: 18px">Check
											Payment</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/checkCancelBooking.html"><i
										class="fa fa-list" style="font-size: 18px">Check
											Cancel Booking</i></a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#"><i
								class="fa fa-folder-open-o" style="font-size: 18px">Manage
									Promotion</i> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/promotion.html"><i
										class="fa fa-plus-square" style="font-size: 18px">Create
											Promotion</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/showPromotion.html"><i
										class="fa fa-list" style="font-size: 18px">List Promotion</i></a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#"><i class="fa fa-print"
								style="font-size: 18px">Report</i> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/report.html?reportName=report01"><i
										class="fa fa-file-text" style="font-size: 18px">Report01</i></a></li>
							</ul></li>
						<li class="dropdown"><a class="dropdown-toggle"
							data-toggle="dropdown" href="#"><i class="fa fa-group"
								style="font-size: 18px">Manage User</i> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/register.html"><i
										class="fa fa-plus-square" style="font-size: 18px">Add User</i></a></li>
								<li><a
									href="http://${ipDomainSystem}/hotelbooking/showRegister.html"><i
										class="fa fa-list" style="font-size: 18px">List User</i></a></li>
							</ul></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"><i
						class="glyphicon glyphicon-user" style="font-size: 16px">${userNameUserSystem}</i>
						<span class="caret"></span></a>
					<ul class="dropdown-menu">
			

						<li><a
							href="http://${ipDomainSystem}/hotelbooking/logoutpage.html">
								<button type="button" class="btn btn-default btn-sm">
									<span class="glyphicon glyphicon-log-out"></span> Logout
								</button>
						</a></li>
					</ul></li>
			</ul>
				</c:when>
			</c:choose>






			
			<!--    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
	        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Language
	        <span class="caret"></span></a>
	        <ul class="dropdown-menu">
	          <li><a href="?lang=en">English</a></li>
	          <li><a href="?lang=th">Thai</a></li>
	        </ul>
	      </li>
      </ul>
	 -->
		</div>
	</nav>

</body>
</html>