<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<link rel="stylesheet"
	href="webjars/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="webjars/jquery/2.1.1/jquery.min.js"></script>
<script src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<body>
	<a download id="downloadFile" hidden></a>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand">Hotel Booking</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="http://${ipDomainSystem}/hotelbooking/home.html">เลือกโครงการ</a></li>
				<li><a href="http://${ipDomainSystem}/hotelbooking/projectInfoSecretary.html">ข้อมูลโครงการ</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">จัดการวัสดุ <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="http://${ipDomainSystem}/hotelbooking/stockIN.html">รับวัสดุ</a></li>
						<li><a href="http://${ipDomainSystem}/hotelbooking/stockOUT.html">เบิกวัสดุ</a></li>
						<li><a href="http://${ipDomainSystem}/hotelbooking/stockINList.html">ประวัติรับวัสดุ</a></li>
						<li><a href="http://${ipDomainSystem}/hotelbooking/stockOUTList.html">ประวัติเบิกวัสดุ</a></li>
					</ul></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">จัดการเวลาทำงาน<span
						class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a >เพิ่มเวลาทำงาน</a></li>
						<li><a >ประวัติเวลาทำงาน</a></li>
					</ul></li>
				<li><a >รายได้พนักงาน</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="http://${ipDomainSystem}/hotelbooking/logoutpage.html">Logout</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#">User Data <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a>User Name : ${userNameUserSystem}</a></li>
						<li><a>User Role : ${roleUserSystem}</a></li>
						<li><a>EmployeeName : ${employeeNameUserSystem}</a></li>
					</ul></li>
			</ul>
			<!--    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
	        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Language
	        <span class="caret"></span></a>
	        <ul class="dropdown-menu">
	          <li><a href="?lang=en">English</a></li>
	          <li><a href="?lang=th">Thai</a></li>
	        </ul>
	      </li>
      </ul>
	 -->
		</div>
	</nav>

</body>
</html>