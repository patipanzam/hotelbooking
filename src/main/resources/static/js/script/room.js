//checkFormValid = function() {
//	return !!room.formPanel.getForm().isValid();
//}
saveFunction = function() {
	if (checkIsDirty()) {
		
		var arstr = room.panel.el.dom.innerHTML.split('base64,');
		var arstr2 = arstr[1].split('</div>');
		var arstr3 = arstr2[0].split('">');
		var arstr4 = arstr3[0].split('" style');
		var imageUpload = arstr4[0];
		console.log(imageUpload);
		var params = {
				
				  method: 'save'
				    , roomID : room.roomID.getValue()
			        ,roomBeds: room.roomBeds.getValue()
			        ,roomDate: room.roomDate.getRawValue()
			        ,freewifi: room.freewifi.getValue()
			        ,smocking: room.smocking.getValue()
			        ,service: room.service.getValue()
			        ,breaks: room.breaks.getValue()
			        ,beds: room.roomBeds.getValue().inputValue
		            ,roomPrice: room.roomPrice.getValue()
		            ,roomNo: room.roomNo.getValue()
		            ,Capacity: room.Capacity.getValue()
		            ,typeDetail: room.typeDetail.getValue()
		            ,buildingID:room.buildingCombobox.getValue()
		            ,roomTypeName:room.roomtypeCombobox.getValue()
		            ,image:imageUpload
		           
		
		          
		       };
		
		Ext.Ajax.request({
			method : 'POST',
			params : params,
			url : room.url,
			success : function(response){
				var json = Ext.decode(response.responseText);
				Ext.Msg.alert('Save Success', 'Save Success');
				room.roomID.setValue(json.records.roomID);
				selectFunction();
			},
			failure : function(response) {
			}
		});
	} else {
		Ext.Msg.alert('Save Failure', 'Save Failure');
	}
}
		
		

selectFunction = function() {
	var params = {
			method : 'select',
			xaction : 'read'
		};
		params.roomID = room.roomID.getValue()
		Ext.Ajax
				.request({
					method : 'POST',
					params : params,
					url : room.url,
					success : function(response) {
						var json = Ext.decode(response.responseText);
						room.roomID.setValue(json.records.roomID);
						room.roomID.originalValue = json.records.roomID;
						room.roomBeds.setValue(json.records.roomBeds);
						room.roomBeds.originalValue = json.records.roomBeds;
						room.roomDate.setValue(json.records.roomDate);
						room.roomDate.originalValue = json.records.roomDate;
						room.freewifi.setValue(json.records.freewifi);
						room.freewifi.originalValue = json.records.freewifi;
						room.smocking.setValue(json.records.smocking);
						room.smocking.originalValue = json.records.smocking;
						room.service.setValue(json.records.service);
						room.service.originalValue = json.records.service;
						room.breaks.setValue(json.records.breaks);
						room.breaks.originalValue = json.records.breaks;
						room.roomPrice.setValue(json.records.roomPrice);
						room.roomPrice.originalValue = json.records.roomPrice;
						room.roomNo.setValue(json.records.roomNo);
						room.roomNo.originalValue = json.records.roomNo;
						room.typeDetail.setValue(json.records.typeDetail);
						room.typeDetail.originalValue = json.records.typeDetail;
						room.Capacity.setValue(json.records.Capacity);
						room.Capacity.originalValue = json.records.Capacity;
				
						console.log(response);
						room.fileupload.setValue(json.records.image);
						var img = 'data:image/jpeg;base64,'+json.records.image;
						room.panel.update('<img src='+img+' style="height:250px; width:250px"; /> ');

						room.buildingCombobox.getStore().load(
										{
											callback : function() {
												room.buildingCombobox.setValue(json.records.buildingID)
												room.buildingCombobox.originalValue = json.records.buildingID
											}
										});
						room.roomtypeCombobox.setValue(json.records.roomTypeName)
						room.roomtypeCombobox.getStore().load(
										{
											callback : function() {
												room.roomtypeCombobox.setValue(json.records.roomTypeName)
												room.roomtypeCombobox.originalValue = json.records.roomTypeName
	
											}
										});
					},
					failure : function(response) {
						Ext.Msg.alert(__messages['title.information'],
								'Please cotact admin.');
					}
				});
	}

	checkIsDirty = function() {
		var check = false;
		if ( room.roomID.isDirty()|| room.roomBeds.isDirty()
				|| room.roomDate.isDirty()
				|| room.freewifi.isDirty()
				|| room.buildingCombobox.isDirty()
				|| room.roomtypeCombobox.isDirty() 
			    || room.smocking.isDirty()
			    || room.service.isDirty()
			    || room.breaks.isDirty()
			    || room.roomPrice.isDirty()
			    || room.roomNo.isDirty()
			    || room.image.isDirty()
				|| room.typeDetail.isDirty()){
		
			check = true;
		}
		return check;
	};

	Ext.onReady(function() {
		if (!BeanUtils.isEmpty(headerId)) {
			room.roomID.setValue(headerId);
			selectFunction();
		}
	});



















	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	
//	saveFunction = function(){
//	var idCheck = room.roomID.getValue();
//	
//	if (idCheck == null) {	
//		 var params = {
//				  method: 'save'
//			        ,roomBeds: room.roomBeds.getValue()
//			        ,roomDate: room.roomDate.getValue()
//			        ,freewifi: room.freewifi.getValue()
//			        ,smocking: room.smocking.getValue()
//			        ,service: room.service.getValue()
//			        ,breaks: room.breaks.getValue()
//			        ,beds: room.roomBeds.getValue().inputValue
//		            ,roomPrice: room.roomPrice.getValue()
//		            ,roomPromotion: room.roomPromotion.getValue()
//		            ,roomNo: room.roomNo.getValue()
//		            ,roomService: room.roomService.getValue()
//		            ,roomStatus: room.roomStatus.getValue()
////		            ,roomStatus:  room.guestCombo .getValue()
//		            ,typeName: room.typeName.getValue()
//		            ,typeDetail: room.typeDetail.getValue()
//		           
//		 };
//		
//		Ext.Ajax.request({
//	        method: 'POST',
//	        params: params,
//	        url: room.url,
//	        success: function (response) {
//	        	alert('Save Success');
//	        	
//	        	nextfunction();
//	        },
//	        failure: function (response) {
//	        	alert('Save Failure');
//	        }
//	    });
//	}else {
//		 var params = {
//				  method: 'update'
//					,roomID: room.roomID.getValue()
//					,udloginname:room.udloginname.getValue()
//					,beds: room.roomBeds.getValue().inputValue
//					,updateUser:room.updateUser.getValue
//					,freewifi: room.freewifi.getValue()
//			        ,smocking: room.smocking.getValue()
//			        ,service: room.service.getValue()
//			        ,breaks: room.breaks.getValue()
//			        ,roomBeds: room.roomBeds.getValue()
//			        ,roomDate: room.roomDate.getValue()
//		            ,roomPrice: room.roomPrice.getValue()
//		            ,roomNo: room.roomNo.getValue()
//		            ,typeName: room.typeName.getValue()
//		            ,typeDetail: room.typeDetail.getValue()
//		            ,roomPromotion: room.roomPromotion.getValue()
//		            ,roomService: room.roomService.getValue()
//		            ,roomStatus: room.roomStatus.getValue()
//		           
//		 };
//		
//		Ext.Ajax.request({
//	        method: 'POST',
//	        params: params,
//	        url: room.url,
//	        success: function (response) {
//	        	alert('Update Success');
//	        	
//	        	nextfunction();
//	        },
//	        failure: function (response) {
//	        	alert('Update Failure');
//	        }
//	    });
//	}	
//	
//	
//}
//
//nextfunction = function() {
//	console.log("tes")
//	var idRoom = room.roomID.getValue();
//	var params = {
//		id : idRoom,
//		method : 'viewroom'
//	};
//	FormHelper.submitTo(application.contextPath + '/listroom.html', params);
//}
//
//cancelFunction = function() {
//	console.log("tes")
//	var idRoom = room.roomID.getValue();
//	var params = {
//		id : idRoom,
//		method : 'viewad'
//	};
//	FormHelper.submitTo(application.contextPath + '/admin.html', params);
//}
//
//goFunction = function() {
//	console.log("tes")
//	var idRoom = room.roomID.getValue();
//	var params = {
//		id : idRoom,
//		method : 'viewlist'
//	};
//	FormHelper.submitTo(application.contextPath + '/listroom.html', params);
//}
//
//EditRoomForm = function(){
//	
//	room.roomID.setValue(roomvalue.roomID);
//	room.roomBeds.setValue(roomvalue.beds);
////	room.roomFacilities.setValue(roomvalue.roomFacilities);
//	room.freewifi.setValue(roomvalue.freewifi);
//	room.smocking.setValue(roomvalue.smocking);
//	room.service.setValue(roomvalue.service);
//	room.breaks.setValue(roomvalue.breaks);
//	room.roomDate.setValue(roomvalue.roomDate);
//	room.roomPrice.setValue(roomvalue.roomPrice);
//	room.roomPromotion.setValue(roomvalue.roomPromotion);
//	room.roomService.setValue(roomvalue.roomService);
//	room.roomStatus.setValue(roomvalue.roomStatus);
//	room.roomNo.setValue(roomvalue.roomNo);
//	room.typeName.setValue(roomvalue.typeName);
//	room.typeDetail.setValue(roomvalue.typeDetail);
//	room.udloginname.setValue(roomvalue.udloginname);
//	room.updateUser.setValue(roomvalue.updateUser);
//}
//Ext.onReady(function () {
//	EditRoomForm();
//	
//});
