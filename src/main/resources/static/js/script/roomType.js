selectFunction = function() {
	bdp.grid.load({
		xaction : 'read',
		method : 'selectGrid'
	}, function() {
	});
};

saveFunction = function() {
	if (checkIsDirty()) {
		
	if (bdp.grid.getStore().isValid()) {
			var params = {
					method : 'saveroomtype'
			,udloginname:bdp.udloginname.getValue()
			
			};
			GridConvert.save(bdp.grid, params, function() {
				selectFunction();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction = function() {
	bdp.grid.remove();
};





searchFunction = function() {
	var params = {
		SearchPositionType : Ext.getCmp('SearchPositionType').getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : bdp.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			bdp.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};



checkIsDirty = function() {
	var check = false;
	if (bdp.grid.isDirty()) {
		check = true;
	}
	return check;
};



Ext.onReady(function() {
	selectFunction();
	
});

