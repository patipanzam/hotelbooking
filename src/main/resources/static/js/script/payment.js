saveFunction = function() {
	if (checkIsDirty()) {
		var params = {
				
				
				  method: 'save'
				    ,paymentID:HBPMRT02.paymentID.getValue()
			        ,time: HBPMRT02.time.getRawValue()
			        ,scb:HBPMRT02.scb.getValue()
			        ,ktb:HBPMRT02.ktb.getValue()
			        ,bkb:HBPMRT02.bkb.getValue()
		            ,katb:HBPMRT02.katb.getValue()
		            ,amount:HBPMRT02.amount.getValue()
		           
		          
		       };
		
		Ext.Ajax.request({
			method : 'POST',
			params : params,
			url : HBPMRT02.url,
			success : function(response){
				var json = Ext.decode(response.responseText);
				Ext.Msg.alert('Save Success', 'Save Success');
			},
			failure : function(response) {
			}
		});
	} else {
		Ext.Msg.alert('Save Failure', 'Save Failure');
	}
}


checkIsDirty = function() {
	var check = false;
	if ( HBPMRT02.paymentID.isDirty()|| HBPMRT02.time.isDirty()
			|| HBPMRT02.amount.isDirty()
			|| HBPMRT02.scb.isDirty()
			|| HBPMRT02.ktb.isDirty() 
		    || HBPMRT02.bkb.isDirty()
		    || HBPMRT02.katb.isDirty()){
	
		check = true;
	}
	return check;
};