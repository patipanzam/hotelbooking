selectFunction = function() {

	var params = {
		id : pro.projectID.getValue(),
		method : 'select',

	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : application.contextPath + '/projectInfoEngineer.html',
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			pro.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});

};

setShowData = function() {

	pro.projectID.setValue(ordersetvalue.projectID)
	pro.projectName.setValue(ordersetvalue.projectName)
	pro.projectLocation.setValue(ordersetvalue.projectLocation)
	pro.projectDateStart.setValue(ordersetvalue.projectDateStart)
	pro.projectDateEnd.setValue(ordersetvalue.projectDateEnd)
	pro.projectBudget.setValue(ordersetvalue.projectBudget)
	pro.projectDescription.setValue(ordersetvalue.projectDescription)
	pro.projectType.setValue(ordersetvalue.projectTypeID)
	pro.projectBranch.setValue(ordersetvalue.branchID)

}

Ext.onReady(function() {
	setShowData();
	selectFunction();

});
