saveFunction = function() {
	if (checkIsDirty()) {
		if (emp.employeeName.isValid() && emp.employeeCitizenID.isValid() && emp.grid.getStore().isValid()) {
			var params = {
					buildingID :   emp.buildingID.getValue(),
					buildingName : emp.buildingName.getValue(),
					buildingCode : emp.buildingCode.getValue(),
				
				method : 'saveEmployee'
			};
			
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

emp.checkPositionData = function(e){
	var rw = e.row;
	if(BeanUtils.equals('positionID', e.field)){
		$.each(emp.grid.store.data.items, function(i) {
			if(!BeanUtils.equals(rw, i)){
				 $.each(emp.grid.store.data.items[i].data, function(key,val) {
					  if(BeanUtils.equals('positionID', key)){
						  if(BeanUtils.equals(e.value, val)){
								Ext.Msg.alert(__messages['title.information'],'Duplicate Position data.'
										, function(){
									emp.grid.store.data.itemAt(rw).data.positionID = '';
									emp.grid.getView().refresh();
								} );
						  }
					  }
				  });
			}
		});
	}else if(BeanUtils.equals('positionTypeID', e.field)){
		if(emp.grid.isDirty()){
			emp.grid.store.data.itemAt(rw).data.positionID = '';
			emp.grid.store.data.itemAt(emp.grid.selModel.lastActive).data.positionName = '';
			emp.grid.getView().refresh();
			
		}
	}
}

selectGridFunction = function() {
	emp.grid.load({
		xaction : 'read',
		method : 'selectGrid',
		id : emp.employeeID.getValue()
	}, function() {
	});
};

selectFunction = function() {
	var params = {
		method : 'select',
		xaction : 'read'
	};
	params.employeeID = emp.employeeID.getValue()
	Ext.Ajax
			.request({
				method : 'POST',
				params : params,
				url : emp.url,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					emp.employeeID.setValue(json.records[0].employeeID);
					emp.employeeID.originalValue = json.records[0].employeeID;
					emp.employeeName.setValue(json.records[0].employeeName);
					emp.employeeName.originalValue = json.records[0].employeeName;
					emp.employeeAddress.setValue(json.records[0].employeeAddress);
					emp.employeeAddress.originalValue = json.records[0].employeeAddress;
					emp.employeePhone.setValue(json.records[0].employeePhone);
					emp.employeePhone.originalValue = json.records[0].employeePhone;
					emp.employeeCitizenID.setValue(json.records[0].employeeCitizenID);
					emp.employeeCitizenID.originalValue = json.records[0].employeeCitizenID;
				},
				failure : function(response) {
					Ext.Msg.alert(__messages['title.information'],
							'Please cotact admin.');
				}
			});
}

checkIsDirty = function() {
	var check = false;
	if (emp.employeeName.isDirty() || emp.employeeAddress.isDirty()
			|| emp.employeePhone.isDirty() || emp.employeeCitizenID.isDirty()
			|| emp.grid.isDirty()) {
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	if (!BeanUtils.isEmpty(headerId)) {
		emp.employeeID.setValue(headerId);
		selectFunction();
		selectGridFunction();
	}
});
