selectFunction = function() {
	var params = {
		method : 'select',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : pro.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			pro.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

removeFunction = function() {

	Ext.Msg.confirm("Confirmation", "Confirm delete?", function(btnText) {
		if (btnText === "no") {
		} else if (btnText === "yes") {

			var selects = pro.grid.getSelectionModel().getSelections();
			var count = selects.length;
			var id = new Array();

			for (var i = 0; i < count; i++) {
				id[i] = selects[i].data.projectID;
				pro.grid.getStore().remove(selects[i]);
			}
			var code = new Array();
			code = id;

			var params = {
				code : id,
				method : 'removeProject'
			};
			Ext.Ajax.request({
				method : 'POST',
				params : params,
				url : pro.url,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					selectFunction();
				},
				failure : function(response) {
				}
			});
		}
	}, this);
};

function renderInstall(value, metaData, record, row, col, store, gridView) {
	return ('<a href="' + pro.urlEditProject + '?projectID='
			+ record.data.projectID + '" class="btn btn-info btn-xs" >แก้ไข</a>');
}

searchFunction = function() {
	var params = {
		SearchProject : Ext.getCmp('SearchProject').getValue(),
		SearchProjectTypeCombobox : Ext.getCmp('SearchProjectTypeCombobox')
				.getValue(),
		SearchBranchCombobox : Ext.getCmp('SearchBranchCombobox').getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : pro.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			pro.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

Ext.onReady(function() {
	selectFunction();
});
