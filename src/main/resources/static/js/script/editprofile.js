checkFormValid = function() {
	return !!register.formPanel.getForm().isValid();
}

saveFunction = function() {
	if (checkIsDirty()) {
		if (checkFormValid()) {
			var userPassword = register.userPassword.getValue();
			var userPasswordConfirm = register.userPasswordConfirm.getValue();

			if (BeanUtils.equals(userPassword, userPasswordConfirm)) {
				var params = {
					method : 'save',
					id : register.id.getValue(),
					userName : register.userName.getValue(),
					password : userPassword,
//					roleId : register.roleCombobox.getValue(),
                    firstName :register.firstName.getValue(),
					lastName :register.lastName.getValue(),
					email :register.email.getValue(),
					phone :register.phone.getValue(),
					roleId:'2',
				};

				Ext.Ajax.request({
					method : 'POST',
					params : params,
					url : register.url,
					success : function(response){
						var json = Ext.decode(response.responseText);
						Ext.Msg.alert('เสร็จสมบูรณ์', 'บันทึกเสร็จสมบูรณ์');
						register.id.setValue(json.records.id);
						selectFunction();
					},
					failure : function(response) {
					}
				});
			} else {
				Ext.Msg.alert('ผิดพลาด', 'รหัสผ่านไม่ตรงกัน');
			}
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
}


checkIsDirty = function() {
	var check = false;
	if ( register.id.isDirty()|| register.userName.isDirty()
			|| register.userPassword.isDirty()
			|| register.userPasswordConfirm.isDirty()
//			|| register.roleCombobox.isDirty()
            || register.firstName.isDirty()
		    || register.lastName.isDirty()
		    || register.email.isDirty()
		    || register.phone.isDirty()){
	
		check = true;
	}
	return check;
};



			

selectFunction = function() {
	var params = {
		method : 'select',
		xaction : 'read'
	};
	Ext.Ajax
			.request({
				method : 'POST',
				params : params,
				url : register.url,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					console.log(response);
					register.id.setValue(json.records.id);
					register.id.originalValue = json.records.id;
					register.userName.setValue(json.records.username);
					register.userName.originalValue = json.records.username;
					register.userPassword.setValue(json.records.password);
					register.userPassword.originalValue = json.records.password;
					register.userPasswordConfirm.setValue(json.records.password);
					register.userPasswordConfirm.originalValue = json.records.password;
					register.firstName.setValue(json.records.firstName);
					register.firstName.originalValue = json.records.firstName;
					register.lastName.setValue(json.records.lastName);
					register.lastName.originalValue = json.records.lastName;
					register.email.setValue(json.records.email);
					register.email.originalValue = json.records.email;
					register.phone.setValue(json.records.phone);
					register.phone.originalValue = json.records.phone;
//					register.roleName.setValue(json.records.roleName);
//					register.roleName.originalValue = json.records.roleName;

					register.roleCombobox.getStore().load(
									{
										callback : function() {
											register.roleCombobox.setValue(json.records.roleId)
											register.roleCombobox.originalValue = json.records.roleId
										}
									});
//					register.employeeCombobox.setValue(json.records[0].employeeID)
//					register.employeeCombobox.getStore().load(
//									{
//										callback : function() {
//											register.employeeCombobox.setValue(json.records[0].employeeID)
//											register.employeeCombobox.originalValue = json.records[0].employeeID
//
//										}
//									});
				},
				failure : function(response) {
					Ext.Msg.alert(__messages['title.information'],
							'Please cotact admin.');
				}
			});
}

checkIsDirty = function() {
	var check = false;
	if ( register.id.isDirty()|| register.userName.isDirty()
			|| register.userPassword.isDirty()
			|| register.userPasswordConfirm.isDirty()
			|| register.roleId.isDirty()
            || register.firstName.isDirty()
		    || register.lastName.isDirty()
		    || register.email.isDirty()
		    || register.phone.isDirty()){
	
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	if (!BeanUtils.isEmpty(headerId)) {
		register.id.setValue(headerId);
		}
           selectFunction();
});