selectFunction = function() {
	var params = {
		method : 'select',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : HBBMRT08.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			HBBMRT08.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};




Ext.onReady(function() {
	selectFunction();
	
});