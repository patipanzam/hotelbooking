selectFunction = function() {
	mat.grid.load({
		xaction : 'read',
		method : 'selectGrid'
	}, function() {
	});
};

saveFunction = function() {
	if (checkIsDirty()) {
		if (mat.grid.getStore().isValid()) {
			var params = {
				method : 'saveMaterialType'
			};
			GridConvert.save(mat.grid, params, function() {
				selectFunction();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction = function() {
	mat.grid.remove();
};

selectFunction2 = function() {
	mat.grid2.load({
		xaction : 'read',
		method : 'selectGrid2'
	}, function() {
	});
};

saveFunction2 = function() {
	if (checkIsDirty2()) {
		if (mat.grid2.getStore().isValid()) {
			var params = {
				method : 'saveMaterial'
			};
			GridConvert.save(mat.grid2, params, function() {
				selectFunction2();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction2 = function() {
	mat.grid2.remove();
};

searchFunction = function() {
	var params = {
		SearchMaterialType : Ext.getCmp('SearchMaterialType').getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : mat.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			mat.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

searchFunction2 = function() {
	var params = {
		SearchMaterial : Ext.getCmp('SearchMaterial').getValue(),
		SearchMaterialTypeCombobox : Ext.getCmp('SearchMaterialTypeCombobox')
				.getValue(),
		method : 'search2',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : mat.url,
		success : function(response) {
			var json2 = Ext.util.JSON.decode(response.responseText);
			mat.grid2.store.loadData(json2);
		},
		failure : function(response) {
		}
	});
};

checkIsDirty = function() {
	var check = false;
	if (mat.grid.isDirty()) {
		check = true;
	}
	return check;
};

checkIsDirty2 = function() {
	var check = false;
	if (mat.grid2.isDirty()) {
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	selectFunction();
	selectFunction2();
});
