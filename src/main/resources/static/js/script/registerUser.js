checkFormValid = function() {
	return !!register.formPanel.getForm().isValid();
}

saveFunction = function() {
	if (checkIsDirty()) {
		if (checkFormValid()) {
			var userPassword = register.userPassword.getValue();
			var userPasswordConfirm = register.userPasswordConfirm.getValue();

			if (BeanUtils.equals(userPassword, userPasswordConfirm)) {
				var params = {
					method : 'save',
					id : register.id.getValue(),
					userName : register.userName.getValue(),
					password : userPassword,
//					roleId : register.roleCombobox.getValue(),
                    firstName :register.firstName.getValue(),
					lastName :register.lastName.getValue(),
					email :register.email.getValue(),
					phone :register.phone.getValue(),
					roleId:'2',
				};

				Ext.Ajax.request({
					method : 'POST',
					params : params,
					url : register.url,
					success : function(response){
						var json = Ext.decode(response.responseText);
						Ext.Msg.alert('เสร็จสมบูรณ์', 'บันทึกเสร็จสมบูรณ์');
						register.id.setValue(json.records.id);
						selectFunction();
					},
					failure : function(response) {
					}
				});
			} else {
				Ext.Msg.alert('ผิดพลาด', 'รหัสผ่านไม่ตรงกัน');
			}
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
}


checkIsDirty = function() {
	var check = false;
	if ( register.id.isDirty()|| register.userName.isDirty()
			|| register.userPassword.isDirty()
			|| register.userPasswordConfirm.isDirty()
//			|| register.roleCombobox.isDirty()
            || register.firstName.isDirty()
		    || register.lastName.isDirty()
		    || register.email.isDirty()
		    || register.phone.isDirty()){
	
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	if (!BeanUtils.isEmpty(headerId)) {
		register.id.setValue(headerId);
		}
});
