saveFunction = function() {
	if (checkIsDirty()) {
		var params = {
				  method: 'save'
				    ,bookingID:booking.bookingID.getValue()
			        ,roomID: booking.roomID.getValue()
			        ,checkIN:booking.fromDate.getRawValue()
			        ,checkOUT:booking.toDate.getRawValue()
			        ,night:booking.date.getValue()
			        ,price:booking.sumPrice.getValue()
			        ,fname:booking.firstNameguest.getValue()
			        ,lname:booking.lastNameguest.getValue()
		            ,email:booking.emailguest.getValue()
		            ,phone: booking.phoneguest.getValue()
		            ,country:booking.guestCombo.getValue()
		            ,large_bed:booking.large_bed.getValue()
		            ,late_ckeckin:booking.late_check.getValue()
		            ,high_floor:booking.high_floor.getValue()
		            ,transfer:booking.transfer.getValue()
		            ,twin_bed:booking.twin_beds.getValue()
		            ,early_check:booking.early_check.getValue()
		            ,comment:booking.comment.getValue()
		          
		       };
		
		Ext.Ajax.request({
			method : 'POST',
			params : params,
			url : booking.url,
			success : function(response){
				var json = Ext.decode(response.responseText);
				Ext.Msg.alert('Your Bookings is Success', 'Booking Success please  sent payment');
//				    nextfunction();
			},
			failure : function(response) {
			}
		});
	} else {
		Ext.Msg.alert('Save Failure', 'Save Failure');
	}
}


//nextfunction = function() {
//	console.log("tes")
//	
//	
//	var params = {
//		method : 'viewBookdetail'
//	};
//	FormHelper.submitTo(application.contextPath + '/booking.html', params);
//}
		



selectFunction = function() {
	var params = {
			method : 'select',
			xaction : 'read'
		};
		params.roomID = booking.roomID.getValue()
		Ext.Ajax
				.request({
					method : 'POST',
					params : params,
					url : booking.url,
					success : function(response) {
						var json = Ext.decode(response.responseText);
                        booking.roomID.setValue(json.records.roomID);
						booking.roomID.originalValue = json.records.roomID;
						booking.roomPrice.setValue(json.records.roomPrice);
						booking.roomPrice.originalValue = json.records.roomPrice;
						booking.roomTypeName.setValue(json.records.roomTypeName);
						booking.roomTypeName.originalValue = json.records.roomTypeName;
						booking.sumPrice.setValue(json.records.sumPrice);
						booking.sumPrice.originalValue = json.records.sumPrice;
						booking.rate.setValue(json.records.rate);
						booking.rate.originalValue = json.records.rate;
						booking.createUser.setValue(json.records.createUser);
//						booking.fromDate.setValue(roomvalue.fromDate);
//						booking.fromDate.originalValue = roomvalue.roomTypeName;
						console.log(response);
						
						booking.fileupload.setValue(json.records.image);
						var img = 'data:image/jpeg;base64,'+json.records.image;
						booking.panel.update('<img src='+img+' style="height:250px; width:250px"; /> ');

//						
					},
					failure : function(response) {
						Ext.Msg.alert(__messages['title.information'],
								'Please cotact admin.');
					}
				});
	}

EditRoomForm = function(){
	booking.fromDate.setRawValue(roomvalue.fromDate);
}

selectFunctionUser = function() {
	var params = {
		method : 'selectus',
		xaction : 'read'
	};
	Ext.Ajax
			.request({
				method : 'POST',
				params : params,
				url : booking.url,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					console.log(response);
					booking.id.setValue(json.records.id);
					booking.id.originalValue = json.records.id;
					booking.userName.setValue(json.records.username);
					booking.userName.originalValue = json.records.username;
					booking.userPassword.setValue(json.records.password);
					booking.userPassword.originalValue = json.records.password;
					booking.userPasswordConfirm.setValue(json.records.password);
					booking.userPasswordConfirm.originalValue = json.records.password;
					booking.firstName.setValue(json.records.firstName);
					booking.firstName.originalValue = json.records.firstName;
					booking.lastName.setValue(json.records.lastName);
					booking.lastName.originalValue = json.records.lastName;
					booking.email.setValue(json.records.email);
					booking.email.originalValue = json.records.email;
					booking.phone.setValue(json.records.phone);
					booking.phone.originalValue = json.records.phone;
//					register.roleName.setValue(json.records.roleName);
//					register.roleName.originalValue = json.records.roleName;

					booking.roleCombobox.getStore().load(
									{
										callback : function() {
											booking.roleCombobox.setValue(json.records.roleId)
											booking.roleCombobox.originalValue = json.records.roleId
										}
									});
//					register.employeeCombobox.setValue(json.records[0].employeeID)
//					register.employeeCombobox.getStore().load(
//									{
//										callback : function() {
//											register.employeeCombobox.setValue(json.records[0].employeeID)
//											register.employeeCombobox.originalValue = json.records[0].employeeID
//
//										}
//									});
				},
				failure : function(response) {
					Ext.Msg.alert(__messages['title.information'],
							'Please cotact admin.');
				}
			});
}

checkIsDirty = function() {
	var check = false;
	if ( booking.bookingID.isDirty()|| booking.roomID.isDirty()
			|| booking.fromDate.isDirty()
			|| booking.toDate.isDirty()
			|| booking.date.isDirty() 
		    || booking.sumPrice.isDirty()
		    || booking.firstNameguest.isDirty()
		    || booking.lastNameguest.isDirty()
		    || booking.emailguest.isDirty()
		    || booking.phoneguest.isDirty()
		    || booking.guestCombo.isDirty()
		    || booking.large_bed.isDirty()
		    || booking.late_check.isDirty()
		    || booking.high_floor.isDirty()
		    || booking.transfer.isDirty()
		    || booking.twin_beds.isDirty()
		    || booking.comment.isDirty()){
	
		check = true;
	}
	return check;
};



//nextfunction = function() {
//	console.log("tes")
//	var idRoom = requests.roomID.getValue();
//	var params = {
//		id : idRoom,
//		method : 'viewBook'
//	};
//	FormHelper.submitTo(application.contextPath + '/booking.html', params);
//}


calculateDate = function() {

	var from = document.getElementById('fromDate').value;
	var to = document.getElementById('toDate').value;

	var fromcp = parseInt(from);
	var tocp = parseInt(to);
	
	console.log(fromcp);
	console.log(tocp);
	
	var diff_date = tocp - fromcp;
	var diff = diff_date.toString();
	booking.date.setValue(diff );

	var years = diff_date / 365;
	var months = (diff_date % 365) / 30;
	var days = ((diff_date % 365) / 30) / 7;

}

calculatePrice = function() {

	var days = document.getElementById('date').value;
   var price1 = document.getElementById('sumPrice').value;
	
	
	console.log(price1 + " 55555");
	console.log(days + " 6666");
}

	Ext.onReady(function() {
		if (!BeanUtils.isEmpty(headerId)) {
			booking.roomID.setValue(headerId);
		if (!BeanUtils.isEmpty(toDate)) {
			booking.toDate.setValue(new Date(toDate));
		if (!BeanUtils.isEmpty(fromDate)) {
			booking.fromDate.setValue(new Date(fromDate));
			
		
			selectFunction();
			selectFunctionUser();
//			EditRoomForm();
			calculateDate();
			calculatePrice();
		
		}
		}
		}
	
		
	});


















	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
