selectFunction = function() {
	branch.grid.load({
		xaction : 'read',
		method : 'selectGrid'
	}, function() {
	});
};

saveFunction = function() {
	if (checkIsDirty()) {
		if (branch.grid.getStore().isValid()) {
			var params = {
				method : 'saveBranch'
			};
			GridConvert.save(branch.grid, params, function() {
				selectFunction();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction = function() {
	branch.grid.remove();
};

searchFunction = function() {
	var params = {
		SearchBranch : Ext.getCmp('SearchBranch').getValue(),
		SearchProvinceCombobox : Ext.getCmp('SearchProvinceCombobox')
				.getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : branch.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			branch.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

checkIsDirty = function() {
	var check = false;
	if (branch.grid.isDirty()) {
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	selectFunction();
});
