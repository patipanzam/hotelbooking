selectFunction = function() {
	var params = {
		method : 'select',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : showEmployee.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			showEmployee.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

removeFunction = function() {

	Ext.Msg.confirm("Confirmation", "Confirm delete?",
			function(btnText) {
				if (btnText === "no") {
				} else if (btnText === "yes") {

					var selects = showEmployee.grid.getSelectionModel()
							.getSelections();
					var count = selects.length;
					var id = new Array();

					for (var i = 0; i < count; i++) {
						id[i] = selects[i].data.employeeID;
						showEmployee.grid.getStore().remove(selects[i]);
					}
					var code = new Array();
					code = id;
					var params = {
						code : id,
						method : 'removeEmployee'
					};
					Ext.Ajax.request({
						method : 'POST',
						params : params,
						url : showEmployee.url,
						success : function(response) {
							var json = Ext.decode(response.responseText);
							selectFunction();
						},
						failure : function(response) {
						}
					});
				}
			}, this);
};

function renderInstall(value, metaData, record, row, col, store, gridView){
    return('<a href="'+showEmployee.urlEditEmployee+'?employeeID='+record.data.employeeID+'" class="btn btn-info btn-xs" >แก้ไข</a>');
 }

searchFunction = function() {
	var params = {
		SearchEmployee : Ext.getCmp('SearchEmployee').getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : showEmployee.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			showEmployee.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

Ext.onReady(function() {
	selectFunction();
});
