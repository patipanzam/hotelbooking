	selectFunction = function() {
		HBBMRT07.grid.load({
			xaction : 'read',
			method : 'selectGrid1'
		}, function() {
		});
	};


searchFunction = function() {
	var params = {
		method : 'search'

	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : HBBMRT07.url,
		success : function(response, opts) {
			var json = Ext.util.JSON.decode(response.responseText);
			// alert('search Success');

			HBBMRT07.grid.store.loadData(json)
		},
		failure : function(response, opts) {
			alert('Search Failure');			
		}
	});
}


	//////////////////////////////////////////////////////
	saveFunction = function() {
		if (checkIsDirty()) {
			if (HBBMRT07.grid.getStore().isValid()) {
				var params = {
					method : 'savecancel'
				,bookingID:HBBMRT07.bookingID.getValue()
						
				};
				GridConvert.save(HBBMRT07.grid, params, function() {
					selectFunction();
				});
			} else {
				Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
			}
		} else {
			Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
		}
	};


///////////////////////////////////////////////

	checkIsDirty = function() {
		var check = false;
		if (HBBMRT07.grid.isDirty()|| HBBMRT07.bookingID.isDirty()) {
			check = true;
		}
		return check;
	};
	
	
	
Ext.onReady(function() {
	searchFunction();
});
