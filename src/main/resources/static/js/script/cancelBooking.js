checkEdit = function(e) {
	var rw = e.row;
	if ((HBBMRT07.grid.store.data.itemAt(rw).data.CancelStatus == "Cancel") || (HBBMRT07.grid.store.data.itemAt(rw).data.CancelStatus == "Approve") || (HBBMRT07.grid.store.data.itemAt(rw).data.CancelStatus == "Pending")) {
		$.each(HBBMRT07.grid.store.data.items,
			function(i) {
					$.each(HBBMRT07.grid.store.data.items[i].data,
						function(key, val) {
//						reser.grid.getColumnModel().setHidden(0, true);
						HBBMRT07.grid.getColumnModel().setEditable('7', false);

						});
			});
	} 

}
/////////////////////////////////////////////////////////
selectFunction = function() {
		HBBMRT07.grid.load({
			xaction : 'read',
			method : 'selectGrid1'
		}, function() {
		});
	};

/////////////////////////////////////////////////////////
searchFunction = function() {
	var params = {
		method : 'search'

	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : HBBMRT07.url,
		success : function(response, opts) {
			var json = Ext.util.JSON.decode(response.responseText);
			// alert('search Success');

			HBBMRT07.grid.store.loadData(json)
		},
		failure : function(response, opts) {
			alert('Search Failure');			
		}
	});
}


	//////////////////////////////////////////////////////
//	saveFunction = function() {
//		if (checkIsDirty()) {
//			if (HBBMRT07.grid.getStore().isValid()) {
//				var params = {
//					method : 'savecancel'
//				,bookingID:HBBMRT07.bookingID.getValue()
//						
//				};
//				GridConvert.save(HBBMRT07.grid, params, function() {
//					selectFunction();
//				});
//			} else {
//				Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
//			}
//		} else {
//			Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
//		}
//	};

updateFunction = function() {

	var selected = HBBMRT07.store.data.items;
	var Excerpt = [];
	Ext.each(selected, function(item) {
		var Obj = {
				bookingID : item.get('bookingID'),	
				createUser : item.get('createUser'),
				fname : item.get('fname'),
				phone : item.get('phone'),
				sumPrice : item.get('sumPrice'),
				checkIN : item.get('checkIN'),
				checkOUT : item.get('checkOUT'),
				CancelStatus : item.get('CancelStatus'),
				night : item.get('night'),
				lname : item.get('lname'),
			email : item.get('email'),
			country : item.get('country'),
			status : item.get('status'),
			rooms : item.get('rooms'),
			roomID : item.get('roomID'),			
		};
		Excerpt.push(Obj);
	}, this);
	//console.log('update');
	//console.log(Excerpt);
	var params = {
			
		savegrid1 : Ext.encode(Excerpt),
		method : 'update'
	};
	Ext.Ajax.request({
		params : params,
		url : HBBMRT07.url,
		Success : function(response, opts) {
			location.reload();
		},
		Failure : function(response, opts) {
		}
	});

}


///////////////////////////////////////////////

	checkIsDirty = function() {
		var check = false;
		if (HBBMRT07.grid.isDirty()|| HBBMRT07.bookingID.isDirty()) {
			check = true;
		}
		return check;
	};
	
	
	
Ext.onReady(function() {
	searchFunction();
});
