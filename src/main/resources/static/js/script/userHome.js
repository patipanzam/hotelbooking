selectFunction = function() {
	var params = {
		method : 'select',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : sr.url,
		success : function(response) {
			console.log(response);
			var json = Ext.util.JSON.decode(response.responseText);
			sr.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};



///////////////////////////////////////////////////////////////////////////////////////
//selectFunction = function() {
//	var params = {
//		method : 'select',
//		xaction : 'read'
//	};
//	Ext.Ajax
//			.request({
//				method : 'POST',
//				params : params,
//				url : sr.url,
//				success : function(response) {
//					var json = Ext.decode(response.responseText);
//					room.roomID.setValue(json.records.roomID);
//					room.roomID.originalValue = json.records.roomID;
//					room.roomBeds.setValue(json.records.beds);
//					room.roomBeds.originalValue = json.records.beds;
//					room.roomDate.setValue(json.records.roomDate);
//					room.roomDate.originalValue = json.records.roomDate;
//					room.freewifi.setValue(json.records.freewifi);
//					room.freewifi.originalValue = json.records.freewifi;
//					room.smocking.setValue(json.records.smocking);
//					room.smocking.originalValue = json.records.smocking;
//					room.service.setValue(json.records.service);
//					room.service.originalValue = json.records.service;
//					room.breaks.setValue(json.records.breaks);
//					room.breaks.originalValue = json.records.breaks;
//					room.roomPrice.setValue(json.records.roomPrice);
//					room.roomPrice.originalValue = json.records.roomPrice;
//					room.roomNo.setValue(json.records.roomNo);
//					room.roomNo.originalValue = json.records.roomNo;
//					room.typeDetail.setValue(json.records.typeDetail);
//					room.typeDetail.originalValue = json.records.typeDetail;
//			
//					console.log(response);
//					room.fileupload.setValue(json.records.image);
//					var img = 'data:image/jpeg;base64,'+json.records.image;
//					room.panel.update('<img src='+img+' style="height:250px; width:250px"; /> ');
//
//					room.buildingCombobox.getStore().load(
//									{
//										callback : function() {
//											room.buildingCombobox.setValue(json.records.buildingID)
//											room.buildingCombobox.originalValue = json.records.buildingID
//										}
//									});
//					room.roomtypeCombobox.setValue(json.records.roomTypeName)
//					room.roomtypeCombobox.getStore().load(
//									{
//										callback : function() {
//											room.roomtypeCombobox.setValue(json.records.roomTypeName)
//											room.roomtypeCombobox.originalValue = json.records.roomTypeName
//
//										}
//									});
//				},
//				failure : function(response) {
//					Ext.Msg.alert(__messages['title.information'],
//							'Please cotact admin.');
//				}
//			});
//}
///////////////////////////////////////////////////////////////////////////////////////
removeFunction = function() {

	Ext.Msg.confirm("Confirmation", "Confirm delete?", function(btnText) {
		if (btnText === "no") {
		} else if (btnText === "yes") {

			var selects = sr.grid.getSelectionModel().getSelections();
			var count = selects.length;
			var roomID = new Array();

			for (var i = 0; i < count; i++) {

				roomID[i] = selects[i].data.roomID;

				sr.grid.getStore().remove(selects[i]);

			}
			var code = new Array();
			code = roomID;

			var params = {
				code : roomID,
				method : 'remove'
			};
			Ext.Ajax.request({
				method : 'POST',
				params : params,
				url : sr.url,
				success : function(response) {

					var json = Ext.decode(response.responseText);
					selectFunction();

				},
				failure : function(response) {

				}
			});
		}

	}, this);
};

function renderInstall(value, metaData, record, row, col, store, gridView){
    return('<a href="'+sr.urlViewRoom+'?roomID='+record.data.roomID+'" class="btn btn-info btn-xs" >View more detail >></a>');
 }



//searchFunction = function() {
//	var params = {
//		SearchRegister : Ext.getCmp('SearchRegister').getValue(),
//		SearchPositionCombobox : Ext.getCmp('SearchPositionCombobox')
//				.getValue(),
//		method : 'search',
//	};
//
//	Ext.Ajax.request({
//		method : 'POST',
//		params : params,
//		url : sr.url,
//		success : function(response) {
//			var json = Ext.util.JSON.decode(response.responseText);
//			sr.grid.store.loadData(json);
//		},
//		failure : function(response) {
//		}
//	});
//};
searchFunction = function() {
	console.log("tes")
	
	var params = {
//		roomTypeName: Ext.getCmp('roomtypeCombobox').getValue()
		  fromDate:Ext.getCmp('fromDate').getRawValue()
		 , toDate: Ext.getCmp('toDate').getRawValue()
		
		,method : 'viewtest'
	};
	FormHelper.submitTo(application.contextPath + '/userHome.html', params);
}

function convertImage(valueImage){
	var u8 = new Uint8Array(valueImage);
	var b64Encode = btoa(String.fromCharCode.apply(null, u8));
	var img = 'data:image/jpeg;base64,'+b64Encode;
	return '<img src='+img+' style="height:80px; width:200px"; />';
}

Ext.onReady(function() {
	selectFunction();
});
