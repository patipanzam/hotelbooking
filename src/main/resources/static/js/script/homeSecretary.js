selectFunction = function() {

	var params = {
		method : 'select',

	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : application.contextPath + '/homeSecretary.html',
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			he.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});

};

he.doView = function(projectID) {

	var state = FormHelper.getGridState(he.grid);

	var params = {
		id : projectID,
		method : 'viewPo'
	};

	FormHelper.submitTo(application.contextPath + '/projectInfoSecretary.html', params,
			state);
};

searchFunction = function() {

	var params = {
			SearchProject : Ext.getCmp('SearchProject').getValue(),
			SearchProjectTypeCombobox : Ext.getCmp('SearchProjectTypeCombobox').getValue(),
			SearchBranchCombobox : Ext.getCmp('SearchBranchCombobox').getValue(),
		method : 'search',

	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : application.contextPath + '/homeSecretary.html',
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			he.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});

};

Ext.onReady(function() {
	selectFunction();
});
