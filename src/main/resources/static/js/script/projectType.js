selectFunction = function() {
	pt.grid.load({
		xaction : 'read',
		method : 'selectGrid'
	}, function() {
	});
};

saveFunction = function() {
	if (checkIsDirty()) {
		if (pt.grid.getStore().isValid()) {
			var params = {
				method : 'saveProjectType'
			};
			GridConvert.save(pt.grid, params, function() {
				selectFunction();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction = function() {
	pt.grid.remove();
};

searchFunction = function() {
	var params = {
		SearchProjectType : Ext.getCmp('SearchProjectType').getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : pt.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			pt.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

checkIsDirty = function() {
	var check = false;
	if (pt.grid.isDirty()) {
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	selectFunction();
});
