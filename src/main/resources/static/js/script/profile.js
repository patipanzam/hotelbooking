//application.toolbar.fn['Create Order'] = function(){
//	   location.href = register.defaultUrl
//}



selectFunction = function() {
	var params = {
		method : 'select',
		xaction : 'read'
	};
	Ext.Ajax
			.request({
				method : 'POST',
				params : params,
				url : register.url,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					console.log(response);
					register.id.setValue(json.records.id);
					register.id.originalValue = json.records.id;
					register.userName.setValue(json.records.username);
					register.userName.originalValue = json.records.username;
					register.userPassword.setValue(json.records.password);
					register.userPassword.originalValue = json.records.password;
					register.userPasswordConfirm.setValue(json.records.password);
					register.userPasswordConfirm.originalValue = json.records.password;
					register.firstName.setValue(json.records.firstName);
					register.firstName.originalValue = json.records.firstName;
					register.lastName.setValue(json.records.lastName);
					register.lastName.originalValue = json.records.lastName;
					register.email.setValue(json.records.email);
					register.email.originalValue = json.records.email;
					register.phone.setValue(json.records.phone);
					register.phone.originalValue = json.records.phone;
//					register.roleName.setValue(json.records.roleName);
//					register.roleName.originalValue = json.records.roleName;

					register.roleCombobox.getStore().load(
									{
										callback : function() {
											register.roleCombobox.setValue(json.records.roleId)
											register.roleCombobox.originalValue = json.records.roleId
										}
									});
//					register.employeeCombobox.setValue(json.records[0].employeeID)
//					register.employeeCombobox.getStore().load(
//									{
//										callback : function() {
//											register.employeeCombobox.setValue(json.records[0].employeeID)
//											register.employeeCombobox.originalValue = json.records[0].employeeID
//
//										}
//									});
				},
				failure : function(response) {
					Ext.Msg.alert(__messages['title.information'],
							'Please cotact admin.');
				}
			});
}

Ext.onReady(function() {
	selectFunction();
});