selectFunction = function() {
	pos.grid.load({
		xaction : 'read',
		method : 'selectGrid'
	}, function() {
	});
};

saveFunction = function() {
	if (checkIsDirty()) {
		if (pos.grid.getStore().isValid()) {
			var params = {
				method : 'savePositionType'
			};
			GridConvert.save(pos.grid, params, function() {
				selectFunction();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction = function() {
	pos.grid.remove();
};

selectFunction2 = function() {
	pos.grid2.load({
		xaction : 'read',
		method : 'selectGrid2'
	}, function() {
	});
};

saveFunction2 = function() {
	if (checkIsDirty2()) {
		if (pos.grid2.getStore().isValid()) {
			var params = {
				method : 'savePosition'
			};
			GridConvert.save(pos.grid2, params, function() {
				selectFunction2();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction2 = function() {
	pos.grid2.remove();
};

searchFunction = function() {
	var params = {
		SearchPositionType : Ext.getCmp('SearchPositionType').getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : pos.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			pos.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

searchFunction2 = function() {
	var params = {
		SearchPosition : Ext.getCmp('SearchPosition').getValue(),
		SearchPositionTypeCombobox : Ext.getCmp('SearchPositionTypeCombobox')
				.getValue(),
		method : 'search2',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : pos.url,
		success : function(response) {
			var json2 = Ext.util.JSON.decode(response.responseText);
			pos.grid2.store.loadData(json2);
		},
		failure : function(response) {
		}
	});
};

checkIsDirty = function() {
	var check = false;
	if (pos.grid.isDirty()) {
		check = true;
	}
	return check;
};

checkIsDirty2 = function() {
	var check = false;
	if (pos.grid2.isDirty()) {
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	selectFunction();
	selectFunction2();
});
