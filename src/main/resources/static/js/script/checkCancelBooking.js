checkEdit = function(e) {
	var rw = e.row;
	if ((HBBMRT09.grid.store.data.itemAt(rw).data.CancelStatus == "Approve")) {
		$.each(HBBMRT09.grid.store.data.items,
			function(i) {
					$.each(HBBMRT09.grid.store.data.items[i].data,
						function(key, val) {

						HBBMRT09.grid.getColumnModel().setEditable('7', false);

						});
			});
	}


}	

///////////////////////////////////////////////////////////////////////////////

selectFunction = function() {
		HBBMRT09.grid.load({
			xaction : 'read',
			method : 'selectGrid'
		}, function() {
		});
	};
	//////////////////////////////////////////////////////
	updateFunction = function() {

		var selected = HBBMRT09.store.data.items;
		var Excerpt = [];
		Ext.each(selected, function(item) {
			var Obj = {
					bookingID : item.get('bookingID'),	
					createUser : item.get('createUser'),
					fname : item.get('fname'),
					phone : item.get('phone'),
					sumPrice : item.get('sumPrice'),
					checkIN : item.get('checkIN'),
					checkOUT : item.get('checkOUT'),
					CancelStatus : item.get('CancelStatus'),
					night : item.get('night'),
					lname : item.get('lname'),
				email : item.get('email'),
				country : item.get('country'),
				status : item.get('status'),
				rooms : item.get('rooms'),
				roomID : item.get('roomID'),		
				
			};
			Excerpt.push(Obj);
		}, this);
		//console.log('update');
		//console.log(Excerpt);
		var params = {
				
			savegrid1 : Ext.encode(Excerpt),
			method : 'update'
		};
		Ext.Ajax.request({
			params : params,
			url : HBBMRT09.url,
			Success : function(response, opts) {
				alert('Cancellation has been');
				searchFunction();
				location.reload();
			},
			Failure : function(response, opts) {
			}
		});

	}
///////////////////////////////////////////////
	removeFunction = function() {
		HBBMRT09.grid.remove();
	};
//////////////////////////////////////////////
	searchFunction = function() {
		var params = {
			method : 'search',
		};

		Ext.Ajax.request({
			method : 'POST',
			params : params,
			url : HBBMRT09.url,
			success : function(response) {
				var json = Ext.util.JSON.decode(response.responseText);
				HBBMRT09.grid.store.loadData(json);
			},
			failure : function(response) {
				alert('Search Failure');
			}
		});
	};
	//////////////////////////////////////////////
	checkIsDirty = function() {
		var check = false;
		if (HBBMRT09.grid.isDirty()|| HBBMRT09.bookingID.isDirty()) {
			check = true;
		}
		return check;
	};
	
Ext.onReady(function() {
	selectFunction();
});
