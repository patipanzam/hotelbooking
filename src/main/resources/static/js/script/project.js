saveFunction = function() {
	if (checkIsDirty()) {
		if (pro.projectName.isValid() && pro.branchCombobox.isValid()
				&& pro.projectTypeCombobox.isValid()
				&& pro.projectDateStart.isValid()
				&& pro.projectDateEnd.isValid() && pro.grid.getStore().isValid()) {
			var params = {
				projectID : pro.projectID.getValue(),
				projectName : pro.projectName.getValue(),
				projectLocation : pro.projectLocation.getValue(),
				projectDateStart : pro.projectDateStart.getValue(),
				projectDateEnd : pro.projectDateEnd.getValue(),
				projectBudget : pro.projectBudget.getValue(),
				projectDescription : pro.projectDescription.getValue(),
				projectTypeID : pro.projectTypeCombobox.getValue(),
				branchID : pro.branchCombobox.getValue(),
				method : 'saveProject'
			};
			GridConvert.save(pro.grid, params, function(jsonData) {
				pro.projectID.setValue(jsonData.records.projectID);
				selectFunction();
				selectGridFunction();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

pro.checkSkillData = function(e){
	var rw = e.row;
	if(BeanUtils.equals('skillID', e.field)){
		$.each(pro.grid.store.data.items, function(i) {
			if(!BeanUtils.equals(rw, i)){
				 $.each(pro.grid.store.data.items[i].data, function(key,val) {
					  if(BeanUtils.equals('employeeID', key)){
						  if(BeanUtils.equals(e.record.data.employeeID, val)){
								Ext.Msg.alert(__messages['title.information'],'Duplicate Employee data.'
										, function(){
									pro.grid.store.data.itemAt(rw).data.skillID = '';
									pro.grid.getView().refresh();
								} );
						  }
					  }
				  });
			}
		});
	}else if(BeanUtils.equals('positionTypeID', e.field)){
		if(pro.grid.isDirty()){
			pro.grid.store.data.itemAt(rw).data.skillID = '';
			pro.grid.store.data.itemAt(pro.grid.selModel.lastActive).data.employeeName = '';
			pro.grid.store.data.itemAt(rw).data.positionID = '';
			pro.grid.store.data.itemAt(pro.grid.selModel.lastActive).data.positionName = '';
			pro.grid.getView().refresh();
			
		}
	}
	else if(BeanUtils.equals('positionID', e.field)){
		if(pro.grid.isDirty()){
			pro.grid.store.data.itemAt(rw).data.skillID = '';
			pro.grid.store.data.itemAt(pro.grid.selModel.lastActive).data.employeeName = '';
			pro.grid.getView().refresh();
			
		}
	}
}

selectGridFunction = function() {
	pro.grid.load({
		xaction : 'read',
		method : 'selectGrid',
		id : pro.projectID.getValue()
	}, function() {
	});
};

selectFunction = function() {
	var params = {
		method : 'select',
		xaction : 'read'
	};
	params.projectID = pro.projectID.getValue()
	Ext.Ajax
			.request({
				method : 'POST',
				params : params,
				url : pro.url,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					pro.projectID.setValue(json.records[0].projectID);
					pro.projectID.originalValue = json.records[0].projectID;
					pro.projectName.setValue(json.records[0].projectName);
					pro.projectName.originalValue = json.records[0].projectName;
					pro.projectLocation.setValue(json.records[0].projectLocation);
					pro.projectLocation.originalValue = json.records[0].projectLocation;
					
					// Date Start Convert
					var dateStringStart = json.records[0].projectDateStart;
					var yearStart = dateStringStart.substring(0, 4);
					var yearIntStart = parseInt(yearStart);
					var yearPStart = yearIntStart;
					var dateStart = dateStringStart.substring(8, 10) + "/" + dateStringStart.substring(5, 7) + "/" + yearPStart;
					
					pro.projectDateStart.setValue(dateStart);
					pro.projectDateStart.originalValue = new Date(json.records[0].projectDateStart);
					
					// Date End Convert
					var dateStringEnd = json.records[0].projectDateEnd;
					var yearEnd = dateStringEnd.substring(0, 4);
					var yearIntEnd = parseInt(yearEnd);
					var yearPEnd = yearIntEnd;
					var dateEnd = dateStringEnd.substring(8, 10) + "/" + dateStringEnd.substring(5, 7) + "/" + yearPEnd;
					
					pro.projectDateEnd.setValue(dateEnd);
					pro.projectDateEnd.originalValue = new Date(json.records[0].projectDateEnd)
					pro.projectBudget.setValue(json.records[0].projectBudget);
					pro.projectBudget.originalValue = json.records[0].projectBudget;
					pro.projectDescription.setValue(json.records[0].projectDescription);
					pro.projectDescription.originalValue = json.records[0].projectDescription;

					pro.projectTypeCombobox.getStore().load(
									{
										callback : function() {
											pro.projectTypeCombobox.setValue(json.records[0].projectTypeID)
											pro.projectTypeCombobox.originalValue = json.records[0].projectTypeID;
										}
									});

					pro.branchCombobox.getStore().load(
									{
										callback : function() {
											pro.branchCombobox.setValue(json.records[0].branchID)
											pro.branchCombobox.originalValue = json.records[0].branchID;
										}
									});
				},
				failure : function(response) {
					Ext.Msg.alert(__messages['title.information'],
							'Please cotact admin.');
				}
			});
}

checkIsDirty = function() {
	var check = false;
	if (pro.projectName.isDirty() || pro.projectLocation.isDirty()
			|| pro.projectDateStart.isDirty() || pro.projectDateEnd.isDirty()
			|| pro.projectBudget.isDirty() || pro.projectDescription.isDirty()
			|| pro.projectTypeCombobox.isDirty()
			|| pro.branchCombobox.isDirty() || pro.grid.isDirty()) {
		check = true;
	}
	return check;
};

Ext.onReady(function() {
	if (!BeanUtils.isEmpty(headerId)) {
		pro.projectID.setValue(headerId);
		selectFunction();
		selectGridFunction();
	}
});