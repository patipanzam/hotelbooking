selectFunction = function() {
	var params = {
		method : 'select',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : sr.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			sr.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

removeFunction = function() {

	Ext.Msg.confirm("Confirmation", "Confirm delete?", function(btnText) {
		if (btnText === "no") {
		} else if (btnText === "yes") {

			var selects = sr.grid.getSelectionModel().getSelections();
			var count = selects.length;
			var promotionID = new Array();

			for (var i = 0; i < count; i++) {

				promotionID[i] = selects[i].data.promotionID;

				sr.grid.getStore().remove(selects[i]);

			}
			var code = new Array();
			code = promotionID;

			var params = {
				code : promotionID,
				method : 'remove'
			};
			Ext.Ajax.request({
				method : 'POST',
				params : params,
				url : sr.url,
				success : function(response) {

					var json = Ext.decode(response.responseText);
					selectFunction();

				},
				failure : function(response) {

				}
			});
		}

	}, this);
};

function renderInstall(value, metaData, record, row, col, store, gridView){
    return('<a href="'+sr.urlEditPromotion+'?promotionID='+record.data.promotionID+'" class="btn btn-info btn-xs" >Edit</a>');
 }

searchFunction = function() {
	var params = {
		SearchRegister : Ext.getCmp('SearchRegister').getValue(),
		SearchPositionCombobox : Ext.getCmp('SearchPositionCombobox')
				.getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : sr.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			sr.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};

Ext.onReady(function() {
	selectFunction();
});
