selectFunction = function() {
	pos.grid.load({
		xaction : 'read',
		method : 'selectGrid'
	}, function() {
	});
};

saveFunction = function() {
	if (checkIsDirty()) {
		if (pos.grid.getStore().isValid()) {
			var params = {
				method : 'savebuilding'
			};
			GridConvert.save(pos.grid, params, function() {
				selectFunction();
			});
		} else {
			Ext.Msg.alert('ผิดพลาด', 'กรุณาตรวจสอบข้อมูล');
		}
	} else {
		Ext.Msg.alert('ผิดพลาด', 'ข้อมูลไม่ได้รับการแก้ไข');
	}
};

removeFunction = function() {
	pos.grid.remove();
};





searchFunction = function() {
	var params = {
		SearchPositionType : Ext.getCmp('SearchPositionType').getValue(),
		method : 'search',
	};

	Ext.Ajax.request({
		method : 'POST',
		params : params,
		url : pos.url,
		success : function(response) {
			var json = Ext.util.JSON.decode(response.responseText);
			pos.grid.store.loadData(json);
		},
		failure : function(response) {
		}
	});
};



checkIsDirty = function() {
	var check = false;
	if (pos.grid.isDirty()) {
		check = true;
	}
	return check;
};



Ext.onReady(function() {
	selectFunction();
	
});
