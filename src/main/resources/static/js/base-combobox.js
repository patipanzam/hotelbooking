var BaseComboBox = function() {
	var resolveConfig = function(defaultConfig, config) {
		if (!defaultConfig.store.listeners) {
			defaultConfig.store.listeners = {};
		}
		defaultConfig.store.listeners.beforeload = function(value, options) {
			options.params.gridStore_start = options.params[defaultConfig.id
					+ "-store_start"];
			options.params.gridStore_limit = options.params[defaultConfig.id
					+ "-store_limit"];
		};
		if (config) {
			if (config.store) {
				if (config.store.baseParams) {
					Ext.apply(defaultConfig.store.baseParams,
							config.store.baseParams);
					delete config.store.baseParams;
				}
				Ext.apply(defaultConfig.store, config.store);
				delete config.store;
			}

			if (config.listeners) {
				if (!defaultConfig.listeners) {
					defaultConfig.listeners = {};
				}

				for ( var p in config.listeners) {
					if (p) {
						if (Ext.isFunction(defaultConfig.listeners[p])
								&& Ext.isFunction(config.listeners[p])) {
							defaultConfig.listeners[p] = defaultConfig.listeners[p]
									.createSequence(config.listeners[p]);
						} else {
							defaultConfig.listeners[p] = config.listeners[p];
						}
					}
				}

				delete config.listeners;
			}

			Ext.apply(defaultConfig, config);
		}
		return defaultConfig;
	};

	return {
		getPeriodDate : function(id, config) {
			var defaultConfig = {
				id : id,
				fieldLabel : 'Period',
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'valueField',
				displayField : 'displayField',
				descriptionField : 'descriptionField',
				showDescription : true,
				pageSize : 7,
				useOriginalTpl : false,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'valueField',
					fields : [ 'valueField', 'displayField',
							'descriptionField', 'valueBigDecimal1',
							'valueBigDecimal2' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'period'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getOrderList : function(id, config) {
			var defaultConfig = {
				id : id,
				allowBlank : false,
				fieldLabel : 'Order Name',
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'hyOrdhId',
				displayField : 'hyOrdhName',
				descriptionField : 'hyPeriodDateString',
				showDescription : true,
				pageSize : 7,
				useOriginalTpl : false,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'valueField',
					fields : [ 'hyOrdhId', 'hyOrdhName', 'hyPeriodDateString' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'orderList'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getPositionTypeCombobox : function(id, config) {
			var defaultConfig = {
				id : id,
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'positionTypeID',
				displayField : 'positionTypeName',
				showDescription : false,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'positionTypeID',
					fields : [ 'positionTypeID', 'positionTypeName' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'positionType'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getPositionCombobox : function(id, config) {
			var defaultConfig = {
				id : id,
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'positionID',
				displayField : 'positionName',
				showDescription : false,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'positionID',
					fields : [ 'positionID', 'positionName' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'position'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getMaterialTypeCombobox : function(id, config) {
			var defaultConfig = {
				id : 'materialTypeCombobox',
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'materialTypeID',
				displayField : 'materialTypeName',
				showDescription : false,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'materialTypeID',
					fields : [ 'materialTypeID', 'materialTypeName' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'materialType'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getMaterialCombobox : function(id, config) {
			var defaultConfig = {
				id : 'materialCombobox',
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'materialID',
				displayField : 'materialName',
				showDescription : false,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'materialID',
					fields : [ 'materialID', 'materialName' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'material'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getProvinceCombobox : function(id, config) {
			var defaultConfig = {
				id : 'provinceCombobox',
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'PROVINCE_NAME_THA',
				displayField : 'PROVINCE_NAME_THA',
				showDescription : false,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'PROVINCE_CODE',
					fields : [ 'PROVINCE_NAME_THA', 'PROVINCE_NAME_THA' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'province'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getBranchCombobox : function(id, config) {
			var defaultConfig = {
				id : 'branchCombobox',
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'branchID',
				displayField : 'branchName',
				showDescription : false,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'branchID',
					fields : [ 'branchID', 'branchName' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'branch'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getProjectTypeCombobox : function(id, config) {
			var defaultConfig = {
				id : 'projectTypeCombobox',
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'projectTypeID',
				displayField : 'projectTypeName',
				showDescription : false,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'projectTypeID',
					fields : [ 'projectTypeID', 'projectTypeName' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'projectType'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getSkillCombobox : function(id, config) {
			var defaultConfig = {
				id : 'skillCombobox',
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'skillID',
				displayField : 'employeeName',
				descriptionField : 'positionName',
				showDescription : true,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'skillID',
					fields : [ 'skillID', 'employeeName', 'positionName' , 'employeeID' ],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'skill'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		},
		getEmployeeCombobox : function(id, config) {
			var defaultConfig = {
				id : 'employeeCombobox',
				allowBlank : false,
				mode : 'remote',
				width : 148,
				triggerAction : 'all',
				minChars : 0,
				forceSelection : true,
				valueField : 'employeeID',
				displayField : 'employeeName',
				descriptionField : 'employeeName',
				showDescription : true,
				// useOriginalTpl: true,
				store : {
					xtype : 'jsonstore',
					storeId : id + '-store',
					idProperty : 'employeeID',
					fields : [ 'employeeID', 'employeeName'],
					url : application.contextPath + '/combobox.html',
					baseParams : {
						method : 'employee'
					}
				}
			};
			return new Ext.ss.form.ComboBox(
					resolveConfig(defaultConfig, config));
		}

	};

}();