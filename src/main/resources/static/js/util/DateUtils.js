var DateUtils = {};
var monthEng = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

DateUtils.convertToDateBuddhistFormat = function(dateValue){
	var result = dateValue;
	if(BeanUtils.isNotEmpty(dateValue)){
		if(dateValue.getFullYear() < 2200){
			dateValue.setFullYear(dateValue.getFullYear()+543);
			result = dateValue;
		}
	}
	return result;
}

DateUtils.convertSaveGridData = function(dateValue){
	var result = dateValue;
	if(BeanUtils.isNotEmpty(dateValue)){
		if(dateValue.getFullYear() < 2200){
			dateValue.setFullYear(dateValue.getFullYear()+543);
		}
		
		var date = dateValue.getDate();
		var month = dateValue.getMonth();
		var year = dateValue.getFullYear();
		result = monthEng[month]+' '+date+','+' '+year+' 12:00:00 AM';
	}
	return result;
}

DateUtils.convertDateFormatDDMMYYYY = function(dateValue){
	var dateFormat = new Date(dateValue);
	var date = dateFormat.getDate();
	var month = dateFormat.getMonth();
	var year = dateFormat.getFullYear();
	if(date.length = 1){
		date = '0'+date
	}
	if(month.length = 1){
		month = '0'+month;
	}
	return [date, month, year].join('/');
}