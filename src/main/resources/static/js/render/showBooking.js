var HBBMRT08 = {};
HBBMRT08.url = application.contextPath + '/showBooking.html';
HBBMRT08.urlEditRoom = application.contextPath +'/room.html';

HBBMRT08.buildBranchDetail = function() {

	HBBMRT08.SearchBooking = new Ext.form.TextField({
		id : 'SearchBooking',
		fieldLabel : "SearchBooking	 :",
		width : 200,
		maxLength : 50
	});

	HBBMRT08.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	HBBMRT08.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ HBBMRT08.SearchBooking ]
		},  {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ HBBMRT08.SearchButton ]
		} ]

	});

	HBBMRT08.bookingID = new Ext.form.TextField({
		id : 'bookingID',
		maxLength : 50
	});

	HBBMRT08.ROOMNO = new Ext.form.TextField({
		id : 'ROOMNO',
		maxLength : 50
	});

	HBBMRT08.ROOMTYPENAME = new Ext.form.TextField({
		id : 'ROOMTYPENAME',
		maxLength : 50
	});

	HBBMRT08.ROOMPRICE = new Ext.form.TextField({
		id : 'ROOMPRICE',
		maxLength : 50
	});

	
	HBBMRT08.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	HBBMRT08.columns = [ HBBMRT08.sm, {
		header : "Booking Number",
		dataIndex : 'bookingID',
		align : 'center'
		
	}, {
		header : "Room Type",
		dataIndex : 'roomTypeName',
		align : 'center'
	},  {
		header : "Total Amount",
		dataIndex : 'sumPrice',
		align : 'center'
	},{
		header : "View Detail",
		dataIndex : 'ROOMPRICE',
		align : 'center'
//		renderer : renderInstall
	}];

	HBBMRT08.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'bookingID',
		url : HBBMRT08.url,
		fields : [{
			name : 'bookingID'
		}, {
			name : 'fname'
		}, {
			name : 'phone'
		}, {
			name : 'sumPrice'
		}, {
			name : 'checkIN'
		}, {
			name : 'checkOUT'
		}, {
			name : 'CancelStatus'
		}, {
			name : 'createUser'
		}, {
			name : 'night'
		}, {
			name : 'lname'
		}, {
			name : 'email'
		}, {
			name : 'country'
		}, {
			name : 'status'
		},{
			name : 'rooms'
		},{
			name : 'roomID'
		},]
	});



	HBBMRT08.PaggingToolbar = new Ext.PagingToolbar({
		store : HBBMRT08.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	HBBMRT08.grid = new Ext.ss.grid.EditorGridPanel({
		store : HBBMRT08.store,
		columns : HBBMRT08.columns,
		sm : HBBMRT08.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ HBBMRT08.removeButton ,HBBMRT08.saveButton],
		bbar : HBBMRT08.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	HBBMRT08.fieldSet = new Ext.form.FieldSet({
		title : 'List Booking',
		items : [ HBBMRT08.SearchContainer, HBBMRT08.grid ]
	});
}

Ext.onReady(function() {
	HBBMRT08.buildBranchDetail();
	HBBMRT08.formPanel = new Ext.form.FormPanel({
		border : false,
		url : HBBMRT08.url,
		items : [ HBBMRT08.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
