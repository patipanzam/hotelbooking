var register = {};
register.url = application.contextPath + '/register.html';

if (!register.loadMask) {
	register.loadMask = new Ext.LoadMask(Ext.getBody(), {
		msg : __messages['message.processing']
	});
}

register.buildUserDetail = function() {
	
	register.id = new Ext.form.Hidden({
		id : 'id',
		fieldLabel : 'รหัสผู้ใช้งาน :',
		allowBlank : false,
	});

	register.userName = new Ext.form.TextField({
		id : 'userName',
		fieldLabel: __messages['label.userName'],
	    allowBlank: false
	});
	
	register.userPassword = new Ext.form.TextField({
		id : 'userPassword',
		inputType: 'password',
		fieldLabel: __messages['label.userPassword'],
	    allowBlank: false
	});
	
	
	register.userPasswordConfirm = new Ext.form.TextField({
		id : 'userPasswordConfirm',
		inputType: 'password',
		fieldLabel: __messages['label.userPasswordConfirm'],
	    allowBlank: false
	});
	
	register.firstName = new Ext.form.TextField({
		id : 'firstName',
		inputType: 'firstName',
		fieldLabel: 'First Name',
	    allowBlank: false
	});
	
	register.lastName = new Ext.form.TextField({
		id : 'lastName',
		inputType: 'lastName',
		fieldLabel: 'Last Name',
	    allowBlank: false
	});
	
	register.email = new Ext.form.TextField({
		id : 'email',
		inputType: 'email',
		fieldLabel: 'Email',
	    allowBlank: false
	});
	
	register.phone = new Ext.form.TextField({
		id : 'phone',
		inputType: 'phone',
		fieldLabel: 'Phone',
	    allowBlank: false
	});

	register.roleCombobox = new Ext.ss.form.ComboBox({
		id : 'roleCombobox',
		fieldLabel : 'ประเภทผู้ใช้งาน :',
		allowBlank : false,
		mode : 'remote',
		width : 150,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
		valueField : 'roleId',
		displayField : 'roleCode',
		descriptionField : 'roleName',
		showDescription : true,
		store : {
			xtype : 'jsonstore',
			storeId : id + '-store',
			idProperty : 'roleId',
			fields : [ 'roleId', 'roleCode', 'roleName' ],
			url : application.contextPath + '/combobox.html',
			baseParams : {
				method : 'role'
			}
		}
	});
	


	register.roleContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ register.userName, register.userPassword,register.firstName,register.email ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [ register.roleCombobox, register.userPasswordConfirm, register.lastName,register.phone ]
		} ]

	});

	register.fieldSet = new Ext.form.FieldSet({
		title : 'เพิ่มผู้ใช้งาน',
		items : [ register.roleContainer ]
	});
}

Ext.onReady(function() {
	register.buildUserDetail();
	register.formPanel = new Ext.form.FormPanel({
		url : register.url,
		items : [ register.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});
});
