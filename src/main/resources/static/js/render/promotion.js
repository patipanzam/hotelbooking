var promotion = {};
promotion.url = application.contextPath +'/promotion.html';

promotion.buildPromotionDetail = function(){
	
	promotion.promotionID = new Ext.form.Hidden({
		id : 'promotionID'
	});
	
	promotion.udloginname = new Ext.form.Hidden({
		id : 'udloginname'
	});
	
	promotion.updateUser = new Ext.form.Hidden({
		id : 'updateUser'
	});
	
	
//	promotion.roomtypeCombobox1 = new Ext.ss.form.ComboBox({
//		 id: 'roomtypeCombobox1',
//		 fieldLabel: ['Room Type1'],
//		 mode: 'remote',
//      width: 250,
//      triggerAction: 'all',
//      minChars: 0,
//      forceSelection: true,
//      valueField: 'roomID',
//      displayField: 'roomTypeName',
//      descriptionField: 'roomTypeName',
//      showDescription: true,
////      useOriginalTpl: true,
//      store: {
//          xtype: 'jsonstore',
//          storeId: id + '-store',
//          idProperty: 'roomID',
//          fields: ['roomID', 'roomTypeName'],
//          url: application.contextPath + '/combobox.html',
//          baseParams: {
//              method: 'roomtype'
//          }
//      }
//	});
//	
//	promotion.roomtypeCombobox = new Ext.ss.form.ComboBox({
//		 id: 'roomtypeCombobox',
//		 fieldLabel: ['Room Type2'],
//		 
//     mode: 'remote',
//     width: 250,
//     triggerAction: 'all',
//     minChars: 0,
//     forceSelection: true,
//     valueField: 'roomID',
//     displayField: 'roomTypeName',
//     descriptionField: 'roomTypeName',
//     showDescription: true,
////     useOriginalTpl: true,
//     store: {
//         xtype: 'jsonstore',
//         storeId: id + '-store',
//         idProperty: 'roomID',
//         fields: ['roomID', 'roomTypeName'],
//         url: application.contextPath + '/combobox.html',
//         baseParams: {
//             method: 'roomtype'
//         }
//     }
//	});
	
	
promotion.status = new Ext.form.RadioGroup({
        fieldLabel:['status'] + ' : ',
        allowBlank: false,
        id: 'status',
        columns: [100, 100, 100],
        items: [{
                boxLabel:['Active'],
                name: 'status',
                inputValue: 'Active'
            }, {
                boxLabel:['Inactive'],
                name: 'status',
                inputValue: 'Inactive'
            }, 
        ]
	});

promotion.fromDate = new Ext.ss.form.DateField({
	id : 'fromDate',
	fieldLabel: 'From-Date',
    allowBlank: false,
    width : 250,
    anchor:'80%',
    toDateFieldId: 'toDate',
	    listeners: {
	    	'select' : function(from,fromdate){
	    		calculateDate();
	    	}
	    }
});

promotion.toDate = new Ext.ss.form.DateField({
	id : 'toDate',
	fieldLabel: 'To-Date',
    allowBlank: false,
    width : 250,
    anchor:'80%',
    fromDateFieldId: 'fromDate',
    listeners: {
    	'select' : function(to,todate){
    		calculateDate();
    	}
    }
});

promotion.date = new Ext.form.TextField({
	id : 'date',
	fieldLabel: 'Promotion duration',
    allowBlank: false,
    width : 250,
    readOnly:true
});

promotion.room = new Ext.form.TextField({
	id : 'room',
	fieldLabel: 'Number of rooms',
	width : 250,
    allowBlank: false
});

promotion.rate = new Ext.form.TextField({
	id : 'rate',
	fieldLabel: 'Promotion discount rate',
	width : 250,
    allowBlank: false
});

promotion.name = new Ext.form.TextField({
	id : 'name',
	fieldLabel: 'Promotion name',
	width : 250,
    allowBlank: false
});
	
promotion.description = new Ext.form.TextArea({
		id : 'description',
		fieldLabel: 'Promotion description', anchor:'30%',
		width : 400,
	    allowBlank: false
	});
	

//////////////////////////////////////////01/////////////////////////////////////////	
//promotion.roomtypeContainer = new Ext.Container({
//	    layout: 'column',
//	    items: [{
//	            xtype: 'container',
//	            layout: 'form',
//	            columnWidth: 0.4,
//	            items: [promotion.roomtypeCombobox1]
//	        },{
//	            xtype: 'container',
//	            layout: 'form',
//	            columnWidth: 0.4,
//	            items: [promotion.roomtypeCombobox]
//	        }
//	    ]
//
//	});
//	
	/////////////////////////////////////////02///////////////////////////////////////
	
	promotion.role2Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [promotion.fromDate]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [promotion.toDate]
	        }
	    ]

	});
	
	////////////03/////////////////////////////////////////////////////////////////
	
	promotion.role3Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [promotion.rate]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [promotion.status]
	        }
	    ]

	});
	////////////////////////4////////////////////////////////////////////
//	room.role4Container = new Ext.Container({
//	    layout: 'column',
//	    items: [{
//	            xtype: 'container',
//	            layout: 'form',
//	            columnWidth: 0.4,
//	            items: [room.roomNo]
//	        },{
//	            xtype: 'container',
//	            layout: 'form',
//	            columnWidth: 0.4,
//	            items: []
//	        }
//	    ]
//
//	});
//	
	promotion.roomFieldSet = new Ext.form.FieldSet({
	    title: '',
	    items: [promotion.name
	            ,promotion.description
	            ,promotion.role2Container 
	            ,promotion.date
	            ,promotion.room
//	            ,promotion.roomtypeContainer
	            ,promotion.role3Container]
	});
}



Ext.onReady(function () {
	
	promotion.buildPromotionDetail();
	
	promotion.allFieldSet = new Ext.form.FieldSet({
        title: 'Create Promotion',
        items: [promotion.roomFieldSet]
    });
	
	promotion.formPanel = new Ext.form.FormPanel({
        url: promotion.url,
        items: [promotion.allFieldSet],
        plugins: [new Ext.ux.FitToParent({
                fitHeight: false
            })],
        renderTo: 'renderDiv'
    });
});
