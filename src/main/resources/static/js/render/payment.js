var HBPMRT02 = {};
HBPMRT02.url = application.contextPath +'/payment.html';

HBPMRT02.HBPMRT02Compo = function(){
	
	HBPMRT02.paymentID = new Ext.form.Hidden({
		id : 'paymentID'
	});
	
	HBPMRT02.udloginname = new Ext.form.Hidden({
		id : 'udloginname'
	});
	
	HBPMRT02.updateUser = new Ext.form.Hidden({
		id : 'updateUser'
	});
	
	
	HBPMRT02.time = new Ext.form.DateField({
		id : 'time',
		fieldLabel: 'Time',
	    allowBlank: false,
	    anchor:'60%'
	});
	
	

	HBPMRT02.scb = new Ext.form.Checkbox({
        id: 'scb',
        boxLabel:'Siam Commercial Bank 123-4-56789-0 Ban du'
    });
	
	HBPMRT02.bkb = new Ext.form.Checkbox({
        id: 'bkb',
        boxLabel:'Bangkok Bank 123-4-56789-0 Ban du'
    });
	HBPMRT02.ktb = new Ext.form.Checkbox({
        id: 'ktb',
        boxLabel:'Krung Thai Bank (KTB) 123-4-56789-0 Ban du'
    });
	HBPMRT02.katb = new Ext.form.Checkbox({
        id: 'katb',
        boxLabel:'Kasikorn Thai  Bank 123-4-56789-0 Ban du'
    });
	HBPMRT02.CheckboxFacilitiesGroup = new Ext.form.CheckboxGroup({
        id: 'CheckboxFacilitiesGroup',
        fieldLabel:'Please select the bank  ',
        columns: [450, 450],
        items: [HBPMRT02.scb,HBPMRT02.bkb,HBPMRT02.ktb,HBPMRT02.katb]
    });
	
	
	
	
	HBPMRT02.amount = new Ext.form.TextField({
		id : 'amount',
		fieldLabel: 'Amount',
		width : 250,
		allowBlank: false
	   
	});
	
	
	

	/////////////////////////////////////////02///////////////////////////////////////
	
	HBPMRT02.role2Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [HBPMRT02.time]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [HBPMRT02.amount]
	        }
	    ]

	});
	

	
	HBPMRT02.HBPMRT02FieldSet = new Ext.form.FieldSet({
	    title: 'Payment Form',
	    items: [HBPMRT02.CheckboxFacilitiesGroup
	            ,HBPMRT02.role2Container 
	         ]
	});
}

//HBPMRT02.nextButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Save',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = HBPMRT02.HBPMRT02ID.getValue();
//			saveFunction();
//			
//		}	
//});
//HBPMRT02.cancelButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Cancel',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = HBPMRT02.HBPMRT02ID.getValue();
//			cancelFunction();
//			
//		}	
//});
//HBPMRT02.goButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'ListHBPMRT02',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = HBPMRT02.HBPMRT02ID.getValue();
//			goFunction();
//			
//		}	
//});
//
//HBPMRT02.buttonFieldSet = new Ext.form.FieldSet({
//	collapsible: false,
//    title: ' ',
//    layout :'column',
//    items: [
//{
//xtype : 'container',
//layout : 'form',
//columnWidth : 0.30,
//items : [HBPMRT02.nextButton  ]
//},
//{
//xtype : 'container',
//layout : 'form',
//columnWidth : 0.30,
//items : [HBPMRT02.cancelButton ]
//},{
//	xtype : 'container',
//	layout : 'form',
//	columnWidth : 0.30,
//	items : [HBPMRT02.goButton ]
//	}
//            
//       ]
//});

Ext.onReady(function () {
	
	HBPMRT02.HBPMRT02Compo();
	
	HBPMRT02.allFieldSet = new Ext.form.FieldSet({
        title: 'Save Payment',
        items: [HBPMRT02.HBPMRT02FieldSet]
    });
	
	HBPMRT02.formPanel = new Ext.form.FormPanel({
        url: HBPMRT02.url,
        items: [HBPMRT02.allFieldSet],
        plugins: [new Ext.ux.FitToParent({
                fitHeight: false
            })],
        renderTo: 'renderDiv'
    });
});
