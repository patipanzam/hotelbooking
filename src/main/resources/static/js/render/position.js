var pos = {};
pos.url = application.contextPath + '/position.html';

pos.buildPositionDetail = function() {

	pos.SearchPositionType = new Ext.form.TextField({
		id : 'SearchPositionType',
		fieldLabel : "ค้นหาประเภทตำแหน่งงาน :",
		width : 200,
		maxLength : 50
	});

	pos.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {

			searchFunction();
		}
	});

	pos.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ pos.SearchPositionType ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ pos.SearchButton ]
		} ]

	});

	pos.positionTypeName = new Ext.form.TextField({
		id : 'positionTypeName',
		maxLength : 50
	});

	pos.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	pos.columns = [ pos.sm, {
		header : "ประเภทตำแหน่งงาน",
		dataIndex : 'positionTypeName',
		align : 'center',
		editor : pos.positionTypeName
	} ];

	pos.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'positionTypeID',
		url : pos.url,
		fields : [ {
			name : 'positionTypeID'
		}, {
			name : 'positionTypeName',
			allowBlank : false
		} ]
	});

	pos.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {

			pos.grid.add({

			});
		}
	});

	pos.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			removeFunction();

		}
	});

	pos.saveButton = new Ext.Button({
		iconCls : 'save',
		handler : function() {

			saveFunction();

		}
	});

	pos.PaggingToolbar = new Ext.PagingToolbar({
		store : pos.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	pos.grid = new Ext.ss.grid.EditorGridPanel({
		store : pos.store,
		columns : pos.columns,
		sm : pos.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,

		tbar : [ pos.addButton, pos.removeButton, pos.saveButton ],
		bbar : pos.PaggingToolbar,

		viewConfig : {
			forceFit : true

		},

		height : 300,

	});

	pos.fieldSet = new Ext.form.FieldSet({
		title : 'ประเภทตำแหน่งงาน',
		items : [ pos.SearchContainer, pos.grid ]
	});

	pos.SearchPosition = new Ext.form.TextField({
		id : 'SearchPosition',
		fieldLabel : "ค้นหาตำแหน่งงาน :",
		width : 200,
		maxLength : 50
	});

	pos.SearchPositionTypeCombobox = new BaseComboBox.getPositionTypeCombobox(
			'SearchPositionTypeCombobox', {
				id : 'SearchPositionTypeCombobox',
				fieldLabel : "ค้นหาประเภทตำแหน่งงาน :",
				width : 265,
				allowBlank : true
			});

	pos.SearchButton2 = new Ext.Button({
		iconCls : 'query',
		handler : function() {

			searchFunction2();
		}
	});

	pos.SearchContainer2 = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ pos.SearchPosition ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.235,
			items : [ pos.SearchPositionTypeCombobox ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ pos.SearchButton2 ]
		} ]

	});

	pos.positionName = new Ext.form.TextField({
		id : 'positionName',
		maxLength : 50
	});
	pos.positionTypeCombobox = new BaseComboBox.getPositionTypeCombobox(
			'positionTypeCombobox', {});

	pos.sm2 = new Ext.ss.grid.CheckboxSelectionModel({});

	pos.columns2 = [ pos.sm2, {
		header : "ประเภทตำแหน่งงาน",
		dataIndex : 'positionTypeID',
		align : 'center',
		editor : pos.positionTypeCombobox,
		renderer : Ext.util.Format.comboRenderer(pos.positionTypeCombobox)
	}, {
		header : "ตำแหน่งงาน",
		dataIndex : 'positionName',
		align : 'center',
		editor : pos.positionName
	} ];

	pos.store2 = new Ext.data.JsonStore({
		storeId : 'mystore2',
		idProperty : 'positionID',
		url : pos.url,
		fields : [ {
			name : 'positionID'
		}, {
			name : 'positionName',
			allowBlank : false
		}, {
			name : 'positionTypeID',
			allowBlank : false
		} ]
	});

	pos.addButton2 = new Ext.Button({
		iconCls : 'add',
		handler : function() {

			pos.grid2.add({

			});
		}
	});

	pos.removeButton2 = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			removeFunction2();

		}
	});

	pos.saveButton2 = new Ext.Button({
		iconCls : 'save',
		handler : function() {

			saveFunction2();
		}
	});

	pos.PaggingToolbar2 = new Ext.PagingToolbar({
		store : pos.store2,
		pageSize : 10,
		id : 'PaggingToolbar2',
		displayInfo : true
	});

	pos.grid2 = new Ext.ss.grid.EditorGridPanel({
		store : pos.store2,
		columns : pos.columns2,
		sm : pos.sm2,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,

		tbar : [ pos.addButton2, pos.removeButton2, pos.saveButton2 ],
		bbar : pos.PaggingToolbar2,

		viewConfig : {
			forceFit : true

		},

		height : 300,

	});

	pos.fieldSet2 = new Ext.form.FieldSet({
		title : 'ตำแหน่งงาน',
		items : [ pos.SearchContainer2, pos.grid2 ]
	});

	pos.allFieldSet = new Ext.form.FieldSet({
		title : 'จัดการตำแหน่งงาน',
		items : [ pos.fieldSet, pos.fieldSet2 ]
	});

}

Ext.onReady(function() {

	pos.buildPositionDetail();

	pos.formPanel = new Ext.form.FormPanel({
		border : false,
		url : pos.url,
		items : [ pos.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
