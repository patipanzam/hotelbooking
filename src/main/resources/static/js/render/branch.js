var emp = {};
emp.url = application.contextPath + '/building.html';

emp.buildEmployeeHeader = function() {

	emp.buildingID = new Ext.form.Hidden({
		id : 'buildingID',
		fieldLabel : 'รหัสตึก :',
		allowBlank : false,
	});

	emp.buildingName = new Ext.form.TextField({
		id : 'buildingName',
		fieldLabel : 'BUILDING NAME :',
		width : 400,
		allowBlank : false
	});
	
	emp.buildingCode = new Ext.form.TextField({
		id : 'buildingCode',
		fieldLabel : 'BUILDING CODE :',
		width : 400,
		allowBlank : false
	});

	

	

	emp.employeeContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [emp.buildingName ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [emp.buildingCode ]
		} ]

	});

	emp.fieldSet = new Ext.form.FieldSet({
		title : 'Add Building',
		items : [ emp.employeeContainer ]
	});
};



Ext.onReady(function() {

	emp.buildEmployeeHeader();
	
	

	emp.allFieldSet = new Ext.form.FieldSet({
		title : 'AddBuilding',
		items : [ emp.fieldSet]
	});

	emp.formPanel = new Ext.form.FormPanel({
		url : emp.url,
		items : [ emp.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
