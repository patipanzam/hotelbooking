var pro = {};
pro.url = application.contextPath + '/project.html';

pro.buildEmployeeDetail = function() {

	pro.projectID = new Ext.form.Hidden({
		id : 'projectID',
		fieldLabel : 'รหัสโครงการ :',
		allowBlank : false,
	});

	pro.projectName = new Ext.form.TextField({
		id : 'projectName',
		fieldLabel : 'ชื่อโครงการ :',
		allowBlank : false
	});

	pro.projectLocation = new Ext.form.TextArea({
		id : 'projectLocation',
		fieldLabel : 'ที่อยู่โครงการ :',
		width : 150,
		height : 100
	});

	pro.projectDateStart = new Ext.ss.form.DateField({
		id : 'projectDateStart',
		fieldLabel : 'วันเริ่มโครงการ :',
		width : 150,
		toDateFieldId : 'projectDateEnd',
		allowBlank : false
	});

	pro.projectDateEnd = new Ext.ss.form.DateField({
		id : 'projectDateEnd',
		fieldLabel : 'วันสิ้นสุดโครงการ :',
		width : 150,
		fromDateFieldId : 'projectDateStart',
		allowBlank : false
	});

	pro.projectBudget = new Ext.form.NumberField({
		id : 'projectBudget',
		fieldLabel : 'งบประมาณ :'
	});

	pro.projectDescription = new Ext.form.TextArea({
		id : 'projectDescription',
		fieldLabel : 'รายละเอียดโครงการ :',
		width : 150,
		height : 100
	});

	pro.branchCombobox = new BaseComboBox.getBranchCombobox('branchCombobox', {
		fieldLabel : 'สาขา :'
	});

	pro.projectTypeCombobox = new BaseComboBox.getProjectTypeCombobox(
			'projectTypeCombobox', {
				fieldLabel : 'ประเภทโครงการ :'
			});

	pro.projectContainer = new Ext.Container({
		layout : 'column',
		items : [
				{
					xtype : 'container',
					layout : 'form',
					columnWidth : 0.44,
					items : [ pro.projectName, pro.branchCombobox,
							pro.projectDateStart, pro.projectLocation ]
				},
				{
					xtype : 'container',
					layout : 'form',
					columnWidth : 0.5,
					items : [ pro.projectTypeCombobox, pro.projectBudget,
							pro.projectDateEnd, pro.projectDescription ]
				} ]
	});

	pro.fieldSet = new Ext.form.FieldSet({
		title : 'ข้อมูลโครงการ',
		items : [ pro.projectContainer ]
	});

	pro.positionTypeCombobox = new BaseComboBox.getPositionTypeCombobox(
			'positionTypeCombobox', {});

	pro.positionCombobox = new BaseComboBox.getPositionCombobox(
			'positionCombobox', {
				listeners : {
					'beforequery' : function(qe) {
						var lastEdit = pro.grid.lastEdit;
						var r = pro.grid.getStore().getAt(lastEdit.row);
						qe.combo.getStore().baseParams['positionTypeID'] = r
								.get('positionTypeID');
						delete pro.positionCombobox.lastQuery;
					}
				}
			});
	
	pro.positionComboboxTmp = new BaseComboBox.getPositionCombobox(
			'positionComboboxTmp', {
			});

	pro.skillCombobox = new BaseComboBox.getSkillCombobox('skillCombobox', {
		listeners : {
			'beforequery' : function(qe) {
				var lastEdit = pro.grid.lastEdit;
				var r = pro.grid.getStore().getAt(lastEdit.row);
				var q = null;
				qe.combo.getStore().baseParams['positionID'] = r.get('positionID');
				qe.combo.getStore().baseParams['EmployeeName'] = q
				delete pro.skillCombobox.lastQuery;
			},
			'change' : function(combo, newValue, oldValue){
        		var rec = combo.store.getById(newValue);
        		pro.grid.getStore().getAt(pro.grid.selModel.lastActive).data.employeeID = rec.data.employeeID;
        	}
		}
	});
	
	pro.skillComboboxTmp = new BaseComboBox.getSkillCombobox(
			'skillComboboxTmp', {
			});

	pro.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	pro.columns = [ pro.sm, {
		header : "ประเภทตำแหน่งงาน",
		dataIndex : 'positionTypeID',
		align : 'center',
		editor : pro.positionTypeCombobox,
		renderer : Ext.util.Format.comboRenderer(pro.positionTypeCombobox)
	}, {
		header : "ตำแหน่งงาน",
		dataIndex : 'positionID',
		align : 'center',
		editor : pro.positionCombobox,
		renderer : Ext.util.Format.comboRenderer(pro.positionComboboxTmp)
	}, {
		header : "ชื่อพนักงาน",
		dataIndex : 'skillID',
		align : 'center',
		editor : pro.skillCombobox,
		renderer : Ext.util.Format.comboRenderer(pro.skillComboboxTmp)
	} ];

	pro.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'employeePositionID',
		url : pro.url,
		fields : [ {
			name : 'employeePositionID'
		}, {
			name : 'positionTypeName'
		}, {
			name : 'positionTypeID',
			allowBlank : false
		}, {
			name : 'positionName'
		}, {
			name : 'positionID',
			allowBlank : false
		}, {
			name : 'employeeName'
		}, {
			name : 'skillID',
			allowBlank : false
		}, {
			name : 'employeeID'
		} ]
	});

	pro.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {
			pro.grid.add({});
		}
	});

	pro.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			pro.grid.remove({});
		}
	});

	pro.PaggingToolbar = new Ext.PagingToolbar({
		store : pro.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	pro.grid = new Ext.ss.grid.EditorGridPanel({
		store : pro.store,
		columns : pro.columns,
		sm : pro.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ pro.addButton, pro.removeButton ],
		bbar : pro.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
		listeners : {
			'afterEdit' : function(e) {
				pro.checkSkillData(e);
			}
		}
	});

	pro.fieldSet2 = new Ext.form.FieldSet({
		title : 'รายชื่อพนักงานในโครงการ',
		items : [ pro.grid ]
	});
}

Ext.onReady(function() {
	pro.buildEmployeeDetail();
	pro.positionComboboxTmp.store.baseParams.positionTypeID = '';
	pro.positionComboboxTmp.doQuery();
	pro.skillComboboxTmp.store.baseParams.positionID = '';
	pro.skillComboboxTmp.store.baseParams.EmployeeName = '';
	pro.skillComboboxTmp.doQuery();
	pro.allFieldSet = new Ext.form.FieldSet({
		title : 'เพิ่มโครงการ',
		items : [ pro.fieldSet, pro.fieldSet2 ]
	});

	pro.formPanel = new Ext.form.FormPanel({
		url : pro.url,
		items : [ pro.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});
});
