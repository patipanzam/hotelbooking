var room = {};
room.url = application.contextPath +'/room.html';

room.roomCompo = function(){
	
	room.roomID = new Ext.form.Hidden({
		id : 'roomID'
	});
	
	room.udloginname = new Ext.form.Hidden({
		id : 'udloginname'
	});
	
	room.updateUser = new Ext.form.Hidden({
		id : 'updateUser'
	});
	
	
	room.buildingCombobox = new Ext.ss.form.ComboBox({
		 id: 'buildingCombobox',
		 fieldLabel: ['Building'],
		 allowBlank: false,
      mode: 'remote',
      width: 148,
      triggerAction: 'all',
      minChars: 0,
      forceSelection: true,
      valueField: 'buildingID',
      displayField: 'buildingID',
      descriptionField: 'buildingName',
      showDescription: true,
//      useOriginalTpl: true,
      store: {
          xtype: 'jsonstore',
          storeId: id + '-store',
          idProperty: 'buildingID',
          fields: ['buildingID', 'buildingName'],
          url: application.contextPath + '/combobox.html',
          baseParams: {
              method: 'building'
          }
      }
	});
	
	room.roomtypeCombobox = new Ext.ss.form.ComboBox({
		 id: 'roomtypeCombobox',
		 fieldLabel: ['Room Type'],
		 allowBlank: false,
     mode: 'remote',
     width: 148,
     triggerAction: 'all',
     minChars: 0,
     forceSelection: true,
     valueField: 'roomTypeName',
     displayField: 'roomTypeName',
     descriptionField: 'roomTypeName',
     showDescription: true,
//     useOriginalTpl: true,
     store: {
         xtype: 'jsonstore',
         storeId: id + '-store',
         idProperty: 'roomID',
         fields: ['roomID', 'roomTypeName'],
         url: application.contextPath + '/combobox.html',
         baseParams: {
             method: 'roomtype'
         }
     }
	});
	
	
//	room.PromotionCombobox = new Ext.ss.form.ComboBox({
//		 id: 'PromotionCombobox',
//		 fieldLabel: ['Promotion'],
//		 allowBlank: false,
//    mode: 'remote',
//    width: 148,
//    triggerAction: 'all',
//    minChars: 0,
//    forceSelection: true,
//    valueField: 'promotionID',
//    displayField: 'rate',
//    descriptionField: 'name',
//    showDescription: true,
////    useOriginalTpl: true,
//    store: {
//        xtype: 'jsonstore',
//        storeId: id + '-store',
//        idProperty: 'promotionID',
//        fields: ['promotionID', 'name'],
//        url: application.contextPath + '/combobox.html',
//        baseParams: {
//            method: 'promotion'
//        }
//    }
//	});
//	
	
room.roomBeds = new Ext.form.RadioGroup({
        fieldLabel:['roomBeds'] + ' : ',
        id: 'roomBeds',
        columns: [100, 100, 100],
        items: [{
                boxLabel:['single beds'],
                name: 'roomBeds',
                inputValue: 'single beds'
            }, {
                boxLabel:['double beds'],
                name: 'roomBeds',
                inputValue: 'double beds'
            }, 
        ]
	});
	room.freewifi = new Ext.form.Checkbox({
        id: 'freewifi',
        boxLabel:'Free WiFi'
    });
	
	room.smocking = new Ext.form.Checkbox({
        id: 'smocking',
        boxLabel:'SmockingAreas'
    });
	room.service = new Ext.form.Checkbox({
        id: 'service',
        boxLabel:'Service-Airport'
    });
	room.breaks = new Ext.form.Checkbox({
        id: 'breaks',
        boxLabel:'Break Fast'
    });
	room.CheckboxFacilitiesGroup = new Ext.form.CheckboxGroup({
        id: 'CheckboxFacilitiesGroup',
        fieldLabel:'Facilities  : ',
        columns: [150, 150],
        items: [room.freewifi,room.smocking,room.service,room.breaks]
    });
	
	
	room.roomDate = new Ext.form.DateField({
		id : 'roomDate',
		fieldLabel: 'Room Date',
	    allowBlank: false,
	    anchor:'60%'
	});
	
	room.roomPrice = new Ext.form.TextField({
		id : 'roomPrice',
		fieldLabel: 'Room Price',
	    allowBlank: false
	});
	
	
	room.roomNo = new Ext.form.TextField({
		id : 'roomNo',
		fieldLabel: 'Room No',
	    allowBlank: false
	});
	
	room.Capacity = new Ext.form.TextField({
		id : 'Capacity',
		fieldLabel: 'Capacity',
	    allowBlank: false
	});
	
	
	
	room.typeDetail = new Ext.form.TextArea({
		id : 'typeDetail',
		fieldLabel: 'Type Detail', anchor:'30%',
	    allowBlank: false
	});
	
	room.fileupload = new Ext.ux.form.FileUploadField({
	    id: 'attachment',
	    allowBlank: false,
	    name: 'documentURL',
	    hideLabel: true,
	    emptyText: 'Select a file',
	    fileUpload: true,
	    msgTarget: 'side',
	    width : 250,
	    buttonCfg: {
	        text: '',
	        iconCls: 'upload-icon'
	    },
	    listeners: {
	        fileselected: function (fp, fileName) {
	        	if(fp.fileInput.dom.files[0].size <= 62000){
	        		var reader = new FileReader();
		            reader.onload = function (e) {
		            	room.panel.update('<img src='+e.target.result+' style="height:250px; width:250px"; />');
		            }
		            reader.readAsDataURL(fp.fileInput.dom.files[0]);
	        	}else{
	        		Ext.Msg.alert('Information', 'Image over size.', function(){
	        			room.fileupload.setValue('');
	        		});
	        	}
	        }
	    }
	});
	
	room.panel = new Ext.Panel({
		  scroll : "vertical",
//		  border : false,
		  title   : "Photo",
		  width: 260,
		  height: 260,
		  bodyStyle: {
			  paddingLeft:'5px',
			  paddingTop:'2px',
		      marginLeft: '3.5px'
		    },
		  html   : ""
		});
//////////////////////////////////////////01/////////////////////////////////////////	
	room.roomtypeContainer = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room. buildingCombobox]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomtypeCombobox]
	        }
	    ]

	});
	
	/////////////////////////////////////////02///////////////////////////////////////
	
	room.role2Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomBeds]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.CheckboxFacilitiesGroup]
	        }
	    ]

	});
	
	////////////03/////////////////////////////////////////////////////////////////
	
	room.role3Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomDate]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomPrice]
	        }
	    ]

	});
	////////////////////////4////////////////////////////////////////////
	room.role4Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomNo]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.Capacity]
	        }
	    ]

	});
	///////////////////////////////////////////////////////////////////////////////////////////
	room.role5Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.5,
	            items: [room.fileupload]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.5,
	            items: [room.panel]
	        }
	    ]

	});
	
	room.roomFieldSet = new Ext.form.FieldSet({
	    title: 'Room',
	    items: [room.roomtypeContainer
	            ,room.role2Container 
	            ,room.role3Container
	            , room.role4Container
	            ,room.typeDetail
//	            ,room.PromotionCombobox
	            ,room.role5Container
//	            , room.fileupload 
//	            ,room.panel
	            ]
	});
}

//room.nextButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Save',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = room.roomID.getValue();
//			saveFunction();
//			
//		}	
//});
//room.cancelButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Cancel',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = room.roomID.getValue();
//			cancelFunction();
//			
//		}	
//});
//room.goButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Listroom',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = room.roomID.getValue();
//			goFunction();
//			
//		}	
//});
//
//room.buttonFieldSet = new Ext.form.FieldSet({
//	collapsible: false,
//    title: ' ',
//    layout :'column',
//    items: [
//{
//xtype : 'container',
//layout : 'form',
//columnWidth : 0.30,
//items : [room.nextButton  ]
//},
//{
//xtype : 'container',
//layout : 'form',
//columnWidth : 0.30,
//items : [room.cancelButton ]
//},{
//	xtype : 'container',
//	layout : 'form',
//	columnWidth : 0.30,
//	items : [room.goButton ]
//	}
//            
//       ]
//});

Ext.onReady(function () {
	
	room.roomCompo();
	
	room.allFieldSet = new Ext.form.FieldSet({
        title: 'Add Room',
        items: [room.roomFieldSet]
    });
	
	room.formPanel = new Ext.form.FormPanel({
        url: room.url,
        items: [room.allFieldSet],
        plugins: [new Ext.ux.FitToParent({
                fitHeight: false
            })],
        renderTo: 'renderDiv'
    });
});
