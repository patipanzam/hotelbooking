var pro = {};
pro.url = application.contextPath + '/showProject.html';
pro.urlEditProject = application.contextPath +'/project.html';

if (!pro.loadMask) {
	pro.loadMask = new Ext.LoadMask(Ext.getBody(), {
		msg : __messages['message.processing']
	});
}

pro.buildProjectDetail = function() {

	pro.SearchProject = new Ext.form.TextField({
		id : 'SearchProject',
		fieldLabel : "ค้นหาโครงการ :",
		width : 200,
		maxLength : 50
	});

	pro.SearchProjectTypeCombobox = new BaseComboBox.getProjectTypeCombobox(
			'SearchProjectTypeCombobox', {
				id : 'SearchProjectTypeCombobox',
				fieldLabel : "ค้นหาประเภทโครงการ :",
				allowBlank : true
			});

	pro.SearchBranchCombobox = new BaseComboBox.getBranchCombobox(
			'SearchBranchCombobox', {
				id : 'SearchBranchCombobox',
				fieldLabel : "ค้นหาสาขา :",
				allowBlank : true
			});

	pro.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	pro.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ pro.SearchProject ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.165,
			items : [ pro.SearchProjectTypeCombobox ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.173,
			items : [ pro.SearchBranchCombobox ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ pro.SearchButton ]
		} ]

	});

	pro.projectID = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.projectName = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.projectLocation = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.projectDateStart = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.projectDateEnd = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.projectBudget = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.projectTypeName = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.branchName = new Ext.form.TextField({
		maxLength : 255,
	});

	pro.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});

	pro.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	pro.columns = [ pro.sm, {
		header : 'แก้ไข',
		dataIndex : 'projectID',
		align : 'center',
		renderer : renderInstall
	}, {
		header : 'ชื่อโครงการ',
		dataIndex : 'projectName',
		align : 'left'
	}, {
		header : 'ประเภทโครงการ',
		dataIndex : 'projectTypeName',
		align : 'left'
	}, {
		header : 'สาขา',
		dataIndex : 'branchName',
		align : 'left'
	}, {
		header : 'ที่อยู่โครงการ',
		dataIndex : 'projectLocation',
		align : 'left'
	}, {
		header : 'งบประมาณ (บาท)',
		dataIndex : 'projectBudget',
		align : 'center',
		renderer : Ext.util.Format.numberRenderer('0,000')
	}, {
		header : 'วันเริ่มโคงการ',
		dataIndex : 'projectDateStart',
		align : 'center',
		renderer : Ext.util.Format.dateRenderer('d/m/Y')
	}, {
		header : 'วันสิ้นสุดโครงการ',
		dataIndex : 'projectDateEnd',
		align : 'center',
		renderer : Ext.util.Format.dateRenderer('d/m/Y')
	} ];

	pro.store = new Ext.data.JsonStore({
		removeAndSave : false,
		storeId : 'gridStore',
		idProperty : 'projectID',
		url : pro.url,
		fields : [ {
			name : 'projectID'
		}, {
			name : 'projectName',
		}, {
			name : 'projectLocation',
		}, {
			name : 'projectDateStart',
		}, {
			name : 'projectDateEnd',
		}, {
			name : 'projectBudget',
		}, {
			name : 'projectTypeName',
		}, {
			name : 'branchName',
		} ]
	});

	pro.grid = new Ext.ss.grid.EditorGridPanel({
		store : pro.store,
		columns : pro.columns,
		sm : pro.sm,
		tbar : [ pro.removeButton ],
		viewConfig : {
			forceFit : true
		},
		height : 300,
		listeners : {}
	});

	pro.gridFieldSet = new Ext.form.FieldSet({
		title : 'รายชื่อโครงการ',
		items : [ pro.SearchContainer, pro.grid ]
	});
}

Ext.onReady(function() {
	pro.buildProjectDetail();
	pro.formPanel = new Ext.form.FormPanel({
		url : pro.url,
		items : [ pro.gridFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});
});
