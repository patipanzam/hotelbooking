var stinl = {};
stinl.url = application.contextPath + '/stockINList.html';

if (!stinl.loadMask) {
	stinl.loadMask = new Ext.LoadMask(Ext.getBody(), {
		msg : __messages['message.processing']
	});
}

stinl.buildProjectDetail = function() {

	stinl.SearchStockInNO = new Ext.form.TextField({
		id : 'SearchStockInNO',
		fieldLabel : "ค้นหาเลขที่ใบรับวัสดุ :",
		width : 200,
		maxLength : 50
	});
	
	stinl.SearchDate = new Ext.ss.form.DateField({
		id : 'SearchDate',
		fieldLabel : 'วันที่รับวัสดุ :',
		width : 150
	});

	stinl.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {

			searchFunction();
		}
	});

	stinl.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ stinl.SearchStockInNO ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.18,
			items : [ stinl.SearchDate ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ stinl.SearchButton ]
		} ]

	});

	stinl.stockInID = new Ext.form.TextField({
		maxLength : 255,
	});

	stinl.stockInNO = new Ext.form.TextField({
		maxLength : 255,
	});

	stinl.date = new Ext.form.TextField({
		maxLength : 255,
	});

	stinl.supplier = new Ext.form.TextField({
		maxLength : 255,
	});

	stinl.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			removeFunction();

		}
	});

	stinl.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	stinl.columns = [ stinl.sm, {
		header : 'รหัส',
		dataIndex : 'stockInID',
		align : 'center',
		renderer : function(value, metaData, record) {
			return '<a href=\"javascript:stinl.doView(\''

			+ record.get('stockInID') + '\');\">' + value + '</a>';
		}
	//		 
	}, {
		header : 'เลขที่ใบรับวัสดุ',
		dataIndex : 'stockInNO',
		align : 'center'
	}, {
		header : 'วันที่รับวัสดุ',
		dataIndex : 'date',
		align : 'center',
		renderer : Ext.util.Format.dateRenderer('d/m/Y')
	}, {
		header : 'ชื่อร้านค้า',
		dataIndex : 'supplier',
		align : 'center'
	}];

	stinl.store = new Ext.data.JsonStore({
		removeAndSave : false,
		storeId : 'gridStore',
		idProperty : 'stockInID',
		url : stinl.url,
		fields : [ {
			name : 'stockInID'
		}, {
			name : 'stockInNO',
		}, {
			name : 'date',
		}, {
			name : 'supplier',
		}]
	});

	stinl.grid = new Ext.ss.grid.EditorGridPanel({
		store : stinl.store,
		columns : stinl.columns,
		sm : stinl.sm,
		tbar : [ stinl.removeButton ],
		viewConfig : {
			forceFit : true
		},
		height : 300,
		listeners : {}
	});

	stinl.gridFieldSet = new Ext.form.FieldSet({
		title : 'รายการใบรับวัสดุ',
		items : [ stinl.SearchContainer, stinl.grid ]
	});

}

Ext.onReady(function() {

	stinl.buildProjectDetail();

	stinl.allFieldSet = new Ext.form.FieldSet({
		title : '',
		items : [ stinl.gridFieldSet ]
	});

	stinl.formPanel = new Ext.form.FormPanel({
		url : stinl.url,
		items : [ stinl.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
