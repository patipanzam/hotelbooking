var HBPMRT01 = {};
HBPMRT01.url = application.contextPath + '/checkpayment.html';
HBPMRT01.urlEditRoom = application.contextPath +'/room.html';

HBPMRT01.buildBranchDetail = function() {

	HBPMRT01.SearchPayment = new Ext.form.TextField({
		id : 'SearchPayment',
		fieldLabel : "Search by BookingID	 :",
		width : 200,
		maxLength : 50
	});

	HBPMRT01.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	HBPMRT01.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ HBPMRT01.SearchPayment ]
		},  {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ HBPMRT01.SearchButton ]
		} ]

	});

	HBPMRT01.roomID = new Ext.form.TextField({
		id : 'roomID',
		maxLength : 50
	});

	HBPMRT01.ROOMNO = new Ext.form.TextField({
		id : 'ROOMNO',
		maxLength : 50
	});

	HBPMRT01.ROOMTYPENAME = new Ext.form.TextField({
		id : 'ROOMTYPENAME',
		maxLength : 50
	});

	HBPMRT01.ROOMPRICE = new Ext.form.TextField({
		id : 'ROOMPRICE',
		maxLength : 50
	});

	
	HBPMRT01.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	HBPMRT01.columns = [ HBPMRT01.sm, {
		header : "Booking Number",
		dataIndex : 'roomID',
		align : 'center'
		
	}, {
		header : "Room Type",
		dataIndex : 'ROOMNO',
		align : 'center'
	}, {
		header : "Booking Created Time",
		dataIndex : 'ROOMTYPENAME',
		align : 'center'
	}, {
		header : "Total Amount",
		dataIndex : 'ROOMPRICE',
		align : 'center'
	},{
		header : "Customer Transferd Amount",
		dataIndex : 'ROOMPRICE',
		align : 'center'
	} ,{
		header : "Check",
		dataIndex : 'ROOMPRICE',
		align : 'center'
//		renderer : renderInstall
	}];

	HBPMRT01.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'roomID',
		url : HBPMRT01.url,
		fields : [ {
			name : 'roomID'
		}, {
			name : 'ROOMNO'
		}, {
			name : 'ROOMTYPENAME'
		}, {
			name : 'ROOMPRICE'
		}]
	});

	HBPMRT01.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});

	HBPMRT01.PaggingToolbar = new Ext.PagingToolbar({
		store : HBPMRT01.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	HBPMRT01.grid = new Ext.ss.grid.EditorGridPanel({
		store : HBPMRT01.store,
		columns : HBPMRT01.columns,
		sm : HBPMRT01.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ HBPMRT01.removeButton ],
		bbar : HBPMRT01.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	HBPMRT01.fieldSet = new Ext.form.FieldSet({
		title : 'List Room',
		items : [ HBPMRT01.SearchContainer, HBPMRT01.grid ]
	});
}

Ext.onReady(function() {
	HBPMRT01.buildBranchDetail();
	HBPMRT01.formPanel = new Ext.form.FormPanel({
		border : false,
		url : HBPMRT01.url,
		items : [ HBPMRT01.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
