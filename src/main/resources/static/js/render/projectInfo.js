var pro = {};
pro.url = application.contextPath + '/projectInfo.html';

pro.buildEmployeeDetail = function() {

	pro.projectID = new Ext.form.Hidden({
		id : 'projectID',
		fieldLabel : 'รหัสโครงการ :'
	});

	pro.projectName = new Ext.form.TextField({
		id : 'projectName',
		fieldLabel : 'ชื่อโครงการ :',
		readOnly: true
	});

	pro.projectLocation = new Ext.form.TextArea({
		id : 'projectLocation',
		fieldLabel : 'ที่อยู่โครงการ :',
		readOnly: true,
		width : 150,
		height : 100
	});

	pro.projectDateStart = new Ext.ss.form.DateField({
		id : 'projectDateStart',
		fieldLabel : 'วันเริ่มโครงการ :',
		readOnly: true,
		width : 150
	});

	pro.projectDateEnd = new Ext.ss.form.DateField({
		id : 'projectDateEnd',
		fieldLabel : 'วันสิ้นสุดโครงการ :',
		readOnly: true,
		width : 150
	});

	pro.projectBudget = new Ext.form.TextField({
		id : 'projectBudget',
		fieldLabel : 'งบประมาณ :',
		readOnly: true
	});

	pro.projectDescription = new Ext.form.TextArea({
		id : 'projectDescription',
		fieldLabel : 'รายละเอียดโครงการ :',
		readOnly: true,
		width : 150,
		height : 100
	});

	pro.projectBranch = new Ext.form.TextField({
		id : 'projectBranch',
		fieldLabel : 'งบประมาณ :',
		readOnly: true
	});
	
	pro.projectType = new Ext.form.TextField({
		id : 'projectType',
		fieldLabel : 'งบประมาณ :',
		readOnly: true
	});

	pro.projectContainer = new Ext.Container({
		layout : 'column',
		items : [
				{
					xtype : 'container',
					layout : 'form',
					columnWidth : 0.44,
					items : [ pro.projectName, pro.projectBranch,
							pro.projectDateStart, pro.projectLocation ]
				},
				{
					xtype : 'container',
					layout : 'form',
					columnWidth : 0.5,
					items : [ pro.projectType, pro.projectBudget,
							pro.projectDateEnd, pro.projectDescription ]
				} ]

	});

	pro.fieldSet = new Ext.form.FieldSet({
		title : '',
		items : [ pro.projectContainer ]
	});
	// ///////////////////////////GRID///////////////////
	pro.positionTypeCombobox = new BaseComboBox.getPositionTypeCombobox(
			'positionTypeCombobox', {});

	pro.positionCombobox = new BaseComboBox.getPositionCombobox(
			'positionCombobox', {});

	pro.skillCombobox = new BaseComboBox.getSkillCombobox('skillCombobox', {});

	pro.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	pro.columns = [ pro.sm, {
		header : "ประเภทตำแหน่งงาน",
		dataIndex : 'positionTypeID',
		align : 'center',
		renderer : Ext.util.Format.comboRenderer(pro.positionTypeCombobox)
	}, {
		header : "ตำแหน่งงาน",
		dataIndex : 'positionID',
		align : 'center',
		renderer : Ext.util.Format.comboRenderer(pro.positionCombobox)
	}, {
		header : "ชื่อพนักงาน",
		dataIndex : 'skillID',
		align : 'center',
		renderer : Ext.util.Format.comboRenderer(pro.skillCombobox)
	} ];

	pro.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'employeePositionID',
		url : pro.url,
		fields : [ {
			name : 'employeePositionID'
		}, {
			name : 'positionTypeID'
		}, {
			name : 'positionID'
		}, {
			name : 'skillID'
		} ]
	});

	pro.PaggingToolbar = new Ext.PagingToolbar({
		store : pro.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	pro.grid = new Ext.ss.grid.EditorGridPanel({
		store : pro.store,
		columns : pro.columns,
		sm : pro.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,

		tbar : [],
		bbar : pro.PaggingToolbar,

		viewConfig : {
			forceFit : true

		},

		height : 300,

	});

	pro.fieldSet2 = new Ext.form.FieldSet({
		title : 'ตำแหน่งงาน',
		items : [ pro.grid ]
	});
}

Ext.onReady(function() {

	pro.buildEmployeeDetail();

	pro.allFieldSet = new Ext.form.FieldSet({
		title : 'เพิ่มประวัติพนักงาน',
		items : [ pro.fieldSet, pro.fieldSet2 ]
	});

	pro.formPanel = new Ext.form.FormPanel({
		url : pro.url,
		items : [ pro.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
