var stoutl = {};
stoutl.url = application.contextPath + '/stockOUTList.html';

if (!stoutl.loadMask) {
	stoutl.loadMask = new Ext.LoadMask(Ext.getBody(), {
		msg : __messages['message.processing']
	});
}

stoutl.buildProjectDetail = function() {

	stoutl.SearchStockOutNO = new Ext.form.TextField({
		id : 'SearchStockOutNO',
		fieldLabel : "ค้นหาเลขที่ใบเบิกวัสดุ :",
		width : 200,
		maxLength : 50
	});
	
	stoutl.SearchDate = new Ext.ss.form.DateField({
		id : 'SearchDate',
		fieldLabel : 'วันที่เบิกวัสดุ :',
		width : 150
	});

	stoutl.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {

			searchFunction();
		}
	});

	stoutl.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ stoutl.SearchStockOutNO ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.18,
			items : [ stoutl.SearchDate ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ stoutl.SearchButton ]
		} ]

	});

	stoutl.stockOutID = new Ext.form.TextField({
		maxLength : 255,
	});

	stoutl.stockOutNO = new Ext.form.TextField({
		maxLength : 255,
	});

	stoutl.date = new Ext.form.TextField({
		maxLength : 255,
	});

	stoutl.supplier = new Ext.form.TextField({
		maxLength : 255,
	});

	stoutl.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			removeFunction();

		}
	});

	stoutl.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	stoutl.columns = [ stoutl.sm, {
		header : 'รหัส',
		dataIndex : 'stockOutID',
		align : 'center',
		renderer : function(value, metaData, record) {
			return '<a href=\"javascript:stoutl.doView(\''

			+ record.get('stockOutID') + '\');\">' + value + '</a>';
		}
	//		 
	}, {
		header : 'เลขที่ใบเบิกวัสดุ',
		dataIndex : 'stockOutNO',
		align : 'center'
	}, {
		header : 'วันที่เบิกวัสดุ',
		dataIndex : 'date',
		align : 'center',
		renderer : Ext.util.Format.dateRenderer('d/m/Y')
	}];

	stoutl.store = new Ext.data.JsonStore({
		removeAndSave : false,
		storeId : 'gridStore',
		idProperty : 'stockOutID',
		url : stoutl.url,
		fields : [ {
			name : 'stockOutID'
		}, {
			name : 'stockOutNO',
		}, {
			name : 'date',
		}]
	});

	stoutl.grid = new Ext.ss.grid.EditorGridPanel({
		store : stoutl.store,
		columns : stoutl.columns,
		sm : stoutl.sm,
		tbar : [ stoutl.removeButton ],
		viewConfig : {
			forceFit : true
		},
		height : 300,
		listeners : {}
	});

	stoutl.gridFieldSet = new Ext.form.FieldSet({
		title : 'รายการใบเบิกวัสดุ',
		items : [ stoutl.SearchContainer, stoutl.grid ]
	});

}

Ext.onReady(function() {

	stoutl.buildProjectDetail();

	stoutl.allFieldSet = new Ext.form.FieldSet({
		title : '',
		items : [ stoutl.gridFieldSet ]
	});

	stoutl.formPanel = new Ext.form.FormPanel({
		url : stoutl.url,
		items : [ stoutl.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
