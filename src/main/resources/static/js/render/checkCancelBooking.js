var HBBMRT09 = {};
HBBMRT09.url = application.contextPath + '/checkCancelBooking.html';

HBBMRT09.HBBMRT09Compo = function() {

	HBBMRT09.bookingID = new Ext.form.TextField({
		id : 'bookingID',

	});

	HBBMRT09.createUser = new Ext.form.TextField({
		id : 'createUser',

	});

	HBBMRT09.phone = new Ext.form.TextField({
		id : 'phone',

	});

	HBBMRT09.sumPrice = new Ext.form.TextField({
		id : 'sumPrice',

	});

	HBBMRT09.checkIN = new Ext.form.TextField({
		id : 'checkIN',

	});
	
	HBBMRT09.checkOUT = new Ext.form.TextField({
		id : 'checkOUT',
		
	});

	HBBMRT09.SearchStatus = new Ext.ss.form.ComboBox({
		id : 'CancelStatus',
		fieldLabel : "findstatus",
		mode : 'local',
		width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
		valueField : 'CancelStatus',
		displayField : 'CancelStatus',
		showDescription : false,
		width : 265,
		store : new Ext.data.ArrayStore({
			id : 0,
			fields : [ 'StatusID', 'CancelStatus' ],
			data : [ [ 1, 'Approve' ], [ 2, 'Pending' ] ]
		})
	});


	HBBMRT09.updateFunction = new Ext.Button({
		iconCls : 'save',
		handler : function() {
			updateFunction();
		}
	});

	HBBMRT09.sm = new Ext.ss.grid.CheckboxSelectionModel({});
	HBBMRT09.columns = [ HBBMRT09.sm, {
		header : "Booking Number",
		dataIndex : 'bookingID',
		align : 'center'

	}, {
		header : "Total Amount",
		dataIndex : 'sumPrice',
		align : 'center'
	}, {
		header : "Booking by",
		dataIndex : 'createUser',
		align : 'center'
	}, {
		header : "Telephone number",
		dataIndex : 'phone',
		align : 'center'
	}, {
		header : "Check in",
		dataIndex : 'checkIN',
		align : 'center'
	}, {
		header : "Check out",
		dataIndex : 'checkOUT',
		align : 'center'

	}, {
		header : "Status",
		dataIndex : 'CancelStatus',
		align : 'center',
		editor : HBBMRT09.SearchStatus
	} ];

	HBBMRT09.store = new Ext.data.JsonStore({
		removeAndSave : true,
		storeId : 'gridStore',
		idProperty : 'bookingID',
		url : HBBMRT09.url,
		fields : [ {
			name : 'bookingID'
		}, {
			name : 'fname'
		}, {
			name : 'phone'
		}, {
			name : 'sumPrice'
		}, {
			name : 'checkIN'
		}, {
			name : 'checkOUT'
		}, {
			name : 'CancelStatus'
		}, {
			name : 'createUser'
		}, {
			name : 'night'
		}, {
			name : 'lname'
		}, {
			name : 'email'
		}, {
			name : 'country'
		}, {
			name : 'status'
		},{
			name : 'rooms'
		},{
			name : 'roomID'
		},]
	});

	HBBMRT09.grid = new Ext.ss.grid.EditorGridPanel({
		store : HBBMRT09.store,
		columns : HBBMRT09.columns,
		sm : HBBMRT09.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ HBBMRT09.updateFunction,"Update status"],

		viewConfig : {
			forceFit : true
		},
		listeners : {
			beforeedit : function(e) {
				checkEdit(e);
			},
	          cellclick: function(grid, rowIndex, columnIndex, e){
	        	  HBBMRT09.grid.getColumnModel().setEditable('7', true);
			}
		},
		height : 250,
	});

	HBBMRT09.fieldSet = new Ext.form.FieldSet({
		title : 'Check cancel booking',
		items : [ HBBMRT09.grid ]
	});
}

Ext.onReady(function() {
	HBBMRT09.HBBMRT09Compo();
	HBBMRT09.formPanel = new Ext.form.FormPanel({
		url : HBBMRT09.url,
		items : [ HBBMRT09.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
