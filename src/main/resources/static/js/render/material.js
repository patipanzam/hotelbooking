var mat = {};
mat.url = application.contextPath + '/material.html';

mat.buildMaterialDetail = function() {

	mat.SearchMaterialType = new Ext.form.TextField({
		id : 'SearchMaterialType',
		fieldLabel : "ค้นหาประเภทวัสดุ :",
		width : 200,
		maxLength : 50
	});

	mat.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	mat.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ mat.SearchMaterialType ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ mat.SearchButton ]
		} ]
	});

	mat.materialTypeName = new Ext.form.TextField({
		id : 'materialTypeName',
		maxLength : 50
	});

	mat.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	mat.columns = [ mat.sm, {
		header : "ประเภทวัสดุ",
		dataIndex : 'materialTypeName',
		align : 'center',
		editor : mat.materialTypeName
	} ];

	mat.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'materialTypeID',
		url : mat.url,
		fields : [ {
			name : 'materialTypeID'
		}, {
			name : 'materialTypeName',
			allowBlank : false
		} ]
	});

	mat.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {
			mat.grid.add({});
		}
	});

	mat.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});

	mat.saveButton = new Ext.Button({
		iconCls : 'save',
		handler : function() {
			saveFunction();
		}
	});

	mat.PaggingToolbar = new Ext.PagingToolbar({
		store : mat.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	mat.grid = new Ext.ss.grid.EditorGridPanel({
		store : mat.store,
		columns : mat.columns,
		sm : mat.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ mat.addButton, mat.removeButton, mat.saveButton ],
		bbar : mat.PaggingToolbar,
		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	mat.fieldSet = new Ext.form.FieldSet({
		title : 'ประเภทวัสดุ',
		items : [ mat.SearchContainer, mat.grid ]
	});

	mat.SearchMaterial = new Ext.form.TextField({
		id : 'SearchMaterial',
		fieldLabel : "ค้นหาวัสดุ :",
		width : 200,
		maxLength : 50
	});

	mat.SearchMaterialTypeCombobox = new BaseComboBox.getMaterialTypeCombobox(
			'SearchMaterialTypeCombobox', {
				id : 'SearchMaterialTypeCombobox',
				fieldLabel : "ค้นหาประเภทวัสดุ :",
				allowBlank : true
			});

	mat.SearchButton2 = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction2();
		}
	});

	mat.SearchContainer2 = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ mat.SearchMaterial ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.173,
			items : [ mat.SearchMaterialTypeCombobox ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ mat.SearchButton2 ]
		} ]
	});

	mat.materialName = new Ext.form.TextField({
		id : 'materialName',
		maxLength : 50
	});

	mat.materialTypeCombobox = new BaseComboBox.getMaterialTypeCombobox(
			'materialTypeCombobox', {});

	mat.materialUnit = new Ext.form.TextField({
		id : 'materialUnit',
		maxLength : 50
	});

	mat.sm2 = new Ext.ss.grid.CheckboxSelectionModel({});

	mat.columns2 = [ mat.sm2,{
		header : "ประเภทวัสดุ",
		dataIndex : 'materialTypeID',
		align : 'center',
		editor : mat.materialTypeCombobox,
		renderer : Ext.util.Format.comboRenderer(mat.materialTypeCombobox)
	}, {
		header : "ชื่อวัสดุ",
		dataIndex : 'materialName',
		align : 'center',
		editor : mat.materialName
	},  {
		header : "หน่วย",
		dataIndex : 'materialUnit',
		align : 'center',
		editor : mat.materialUnit
	} ];

	mat.store2 = new Ext.data.JsonStore({
		storeId : 'mystore2',
		idProperty : 'materialID',
		url : mat.url,
		fields : [ {
			name : 'materialID'
		}, {
			name : 'materialName',
			allowBlank : false
		}, {
			name : 'materialTypeID',
			allowBlank : false
		}, {
			name : 'materialUnit',
			allowBlank : false
		} ]
	});

	mat.addButton2 = new Ext.Button({
		iconCls : 'add',
		handler : function() {
			mat.grid2.add({});
		}
	});

	mat.removeButton2 = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction2();
		}
	});

	mat.saveButton2 = new Ext.Button({
		iconCls : 'save',
		handler : function() {
			saveFunction2();
		}
	});

	mat.PaggingToolbar2 = new Ext.PagingToolbar({
		store : mat.store2,
		pageSize : 10,
		id : 'PaggingToolbar2',
		displayInfo : true
	});

	mat.grid2 = new Ext.ss.grid.EditorGridPanel({
		store : mat.store2,
		columns : mat.columns2,
		sm : mat.sm2,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ mat.addButton2, mat.removeButton2, mat.saveButton2 ],
		bbar : mat.PaggingToolbar2,
		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	mat.fieldSet2 = new Ext.form.FieldSet({
		title : 'วัสดุ',
		items : [ mat.SearchContainer2, mat.grid2 ]
	});

	mat.allFieldSet = new Ext.form.FieldSet({
		title : 'จัดการวัสดุ',
		items : [ mat.fieldSet, mat.fieldSet2 ]
	});

}

Ext.onReady(function() {
	mat.buildMaterialDetail();
	mat.formPanel = new Ext.form.FormPanel({
		border : false,
		url : mat.url,
		items : [ mat.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});
});
