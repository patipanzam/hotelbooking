var booking = {};
booking.url = application.contextPath +'/booking.html';

booking.bookingCompo = function(){

	
booking.bookingID = new Ext.form.Hidden({
	id : 'bookingID'
	});
booking.roomID = new Ext.form.Hidden({
	id : 'roomID'
});

booking.udloginname = new Ext.form.Hidden({
	id : 'udloginname'
});

booking.updateUser = new Ext.form.Hidden({
	id : 'updateUser'
});

booking.fileupload = new Ext.form.Hidden({
    id: 'attachment',
    name: 'documentURL',
    hideLabel: true,
    readOnly:true,
    emptyText: 'Select a file',
    fileUpload: false,
    msgTarget: 'side',
    width : 250,
    buttonCfg: {
        text: '',
        iconCls: ''
    },
    listeners: {
        fileselected: function (fp, fileName) {
        	if(fp.fileInput.dom.files[0].size <= 62000){
        		var reader = new FileReader();
	            reader.onload = function (e) {
	            	booking.panel.update('<img src='+e.target.result+' style="height:250px; width:250px"; />');
	            }
	            reader.readAsDataURL(fp.fileInput.dom.files[0]);
        	}else{
        		Ext.Msg.alert('Information', 'Image over size.', function(){
        			booking.fileupload.setValue('');
        		});
        	}
        }
    }
});

booking.panel = new Ext.Panel({
	  scroll : "vertical",
//	  border : false,
	  title   : "",
	  width: 260,
	  height: 260,
	  bodyStyle: {
		  paddingLeft:'5px',
		  paddingTop:'2px',
	      marginLeft: '3.5px'
	    },
	  html   : ""
	});

booking.fromDate = new Ext.ss.form.DateField({
	id : 'fromDate',
	fieldLabel: 'From-Date',
    width : 250,
    anchor:'60%',
    readOnly:true,
    toDateFieldId: 'toDate',
	    listeners: {
	    	'select' : function(from,fromdate){
	    		calculateDate();
	    	}
	    }
});

booking.toDate = new Ext.ss.form.DateField({
	id : 'toDate',
	fieldLabel: 'To-Date',
    width : 250,
    anchor:'60%',
    readOnly:true,
    fromDateFieldId: 'fromDate',
    listeners: {
    	'select' : function(to,todate){
    		calculateDate();
    	}
    }
});

booking.date = new Ext.form.TextField({
	id : 'date',
	fieldLabel: 'Stay',
    width : 250,
    readOnly:true
});



booking.roomTypeName = new Ext.form.TextField({
	id : 'roomTypeName',
	fieldLabel: 'Room',
	width : 250,
    readOnly:true
});

booking.roomPrice = new Ext.form.TextField({
	id : 'roomPrice',
	fieldLabel: 'Price per night',
	width : 250,
    readOnly:true
});

booking.rate = new Ext.form.TextField({
	id : 'rate',
	fieldLabel: 'Rate includes',
	width : 250,
    readOnly:true
});

booking.sumPrice = new Ext.form.TextField({
	id : 'sumPrice',
	fieldLabel: 'Total Price',
	width : 250,
    readOnly:true
});

//booking.total = new Ext.form.TextField({
//	id : 'total',
//	fieldLabel: 'Total Price',
//	width : 250,
//    readOnly:true
//});

////////////////////////////////////////////////////////////////////////////////////////

booking.bookingContainer = new Ext.Container({
    layout: 'column',
    items: [{
            xtype: 'container',
            layout: 'form',
            columnWidth: 0.4,
            items: [booking.panel]
        },{
            xtype: 'container',
            layout: 'form',
            columnWidth: 0.4,
            items: [booking.fromDate,booking.toDate,booking.date,booking.roomTypeName,booking.rate,booking.roomPrice,booking.sumPrice]
        }
    ]

});

booking.bookingFieldSet = new Ext.form.FieldSet({
    title: 'Enter booking details',
    items: [booking.bookingContainer]
});
}

///////////////////////////////////user////////////////////////////////////////////////////////

booking.buildUserDetail = function() {
	
	booking.id = new Ext.form.Hidden({
		id : 'id',
		fieldLabel : 'รหัสผู้ใช้งาน :',
		allowBlank : false,
	});

	booking.userName = new Ext.form.Hidden({
		id : 'userName',
		fieldLabel: __messages['label.userName'],
	    readOnly:true
	});
	
	booking.userPassword = new Ext.form.Hidden({
		id : 'userPassword',
		inputType: 'password',
		fieldLabel: __messages['label.userPassword'],
		 readOnly:true
	    
	});
	
	
	booking.userPasswordConfirm = new Ext.form.Hidden({
		id : 'userPasswordConfirm',
		inputType: 'password',
		fieldLabel: __messages['label.userPasswordConfirm'],
		 readOnly:true
	   
	});
	
	booking.firstName = new Ext.form.TextField({
		id : 'firstName',
		inputType: 'firstName',
		fieldLabel: 'First Name',
		 readOnly:true
	  
	});
	
	booking.lastName = new Ext.form.TextField({
		id : 'lastName',
		inputType: 'lastName',
		fieldLabel: 'Last Name',
		 readOnly:true
	    
	});
	

	
	booking.email = new Ext.form.TextField({
		id : 'email',
		fieldLabel: 'Email',
	    width : 250,
	    readOnly:true
	});

	booking.phone = new Ext.form.TextField({
		id : 'PHONE',
		inputType: 'phone',
		fieldLabel: 'Phone',
		 readOnly:true,
	   
	});
	


	
	
	booking.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'id',
		url : booking.url,
		fields : [ {
			name : 'id'
		}, {
			name : 'LGUSERNAME'
		}, {
			name : 'LGPASSWORD'
		}, {
			name : 'ROLENAME'
		},{
			name:'PHONE'
		}]
	});
	


	booking.roleContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ booking.firstName,booking.email]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [booking.lastName,booking.phone ]
		} ]

	});

	booking.userfieldSet = new Ext.form.FieldSet({
		title : 'Your information',
		items : [ booking.roleContainer]
	});
}

//////////////////////////////////////////////////////////////////////////////////////////////////

booking.buildGuestDetail = function() {
	
	booking.guestid = new Ext.form.Hidden({
		id : 'guestid',
		fieldLabel : 'รหัสผู้ใช้งาน :',
		allowBlank : false,
	});

	booking.firstNameguest = new Ext.form.TextField({
		id : 'firstNameguest',
		fieldLabel: 'Guest first name',
		allowBlank: false
	  
	});
	
	booking.lastNameguest = new Ext.form.TextField({
		id : 'lastNameguest',
		fieldLabel: 'Guest last name',
		allowBlank: false
	    
	});
	

	booking.emailguest = new Ext.form.TextField({
		id : 'emailguest',
		fieldLabel: 'Email',
	    width : 250,
	    allowBlank: false
	});

	booking.phoneguest = new Ext.form.TextField({
		id : 'phoneguest',
		fieldLabel: 'Mobile number',
		allowBlank: false
		
	   
	});
	booking.guestStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'Thailand','desc':'Thailand'},{'code':'Chinese','desc':'Chinese'},{'code':'Korea','desc':'Korea'}]
		},
		fields : ['code', 'desc']
	});
  
	booking.guestCombo = new Ext.ss.form.ComboBox({
	    store: booking.guestStore,
	    fieldLabel : [ 'Country of residence' ],
	    mode: 'local',
	    width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
	    valueField : 'code',
	    displayField:'code',
	    descriptionField : 'desc',
		showDescription : true,
	    triggerAction: 'all',
	    emptyText:'Please Select One',
//	    selectOnFocus:true
//	    applyTo: 'local-states'
	});
	


	
	
	booking.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'id',
		url : booking.url,
		fields : [ {
			name : 'id'
		}, {
			name : 'LGUSERNAME'
		}, {
			name : 'LGPASSWORD'
		}, {
			name : 'ROLENAME'
		},{
			name:'PHONE'
		}]
	});
	


	booking.guestContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ booking.firstNameguest,booking.emailguest]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [booking.lastNameguest]
		} ]

	});
  
	booking.guestContainer1 = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [booking.phoneguest]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [booking.guestCombo]
		} ]

	});

	booking.guestfieldSet = new Ext.form.FieldSet({
		title : 'Guest information',
		items : [ booking.guestContainer
		         ,booking.guestContainer1]
	});
}
   
   ///////////////////////////////////////////////////////////////////////////////////////////////
   
 

booking.buildRequestsDetail = function() {
   	
	booking.guestid = new Ext.form.Hidden({
   		id : 'id',
   		fieldLabel : 'รหัสผู้ใช้งาน :',
   		allowBlank : false,
   	});

	booking.non_smoking = new Ext.form.Checkbox({
	        id: 'non_smoking',
	        boxLabel:'I would like a non-smoking room'
	    });
	booking.late_check = new Ext.form.Checkbox({
	        id: 'late_check',
	        boxLabel:'I would like a late check-in'
	    });
	booking.high_floor = new Ext.form.Checkbox({
	        id: 'high_floor',
	        boxLabel:'I would like a room on a high floor'
	    });
	booking.large_bed = new Ext.form.Checkbox({
	        id: 'large_bed',
	        boxLabel:'I would like a large bed'
	    });
	booking.twin_beds = new Ext.form.Checkbox({
	        id: 'twin_beds',
	        boxLabel:'I would like twin beds'
	    });
	 
	booking.early_check = new Ext.form.Checkbox({
	        id: 'early_check',
	        boxLabel:'I would like an early check-in'
	    });
	booking.transfer = new Ext.form.Checkbox({
	        id: 'transfer',
	        boxLabel:'I would like an airport transfer'
	    });
   	
	booking.comment = new Ext.form.TextArea({
			id : 'comment',
			fieldLabel: 'Please enter comments in English or Thai.', 
			anchor:'60%',
		    allowBlank: false
		});
	 
	booking.nextButton = new Ext.Button({
			
//			iconCls : 'query',
			text:'Book Now!',
//				cls:'startbutton',
			
				
				handler : function(){
//					var id = room.roomID.getValue();
					saveFunction();
					
				}	
		});

     
     
	booking.CheckboxRequestsGroup = new Ext.form.CheckboxGroup({
         id: 'CheckboxRequestsGroup',
         fieldLabel:'Special requests  : ',
         columns: [300, 300,300],
         items: [booking.non_smoking,booking.late_check,booking.high_floor,booking.large_bed,booking.twin_beds,booking.early_check, booking.transfer]
     });
	 
	booking.requestsContainer = new Ext.Container({
			layout : 'column',
			items : [ {
				xtype : 'container',
				layout : 'form',
				columnWidth : 0.9,
				items : [booking.comment]
			}, {
				xtype : 'container',
				layout : 'form',
				columnWidth : 0.1,
				items : [booking.nextButton]
			} ]

		});

	booking.requestsfieldSet = new Ext.form.FieldSet({
   		title : 'Special requests',
   		items : [ booking.CheckboxRequestsGroup
   		          ,booking.requestsContainer]
   	});
   }

Ext.onReady(function () {

booking.bookingCompo();
booking.buildUserDetail();
booking.buildGuestDetail();
booking.buildRequestsDetail();

booking.allFieldSet = new Ext.form.FieldSet({
    title: '',
    items: [booking.bookingFieldSet,booking.userfieldSet,booking.guestfieldSet,booking.requestsfieldSet]
});

booking.formPanel = new Ext.form.FormPanel({
    url: booking.url,
    items: [booking.allFieldSet],
    plugins: [new Ext.ux.FitToParent({
            fitHeight: false
        })],
    renderTo: 'renderDiv'
});
});
