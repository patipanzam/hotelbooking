var pt = {};
pt.url = application.contextPath + '/projectType.html';

pt.buildProjectTypeDetail = function() {

	pt.SearchProjectType = new Ext.form.TextField({
		id : 'SearchProjectType',
		fieldLabel : "ค้นหาประเภทโครงการ :",
		width : 200,
		maxLength : 50
	});

	pt.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	pt.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ pt.SearchProjectType ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ pt.SearchButton ]
		} ]
	});

	pt.projectTypeName = new Ext.form.TextField({
		id : 'projectTypeName',
		maxLength : 50
	});

	pt.projectTypeDescription = new Ext.form.TextField({
		id : 'projectTypeDescription',
		maxLength : 50
	});

	pt.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	pt.columns = [ pt.sm, {
		header : "ประเภทโครงการ",
		dataIndex : 'projectTypeName',
		align : 'center',
		editor : pt.projectTypeName
	}, {
		header : "รายละเอียดเพิ่มเติม",
		dataIndex : 'projectTypeDescription',
		align : 'center',
		editor : pt.projectTypeDescription
	} ];

	pt.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'projectTypeID',
		url : pt.url,
		fields : [ {
			name : 'projectTypeID'
		}, {
			name : 'projectTypeName',
			allowBlank : false
		}, {
			name : 'projectTypeDescription',
			allowBlank : false
		} ]
	});

	pt.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {
			pt.grid.add({});
		}
	});

	pt.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});

	pt.PaggingToolbar = new Ext.PagingToolbar({
		store : pt.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	pt.grid = new Ext.ss.grid.EditorGridPanel({
		store : pt.store,
		columns : pt.columns,
		sm : pt.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ pt.addButton, pt.removeButton ],
		bbar : pt.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	pt.fieldSet = new Ext.form.FieldSet({
		title : 'จัดการประเภทโครงการ',
		items : [ pt.SearchContainer, pt.grid ]
	});
}

Ext.onReady(function() {
	pt.buildProjectTypeDetail();
	pt.formPanel = new Ext.form.FormPanel({
		border : false,
		url : pt.url,
		items : [ pt.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});
});
