var stout = {};
stout.url = application.contextPath + '/stockOUT.html';

stout.buildEmployeeDetail = function() {

	stout.stockOutID = new Ext.form.Hidden({
		id : 'stockOutID',
		fieldLabel : 'รหัสใบเบิกวัสดุ :',
		allowBlank : false,
	});

	stout.stockOutNO = new Ext.form.TextField({
		id : 'stockOutNO',
		fieldLabel : 'เลขที่ใบเบิกวัสดุ :',
		allowBlank : false
	});

	stout.date = new Ext.ss.form.DateField({
		id : 'date',
		fieldLabel : 'วันที่เบิกวัสดุ :',
		width : 150
	});

	stout.projectContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.44,
			items : [ stout.stockOutNO]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [ stout.date  ]
		} ]

	});

	stout.fieldSet = new Ext.form.FieldSet({
		title : 'ข้อมูลใบเบิกวัสดุ',
		items : [ stout.projectContainer ]
	});
	// ///////////////////////////GRID///////////////////
	stout.materialTypeCombobox = new BaseComboBox.getMaterialTypeCombobox(
			'materialTypeCombobox', {});

	stout.materialCombobox = new BaseComboBox.getMaterialCombobox(
			'materialCombobox', {});

	stout.quantity = new Ext.form.TextField({
		id : 'quantity',
		fieldLabel : 'จำนวนวัสดุ :',
		allowBlank : false
	});

	stout.materialUnit = new Ext.form.TextField({
		id : 'materialUnit',
		fieldLabel : 'หน่วย :'
	});

	stout.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	stout.columns = [ stout.sm, {
		header : "ประเภทวัสดุ",
		dataIndex : 'materialTypeID',
		align : 'center',
		editor : stout.materialTypeCombobox,
		renderer : Ext.util.Format.comboRenderer(stout.materialTypeCombobox)
	}, {
		header : "ชื่อวัสดุ",
		dataIndex : 'materialID',
		align : 'center',
		editor : stout.materialCombobox,
		renderer : Ext.util.Format.comboRenderer(stout.materialCombobox)
	}, {
		header : "จำนวน",
		dataIndex : 'quantity',
		align : 'center',
		editor : stout.quantity
	}, {
		header : "หน่วย",
		dataIndex : 'materialUnit',
		align : 'center'
	} ];

	stout.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'stockOutItemID',
		url : stout.url,
		fields : [ {
			name : 'stockOutItemID'
		}, {
			name : 'materialTypeID'
		}, {
			name : 'materialID'
		}, {
			name : 'quantity'
		}, {
			name : 'materialUnit'
		} ]
	});

	stout.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {

			stout.grid.add({

			});

		}
	});

	stout.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			stout.grid.remove({

			});

		}
	});

	stout.PaggingToolbar = new Ext.PagingToolbar({
		store : stout.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	stout.grid = new Ext.ss.grid.EditorGridPanel({
		store : stout.store,
		columns : stout.columns,
		sm : stout.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,

		tbar : [ stout.addButton, stout.removeButton ],
		bbar : stout.PaggingToolbar,

		viewConfig : {
			forceFit : true

		},

		height : 300,

	});

	stout.fieldSet2 = new Ext.form.FieldSet({
		title : 'ข้อมูลวัสดุ',
		items : [ stout.grid ]
	});
}

Ext.onReady(function() {

	stout.buildEmployeeDetail();

	stout.allFieldSet = new Ext.form.FieldSet({
		title : 'เพิ่มประวัติการเบิกวัสดุ',
		items : [ stout.fieldSet, stout.fieldSet2 ]
	});

	stout.formPanel = new Ext.form.FormPanel({
		url : stout.url,
		items : [ stout.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
