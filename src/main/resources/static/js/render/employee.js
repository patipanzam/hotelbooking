var emp = {};
emp.url = application.contextPath + '/employee.html';

emp.buildEmployeeHeader = function() {

	emp.employeeID = new Ext.form.Hidden({
		id : 'employeeID',
		fieldLabel : 'รหัสพนักงาน :',
		allowBlank : false,
	});

	emp.employeeName = new Ext.form.TextField({
		id : 'employeeName',
		fieldLabel : 'ชื่อพนักงาน :',
		width : 400,
		allowBlank : false
	});

	emp.employeeAddress = new Ext.ss.form.TextArea({
		id : 'employeeAddress',
		fieldLabel : 'ที่อยู่พนักงาน :',
		width : 400,
		height : 100
	});

	emp.employeePhone = new Ext.form.TextField({
		id : 'employeePhone',
		fieldLabel : 'เบอร์โทรศัพท์ :',
		width : 400,
		maskRe : /[0-9]/
	});

	emp.employeeCitizenID = new Ext.form.TextField({
		id : 'employeeCitizenID',
		fieldLabel : 'รหัสบัตรประชาชน :',
		allowBlank : false,
		width : 400,
		maxLength : 13,
		minLength : 13,
		maskRe : /[0-9]/
	});

	emp.employeeContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.44,
			items : [ emp.employeeName, emp.employeeAddress ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [ emp.employeeCitizenID, emp.employeePhone ]
		} ]

	});

	emp.fieldSet = new Ext.form.FieldSet({
		title : 'ประวัติพนักงาน',
		items : [ emp.employeeContainer ]
	});
};

emp.buildEmployeeGrid = function() {
	emp.positionTypeCombobox = new BaseComboBox.getPositionTypeCombobox(
			'positionTypeCombobox', {});

	emp.positionCombobox = new BaseComboBox.getPositionCombobox(
			'positionCombobox', {
				listeners : {
					'beforequery' : function(qe) {
						var lastEdit = emp.grid.lastEdit;
						var r = emp.grid.getStore().getAt(lastEdit.row);
						qe.combo.getStore().baseParams['positionTypeID'] = r
								.get('positionTypeID');
						delete emp.positionCombobox.lastQuery;
					}
				}
			});

	emp.positionComboboxTmp = new BaseComboBox.getPositionCombobox(
			'positionComboboxTmp', {});

	emp.salary = new Ext.form.NumberField({
		id : 'salary',
		allowBlank : false
	});

	emp.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	emp.columns = [ emp.sm, {
		header : "ประเภทตำแหน่งงาน",
		dataIndex : 'positionTypeID',
		align : 'center',
		editor : emp.positionTypeCombobox,
		renderer : Ext.util.Format.comboRenderer(emp.positionTypeCombobox)
	}, {
		header : "ตำแหน่งงาน",
		dataIndex : 'positionID',
		align : 'center',
		editor : emp.positionCombobox,
		allowBlank : false,
		renderer : Ext.util.Format.comboRenderer(emp.positionComboboxTmp)
	}, {
		header : "ค่าแรงรายวัน",
		dataIndex : 'salary',
		align : 'center',
		allowBlank : false,
		editor : emp.salary
	} ];

	emp.store = new Ext.data.JsonStore({
		removeAndSave : false,
		storeId : 'mystore',
		idProperty : 'skillID',
		url : emp.url,
		fields : [ {
			name : 'skillID'
		}, {
			name : 'positionID',
			allowBlank : false
		}, {
			name : 'positionName'
		}, {
			name : 'positionTypeName'
		}, {
			name : 'positionTypeID',
			allowBlank : false
		}, {
			name : 'salary',
			allowBlank : false
		} ]
	});

	emp.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {

			emp.grid.add({});
		}
	});

	emp.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			emp.grid.remove({

			});

		}
	});

	emp.saveButton = new Ext.Button({
		iconCls : 'save',
		handler : function() {

			saveFunction();
		}
	});

	emp.grid = new Ext.ss.grid.EditorGridPanel({
		store : emp.store,
		columns : emp.columns,
		sm : emp.sm,
		tbar : [ emp.addButton, emp.removeButton ],
		bbar : new Ext.PagingToolbar({
			pageSize : 5,
			store : emp.store
		}),
		viewConfig : {
			forceFit : true
		},
		height : 300,
		listeners : {
			'afterEdit' : function(e) {
				emp.checkPositionData(e);
			}
		}
	});

	emp.fieldSet2 = new Ext.form.FieldSet({
		title : 'ตำแหน่งงาน',
		items : [ emp.grid ]
	});
}

Ext.onReady(function() {

	emp.buildEmployeeHeader();
	emp.buildEmployeeGrid();
	emp.positionComboboxTmp.store.baseParams.positionTypeID = '';
	emp.positionComboboxTmp.doQuery();

	emp.allFieldSet = new Ext.form.FieldSet({
		title : 'เพิ่มพนักงาน',
		items : [ emp.fieldSet, emp.fieldSet2 ]
	});

	emp.formPanel = new Ext.form.FormPanel({
		url : emp.url,
		items : [ emp.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
