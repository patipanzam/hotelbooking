var fb = {};
fb.url = application.contextPath + '/feedblack.html';


fb.buildFeedbackHeader = function() {
	
	fb.feedbackStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'001','desc':'site Design'},{'code':'002','desc':'Property Imformation'},{'code':'003','desc':'Technical Issue'},{'code':'004','desc':'Language'},{'code':'005','desc':'Other'}]
		},
		fields : ['code', 'desc']
	});
	
	fb.feedbackCombo = new Ext.ss.form.ComboBox({
	    store: fb.feedbackStore,
	    fieldLabel : [ 'Please select a feedback category' ],
	    allowBlank: false,
	    mode: 'local',
	    width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
	    valueField : 'code',
	    displayField:'code',
	    descriptionField : 'desc',
		showDescription : true,
	    triggerAction: 'all',
	    emptyText:'Please select a feedback...',
//	    selectOnFocus:true
//	    applyTo: 'local-states'
	});
	
	
	fb.feedbackDetail = new Ext.form.TextArea({
		id : 'feedbackDetail',
		fieldLabel: 'Please share any comments you have for improving our System', anchor:'80%',
	    allowBlank: false
	});
	
	fb.feedbackContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [ fb.feedbackCombo]
		},  {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [ fb.feedbackDetail ]
		}]

	});
	
	

	fb.allfieldSet = new Ext.form.FieldSet({
		title : 'Give us feedback on our system!',
		items : [fb.feedbackContainer]
	});
	
}
	
	Ext.onReady(function () {
		
	fb.buildFeedbackHeader ();
		
		fb.allFieldSet = new Ext.form.FieldSet({
	        title: 'feedback',
	        items: [fb.allfieldSet]
	    });
		
		fb.formPanel = new Ext.form.FormPanel({
	        url: fb.url,
	        items: [fb.allFieldSet],
	        plugins: [new Ext.ux.FitToParent({
	                fitHeight: false
	            })],
	        renderTo: 'renderDiv'
	    });
	});

