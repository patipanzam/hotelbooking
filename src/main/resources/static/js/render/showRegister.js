var sr = {};
sr.url = application.contextPath + '/showRegister.html';
sr.urlEditRegister = application.contextPath +'/register.html';

sr.buildBranchDetail = function() {

	sr.SearchRegister = new Ext.form.TextField({
		id : 'SearchRegister',
		fieldLabel : "ค้นหาชื่อพนักงาน	 :",
		width : 200,
		maxLength : 50
	});

	sr.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	sr.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ sr.SearchRegister ]
		},  {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ sr.SearchButton ]
		} ]

	});

	sr.id = new Ext.form.TextField({
		id : 'id',
		maxLength : 50
	});

	sr.LGUSERNAME = new Ext.form.TextField({
		id : 'LGUSERNAME',
		maxLength : 50
	});

	sr.LGPASSWORD = new Ext.form.TextField({
		id : 'LGPASSWORD',
		maxLength : 50
	});

	sr.ROLENAME = new Ext.form.TextField({
		id : 'ROLENAME',
		maxLength : 50
	});

	
	sr.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	sr.columns = [ sr.sm, {
		header : "แก้ไข",
		dataIndex : 'id',
		align : 'center',
		renderer : renderInstall
	}, {
		header : "ชื่อผู้ใช้งาน",
		dataIndex : 'LGUSERNAME',
		align : 'center'
	}, {
		header : "รหัสผ่าน",
		dataIndex : 'LGPASSWORD',
		align : 'center'
	}, {
		header : "ประเภทผู้ใช้",
		dataIndex : 'ROLENAME',
		align : 'center'
	} ];

	sr.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'id',
		url : sr.url,
		fields : [ {
			name : 'id'
		}, {
			name : 'LGUSERNAME'
		}, {
			name : 'LGPASSWORD'
		}, {
			name : 'ROLENAME'
		}]
	});

	sr.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});

	sr.PaggingToolbar = new Ext.PagingToolbar({
		store : sr.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	sr.grid = new Ext.ss.grid.EditorGridPanel({
		store : sr.store,
		columns : sr.columns,
		sm : sr.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ sr.removeButton ],
		bbar : sr.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	sr.fieldSet = new Ext.form.FieldSet({
		title : 'รายชื่อผู้ใช้งาน',
		items : [ sr.SearchContainer, sr.grid ]
	});
}

Ext.onReady(function() {
	sr.buildBranchDetail();
	sr.formPanel = new Ext.form.FormPanel({
		border : false,
		url : sr.url,
		items : [ sr.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
