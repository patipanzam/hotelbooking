var HBBMRT07 = {};
HBBMRT07.url = application.contextPath + '/cancelBooking.html';

HBBMRT07.HBBMRT07Compo = function() {

	HBBMRT07.bookingID = new Ext.form.TextField({
		id : 'bookingID',

	});

	HBBMRT07.createUser = new Ext.form.TextField({
		id : 'createUser',

	});

	HBBMRT07.phone = new Ext.form.TextField({
		id : 'phone',

	});

	HBBMRT07.sumPrice = new Ext.form.TextField({
		id : 'sumPrice',

	});
	
	HBBMRT07.checkIN = new Ext.form.DateField({
		id : 'checkIN',

	});
	
	HBBMRT07.checkOUT = new Ext.form.DateField({
		id : 'checkOUT',
		
	});

	HBBMRT07.SearchStatus = new Ext.ss.form.ComboBox({
		id : 'CancelStatus',
		fieldLabel : "findstatus",
		mode : 'local',
		width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
		valueField : 'CancelStatus',
		displayField : 'CancelStatus',
		showDescription : false,
		width : 265,
		store : new Ext.data.ArrayStore({
			id : 0,
			fields : [ 'StatusID', 'CancelStatus' ],
			data : [ [ 1, 'Cancel' ]]
		})
	});

	HBBMRT07.updateFunction = new Ext.Button({
		iconCls : 'save',
		handler : function() {
			updateFunction();
		}
	});

	HBBMRT07.sm = new Ext.ss.grid.CheckboxSelectionModel({});
	HBBMRT07.columns = [ HBBMRT07.sm, {
		header : "Booking Number",
		dataIndex : 'bookingID',
		align : 'center'

	}, {
		header : "Total Amount",
		dataIndex : 'sumPrice',
		align : 'center'
	}, {
		header : "Booking by",
		dataIndex : 'createUser',
		align : 'center'
	}, {
		header : "Telephone number",
		dataIndex : 'phone',
		align : 'center'
	}, {
		header : "Check in",
		dataIndex : 'checkIN',
		align : 'center'
	}, {
		header : "Check out",
		dataIndex : 'checkOUT',
		align : 'center'

	}, {
		header : "Status",
		dataIndex : 'CancelStatus',
		align : 'center',
		editor : HBBMRT07.SearchStatus
	}];

	HBBMRT07.store = new Ext.data.JsonStore({
		removeAndSave : true,
		storeId : 'gridStore',
		idProperty : 'bookingID',
		url : HBBMRT07.url,
		fields : [ {
			name : 'bookingID'
		}, {
			name : 'fname'
		}, {
			name : 'phone'
		}, {
			name : 'sumPrice'
		}, {
			name : 'checkIN'
		}, {
			name : 'checkOUT'
		}, {
			name : 'CancelStatus'
		}, {
			name : 'createUser'
		}, {
			name : 'night'
		}, {
			name : 'lname'
		}, {
			name : 'email'
		}, {
			name : 'country'
		}, {
			name : 'status'
		},{
			name : 'rooms'
		},{
			name : 'roomID'
		},]
	});

	HBBMRT07.grid = new Ext.ss.grid.EditorGridPanel({
		store : HBBMRT07.store,
		columns : HBBMRT07.columns,
		sm : HBBMRT07.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [HBBMRT07.updateFunction,"Send cancellation booking"],

		viewConfig : {
			forceFit : true
		},
		listeners : {
			beforeedit : function(e) {
				 checkEdit(e);
			},
	          cellclick: function(grid, rowIndex, columnIndex, e){
	        	  HBBMRT07.grid.getColumnModel().setEditable('7', true);
			}
		},
		height : 250,
	});

//	HBBMRT07.fieldSet = new Ext.form.FieldSet({
//		title : 'My Booking',
//		items : [ HBBMRT07.grid ]
//	});
}

Ext.onReady(function() {
	HBBMRT07.HBBMRT07Compo();
	
	HBBMRT07.formPanel = new Ext.form.FormPanel({
		url : HBBMRT07.url,
		items : [ HBBMRT07.grid ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

	
	
});
