var register = {};
register.url = application.contextPath + '/profile.html';


if (!register.loadMask) {
	register.loadMask = new Ext.LoadMask(Ext.getBody(), {
		msg : __messages['message.processing']
	});
}

register.buildUserDetail = function() {
	
	register.id = new Ext.form.Hidden({
		id : 'id',
		fieldLabel : 'รหัสผู้ใช้งาน :',
		allowBlank : false,
	});

	register.userName = new Ext.form.TextField({
		id : 'userName',
		fieldLabel: __messages['label.userName'],
	    readOnly:true
	});
	
	register.userPassword = new Ext.form.TextField({
		id : 'userPassword',
		inputType: 'password',
		fieldLabel: __messages['label.userPassword'],
		 readOnly:true
	    
	});
	
	
	register.userPasswordConfirm = new Ext.form.TextField({
		id : 'userPasswordConfirm',
		inputType: 'password',
		fieldLabel: __messages['label.userPasswordConfirm'],
		 readOnly:true
	    
	});
	
	register.firstName = new Ext.form.TextField({
		id : 'firstName',
		inputType: 'firstName',
		fieldLabel: 'First Name',
		 readOnly:true
	    
	});
	
	register.lastName = new Ext.form.TextField({
		id : 'lastName',
		inputType: 'lastName',
		fieldLabel: 'Last Name',
		 readOnly:true
	   
	});
	
	register.email = new Ext.form.TextField({
		id : 'email',
	    fieldLabel: 'Email',
		 readOnly:true
	    
	});
	
	register.phone = new Ext.form.TextField({
		id : 'PHONE',
		inputType: 'phone',
		fieldLabel: 'Phone',
		 readOnly:true
	    
	});
	
//	register.roleName = new Ext.form.TextField({
//		id : 'ROLENAME',
//		inputType: 'roleName',
//		fieldLabel: 'User Type',
//		 readOnly:true,
//	    allowBlank: false
//	});

//	register.roleCombobox = new Ext.ss.form.Hidden({
//		id : 'roleCombobox',
//		fieldLabel : 'User Type :',
//		allowBlank : false,
//		 readOnly:true,
//		mode : 'remote',
//		width : 150,
//		triggerAction : 'all',
//		minChars : 0,
////		forceSelection : false,
//		valueField : 'roleId',
//		displayField : 'roleCode',
//		descriptionField : 'roleName',
//		showDescription : false,
//		store : {
//			xtype : 'jsonstore',
//			storeId : id + '-store',
//			idProperty : 'roleId',
//			fields : [ 'roleId', 'roleCode', 'roleName' ],
//			url : application.contextPath + '/combobox.html',
//			baseParams : {
//				method : 'role'
//			}
//		}
//	});
	
	register.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'id',
		url : register.url,
		fields : [ {
			name : 'id'
		}, {
			name : 'LGUSERNAME'
		}, {
			name : 'LGPASSWORD'
		}, {
			name : 'ROLENAME'
		},{
			name:'PHONE'
		}]
	});
	


	register.roleContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ register.userName, register.userPassword,register.firstName,register.email ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [register.userPasswordConfirm, register.lastName,register.phone ]
		} ]

	});

	register.fieldSet = new Ext.form.FieldSet({
		title : 'Profile details',
		items : [ register.roleContainer ]
	});
}

/////////////////////////////////////////////////////////////////////////////////////////////////


Ext.onReady(function() {
	register.buildUserDetail();
	register.formPanel = new Ext.form.FormPanel({
		url : register.url,
		items : [ register.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});
	
	
});
