var he = {};
he.url = application.contextPath + '/homeEngineer.html';

if (!he.loadMask) {
	he.loadMask = new Ext.LoadMask(Ext.getBody(), {
		msg : __messages['message.processing']
	});
}

he.buildProjectDetail = function() {

	he.SearchProject = new Ext.form.TextField({
		id : 'SearchProject',
		fieldLabel : "ค้นหาโครงการ :",
		width : 200,
		maxLength : 50
	});

	he.SearchProjectTypeCombobox = new BaseComboBox.getProjectTypeCombobox(
			'SearchProjectTypeCombobox', {
				id : 'SearchProjectTypeCombobox',
				fieldLabel : "ค้นหาประเภทโครงการ :",
				allowBlank : true
			});

	he.SearchBranchCombobox = new BaseComboBox.getBranchCombobox(
			'SearchBranchCombobox', {
				id : 'SearchBranchCombobox',
				fieldLabel : "ค้นหาสาขา :",
				allowBlank : true
			});

	he.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {

			searchFunction();
		}
	});

	he.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ he.SearchProject ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.165,
			items : [ he.SearchProjectTypeCombobox ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.173,
			items : [ he.SearchBranchCombobox ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ he.SearchButton ]
		} ]

	});

	he.projectID = new Ext.form.TextField({
		maxLength : 255,
	});

	he.projectName = new Ext.form.TextField({
		maxLength : 255,
	});

	he.projectLocation = new Ext.form.TextField({
		maxLength : 255,
	});

	he.projectDateStart = new Ext.form.TextField({
		maxLength : 255,
	});

	he.projectDateEnd = new Ext.form.TextField({
		maxLength : 255,
	});

	he.projectBudget = new Ext.form.TextField({
		maxLength : 255,
	});

	he.projectTypeName = new Ext.form.TextField({
		maxLength : 255,
	});

	he.branchName = new Ext.form.TextField({
		maxLength : 255,
	});

	he.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	he.columns = [ he.sm, {
		header : 'รหัสโครงการ',
		dataIndex : 'projectID',
		align : 'center',
		renderer : function(value, metaData, record) {
			return '<a href=\"javascript:he.doView(\''

			+ record.get('projectID') + '\');\">' + value + '</a>';
		}
	//		 
	}, {
		header : 'ชื่อโครงการ',
		dataIndex : 'projectName',
		align : 'left'
	}, {
		header : 'ประเภทโครงการ',
		dataIndex : 'projectTypeName',
		align : 'center'
	}, {
		header : 'สาขา',
		dataIndex : 'branchName',
		align : 'center'
	}, {
		header : 'ที่อยู่โครงการ',
		dataIndex : 'projectLocation',
		align : 'left'
	}, {
		header : 'งบประมาณ (บาท)',
		dataIndex : 'projectBudget',
		align : 'center',
		renderer : Ext.util.Format.numberRenderer('0,000')
	}, {
		header : 'วันเริ่มโคงการ',
		dataIndex : 'projectDateStart',
		align : 'center',
		renderer : Ext.util.Format.dateRenderer('d/m/Y')
	}, {
		header : 'วันสิ้นสุดโครงการ',
		dataIndex : 'projectDateEnd',
		align : 'center',
		renderer : Ext.util.Format.dateRenderer('d/m/Y')
	} ];

	he.store = new Ext.data.JsonStore({
		removeAndSave : false,
		storeId : 'gridStore',
		idProperty : 'projectID',
		url : he.url,
		fields : [ {
			name : 'projectID'
		}, {
			name : 'projectName',
		}, {
			name : 'projectLocation',
		}, {
			name : 'projectDateStart',
		}, {
			name : 'projectDateEnd',
		}, {
			name : 'projectBudget',
		}, {
			name : 'projectTypeName',
		}, {
			name : 'branchName',
		} ]
	});

	he.grid = new Ext.ss.grid.EditorGridPanel({
		store : he.store,
		columns : he.columns,
		sm : he.sm,
		tbar : [],
		viewConfig : {
			forceFit : true
		},
		height : 300,
		listeners : {}
	});

	he.gridFieldSet = new Ext.form.FieldSet({
		title : 'เลือกโครงการโครงการ',
		items : [ he.SearchContainer, he.grid ]
	});

}

Ext.onReady(function() {

	he.buildProjectDetail();

	he.allFieldSet = new Ext.form.FieldSet({
		title : '',
		items : [ he.gridFieldSet ]
	});

	he.formPanel = new Ext.form.FormPanel({
		url : he.url,
		items : [ he.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
