var room = {};
room.url = application.contextPath +'/room.html';

room.roomCompo = function(){
	
	room.roomID = new Ext.form.Hidden({
		id : 'roomID'
	});
	
	room.udloginname = new Ext.form.Hidden({
		id : 'udloginname'
	});
	
	room.updateUser = new Ext.form.Hidden({
		id : 'updateUser'
	});
	
	
	room.buildingCombobox = new Ext.ss.form.ComboBox({
		 id: 'buildingCombobox',
		 fieldLabel: ['Building'],
		 readOnly:true,
      mode: 'remote',
      width: 148,
      triggerAction: 'all',
      minChars: 0,
      forceSelection: true,
      valueField: 'buildingID',
      displayField: 'buildingID',
      descriptionField: 'buildingName',
      showDescription: true,
//      useOriginalTpl: true,
      store: {
          xtype: 'jsonstore',
          storeId: id + '-store',
          idProperty: 'buildingID',
          fields: ['buildingID', 'buildingName'],
          url: application.contextPath + '/combobox.html',
          baseParams: {
              method: 'building'
          }
      }
	});
	
//	room.roomtypeCombobox = new Ext.ss.form.ComboBox({
//		 id: 'roomtypeCombobox',
//		 fieldLabel: ['Room Type'],
//		 allowBlank: false,
//     mode: 'remote',
//     width: 148,
//     triggerAction: 'all',
//     minChars: 0,
//     forceSelection: true,
//     valueField: 'roomTypeName',
//     displayField: 'roomTypeName',
//     descriptionField: 'roomTypeName',
//     showDescription: true,
////     useOriginalTpl: true,
//     store: {
//         xtype: 'jsonstore',
//         storeId: id + '-store',
//         idProperty: 'roomID',
//         fields: ['roomID', 'roomTypeName'],
//         url: application.contextPath + '/combobox.html',
//         baseParams: {
//             method: 'roomtype'
//         }
//     }
//	});
	
	
   room.roomTypeName = new Ext.form.TextField({
		id : 'roomTypeName',
		fieldLabel: 'Room Type',
		width : 250,
	    readOnly:true
	});
	
room.roomBeds = new Ext.form.RadioGroup({
        fieldLabel:['roomBeds'] + ' : ',
        id: 'roomBeds',
        readOnly:true,
        columns: [100, 100, 100],
        items: [{
                boxLabel:['single beds'],
                name: 'roomBeds',
                inputValue: 'single beds'
            }, {
                boxLabel:['double beds'],
                name: 'roomBeds',
                inputValue: 'double beds'
            }, 
        ]
	});
	room.freewifi = new Ext.form.Checkbox({
        id: 'freewifi',
        boxLabel:'Free WiFi'
    });
	
	room.smocking = new Ext.form.Checkbox({
        id: 'smocking',
        boxLabel:'SmockingAreas'
    });
	room.service = new Ext.form.Checkbox({
        id: 'service',
        boxLabel:'Service-Airport'
    });
	room.breaks = new Ext.form.Checkbox({
        id: 'breaks',
        boxLabel:'Break Fast'
    });
	room.CheckboxFacilitiesGroup = new Ext.form.CheckboxGroup({
        id: 'CheckboxFacilitiesGroup',
        fieldLabel:'Facilities  : ',
        columns: [150, 150],
        items: [room.freewifi,room.smocking,room.service,room.breaks]
    });
	
	
	room.roomDate = new Ext.form.DateField({
		id : 'roomDate',
		fieldLabel: 'Room Date',
	    anchor:'60%',
	    readOnly:true
	});
	
	room.roomPrice = new Ext.form.TextField({
		id : 'roomPrice',
		fieldLabel: 'Room Price',
	   readOnly:true
	});
	
	
	room.roomNo = new Ext.form.TextField({
		id : 'roomNo',
		fieldLabel: 'Room No',
	    readOnly:true
	});
	
	
	
	room.typeDetail = new Ext.form.TextArea({
		id : 'typeDetail',
		fieldLabel: 'Type Detail', anchor:'30%',
	    readOnly:true
	});
	
	room.fileupload = new Ext.form.Hidden({
	    id: 'attachment',
	    name: 'documentURL',
	    hideLabel: true,
	    readOnly:true,
	    emptyText: 'Select a file',
	    fileUpload: true,
	    msgTarget: 'side',
	    width : 250,
	    buttonCfg: {
	        text: '',
	        iconCls: ''
	    },
	    listeners: {
	        fileselected: function (fp, fileName) {
	        	if(fp.fileInput.dom.files[0].size <= 62000){
	        		var reader = new FileReader();
		            reader.onload = function (e) {
		            	room.panel.update('<img src='+e.target.result+' style="height:250px; width:250px"; />');
		            }
		            reader.readAsDataURL(fp.fileInput.dom.files[0]);
	        	}else{
	        		Ext.Msg.alert('Information', 'Image over size.', function(){
	        			room.fileupload.setValue('');
	        		});
	        	}
	        }
	    }
	});
	
	room.panel = new Ext.Panel({
		  scroll : "vertical",
//		  border : false,
		  title   : "",
		  width: 260,
		  height: 260,
		  bodyStyle: {
			  paddingLeft:'5px',
			  paddingTop:'2px',
		      marginLeft: '3.5px'
		    },
		  html   : ""
		});
//////////////////////////////////////////01/////////////////////////////////////////	
	room.roomtypeContainer = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room. buildingCombobox]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomTypeName ]
	        }
	    ]

	});
	
	/////////////////////////////////////////02///////////////////////////////////////
	
	room.role2Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomBeds]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.CheckboxFacilitiesGroup]
	        }
	    ]

	});
	
	////////////03/////////////////////////////////////////////////////////////////
	
	room.role3Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomDate]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomPrice]
	        }
	    ]

	});
	////////////////////////4////////////////////////////////////////////
	room.role4Container = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: [room.roomNo]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.4,
	            items: []
	        }
	    ]

	});
	
	///////////////////////////////////////////////////////////////////
	
	room.imgeContainer = new Ext.Container({
	    layout: 'column',
	    items: [{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.5,
	            items: [room.fileupload]
	        },{
	            xtype: 'container',
	            layout: 'form',
	            columnWidth: 0.5,
	            items: [room.panel ]
	        }
	    ]

	});
	
	room.roomFieldSet = new Ext.form.FieldSet({
	    title: 'Room',
	    items: [room.roomtypeContainer
	            ,room.role2Container 
	            ,room.role3Container
	            , room.role4Container
	            ,room.typeDetail
	            ,room.imgeContainer]
	});
}

//room.nextButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Save',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = room.roomID.getValue();
//			saveFunction();
//			
//		}	
//});
//room.cancelButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Cancel',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = room.roomID.getValue();
//			cancelFunction();
//			
//		}	
//});
//room.goButton = new Ext.Button({
//	
////	iconCls : 'query',
//	text:'Listroom',
////		cls:'startbutton',
//	
//		
//		handler : function(){
////			var id = room.roomID.getValue();
//			goFunction();
//			
//		}	
//});
//
//room.buttonFieldSet = new Ext.form.FieldSet({
//	collapsible: false,
//    title: ' ',
//    layout :'column',
//    items: [
//{
//xtype : 'container',
//layout : 'form',
//columnWidth : 0.30,
//items : [room.nextButton  ]
//},
//{
//xtype : 'container',
//layout : 'form',
//columnWidth : 0.30,
//items : [room.cancelButton ]
//},{
//	xtype : 'container',
//	layout : 'form',
//	columnWidth : 0.30,
//	items : [room.goButton ]
//	}
//            
//       ]
//});

Ext.onReady(function () {
	
	room.roomCompo();
	
	room.allFieldSet = new Ext.form.FieldSet({
        title: 'Room Detail',
        items: [room.roomFieldSet]
    });
	
	room.formPanel = new Ext.form.FormPanel({
        url: room.url,
        items: [room.allFieldSet],
        plugins: [new Ext.ux.FitToParent({
                fitHeight: false
            })],
        renderTo: 'renderDiv'
    });
});
