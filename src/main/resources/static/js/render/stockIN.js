var stin = {};
stin.url = application.contextPath + '/stockIN.html';

stin.buildEmployeeDetail = function() {

	stin.stockInID = new Ext.form.Hidden({
		id : 'stockInID',
		fieldLabel : 'รหัสใบรับวัสดุ :',
		allowBlank : false,
	});

	stin.stockInNO = new Ext.form.TextField({
		id : 'stockInNO',
		fieldLabel : 'เลขที่ใบรับวัสดุ :',
		allowBlank : false
	});

	stin.date = new Ext.ss.form.DateField({
		id : 'date',
		fieldLabel : 'วันที่รับวัสดุ :',
		width : 150
	});

	stin.supplier = new Ext.form.TextField({
		id : 'supplier',
		fieldLabel : 'ชื่อร้านค้า :',
		width : 150
	});

	stin.projectContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.44,
			items : [ stin.stockInNO, stin.date ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [ stin.supplier ]
		} ]

	});

	stin.fieldSet = new Ext.form.FieldSet({
		title : 'ข้อมูลใบรับวัสดุ',
		items : [ stin.projectContainer ]
	});
	// ///////////////////////////GRID///////////////////
	stin.materialTypeCombobox = new BaseComboBox.getMaterialTypeCombobox(
			'materialTypeCombobox', {});

	stin.materialCombobox = new BaseComboBox.getMaterialCombobox(
			'materialCombobox', {});

	stin.quantity = new Ext.form.TextField({
		id : 'quantity',
		fieldLabel : 'จำนวนวัสดุ :',
		allowBlank : false
	});

	stin.materialUnit = new Ext.form.TextField({
		id : 'materialUnit',
		fieldLabel : 'หน่วย :'
	});

	stin.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	stin.columns = [ stin.sm, {
		header : "ประเภทวัสดุ",
		dataIndex : 'materialTypeID',
		align : 'center',
		editor : stin.materialTypeCombobox,
		renderer : Ext.util.Format.comboRenderer(stin.materialTypeCombobox)
	}, {
		header : "ชื่อวัสดุ",
		dataIndex : 'materialID',
		align : 'center',
		editor : stin.materialCombobox,
		renderer : Ext.util.Format.comboRenderer(stin.materialCombobox)
	}, {
		header : "จำนวน",
		dataIndex : 'quantity',
		align : 'center',
		editor : stin.quantity
	}, {
		header : "หน่วย",
		dataIndex : 'materialUnit',
		align : 'center'
	} ];

	stin.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'stockInItemID',
		url : stin.url,
		fields : [ {
			name : 'stockInItemID'
		}, {
			name : 'materialTypeID'
		}, {
			name : 'materialID'
		}, {
			name : 'quantity'
		}, {
			name : 'materialUnit'
		} ]
	});

	stin.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {

			stin.grid.add({

			});

		}
	});

	stin.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			stin.grid.remove({

			});

		}
	});

	stin.PaggingToolbar = new Ext.PagingToolbar({
		store : stin.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	stin.grid = new Ext.ss.grid.EditorGridPanel({
		store : stin.store,
		columns : stin.columns,
		sm : stin.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,

		tbar : [ stin.addButton, stin.removeButton ],
		bbar : stin.PaggingToolbar,

		viewConfig : {
			forceFit : true

		},

		height : 300,

	});

	stin.fieldSet2 = new Ext.form.FieldSet({
		title : 'ข้อมูลวัสดุ',
		items : [ stin.grid ]
	});
}

Ext.onReady(function() {

	stin.buildEmployeeDetail();

	stin.allFieldSet = new Ext.form.FieldSet({
		title : 'เพิ่มประวัติการรับวัสดุ',
		items : [ stin.fieldSet, stin.fieldSet2 ]
	});

	stin.formPanel = new Ext.form.FormPanel({
		url : stin.url,
		items : [ stin.allFieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
