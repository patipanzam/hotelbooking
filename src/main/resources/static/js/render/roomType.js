var bdp = {};
bdp.url = application.contextPath + '/roomtype.html';

bdp.buildRoomtypeDetail = function() {
	
	bdp.roomID = new Ext.form.Hidden({
		id : 'roomID'
	});
	
	bdp.udloginname = new Ext.form.Hidden({
		id : 'udloginname'
	});
	
	bdp.updateUser = new Ext.form.Hidden({
		id : 'updateUser'
	});

	bdp.SearchRoomType = new Ext.form.TextField({
		id : 'SearchRoomType',
		fieldLabel : "SearchRoomType :",
		width : 200,
		maxLength : 50
	});

	bdp.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {

			searchFunction();
		}
	});

	bdp.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ bdp.SearchRoomType ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ bdp.SearchButton ]
		} ]

	});

	bdp.roomTypeName = new Ext.form.TextField({
		id : 'roomTypeName',
		maxLength : 50
	});
	
	bdp.rate = new Ext.form.TextField({
		id : 'rate',
		maxLength : 50
	});
	
	bdp.max = new Ext.form.TextField({
		id : 'max',
		maxLength : 50
	});

	
	bdp.PromotionCombobox = new Ext.ss.form.ComboBox({
		 id: 'PromotionCombobox',
		 fieldLabel: ['Promotion'],
		 allowBlank: false,
   mode: 'remote',
   width: 148,
   triggerAction: 'all',
   minChars: 0,
   forceSelection: true,
   valueField: 'promotionID',
   displayField: 'rate',
   descriptionField: 'name',
   showDescription: true,
//   useOriginalTpl: true,
   store: {
       xtype: 'jsonstore',
       storeId: id + '-store',
       idProperty: 'promotionID',
       fields: ['promotionID', 'name','rate'],
       url: application.contextPath + '/combobox.html',
       baseParams: {
           method: 'promotion'
       }
   }
	});
	

	bdp.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	bdp.columns = [ bdp.sm, {
		header : "RoomType Name",
		dataIndex : 'roomTypeName',
		align : 'center',
		editor : bdp.roomTypeName
	},{
		header : "Promotion of Room",
		dataIndex : 'promotionID',
		align : 'center',
		editor :bdp.PromotionCombobox ,
		editor1 : bdp.rate
	},{
		header : "Maximum of RoomType",
		dataIndex : 'max',
		align : 'center',
		editor : bdp.max
	}
	];

	bdp.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'roomID',
		url : bdp.url,
		fields : [ {
			name : 'roomID'
		},{
			name : 'rate'
		},{
			name : 'roomTypeName',
			allowBlank : false
		},{
			name : 'promotionID',
			allowBlank : false
		},{
			name : 'max',
			allowBlank : false
		}
		]
	});

	bdp.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {

			bdp.grid.add({

			});
		}
	});

	bdp.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			removeFunction();

		}
	});

	bdp.saveButton = new Ext.Button({
		iconCls : 'save',
		handler : function() {

			saveFunction();

		}
	});

	bdp.PaggingToolbar = new Ext.PagingToolbar({
		store : bdp.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	bdp.grid = new Ext.ss.grid.EditorGridPanel({
		store : bdp.store,
		columns : bdp.columns,
		sm : bdp.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,

		tbar : [ bdp.addButton, bdp.removeButton, bdp.saveButton ],
		bbar : bdp.PaggingToolbar,

		viewConfig : {
			forceFit : true

		},

		height : 300,

	});

	bdp.fieldSet = new Ext.form.FieldSet({
		title : 'Manage RoomType',
		items : [ bdp.SearchContainer, bdp.grid ]
	});

	


//      pos.allFieldSet = new Ext.form.FieldSet({
//		title : 'Manage Building',
//		items : [ pos.fieldSet ]
//	});

}

Ext.onReady(function() {

	bdp.buildRoomtypeDetail();

	bdp.formPanel = new Ext.form.FormPanel({
		border : false,
		url : bdp.url,
		items : [ bdp.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});

