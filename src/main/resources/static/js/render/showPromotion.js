var sr = {};
sr.url = application.contextPath + '/showPromotion.html';
sr.urlEditPromotion = application.contextPath +'/promotion.html';

sr.buildBranchDetail = function() {

	sr.SearchRegister = new Ext.form.TextField({
		id : 'SearchRegister',
		fieldLabel : "SearchPromotion	 :",
		width : 200,
		maxLength : 50
	});

	sr.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	sr.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ sr.SearchRegister ]
		},  {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ sr.SearchButton ]
		} ]

	});

	sr.promotionID = new Ext.form.TextField({
		id : 'promotionID',
		maxLength : 50
	});

	sr.NAME = new Ext.form.TextField({
		id : 'NAME',
		maxLength : 50
	});

	sr.SUMDATE = new Ext.form.TextField({
		id : 'SUMDATE',
		maxLength : 50
	});

	sr.RATE = new Ext.form.TextField({
		id : 'RATE',
		maxLength : 50
	});

	
	sr.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	sr.columns = [ sr.sm, {
		header : "แก้ไข",
		dataIndex : 'promotionID',
		align : 'center',
		renderer : renderInstall
	}, {
		header : "Promotion Name",
		dataIndex : 'NAME',
		align : 'center'
	}, {
		header : "Promotion duration left",
		dataIndex : 'SUMDATE',
		align : 'center'
	}, {
		header : "Promotion discount rate",
		dataIndex : 'RATE',
		align : 'center'
	} ];

	sr.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'promotionID',
		url : sr.url,
		fields : [ {
			name : 'promotionID'
		}, {
			name : 'NAME'
		}, {
			name : 'SUMDATE'
		}, {
			name : 'RATE'
		}]
	});

	sr.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});

	sr.PaggingToolbar = new Ext.PagingToolbar({
		store : sr.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	sr.grid = new Ext.ss.grid.EditorGridPanel({
		store : sr.store,
		columns : sr.columns,
		sm : sr.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ sr.removeButton ],
		bbar : sr.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	sr.fieldSet = new Ext.form.FieldSet({
		title : 'รายชื่อผู้ใช้งาน',
		items : [ sr.SearchContainer, sr.grid ]
	});
}

Ext.onReady(function() {
	sr.buildBranchDetail();
	sr.formPanel = new Ext.form.FormPanel({
		border : false,
		url : sr.url,
		items : [ sr.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
