var showEmployee = {};
showEmployee.url = application.contextPath + '/showEmployee.html';
showEmployee.urlEditEmployee = application.contextPath +'/employee.html';

if (!showEmployee.loadMask) {
	showEmployee.loadMask = new Ext.LoadMask(Ext.getBody(), {
		msg : __messages['message.processing']
	});
}

showEmployee.buildEmployeeDetail = function() {
	
	showEmployee.SearchEmployee = new Ext.form.TextField({
		id : 'SearchEmployee',
		fieldLabel: "ค้นหาชื่อพนักงาน :",
		width : 200,
		maxLength : 50
	});
	
	showEmployee.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});
	
	showEmployee.SearchContainer = new Ext.Container({
        layout: 'column',
        items: [{
            xtype: 'container',
            layout: 'form',
            columnWidth: 0.2,
            items: [showEmployee.SearchEmployee]
        },
        {
            xtype: 'container',
            layout: 'form',
            columnWidth: 0.3,
            items: [showEmployee.SearchButton]
        }]

    });
	
	showEmployee.employeeID = new Ext.form.TextField({
		maxLength : 255,
		allowBlank : false
	});

	showEmployee.employeeName = new Ext.form.TextField({
		maxLength : 255,
		allowBlank : false
	});

	showEmployee.employeeAddress = new Ext.form.TextField({
		maxLength : 255,
		allowBlank : false
	});

	showEmployee.employeePhone = new Ext.form.TextField({
		maxLength : 255,
		allowBlank : false
	});

	showEmployee.employeeCitizenID = new Ext.form.TextField({
		maxLength : 255,
		allowBlank : false
	});

	showEmployee.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});
	

	showEmployee.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	showEmployee.columns = [ showEmployee.sm, {
		header : 'แก้ไข',
		dataIndex : 'employeeID',
		align : 'center',
		 renderer: renderInstall
	},{
		header : 'ชื่อพนักงาน',
		dataIndex : 'employeeName',
		align : 'left'
	}, {
		header : 'ที่อยู่พนักงาน',
		dataIndex : 'employeeAddress',
		align : 'left'
	}, {
		header : 'เบอร์โทร',
		dataIndex : 'employeePhone',
		align : 'center'
	}, {
		header : 'รหัสบัตรประชาชน',
		dataIndex : 'employeeCitizenID',
		align : 'center'
	}];
	


	showEmployee.store = new Ext.data.JsonStore({
		removeAndSave : false,
		storeId : 'gridStore',
		idProperty : 'employeeID',
		url : showEmployee.url,
		fields : [ {
			name : 'employeeID'
		}, {
			name : 'employeeName',
		}, {
			name : 'employeeAddress',
		}, {
			name : 'employeePhone',
		}, {
			name : 'employeeCitizenID',
		}]
	});

	showEmployee.grid = new Ext.ss.grid.EditorGridPanel({
		store : showEmployee.store,
		columns : showEmployee.columns,
		sm : showEmployee.sm,
		tbar : [ showEmployee.removeButton ],
		viewConfig : {
			forceFit : true
		},
		height : 300,
		listeners : {}
	});

	showEmployee.gridFieldSet = new Ext.form.FieldSet({
		title : 'รายชื่อพนักงาน',
		items : [ showEmployee.SearchContainer , showEmployee.grid ]
	});

}

Ext.onReady(function() {
	showEmployee.buildEmployeeDetail();
	showEmployee.formPanel = new Ext.form.FormPanel({
		url : showEmployee.url,
		items : [ showEmployee.gridFieldSet  ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});
});
