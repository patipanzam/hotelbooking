var pos = {};
pos.url = application.contextPath + '/buildingpage.html';

pos.buildPositionDetail = function() {

	pos.SearchPositionType = new Ext.form.TextField({
		id : 'SearchPositionType',
		fieldLabel : "SearchBuilding :",
		width : 200,
		maxLength : 50
	});

	pos.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {

			searchFunction();
		}
	});

	pos.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ pos.SearchPositionType ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ pos.SearchButton ]
		} ]

	});

	pos.buildingName = new Ext.form.TextField({
		id : 'buildingName',
		maxLength : 50
	});

	pos.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	pos.columns = [ pos.sm, {
		header : "Building Name",
		dataIndex : 'buildingName',
		align : 'center',
		editor : pos.buildingName
	} ];

	pos.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'buildingID',
		url : pos.url,
		fields : [ {
			name : 'buildingID'
		}, {
			name : 'buildingName',
			allowBlank : false
		} ]
	});

	pos.addButton = new Ext.Button({
		iconCls : 'add',
		handler : function() {

			pos.grid.add({

			});
		}
	});

	pos.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {

			removeFunction();

		}
	});

	pos.saveButton = new Ext.Button({
		iconCls : 'save',
		handler : function() {

			saveFunction();

		}
	});

	pos.PaggingToolbar = new Ext.PagingToolbar({
		store : pos.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	pos.grid = new Ext.ss.grid.EditorGridPanel({
		store : pos.store,
		columns : pos.columns,
		sm : pos.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,

		tbar : [ pos.addButton, pos.removeButton, pos.saveButton ],
		bbar : pos.PaggingToolbar,

		viewConfig : {
			forceFit : true

		},

		height : 300,

	});

	pos.fieldSet = new Ext.form.FieldSet({
		title : 'Manage Building',
		items : [ pos.SearchContainer, pos.grid ]
	});

	


//      pos.allFieldSet = new Ext.form.FieldSet({
//		title : 'Manage Building',
//		items : [ pos.fieldSet ]
//	});

}

Ext.onReady(function() {

	pos.buildPositionDetail();

	pos.formPanel = new Ext.form.FormPanel({
		border : false,
		url : pos.url,
		items : [ pos.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
