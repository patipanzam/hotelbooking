var sr = {};
sr.url = application.contextPath + '/result.html';
sr.urlbookRoom = application.contextPath +'/complete.html';
sr.urlcancelRoom = application.contextPath +'/userHome.html';

sr.buildBranchDetail = function() {
	
	sr.bookingID = new Ext.form.Hidden({
		id : 'bookingID'
		});
	sr.udloginname = new Ext.form.Hidden({
		id : 'udloginname'
	});
	
	sr.updateUser = new Ext.form.Hidden({
		id : 'updateUser'
	});
	
	sr.createUser = new Ext.form.Hidden({
		id : 'createUser'
	});

//	sr.roomtypeCombobox = new Ext.ss.form.ComboBox({
//		 id: 'roomtypeCombobox',
//		 fieldLabel: ['Room TypeName'],
//		 
//    mode: 'remote',
//    width: 148,
//    triggerAction: 'all',
//    minChars: 0,
//    forceSelection: true,
//    valueField: 'roomTypeName',
//    displayField: 'roomTypeName',
//    descriptionField: 'roomTypeName',
//    showDescription: true,
////    useOriginalTpl: true,
//    store: {
//        xtype: 'jsonstore',
//        storeId: id + '-store',
//        idProperty: 'roomID',
//        fields: ['roomID', 'roomTypeName'],
//        url: application.contextPath + '/combobox.html',
//        baseParams: {
//            method: 'roomtype'
//        }
//    }
//	});
	
	sr.fromDate = new Ext.ss.form.DateField({
		id : 'fromDate',
		fieldLabel: ['Check-IN'],
	    toDateFieldId: 'toDate',
	    anchor:'80%'
	});
	
	sr.toDate = new Ext.ss.form.DateField({
		id : 'toDate',
		fieldLabel: ['Check-Out'],
	    fromDateFieldId: 'fromDate',
	    anchor:'80%'
	});
	
	sr.date = new Ext.form.TextField({
		id : 'date',
		fieldLabel: 'Stay',
	    readOnly:true
	});
//	
//	sr.SearchStatus = new Ext.ss.form.ComboBox({
//		id : 'SearchStatus',
//		fieldLabel : "ค้นหาสถานะ",
//		mode : 'local',
//		width : 148,
//		triggerAction : 'all',
//		minChars : 0,
//		forceSelection : true,
//		valueField : 'RSVSTATUS',
//		displayField : 'RSVSTATUS',
//		showDescription : false,
//		width : 265,
//		store: new Ext.data.ArrayStore({
//	        id: 0,
//	        fields: [
//	            'StatusID',
//	            'RSVSTATUS'
//	        ],
//	        data: [[1, 'ไม่อนุมัติ'], [2, 'อนุมัติ'], [3, 'รออนุมัติ']]
//	    })
//	});

	
	sr.guestStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'1','desc':'Person'},{'code':'2','desc':'Person'},{'code':'3','desc':'Person'}]
		},
		fields : ['code', 'desc']
	});
	sr.guestCombo = new Ext.ss.form.ComboBox({
	    store: sr.guestStore,
	    fieldLabel : [ 'Adults' ],
	    mode: 'local',
	    width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
	    valueField : 'code',
	    displayField:'code',
	    descriptionField : 'desc',
		showDescription : true,
	    triggerAction: 'all',
	    emptyText:'Select a Adults...',
//	    selectOnFocus:true
//	    applyTo: 'local-states'
	});
	
	sr.childrenStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'1','desc':'Person'},{'code':'2','desc':'Person'},{'code':'3','desc':'Person'}]
		},
		fields : ['code', 'desc']
	});
	sr.childrenCombo = new Ext.ss.form.ComboBox({
	    store: sr.childrenStore,
	    fieldLabel : [ 'Children (age 2-11)' ],
	    mode: 'local',
	    width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
	    valueField : 'code',
	    displayField:'code',
	    descriptionField : 'desc',
		showDescription : true,
	    triggerAction: 'all',
	    emptyText:'Select a Children...',
//	    selectOnFocus:true
//	    applyTo: 'local-states'
	});
	

	sr.roomsStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'1','desc':'rooms'},{'code':'2','desc':'rooms'},{'code':'3','desc':'rooms'}]
		},
		fields : ['code', 'desc']
	});
	sr.roomsCombo = new Ext.ss.form.ComboBox({
	    store: sr.roomsStore,
	    fieldLabel : [ 'Rooms' ],
	    mode: 'local',
	    width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
	    valueField : 'code',
	    displayField:'code',
	    descriptionField : 'desc',
		showDescription : true,
	    triggerAction: 'all',
	    emptyText:'Select a Rooms...',
//	    selectOnFocus:true,
//	    applyTo: 'local-states',
	    
	    listeners : {
	                 'select' : function(combo, record){
	    	
	    	            //we can get the selected value using getValue()
	    	
	                	 code = this.getValue();
	                	 console.log(code);
	    	
	    	        }
	    	
	    	    }
	    	
	    	
	});

//	sr.SearchButton = new Ext.Button({
//		iconCls : 'query',
//		handler : function() {
//			searchFunction();
//		}
//	});

	sr.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [{
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ sr.fromDate ]
		} ,{
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ sr.toDate ]
		},{
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.3,
			items : [ sr.date]
		}]

	});
	
	sr.SearchContainer1 = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ sr.guestCombo ]
			
		},{
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [sr.childrenCombo ]
			
		}]

	});
	
//	sr.fromDate = new Ext.form.TextField({
//		id : 'fromDate',
//		fieldLabel: 'Stay',
//	    width : 250,
//	    readOnly:true
//	});
//	
//	sr.fromDate = new Ext.form.DateField({
//		id : 'roomDate',
//		fieldLabel: 'Room Date',
//		maxLength : 50
//	});
//	
//	sr.toDate = new Ext.form.DateField({
//		id : 'Date',
//		maxLength : 50
//	});
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	sr.id = new Ext.form.Hidden({
		id : 'id',
		fieldLabel : 'รหัสผู้ใช้งาน :',
		allowBlank : false,
	});
//	
//	sr.roleId = new Ext.form.Hidden({
//		id : '2',
//		fieldLabel : 'รหัสผู้ใช้งาน :',
//		allowBlank : false,
//	});
//
//	sr.userName = new Ext.form.TextField({
//		id : 'userName',
//		fieldLabel: __messages['label.userName'],
//	    
//	});
//	
//	sr.userPassword = new Ext.form.TextField({
//		id : 'userPassword',
//		inputType: 'password',
//		fieldLabel: __messages['label.userPassword'],
//		 
//	    
//	});
//	
//	
//	sr.userPasswordConfirm = new Ext.form.TextField({
//		id : 'userPasswordConfirm',
//		inputType: 'password',
//		fieldLabel: __messages['label.userPasswordConfirm'],
//		 
//	    
//	});
	
	sr.firstName = new Ext.form.TextField({
		id : 'firstName',
		inputType: 'firstName',
		fieldLabel: 'First Name',
		 
	    
	});
	
	sr.lastName = new Ext.form.TextField({
		id : 'lastName',
		inputType: 'lastName',
		fieldLabel: 'Last Name',
		 
	   
	});
	
	sr.email = new Ext.form.TextField({
		id : 'email',
	    fieldLabel: 'Email',
		
	    
	});
	
	sr.phone = new Ext.form.TextField({
		id : 'PHONE',
		inputType: 'phone',
		fieldLabel: 'Phone',
		 
	    
	});
	

	sr.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'id',
		url : sr.url,
		fields : [ {
			name : 'id'
		}, {
			name : 'LGUSERNAME'
		}, {
			name : 'LGPASSWORD'
		}, {
			name : 'ROLENAME'
		},{
			name:'PHONE'
		}]
	});
	


	sr.roleContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [sr.firstName,sr.email ]
		}, {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.5,
			items : [sr.lastName,sr.phone ]
		} ,]

	});
	
//	----------------------------------------------------------------------------------
	
	sr.roomsStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'1','desc':'rooms'},{'code':'2','desc':'rooms'},{'code':'3','desc':'rooms'}]
		},
		fields : ['code', 'desc']
	});
	
//	sr.roomsCombobox = new BaseComboBox.getroomsCombobox(
//			'roomsCombobox', {});
	
	sr.roomID = new Ext.form.TextField({
		id : 'roomID',
		maxLength : 50
	});

	sr.ROOMNO = new Ext.form.TextField({
		id : 'ROOMNO',
		maxLength : 50
	});

	sr.ROOMTYPENAME = new Ext.form.TextField({
		id : 'ROOMTYPENAME',
		maxLength : 50
	});

	sr.ROOMPRICE = new Ext.form.TextField({
		id : 'ROOMPRICE',
		maxLength : 50
	});
	
	sr.sumPrice = new Ext.form.TextField({
		id : 'sumPrice',
		maxLength : 50
	});

	
	sr.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	sr.columns = [sr.sm ,{
		header : "",
		dataIndex : 'image',
		align : 'center',
		renderer : function(value, meData){
			if(!(value == '' || value == null || value == undefined)){
	    		return convertImage(value);
			}
		}
		
	}, {
		header : "The space",
		dataIndex : 'roomTypeName',
		align : 'center'
	},{
		header : "What's included",
		dataIndex : 'typeDetail',
		align : 'center'
		
	},{
		header : "Capacity",
		dataIndex : 'Capacity',
		align : 'center'
		
	},{
		header : "Price per night",
		dataIndex : 'roomPrice',
		align : 'center'
		
	},{
		header : "Rate includes",
		dataIndex : 'rate',
		align : 'center'
		
	},{
		header : "Maximum of Room for Booking",
		dataIndex : 'max',
		align : 'center'
		
	},{
		header : "Rooms",
		dataIndex : 'rooms',
		align : 'center',
		editor : sr.roomsCombo
//		renderer : sr.util.Format.comboRenderer(sr.roomsCombobox)
			
	},{
		header : "Total Price",
		dataIndex : 'sumPrice',
		align : 'center',
		editor : sr.sumPrice
	}];

	sr.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'roomID',
		url : sr.url,
		fields : [ {
			name : 'roomID'
		}, {
			name : 'image'
		}, {
			name : 'roomTypeName'
		}, {
			name : 'roomPrice'
		},{
			name:'typeDetail'
		},{
			name:'Capacity'
		},{
			name:'udloginname'
		},{
			name:'rate'
		},{
			name:'rooms'
		},{
			name:'sumPrice'
		},{
			name:'max'
		}]
	});

	
//	sr.removeButton = new Ext.Button({
//		iconCls : 'remove',
//		handler : function() {
//			removeFunction();
//		}
//	});
	
//	sr.addButton = new Ext.Button({
//		iconCls : 'add',
//		handler : function() {
//
//			sr.grid.add({
//
//			});
//		}
//	});

	sr.PaggingToolbar = new Ext.PagingToolbar({
		store : sr.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	sr.grid = new Ext.ss.grid.EditorGridPanel({
		store : sr.store,
		columns : sr.columns,
		sm : sr.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ ],
		bbar : sr.PaggingToolbar,
		
		listeners: {
			'afteredit': function(e) {
                if (e.field == 'rooms'){
                	var lastEdit = sr.grid.getStore().getAt(sr.grid.lastEdit.row);
                	lastEdit.set('sumPrice',((lastEdit.get('roomPrice')*lastEdit.get('rooms'))-(lastEdit.get('rate')/(100)*lastEdit.get('roomPrice'))*lastEdit.get('rooms'))*sr.date.getValue());
            }
			}
		},
//		listeners : {
//			'afterEdit' : function(e) {
//				var lastEdit = sr.grid.getStore().getAt(sr.grid.lastEdit.row);
//				if(e.field == 'code'){
//					lastEdit.set('sumPrice',(lastEdit.get('roomPrice')*lastEdit.get('rate')*lastEdit.get('rate')));
////					
//				}
//
//				var sumPrice = 0;
//				Ext.each(sr.grid.getStore().data.items, function (record, row){
//					sumPrice += record.data.sumPrice;
//						
//				});
//			
//				sr.sumPrice.setValue(sumPrice);
//	
//			}
//		},
//
		viewConfig : {
			forceFit : true
		},
		height : 300,
	});
	

	sr.fieldSet = new Ext.form.FieldSet({
		title : '',
		items : [ sr.SearchContainer, sr.SearchContainer1 ,sr.roleContainer,sr.grid ]
	});
}

//sumPrice = function(){
//	var sumPrice = 0;
//	Ext.each(sr.store.data.items, function( key, value) {
//		sumPrice += sr.store.data.itemAt(value).data.roomPrice*sr.store.data.itemAt(value).data.rate;
//	});
//	sr.sumPrice.setValue(sumPrice);
////	Sum function------
//};

Ext.onReady(function() {
	sr.buildBranchDetail();
	sr.formPanel = new Ext.form.FormPanel({
		border : false,
		url : sr.url,
		items : [ sr.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
