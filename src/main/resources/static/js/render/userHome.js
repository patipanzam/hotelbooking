var sr = {};
sr.url = application.contextPath + '/userHome.html';
sr.urlViewRoom = application.contextPath +'/roomdetail.html';
sr.urlViewResult = application.contextPath +'/result.html';

sr.buildBranchDetail = function() {

	sr.udloginname = new Ext.form.Hidden({
		id : 'udloginname'
	});
	

//	sr.roomtypeCombobox = new Ext.ss.form.ComboBox({
//		 id: 'roomtypeCombobox',
//		 fieldLabel: ['Room TypeName'],
//		 
//    mode: 'remote',
//    width: 148,
//    triggerAction: 'all',
//    minChars: 0,
//    forceSelection: true,
//    valueField: 'roomTypeName',
//    displayField: 'roomTypeName',
//    descriptionField: 'roomTypeName',
//    showDescription: true,
////    useOriginalTpl: true,
//    store: {
//        xtype: 'jsonstore',
//        storeId: id + '-store',
//        idProperty: 'roomID',
//        fields: ['roomID', 'roomTypeName'],
//        url: application.contextPath + '/combobox.html',
//        baseParams: {
//            method: 'roomtype'
//        }
//    }
//	});
	
	sr.fromDate = new Ext.ss.form.DateField({
		id : 'fromDate',
		fieldLabel: ['Check-IN'],
	    toDateFieldId: 'toDate',
	    anchor:'80%'
	});
	
	sr.toDate = new Ext.ss.form.DateField({
		id : 'toDate',
		fieldLabel: ['Check-Out'],
	    fromDateFieldId: 'fromDate',
	    anchor:'80%'
	});
	
	sr.guestStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'1','desc':'Person'},{'code':'2','desc':'Person'},{'code':'3','desc':'Person'}]
		},
		fields : ['code', 'desc']
	});
	sr.guestCombo = new Ext.ss.form.ComboBox({
	    store: sr.guestStore,
	    fieldLabel : [ 'Adults' ],
	    mode: 'local',
	    width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
	    valueField : 'code',
	    displayField:'code',
	    descriptionField : 'desc',
		showDescription : true,
	    triggerAction: 'all',
	    emptyText:'Select a Adults...',
//	    selectOnFocus:true
//	    applyTo: 'local-states'
	});
	
	sr.childrenStore = new Ext.data.JsonStore({// เก็บข้อมูลในตาราง
		storeId : 'gridStore',
		idProperty : 'itemCd',
		data : {
			records:[{'code':'1','desc':'Person'},{'code':'2','desc':'Person'},{'code':'3','desc':'Person'}]
		},
		fields : ['code', 'desc']
	});
	sr.childrenCombo = new Ext.ss.form.ComboBox({
	    store: sr.childrenStore,
	    fieldLabel : [ 'Children (age 2-11)' ],
	    mode: 'local',
	    width : 148,
		triggerAction : 'all',
		minChars : 0,
		forceSelection : true,
	    valueField : 'code',
	    displayField:'code',
	    descriptionField : 'desc',
		showDescription : true,
	    triggerAction: 'all',
	    emptyText:'Select a Children...',
//	    selectOnFocus:true
//	    applyTo: 'local-states'
	});
	


	sr.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	sr.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ sr.fromDate ]
		} ,{
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ sr.toDate ]
		}]

	});
	
	sr.SearchContainer1 = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [ sr.guestCombo ]
			
		},{
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.4,
			items : [sr.childrenCombo ]
			
		},{
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.1,
			items : [ sr.SearchButton ]
		}]

	});
////////////////////////////////////////////////////////////////////////////////////////////////
	sr.roomID = new Ext.form.TextField({
		id : 'roomID',
		maxLength : 50
	});

	sr.ROOMNO = new Ext.form.TextField({
		id : 'ROOMNO',
		maxLength : 50
	});

	sr.ROOMTYPENAME = new Ext.form.TextField({
		id : 'ROOMTYPENAME',
		maxLength : 50
	});

	sr.ROOMPRICE = new Ext.form.TextField({
		id : 'ROOMPRICE',
		maxLength : 50
	});

	
	sr.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	sr.columns = [  {
		header : "",
		dataIndex : 'IMAGE',
		align : 'center',
		renderer : function(value, meData){
			if(!(value == '' || value == null || value == undefined)){
	    		return convertImage(value);
			}
		}
		
	}, {
		header : "The space",
		dataIndex : 'ROOMTYPENAME',
		align : 'center'
	},{
		header : "Price per night",
		dataIndex : 'ROOMPRICE',
		align : 'center'
		
	},{
		header : "What's included",
		dataIndex : 'TYPEDETAIL',
		align : 'center'
	}, {
		header : "",
		dataIndex : 'roomID',
		align : 'center',
		renderer : renderInstall
	} ];

	sr.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'roomID',
		url : sr.url,
		fields : [ {
			name : 'roomID'
		}, {
			name : 'IMAGE'
		}, {
			name : 'ROOMTYPENAME'
		}, {
			name : 'ROOMPRICE'
		},{
			name:'TYPEDETAIL'
		},{
			name:'udloginname'
		}]
	});

//	sr.removeButton = new Ext.Button({
//		iconCls : 'remove',
//		handler : function() {
//			removeFunction();
//		}
//	});

	sr.PaggingToolbar = new Ext.PagingToolbar({
		store : sr.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	sr.grid = new Ext.ss.grid.EditorGridPanel({
		store : sr.store,
		columns : sr.columns,
		sm : sr.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ ],
		bbar : sr.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	sr.fieldSet = new Ext.form.FieldSet({
		title : '',
		items : [ sr.SearchContainer, sr.SearchContainer1 ,sr.grid ]
	});
}

Ext.onReady(function() {
	sr.buildBranchDetail();
	sr.formPanel = new Ext.form.FormPanel({
		border : false,
		url : sr.url,
		items : [ sr.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
