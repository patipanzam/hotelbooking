var sr = {};
sr.url = application.contextPath + '/showRoom.html';
sr.urlEditRoom = application.contextPath +'/room.html';

sr.buildBranchDetail = function() {

	sr.SearchRegister = new Ext.form.TextField({
		id : 'SearchRegister',
		fieldLabel : "SearchRoom	 :",
		width : 200,
		maxLength : 50
	});

	sr.SearchButton = new Ext.Button({
		iconCls : 'query',
		handler : function() {
			searchFunction();
		}
	});

	sr.SearchContainer = new Ext.Container({
		layout : 'column',
		items : [ {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ sr.SearchRegister ]
		},  {
			xtype : 'container',
			layout : 'form',
			columnWidth : 0.2,
			items : [ sr.SearchButton ]
		} ]

	});

	sr.roomID = new Ext.form.TextField({
		id : 'roomID',
		maxLength : 50
	});

	sr.ROOMNO = new Ext.form.TextField({
		id : 'ROOMNO',
		maxLength : 50
	});

	sr.ROOMTYPENAME = new Ext.form.TextField({
		id : 'ROOMTYPENAME',
		maxLength : 50
	});

	sr.ROOMPRICE = new Ext.form.TextField({
		id : 'ROOMPRICE',
		maxLength : 50
	});

	
	sr.sm = new Ext.ss.grid.CheckboxSelectionModel({});

	sr.columns = [ sr.sm, {
		header : "แก้ไข",
		dataIndex : 'roomID',
		align : 'center',
		renderer : renderInstall
	}, {
		header : "ROOM NUMBER",
		dataIndex : 'ROOMNO',
		align : 'center'
	}, {
		header : "ROOMTYPENAME",
		dataIndex : 'ROOMTYPENAME',
		align : 'center'
	}, {
		header : "PRICE",
		dataIndex : 'ROOMPRICE',
		align : 'center'
	} ];

	sr.store = new Ext.data.JsonStore({
		storeId : 'mystore',
		idProperty : 'roomID',
		url : sr.url,
		fields : [ {
			name : 'roomID'
		}, {
			name : 'ROOMNO'
		}, {
			name : 'ROOMTYPENAME'
		}, {
			name : 'ROOMPRICE'
		}]
	});

	sr.removeButton = new Ext.Button({
		iconCls : 'remove',
		handler : function() {
			removeFunction();
		}
	});

	sr.PaggingToolbar = new Ext.PagingToolbar({
		store : sr.store,
		pageSize : 10,
		id : 'PaggingToolbar',
		displayInfo : true
	});

	sr.grid = new Ext.ss.grid.EditorGridPanel({
		store : sr.store,
		columns : sr.columns,
		sm : sr.sm,
		frame : false,
		stripeRows : true,
		enableColumnMove : false,
		columnLines : true,
		enableHdMenu : false,
		tbar : [ sr.removeButton ],
		bbar : sr.PaggingToolbar,

		viewConfig : {
			forceFit : true
		},
		height : 300,
	});

	sr.fieldSet = new Ext.form.FieldSet({
		title : 'List Room',
		items : [ sr.SearchContainer, sr.grid ]
	});
}

Ext.onReady(function() {
	sr.buildBranchDetail();
	sr.formPanel = new Ext.form.FormPanel({
		border : false,
		url : sr.url,
		items : [ sr.fieldSet ],
		plugins : [ new Ext.ux.FitToParent({
			fitHeight : false
		}) ],
		renderTo : 'renderDiv'
	});

});
